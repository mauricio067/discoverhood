package com.miokode.discoverhood;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.miokode.discoverhood.adapter.AdapterComment;
import com.miokode.discoverhood.dialogs.DialogFragmentActivity;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationComment;
import com.miokode.discoverhood.restful.publication.SDPublicationPOSTComment;
import com.miokode.discoverhood.restful.publication.SDPublicationsGETComments;

@SuppressLint("ResourceAsColor")
public class ActivityCommentPost extends DialogFragmentActivity {
	private EditText post;
	private TextView button_cancel;
	private TextView buttom_comment;
	private AdapterComment adapter;
	private ListView list;
	private ArrayList<SDPublicationComment> comments; 
	private SDPublication publi;
	private TextView comment;
	private int cc;
	//private String publicationID;
	
	
	@Override
	public void onSaveInstanceState(Bundle state) {
	      super.onSaveInstanceState(state);

		if (comments != null) {
			state.putSerializable("comments",(Serializable) comments);
		}
	}
	
		
     @SuppressWarnings("unchecked")
	protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_comment);
		
	    ((DiscoverHoodApp)getApplicationContext()).setesN(false);	
    	
    	post = (EditText) findViewById(R.id.post);  
		
    	publi = ((DiscoverHoodApp)getApplicationContext()).getCurrentPublication();
    	
      			
    	Bundle extra = getIntent().getExtras();
		
		final String publicationID = extra.getString("publicationID");
						
		post.setOnFocusChangeListener(new OnFocusChangeListener() {          
				public void onFocusChange(View v, boolean hasFocus) {
	    			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	    		    imm.showSoftInput(post, InputMethodManager.SHOW_IMPLICIT);
	    		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
					if (hasFocus){
						post.setHint("");
					}
					else {
						post.setHint(getResources().getString(R.string.email));
					}
		        }
			   });
			    
		button_cancel = (TextView)findViewById(R.id.cancel_post);
		button_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	onBackPressed();
            	overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 

            }
		});
		
		 buttom_comment = (TextView)findViewById(R.id.send_post);
		 buttom_comment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            	if (post.getText().length()==0) {
            		dialogs.ShowErrorDialog(getResources().getString(R.string.emptycomment));
            	}
            	else{
					SDPublication publ = ((DiscoverHoodApp)getApplicationContext()).getCurrentPublication();
            		publ.setIComment(true);
					publ.increaseComments();
            		sendCommentPost(publicationID, post.getText().toString());
            	}
             }
		});
		  
		 
		list = (ListView)findViewById(R.id.list);    
		DateFormat format = DateFormat.getDateInstance(1);
		format.setTimeZone(TimeZone.getDefault()); //Seteo al imezone del usuario
       	comments = new ArrayList<SDPublicationComment>();
		adapter = new AdapterComment(this, comments);
  	    list.setAdapter(adapter);
            	    
       	Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
       	button_cancel.setTypeface(face);
       	buttom_comment.setTypeface(face);
       	post.setTypeface(face);
       	
       	if (savedInstanceState == null || !savedInstanceState.containsKey("comments")){
       		if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
       		    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
       		}
       		else {		     
       		    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
       		}	
  	        getComments(publicationID);
       	}
       	else {
       		comments = (ArrayList<SDPublicationComment>)savedInstanceState.getSerializable("comments");
			addComments(comments);
			
       	}
    }
   
     
     private void addComments(ArrayList<SDPublicationComment> comments){
    	 ListIterator<SDPublicationComment> listItr = comments.listIterator();
 		while(listItr.hasNext()){
 			SDPublicationComment com = listItr.next();
 			adapter.addItem(com);
 		}
     }
     

     private void getComments(String publicationID){
		 dialogs.showLoadingProgressDialog(getString(R.string.retrieving_comments));
		 new SDPublicationsGETComments(SD_Url, SD_Key, publicationID, new SDConnectionResponseListener(){

			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(String response, Object resultElement) {
				comments = (ArrayList<SDPublicationComment>) resultElement;
				addComments(comments);
				dialogs.dismissProgressDialog();
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  

			}

			@Override
			public void onIssues(String issues) {
			 dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(getString(R.string.error_servidor_no_disponible) + issues);
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  

			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}	 	 
		 });  
		 
	 }
    
	 /**
	  * Envia el nuevo comentario
	  * 
	  * @param pub
	  * 	Publicacion que contiene el comentario
	  */
	 private void sendCommentPost(String pub, String text){
		 dialogs.showLoadingProgressDialog(getString(R.string.sending_comment));
		 new SDPublicationPOSTComment(SD_Url, SD_Key, pub, text, new SDConnectionResponseListener(){

				@Override
				public void onSuccess(String response, Object resultElement) {
			    	
					SDPublication publ = ((DiscoverHoodApp)getApplicationContext()).getCurrentPublication();
					//((DiscoverHoodApp)getApplicationContext()).increaseCommentPublication(publ.getId());
					dialogs.dismissProgressDialog();
					//publ.setIComment(true);
					//publ.increaseComments();
					((DiscoverHoodApp)getApplicationContext()).setCurrentPublication(publ);
					post.setText("");
			    	list.setSelection(list.getCount()); 
					adapter.addItem((SDPublicationComment) resultElement);
					onBackPressed();
	            	overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
	                    	
	            	
				}

				@Override
				public void onIssues(String issues) {
					dialogs.dismissProgressDialog();
					dialogs.ShowErrorDialog(getString(R.string.error_servidor_no_disponible) + issues);
				}

				@Override
				public void onError(SDError error) {
					dialogs.dismissProgressDialog();
					 if (error.getReason() != null ){
						 if (error.getDetails() == null){
							 dialogs.ShowErrorDialog(error.getReason());
						 }else{
							 Enumeration<String> element = error.getDetails().elements();
							 while(element.hasMoreElements()){
								 dialogs.ShowErrorDialog(element.nextElement());
							 }
						 }
					 }else{
						 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
					 }
				}	 	 
			 });  
	 }

}
	
	
	
	
	


