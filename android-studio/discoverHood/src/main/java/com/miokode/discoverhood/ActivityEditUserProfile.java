package com.miokode.discoverhood;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityEditUserProfile extends Activity{
	

	private static int TAKE_PIC = 1;
	private static int SELECT_PIC = 2;
	private String name = " ";
	private AlertDialog ImageDialog =null;

	
	static final int DATE_DIALOG_ID = 999;

    private FragmentActivity fa;
	private TextView ug, bd, genre;
	private EditText firstname, lastname, pass1, pass2, mail,fn, ln;
	private ImageView image;

	  protected void onCreate(Bundle savedInstanceState) {
	    	super.onCreate(savedInstanceState);
	    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    	setContentView(R.layout.screen_edit_user_profile);
	    	
	    	firstname=(EditText) findViewById(R.id.user_first_name);
	        
	        lastname=(EditText) findViewById(R.id.user_last_name);
	        
			pass1=(EditText) findViewById(R.id.user_password);
	     	
	     	pass2=(EditText) findViewById(R.id.user_password_confirm);
	     	
	     	mail=(EditText) findViewById(R.id.user_email);
	     	
	     	bd=(TextView) findViewById(R.id.user_birthday);

	     	image=(ImageView) findViewById(R.id.user_pic);
	     	
	     	genre = (TextView) findViewById(R.id.user_genre);
	 
 	
		bd.setOnClickListener(new OnClickListener() {
			 
			@Override
		public void onClick(View v) {
				
				
				
 /* VER COMO MOSTRAR EL DIALOG EN FRAGMENT !!!!!!!!!!!!!!*/
	//	 showDialog(DATE_DIALOG_ID);
		}
 
		});
		
		
	    Typeface face = Typeface.createFromAsset(fa.getAssets(),"fonts/arial.ttf");
	    Button signup = (Button)  fa.findViewById(R.id.sign_up);
	    TextView pouallr = (TextView)fa.findViewById(R.id.pou_all_rigths);
	    
	    
	    
        firstname.setTypeface(face);
        lastname.setTypeface(face);
		pass1.setTypeface(face);
     	pass2.setTypeface(face);
     	mail.setTypeface(face);
     	bd.setTypeface(face);
     	genre .setTypeface(face);
	    pouallr.setTypeface(face);
	    signup.setTypeface(face);

		
		
	    
	 }
	
	 
	 
	    ///////////////////////////
	    /// Obtengo la foto
	    ///////////////////////////
	    public void getPhoto(View v){  	   	
	    /*	ImageDialog = new fa.AlertDialog.Builder(this)
	        .setTitle("Get a photo")
	        .setMessage("Choose a photo from the gallery or take a photo with the camera?")
	        .setPositiveButton("Gallery", new AlertDialog.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					fromGallery();
					
				}
				
	        })
	        .setNegativeButton("Camera", new AlertDialog.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) {
		        	fromCamera();
		        }
	        })
	        
	       .setNeutralButton("Cancel", new AlertDialog.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) {
		        	ImageDialog.dismiss();
		        	
		        }
	        })
	        .create();
	        ImageDialog.show();
	                     */  
	    }
			 
	 
	    private void fromGallery(){
	    	    	
	    	Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
	    	
	    	int code = SELECT_PIC;
	    	  	
	    	startActivityForResult(i, code);
	    	    	
	    }

	    
	    private void fromCamera(){
	    	
	    	Intent i= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    	
	    	Uri output = Uri.fromFile(new File(name));

			int code = TAKE_PIC;

			startActivityForResult(i, code);
		    	
	    }


	    
	    public void chooseGenre(View v){
	/*    	final CharSequence[] items = {"Male", "Female"};  	 
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	builder.setTitle("Genre");
	    	builder.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
	    	    public void onClick(DialogInterface dialog, int item) {
	    	    	genre.setText(items[item]);    	    	
	    	        dialog.cancel();
	    	    }
	    	});
	    	AlertDialog alert = builder.create();
	    	alert.show();*/
	    }	    
	 
	 
	    
	    
	    
	    ///////////////////////////////
	    //// Verificacion y acciones
	    ///////////////////////////////
	    
	    /*
	     * Realiza los chequeos estaticos de los campos y hace la conexion con el server  
	     */
	    public void signUp(View v){ 	
	  	   if ( this.dataVerification() ) { 
	  			//enviarDatosAlServer();
	  	   }
	  		   
	    }	 
	 
	    
	    /*
	     * Verifica que los campos esten completos y que el email tenga el formato correcto
	     */
	    public boolean dataVerification(){  	
	    	 String n= firstname.getText().toString();
	    	 String l= lastname.getText().toString(); 
	    	 String p1= pass1.getText().toString();
	    	 String p2= pass2.getText().toString();
	    	 String m= mail.getText().toString();
	    	 String g= genre.getText().toString();
	    	 String b= bd.getText().toString();

	    	 if(n.length()==0 || l.length()==0 || m.length()==0 || p1.length()==0 || p2.length()==0 || g.compareTo("Genre")==0 || g.compareTo("Birthday")==0 ){ //TODO obtener texto del hint predefinido para ocmparar genero y birthday 
	//    		 ShowErrorDialog("Error: Please make sure you fill all the fields.");
	     		 return false;
	    	 } else if (p1.compareTo(p2)!=0) {
	//    		 ShowErrorDialog("Error: The password must be the same.");
	    		 return false;
	    	 } else if (!checkEmail(m)) {
	  //  		 ShowErrorDialog("Error: The email is invalid.");
	    		 return false;
	    	 }
	    	 return true;
	     }
	    
	    /*
	     * Verifica que el email ingresado tenga el formato correcto
	     */
	    public static boolean checkEmail (String email) {
	        Pattern p = Pattern.compile("[-\\w\\.]+@\\w+\\.\\w+");
	        Matcher m = p.matcher(email);
	        return m.matches();
	    }
	    
	    /*
	     * Verifica que los pass ingresados sean iguales 
	     */
	    public boolean equalPass(String s1, String s2){
	    	if (s1.length()==0 && s2.length()==0) 
	    		return false; 
	    	else 
	    		return s1.compareTo(s2)==0;
	    }
	    	 
	 
	 

}
