package com.miokode.discoverhood;

import android.support.v4.app.FragmentActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.miokode.discoverhood.tools.MTImageView;
import com.miokode.discoverhood.tools.MemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;


public class ActivityEnlargeAvatar extends FragmentActivity{

	private MemoryCache m;
    private String urlImage;
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private DisplayImageOptions options;
        
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(this);
    	    
    	setContentView(R.layout.screen_avatar);
    	
	    ((DiscoverHoodApp)getApplicationContext()).setesN(false);	
    	
    	Intent intent = getIntent();
	    urlImage = intent.getStringExtra("url");
	    

	  	           
	    MTImageView l_image = (MTImageView) findViewById(R.id.photo);
        imageLoader.displayImage(urlImage, l_image, options);
//	    l_image.setImageBitmap();
       
	    TextView Xclose = (TextView) findViewById(R.id.close); 
       	Xclose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	finish();
            	
      	    }
        });
      
    }
    
       
}
