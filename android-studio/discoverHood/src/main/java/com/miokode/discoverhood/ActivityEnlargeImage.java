package com.miokode.discoverhood;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationPOSTshare;
import com.miokode.discoverhood.restful.publication.SDPublicationPOSTtoggleSmile;
import com.miokode.discoverhood.restful.publication.SDPublicationShared;
import com.miokode.discoverhood.tools.ConnectionDialogs;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.miokode.discoverhood.tools.MTImageView;
import com.miokode.discoverhood.tools.MemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.Enumeration;


public class ActivityEnlargeImage extends FragmentActivity {

    private MemoryCache m;
    private String urlImage;
    private SDPublication publi;
    private TextView smi;
    private TextView smis;
    private TextView coms;
    private TextView sha;
    private TextView shas;
    private SDAccount account;
    private Activity activity;
    private String SD_Url;
    private String SD_Key;
    private double latitude;
    @SuppressWarnings("unused")
    private double longitude;
    private ConnectionDialogs dialogs;
    private SDPublicationShared postShared = null;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    DisplayImageOptions options;
    private ProgressBar spinner;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.account = ((DiscoverHoodApp) getApplication()).getUserDataAccount();
        this.SD_Url = ((DiscoverHoodApp) getApplication()).getServerUrl();
        this.SD_Key = ((DiscoverHoodApp) getApplication()).getServerKey();
        setContentView(R.layout.screen_photo);

        ((DiscoverHoodApp) getApplicationContext()).setesN(false);

        getMyLocation();

        publi = ((DiscoverHoodApp) getApplicationContext()).getCurrentPublication();

        if (publi instanceof SDPublicationShared) {
            postShared = (SDPublicationShared) publi;
            publi = postShared.getSharedPublication();
            ((DiscoverHoodApp) getApplicationContext()).setCurrentPublication(publi);
        }

        activity = this;
        Intent intent = getIntent();
        urlImage = intent.getStringExtra("url");

        // m=((DiscoverHoodApp)getApplication()).getGalleryMemoryCache();

        spinner = (ProgressBar) findViewById(R.id.spinner);
        spinner.setVisibility(View.INVISIBLE);
        m = new MemoryCache();

        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(this);

        smi = (TextView) findViewById(R.id.smile);
        smis = (TextView) findViewById(R.id.smiles);
        coms = (TextView) findViewById(R.id.comments);
        sha = (TextView) findViewById(R.id.share);
        shas = (TextView) findViewById(R.id.shares);

        smis.setText(publi.getSmilesCounter() + " " + getResources().getString(R.string.Smiles));
        coms.setText(publi.getCommentsCounter() + " " + getResources().getString(R.string.Comments));
        shas.setText(publi.getSharesCounter() + " " + getResources().getString(R.string.Shares));


        MTImageView l_image = (MTImageView) findViewById(R.id.photo);
        //  l_image.setImageBitmap(m.get(urlImage));
//       Iloader.DisplayImage(urlImage, l_image);
        imageLoader.displayImage(urlImage, l_image, options, new ImageLoadingListener() {

            @Override
            public void onLoadingFailed(String imageUri, View view,
                                        FailReason failReason) {
                if (spinner != null) {
                    spinner.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view,
                                          Bitmap loadedImage) {
                if (spinner != null) {
                    spinner.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onLoadingStarted(String imageUri, View view) {

                if (spinner != null) {
                    spinner.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                if (spinner != null) {
                    spinner.setVisibility(View.INVISIBLE);
                }
            }
        });

        TextView Xclose = (TextView) findViewById(R.id.close);
        Xclose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final TextView Ss = (TextView) findViewById(R.id.smiles);

        final TextView S = (TextView) findViewById(R.id.smile);

        final TableRow trSmile = (TableRow) findViewById(R.id.tableRow_smile);
        trSmile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                SDPublication publ = ((DiscoverHoodApp) getApplication()).getCurrentPublication();
                SDPublicationPOSTtoggleSmile.smileClicked(publ, trSmile, Ss, S, (DiscoverHoodApp) activity.getApplication(),null);

            }
        });


        if (publi.getIsmile()) {
            smi.setTextColor(Color.MAGENTA);
        } else {
            smi.setTextColor(Color.WHITE);
        }

        if (publi.iComment()) {
            TextView Scomments = (TextView) findViewById(R.id.comment);
            Scomments.setTextColor(Color.MAGENTA);
        }

        if (publi.isIshared()) {
            sha.setTextColor(Color.MAGENTA);
        } else {
            sha.setTextColor(Color.WHITE);
        }


        TableRow trShare = (TableRow) findViewById(R.id.tableRow_share);

        TableRow trComment = (TableRow) findViewById(R.id.tableRow_comment);
        trComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte = new Intent(activity, ActivityCommentPost.class);
                inte.putExtra("publicationID", publi.getId());
                startActivity(inte);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);

                //coms.setText(publi.getCommentsCounter()+" "+getResources().getString(R.string.Comments));
            }
        });

        if (publi.getAuthor().getId().compareTo(account.getId()) == 0) {
            sha.setText(null);
            final ImageView isha = (ImageView) findViewById(R.id.i_share);
            isha.setImageDrawable(null);
        } else {
            trShare.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    SDPublication postt = ((DiscoverHoodApp) getApplication()).getCurrentPublication();
                    TextView shares = (TextView) findViewById(R.id.shares);

                    if (postt instanceof SDPublicationShared) {

                        shares.setText(((SDPublicationShared) postt).getSharedPublication().getSharesCounter() + " " + activity.getResources().getString(R.string.Shares));
                        sharePost(((SDPublicationShared) postt).getSharedPublication());
                    } else {
                        shares.setText(postt.getSharesCounter() + " " + activity.getResources().getString(R.string.Shares));
                        sharePost(postt);
                    }
                }
            });
        }
    }

    public void sharePost(SDPublication publi) {

        final SDPublication pu = publi;

        new SDPublicationPOSTshare(SD_Url, SD_Key, pu.getId(), latitude, longitude, new SDConnectionResponseListener() {

            @Override
            public void onSuccess(String response, Object resultElement) {
                pu.increaseShares();
                pu.setIshared(true);
                Toast.makeText(activity, activity.getResources().getString(R.string.publication_shared), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onIssues(String issues) {
                dialogs.dismissProgressDialog();
                dialogs.ShowErrorDialog(issues);
            }

            @Override
            public void onError(SDError error) {
                dialogs.dismissProgressDialog();
                if (error.getReason() != null) {
                    if (error.getDetails() == null) {
                        dialogs.ShowErrorDialog(error.getReason());
                    } else {
                        Enumeration<String> element = error.getDetails().elements();
                        while (element.hasMoreElements()) {
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                } else {
                    dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
                }
            }
        });
    }

    private void getMyLocation() {

        latitude = ((DiscoverHoodApp) getApplicationContext()).getLatitude();

        longitude = ((DiscoverHoodApp) getApplicationContext()).getLongitude();

    }

    public void onResume() {
        super.onResume();
        //Log.d("[RESUME IMAGEN]", "onResume");
        coms.setText(publi.getCommentsCounter() + " " + getResources().getString(R.string.Comments));
    }

}
    
  

