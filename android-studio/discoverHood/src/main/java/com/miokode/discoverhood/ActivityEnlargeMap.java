package com.miokode.discoverhood;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ActivityEnlargeMap extends FragmentActivity{
  
    private double lati;
    private double longi;
       
    
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_large_map);
				
	    ((DiscoverHoodApp)getApplicationContext()).setesN(false);	

	    Intent intent = getIntent();
	    
	    lati = intent.getDoubleExtra("latitude", 40);
	    longi = intent.getDoubleExtra("longitude", 73);
	   		    
	    SupportMapFragment mMapFragment = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map));
		
	    final GoogleMap map = mMapFragment.getMap();
		
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		  
		 LatLng PostLocation=new LatLng(lati,longi);
		  
		 map.addMarker(new MarkerOptions().position(PostLocation).title(this.getResources().getString(R.string.location)));

		 map.moveCamera(CameraUpdateFactory.newLatLngZoom(PostLocation, 16));
		 map.setOnCameraChangeListener(new OnCameraChangeListener() {
			    @Override
				public void onCameraChange(CameraPosition arg0) {
					 if (arg0.zoom > 12){
						 map.animateCamera(CameraUpdateFactory.zoomTo(arg0.zoom ));
					 }
					 else{
						 map.animateCamera(CameraUpdateFactory.zoomTo(12));

					 }
				}
		 
		 });
		 		 
	        
	      TextView closeM = (TextView)findViewById(R.id.close_map);
		  closeM.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	finish();
	            	
	            }
	        });
	      
	    }

	}

