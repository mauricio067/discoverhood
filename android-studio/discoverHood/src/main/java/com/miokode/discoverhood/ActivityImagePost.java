package com.miokode.discoverhood;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationImage;
import com.miokode.discoverhood.restful.publication.SDPublicationPOST;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.miokode.discoverhood.tools.MemoryCache;
import com.miokode.discoverhood.tools.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

@SuppressLint("CutPasteId")
public class ActivityImagePost extends FragmentActivity {

	private static final int MAX_HEIGHT_IMG = 1024;
	private static final int MAX_WIDTH_IMG = 1024;
	private ProgressDialog proD = null;
	private String SD_Url;
	private String SD_Key;	
	private static int TAKE_PIC=1;
	private static int SELECT_PIC=2;
	private String name=" ";
    private EditText textoPublication;
    private ImageView user_pic;
    private ImageView image;
    private AlertDialog ImageDialog;
    private Bitmap picture = null;
    private double latitude;
    private double longitude;
    private SDAccount account;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    DisplayImageOptions options;
    private MemoryCache avatarCache;
	private Bitmap avatarImage;
	private String url;
    
	
    @SuppressLint("ResourceAsColor")
	protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);

    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_take_a_pic_post);
    	   
	    ((DiscoverHoodApp)getApplicationContext()).setesN(false);	

    	SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
    	SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
    	account = ((DiscoverHoodApp)getApplication()).getUserDataAccount();
        avatarCache = ((DiscoverHoodApp)getApplication()).getAvatarMemoryCache();
       
        textoPublication=(EditText)findViewById(R.id.post);

        user_pic = (ImageView)findViewById(R.id.user_p);

        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(this);
      
        if (savedInstanceState != null) {
        	
        	picture = savedInstanceState.getParcelable("picture"); 
        	
        	String text = savedInstanceState.getString("text");
            textoPublication.setText(text);
            
            String url = savedInstanceState.getString("urlAvatar");
            imageLoader.displayImage(url, user_pic, options);
                      
            if(((DiscoverHoodApp)getApplication()).getInstanceBitmap()!=null){
         		avatarImage =  ((DiscoverHoodApp)getApplication()).getInstanceBitmap();
			    ImageView iv = (ImageView)findViewById(R.id.picture);
			    iv.setImageBitmap(avatarImage);
         	}
        }			
        else{
                
	        if(account.getAvatar()!=null){
	         	url = account.getAvatar().getPictureUrl().trim();
                imageLoader.displayImage(url, user_pic, options);
	        }
        }
        
        @SuppressWarnings("unused")
		Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");

		
        image=(ImageView)findViewById(R.id.picture);
        image.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
             takeApicture();	
           
            }
         });
        
        textoPublication.setOnFocusChangeListener(new OnFocusChangeListener() {          
    		public void onFocusChange(View v, boolean hasFocus) {
    			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    		    imm.showSoftInput(textoPublication, InputMethodManager.SHOW_IMPLICIT);
    		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    			if (hasFocus){
    				textoPublication.setHint("");
    			}
    			else {
    				textoPublication.setHint(getResources().getString(R.string.email));
    			}
            }
    	   });
		TextView CancelPost= (TextView)findViewById(R.id.cancel_post);
		CancelPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	 onBackPressed();
            	 overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
            }
		});
		
		
		TextView SendPost= (TextView)findViewById(R.id.send_post);
		SendPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            		if(picture == null) ShowErrorDialog(getString(R.string.post_without_pic));
            		else{
						v.setEnabled(false);
                        showLoadingProgressDialog();
        		  		((DiscoverHoodApp) getApplication()).postSended(true);
        		  		getMyLocation();
                    	enviarDatosAlServer(picture);
            		}
           }
            });
        findViewById(R.id.btRotateLeft).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                rotate(-90);
            }
        });
        findViewById(R.id.btRotateRight).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                rotate(90);
            }
        });
	}

    private void rotate(int grados) {
        if (picture!=null) {
            Matrix matrix = new Matrix();

            matrix.postRotate(grados);

            picture = Bitmap.createBitmap(picture , 0, 0, picture .getWidth(), picture .getHeight(), matrix, true);

            avatarImage = picture;
            ImageView iv = (ImageView)findViewById(R.id.picture);
            iv.setImageBitmap(picture);
            ((DiscoverHoodApp)this.getApplication()).setPictureTaken(picture);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
	     super.onSaveInstanceState(bundle);
	     bundle.putString("text",textoPublication.getText().toString() );
	     bundle.putString("urlAvatar",url);
	     ((DiscoverHoodApp)getApplication()).setInstanceBitmap(avatarImage);
	     bundle.putParcelable("picture", (Parcelable) picture);
	     //Log.d("aferlegusta", "guardo los datos");
	}
 
      
      
   private void takeApicture(){
       	ImageDialog = new AlertDialog.Builder(this)
        .setTitle(getString(R.string.obtener_foto))
        .setMessage(getString(R.string.foto_option))
        .setPositiveButton(getString(R.string.galeria), new AlertDialog.OnClickListener() {
        	
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				fromGallery();
			}
		})
        .setNegativeButton(getString(R.string.camara), new AlertDialog.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	fromCamera();
	        }
        })
        
       .setNeutralButton(getString(R.string.cancel_take_photo), new AlertDialog.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	ImageDialog.dismiss();
	        	onBackPressed();
           	    overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
	        }
        })
        .create();
        ImageDialog.show();
          
   }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	
	  	    if (resultCode != Activity.RESULT_OK) {
		       return;
		    }else{
                if (requestCode == TAKE_PIC) {

                    ImageView iv = (ImageView)findViewById(R.id.picture);
                    picture = Utils.decodeSampledBitmapFromFile(name,MAX_WIDTH_IMG,MAX_HEIGHT_IMG);
                    picture = Utils.redimensionarImagen(picture, MAX_WIDTH_IMG, MAX_HEIGHT_IMG);
                    avatarImage = Bitmap.createScaledBitmap(picture, 300, 300, true);
                    iv.setImageBitmap(avatarImage);
                    ((DiscoverHoodApp)this.getApplication()).setPictureTaken(picture);

                    new MediaScannerConnectionClient() {
                        private MediaScannerConnection msc = null; {
                            msc = new MediaScannerConnection(getApplicationContext(), this); msc.connect();
                        }
                        public void onMediaScannerConnected() {
                            msc.scanFile(name, null);
                        }
                        public void onScanCompleted(String path, Uri uri) {
                            msc.disconnect();
                        }
                    };
                } else if (requestCode == SELECT_PIC){
                        Uri selectedImage = data.getData();
                            /*
                        InputStream is;
                        try {
                            is= getContentResolver().openInputStream(selectedImage);
                            BufferedInputStream bis = new BufferedInputStream(is);
                            BitmapFactory.Options options=new BitmapFactory.Options();
                            options.inSampleSize = 3;
                            picture = BitmapFactory.decodeStream(bis,null,options);

                        }catch (FileNotFoundException e) {}
                            */
                        picture = Utils.decodeSampledBitmapFromFile(getPath(selectedImage),MAX_WIDTH_IMG,MAX_HEIGHT_IMG);
                        picture = Utils.redimensionarImagen(picture, MAX_WIDTH_IMG, MAX_HEIGHT_IMG);
                        avatarImage = picture;
                        ImageView iv = (ImageView)findViewById(R.id.picture);
                        iv.setImageBitmap(picture);
                        ((DiscoverHoodApp)this.getApplication()).setPictureTaken(picture);
                    }
				if(picture!=null){
					findViewById(R.id.layoutRotate).setVisibility(View.VISIBLE);
				}
		   }

	   	   	    			 
	  }

	public String getPath(Uri uri) {
        try {
            String[] projection = { MediaStore.Images.Media.DATA };
            Cursor cursor = managedQuery(uri, projection, null, null, null);
            startManagingCursor(cursor);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return "";
    }

	private void fromGallery(){
	   Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
	   int code = SELECT_PIC;
	   startActivityForResult(i, code);
	   
	}


	@SuppressWarnings("unused")
	private void fromCamera(){
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			try {
				File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
				dir.mkdirs();
				File output = new File(dir,"photo"+new Date().getTime()+".png");
				name = output.getPath();
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
				startActivityForResult(takePictureIntent, TAKE_PIC);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Log.e("DiscoverHood","Sin aplicación de la cámara");
		}
	}
 
	
	
	 private void getMyLocation(){
	    	
		 latitude = ((DiscoverHoodApp) this.getApplication()).getLatitude();
		 longitude = ((DiscoverHoodApp) this.getApplication()).getLongitude();
	}
	

	public void ShowErrorDialog( String msg ){   	
 		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
 		alertDialog.setTitle(getString(R.string.error));    	   		
 		alertDialog.setMessage(msg);
 		alertDialog.show();
	}
 
 
	public void showLoadingProgressDialog() {
		this.proD = ProgressDialog.show(this, "", getString(R.string.sending_post), true, false);
	}
	
	
	public void dismissProgressDialog(){
		if (this.proD != null ) {
			this.proD .dismiss();
		}
	}
 	
	private void enviarDatosAlServer(Bitmap picture){


		 final SDPublication publication = new SDPublicationImage( textoPublication.getText().toString(), latitude, longitude, picture);
		 
		  new SDPublicationPOST(SD_Url,  SD_Key, publication,  new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dismissProgressDialog();
								
				finish();
				
			}

			@Override
			public void onIssues(String issues) {
				 dismissProgressDialog();
				 ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				 dismissProgressDialog();
				((TextView)findViewById(R.id.send_post)).setEnabled(true);
				 ActivityImagePost.this.proD.dismiss();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	
 	 
		 }); 
	 }
}





