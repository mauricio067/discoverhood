package com.miokode.discoverhood;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Display;
import android.view.WindowManager;

import com.miokode.discoverhood.dialogs.DialogFriendRequest;
import com.miokode.discoverhood.dialogs.DialogMessages;
import com.miokode.discoverhood.dialogs.DialogRecentActivity;
import com.miokode.discoverhood.dialogs.DialogSherlockListFragment;
import com.miokode.discoverhood.fragment.FragmentFriend;
import com.miokode.discoverhood.fragment.FragmentGeneralFeeds;
import com.miokode.discoverhood.fragment.FragmentLeftMenu;
import com.miokode.discoverhood.fragment.FragmentListConversation;
import com.miokode.discoverhood.gcm.GcmIntentService;
import com.miokode.discoverhood.gcm.GcmUtils;
import com.miokode.discoverhood.services.DaemonGeolocalization;
import com.slidingmenu.lib.SlidingMenu;


public class ActivityMenu extends ActivityMenuBase {

    private Fragment mContent, previous, friendreq, geo, notify, frag;
    private BroadcastReceiver messagesReceiver;
    private BroadcastReceiver friendRequestReceiver;
    private BroadcastReceiver publicationsReceiver;

    public ActivityMenu() {
        super(R.string.app_name);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Log.d("[activityMenu]", "Created");
        super.onCreate(savedInstanceState);
        stopService(new Intent(this.getApplicationContext(), DaemonGeolocalization.class));
        ((DiscoverHoodApp) this.getApplication()).setActivitMenu(this);

        startGeoService();

        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionBar_background)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        setContentView(R.layout.screen_menu_content_frame);

        boolean esN = ((DiscoverHoodApp) this.getApplication()).getesN();

        if (savedInstanceState != null) {
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
            ((DiscoverHoodApp) this.getApplication()).setesN(true);
        }

        Bundle extras = this.getIntent().getExtras();


        if (extras != null) {// && esN
            if (extras.containsKey("M"))
                mContent = new FragmentListConversation();
            else if (extras.containsKey("FR"))
                mContent = new FragmentFriend();
        }

        if (mContent == null) {
            mContent = new FragmentGeneralFeeds();
            previous = mContent;
            ((DiscoverHoodApp) this.getApplication()).setesN(true);
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, mContent).commit();


        setBehindContentView(R.layout.screen_menu_frame);
        FragmentLeftMenu FLM = new FragmentLeftMenu();
        ((DiscoverHoodApp) this.getApplicationContext()).setFragmentLeftMenu(FLM);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, FLM).commit();

        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        setSlidingActionBarEnabled(true);

        friendreq = new DialogFriendRequest();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_framefriendicon, friendreq).commit();

        geo = new DialogRecentActivity();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_framegeoicon, geo).commit();

        notify = new DialogMessages();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_framemessageicon, notify).commit();

        registerReceivers();

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            getSlidingMenu().setBehindWidth(600);
        } else {

            int width = getScreenWidth();
            getSlidingMenu().setBehindWidth(width - 35);
        }

        new GcmUtils(this).configureGcmIfNecessary();
    }

    private void registerReceivers() {
        messagesReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (notify != null) {
                    ((DialogMessages) notify).loadNewMessages();
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(messagesReceiver, new IntentFilter(GcmIntentService.NEW_MESSAGES));
        publicationsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (geo != null) {
                    ((DialogRecentActivity) geo).loadRecentActivity();
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(publicationsReceiver, new IntentFilter(GcmIntentService.NEW_PUB_NOTIFS));
        friendRequestReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (friendreq != null) {
                    ((DialogFriendRequest) friendreq).loadFriendRequests();
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(friendRequestReceiver, new IntentFilter(GcmIntentService.NEW_FRIEND_REQUEST));
    }

    @Override
    public void finish() {
        if(messagesReceiver!=null) LocalBroadcastManager.getInstance(this).unregisterReceiver(messagesReceiver);
        if(publicationsReceiver!=null) LocalBroadcastManager.getInstance(this).unregisterReceiver(publicationsReceiver);
        if(friendRequestReceiver!=null) LocalBroadcastManager.getInstance(this).unregisterReceiver(friendRequestReceiver);
        super.finish();
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();
        try {
            if(((DialogSherlockListFragment) mContent).isVIsibleOtherFrame()){
                ((DialogSherlockListFragment) mContent).showContentFrame();
            }else{
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.onBackPressed();
        }


    }

    private void startGeoService() {
        Bundle bundle = new Bundle();
        bundle.putString("SD_Key", ((DiscoverHoodApp) this.getApplication()).getServerKey());
        bundle.putString("SD_url", ((DiscoverHoodApp) this.getApplication()).getServerUrl());

        Intent intent = new Intent(this, DaemonGeolocalization.class);

        intent.putExtras(bundle);

        this.startService(intent);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//		getSupportFragmentManager().putFragment(outState, "mContent", mContent);
    }

    public void switchContent(Fragment fragment) {
//        previous = mContent;
//        mContent = fragment;
        getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.content_frame, fragment)
                .commit();
        getSlidingMenu().showContent();
    }

    public void switchContentWithoutBack(Fragment fragment) {
//        previous = mContent;
//        mContent = fragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
        getSlidingMenu().showContent();
    }

    public void switchFragment(Fragment fragment) {

        mContent = fragment;

        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, Fragment.instantiate(ActivityMenu.this, fragment.getClass().getName()))
                .commitAllowingStateLoss();
        getSlidingMenu().showContent();
    }


    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    /**
     * Obtengo el ancho de la pantalla del dispositivo
     */
    private int getScreenWidth() {

        int sw = 320;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1) {
            WindowManager wm = (WindowManager) getSystemService(ActivityMenu.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            sw = size.x;
        } else {
            Display display = getWindowManager().getDefaultDisplay();
            sw = display.getWidth();
        }

        return sw;
    }


    public void startSearchingLocation() {
        startActivity(new Intent(ActivityMenu.this, ActivitySearchingLocation.class));
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);

    }

}

