package com.miokode.discoverhood;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.miokode.discoverhood.dialogs.DialogFriendRequest;
import com.miokode.discoverhood.dialogs.DialogMessages;
import com.miokode.discoverhood.dialogs.DialogRecentActivity;
import com.miokode.discoverhood.fragment.FragmentLeftMenu;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingFragmentActivity;

import java.util.ArrayList;
import java.util.List;


public class ActivityMenuBase extends SlidingFragmentActivity {

    private int mTitleRes;
    protected ListFragment mFrag;
    protected Fragment OnewFriends, OnewMessages, OrecentActivity;
    protected FragmentTransaction ft;
    protected FragmentManager fm;
    protected Dialog bubble;
    private Fragment DFriendRequest, DMesages, DRecentActivity;
    private boolean DFRvisible = false;
    private boolean DMvisible, DRAvisible;
    private MenuItem menuFriendIcon, menuMailIcon, menuGeoIcon;

    public ActivityMenuBase(int titleRes) {
        mTitleRes = titleRes;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(mTitleRes);

        setBehindContentView(R.layout.screen_menu_frame);
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        mFrag = new FragmentLeftMenu();
        ft.replace(R.id.menu_frame, mFrag);
        ft.commit();

        SlidingMenu sm = getSlidingMenu();
        sm.setShadowWidth(15);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindOffset(60);
        sm.setFadeDegree(0.35f);
        sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.menu_button);
        @SuppressWarnings("unused")
        View fi = sm.findViewById(R.id.friends_icon);
        DFriendRequest = new DialogFriendRequest();
        DMesages = new DialogMessages();
        DRecentActivity = new DialogRecentActivity();
        DMvisible = false;
        DRAvisible = false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggle();
                return true;

            case R.id.friends_icon:
                if (getSupportFragmentManager().findFragmentByTag("DFR") == null) {
                    //Si es nulo lo creo.

                    menuFriendIcon.setIcon(R.drawable.friends_icon_selected);
                    menuMailIcon.setIcon(R.drawable.mail_icon);
                    menuGeoIcon.setIcon(R.drawable.web_icon);
                    DMvisible = false;
                    DFRvisible = true;
                    DRAvisible = false;

                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(0, R.anim.slide_in_up)
                            .add(R.id.content_frame, DFriendRequest, "DFR")
                            .addToBackStack(null)
                            .show(DFriendRequest)
                            .hide(DRecentActivity)
                            .hide(DMesages)
                            .commit();
                } else {
                    if (DFRvisible) {
                        menuFriendIcon.setIcon(R.drawable.friends_icon);
                        DFRvisible = false;
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(0, R.anim.slide_out_up)
                                .hide(DFriendRequest)
                                .hide(DMesages)
                                .hide(DRecentActivity)
                                .addToBackStack(null)
                                .commit();
                        System.out.println("oculto  al fragment");
                    } else {
                        menuFriendIcon.setIcon(R.drawable.friends_icon_selected);
                        menuMailIcon.setIcon(R.drawable.mail_icon);
                        menuGeoIcon.setIcon(R.drawable.web_icon);

                        DMvisible = false;
                        DFRvisible = true;
                        DRAvisible = false;
                        Fragment fr = getSupportFragmentManager().findFragmentByTag("DFR");
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(0, R.anim.slide_in_up)
                                .add(R.id.content_frame, fr, "DFR")
                                .show(DFriendRequest)
                                .hide(DRecentActivity)
                                .hide(DMesages)
                                .addToBackStack(null)
                                .commit();
                        System.out.println("muestro el fragment");

                    }
                }
                return true;

            case R.id.mail_icon:

                if (getSupportFragmentManager().findFragmentByTag("DM") == null) {
                    //Si es nulo lo creo.

                    menuFriendIcon.setIcon(R.drawable.friends_icon);
                    menuMailIcon.setIcon(R.drawable.mail_icon_selected);
                    menuGeoIcon.setIcon(R.drawable.web_icon);

                    DMvisible = true;
                    DFRvisible = false;
                    DRAvisible = false;
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(0, R.anim.slide_in_up)
                            .add(R.id.content_frame, DMesages, "DM")
                            .addToBackStack(null)
                            .show(DMesages)
                            .hide(DFriendRequest)
                            .hide(DRecentActivity)
                            .commit();
                } else {
                    if (DMvisible) {
                        menuMailIcon.setIcon(R.drawable.mail_icon);
                        DMvisible = false;
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(0, R.anim.slide_out_up)
                                .hide(DFriendRequest)
                                .hide(DMesages)
                                .hide(DRecentActivity)
                                .addToBackStack(null)
                                .commit();
                    } else {

                        menuFriendIcon.setIcon(R.drawable.friends_icon);
                        menuMailIcon.setIcon(R.drawable.mail_icon_selected);
                        menuGeoIcon.setIcon(R.drawable.web_icon);

                        DMvisible = true;
                        DFRvisible = false;
                        DRAvisible = false;
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(0, R.anim.slide_in_up)
                                .hide(DFriendRequest)
                                .hide(DRecentActivity)
                                .show(DMesages)
                                .addToBackStack(null)
                                .commit();

                    }
                }
                return true;

            case R.id.geo_icon:
                if (getSupportFragmentManager().findFragmentByTag("DRA") == null) {
                    //Si es nulo lo creo.

                    menuFriendIcon.setIcon(R.drawable.friends_icon);
                    menuMailIcon.setIcon(R.drawable.mail_icon);
                    menuGeoIcon.setIcon(R.drawable.web_icon_selected);

                    DRAvisible = true;
                    DFRvisible = false;
                    DMvisible = false;
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(0, R.anim.slide_in_up)
                            .add(R.id.content_frame, DRecentActivity, "DRA")
                            .addToBackStack(null)
                            .hide(DFriendRequest)
                            .show(DRecentActivity)
                            .hide(DMesages)
                            .commit();
                } else {
                    if (DRAvisible) {
                        menuGeoIcon.setIcon(R.drawable.web_icon);
                        DRAvisible = false;
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(0, R.anim.slide_out_up)
                                .hide(DFriendRequest)
                                .hide(DMesages)
                                .hide(DRecentActivity)
                                .addToBackStack(null)
                                .commit();
                    } else {

                        menuFriendIcon.setIcon(R.drawable.friends_icon);
                        menuMailIcon.setIcon(R.drawable.mail_icon);
                        menuGeoIcon.setIcon(R.drawable.web_icon_selected);

                        DRAvisible = true;
                        DFRvisible = false;
                        DMvisible = false;

                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(0, R.anim.slide_in_up)
                                .hide(DFriendRequest)
                                .show(DRecentActivity)
                                .hide(DMesages)
                                .addToBackStack(null)
                                .commit();

                    }
                }
                return true;

        }
        return onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getSupportMenuInflater().inflate(R.menu.main, menu);

        return true;
    }


    public class BasePagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragments = new ArrayList<Fragment>();

        private ViewPager mPager;

        public BasePagerAdapter(FragmentManager fm, ViewPager vp) {
            super(fm);
            mPager = vp;
            mPager.setAdapter(this);
            for (int i = 0; i < 10; i++) {
                addTab(new FragmentLeftMenu());
            }
        }

        public void addTab(Fragment frag) {
            mFragments.add(frag);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }

    public void listfriendrequest(View v) {
        System.out.println("hice click");
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menuFriendIcon = menu.findItem(R.id.friends_icon);
        menuMailIcon = menu.findItem(R.id.mail_icon);
        menuGeoIcon = menu.findItem(R.id.geo_icon);
        return super.onPrepareOptionsMenu(menu);

    }

    public void refreshFragmentLeftMenu(String distance, String nickname) {
        Bundle bundle = new Bundle();
        ft = this.getSupportFragmentManager().beginTransaction();
        if (ft != null) {
            if (nickname != null) bundle.putString("nickname", nickname);
            if (distance != null && !distance.isEmpty()) bundle.putString("distance", distance);
            //Log.d("[refreshmenu]", distance);
            mFrag = new FragmentLeftMenu();
            mFrag.setArguments(bundle);
            ft.replace(R.id.menu_frame, mFrag);
            ft.commit();
        }
    }

}
