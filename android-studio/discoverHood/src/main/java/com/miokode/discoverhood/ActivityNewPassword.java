package com.miokode.discoverhood;

import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.miokode.discoverhood.restful.account.SDAccountForgotPasswSend;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.tools.ConnectionDialogs;


public class ActivityNewPassword extends FragmentActivity {
	private EditText Tmail;
	private ConnectionDialogs dialogs;
	private String SD_Url;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dialogs = new ConnectionDialogs(this);
        SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
        final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.screen_forget_password);
        
        if (customTitleSupported) {
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar_back);
        }
        
	    Tmail=(EditText)(EditText)findViewById(R.id.inmail);

	    Tmail.setOnFocusChangeListener(new OnFocusChangeListener() {          
				public void onFocusChange(View v, boolean hasFocus) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				    imm.showSoftInput(Tmail, InputMethodManager.SHOW_IMPLICIT);
				    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
					if (hasFocus){
						Tmail.setHint("");
						Tmail.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
					}
					else {
						Tmail.setHint(getResources().getString(R.string.email));
						Tmail.setBackgroundColor(getResources().getColor(R.color.white));
	
					}
		        }
		});  	
	    
        Button backButton = (Button)findViewById(R.id.backbutton);
        backButton.setOnClickListener(new OnClickListener() {
	          @Override
	          public void onClick(View v) {
	        	  onBackPressed();
	          }
        });

        /*aca definimos el tipo de fuente*/
        Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
        Button sendacode = (Button)  findViewById(R.id.sendcodeb);
        TextView textvie = (TextView)findViewById(R.id.textView);
        TextView copyr = (TextView)findViewById(R.id.pou_all_rigths);
        Tmail.setTypeface(face);
	    sendacode.setTypeface(face);
	    textvie.setTypeface(face);
	    copyr.setTypeface(face);
	    
	    /*Solo para los botones*/
	    backButton.setTypeface(face);    
    }
	
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
    
    public void startNextActivity(){
    	Intent i=new Intent(this, ActivityValidatePassword.class);
    	i.putExtra("email", Tmail.getText().toString());
        startActivity(i);
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }

	//***********************************************************
	//********************   Extra methods   ********************
	//***********************************************************
    
    /**
     * Send request code to the server if the email is valid.
     */
    public void RequestCode(View v){
    	String mail=Tmail.getText().toString();
    	if (checkEmail(mail)) {
    		enviarDatosAlServer();
    	}else{
    		dialogs.ShowErrorDialog(getString(R.string.invalid_mail_format));
    	}
    }
    
    /**
     * Validate email format
     * 
     * @param email
     * @return
     * 		True if is valid email format.
     */
    public boolean checkEmail (String email) {
    	Pattern p = Pattern.compile("[-\\w\\.]+@\\w+\\.\\w+");
    	Matcher m = p.matcher(email);
    	return m.matches();
    }

	//***********************************************************
	//******************** Server connection  *******************
	//***********************************************************
	/**
	 * Send data to server. 
	 */
	private void enviarDatosAlServer(){
		 dialogs.showLoadingProgressDialog(getString(R.string.sending_email));
		 new SDAccountForgotPasswSend(SD_Url,  null, Tmail.getText().toString(),  new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dialogs.dismissProgressDialog();
				startNextActivity();
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 }); 
	 }
	
}