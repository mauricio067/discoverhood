package com.miokode.discoverhood;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

public class ActivityPrivateSetting extends Activity {
	
	private ToggleButton tg;
		
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
       
        setContentView(R.layout.screen_privacy_settings);
        tg = (ToggleButton) findViewById(R.id.toggleb); 
        
        /*aca definimos el tipo de fuente*/
        Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
        Button continu = (Button)  findViewById(R.id.continue_button);
        TextView textview = (TextView)findViewById(R.id.post_distance);
        TextView pouallr= (TextView)findViewById(R.id.pou_all_rigths);
        TextView textvie1 = (TextView)findViewById(R.id.addTOfavorites);

        
        tg.setTypeface(face);
        continu.setTypeface(face);
        textview.setTypeface(face);
        pouallr.setTypeface(face);
        textvie1.setTypeface(face);
        
        /*Solo para los botones*/
      /*  backButton.setTypeface(face);    */
        
        tg.setOnClickListener(new OnClickListener()
        {
     	    @Override
     	    public void onClick(View v)
     	    {
     	    	if(tg.isChecked()){ 
     				tg.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
     				
     			}else{ 
     				tg.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
     			}
     	    }
     	});
       
        
        continu.setOnClickListener(new OnClickListener(){
     	    @Override
     	    public void onClick(View v) {
		     	   startActivity(new Intent(ActivityPrivateSetting.this, ActivityMenu.class));
			        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
     	    }});
	}
	
	/*public void turnOnPrivateSettings(View v){
		
		if(tg.isChecked()){
			tg.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			
		}else{ 
			tg.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
		}
	}*/
	
	public void onBackPressed() {
	        super.onBackPressed();
	        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
	}
	 
	 
	public void goToPersonalProfile(View v){
		 startActivity(new Intent(this, ActivityMenu.class));
	        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
	}

	
	
	 
}
