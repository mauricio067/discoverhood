package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import com.actionbarsherlock.app.SherlockListActivity;
import com.miokode.discoverhood.adapter.AdapterLisItemFeed;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationNotification;
import com.miokode.discoverhood.restful.publication.SDPublicationsGETAll;
import com.miokode.discoverhood.restful.publication.SDPublicationsGETAllResponse;
import com.miokode.discoverhood.tools.ConnectionDialogs;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityShowPublication extends SherlockListActivity{
	
	private ListView list;
	private AdapterLisItemFeed adapter;
	private ArrayList<SDPublication> JustOnePublication;
	private ArrayList<SDPublicationNotification> PublicationsNotification;
	private SDPublication p;
	private ArrayList<SDPublication> UserPublications;
	private ConnectionDialogs dialogs;

		
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_publication);

        dialogs = new ConnectionDialogs(this);
	
    	Bundle extras= getIntent().getExtras(); 
		
		String id = extras.getString("ID");
		
		loadPublication(id);
		
		TextView closeM = (TextView)findViewById(R.id.close_publication);
		  closeM.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	finish();
	            	
	            }
	        });
	}

	private void loadPublication(String id) {
		DiscoverHoodApp app = (DiscoverHoodApp) getApplication();
		new SDPublicationsGETAll(app.getServerUrl(), app.getServerKey(), id, new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {
                SDPublicationsGETAllResponse resp = (SDPublicationsGETAllResponse) resultElement;
                ArrayList<SDPublication> publications = resp.getPublications();

                if(publications.size()>0){
                    SDPublication publi = publications.get(0);

                    JustOnePublication = new ArrayList<SDPublication>();

                    adapter = new AdapterLisItemFeed(ActivityShowPublication.this, JustOnePublication);

                    list = getListView();

                    list.setAdapter(adapter);


                    //Log.DEBUG("[Notificacion Imagen]", publi.ge)

                    adapter.addItem(publi);
                    adapter.notifyDataSetChanged();

                    findViewById(R.id.progressBar).setVisibility(View.GONE);
                }
            }

            @Override
            public void onIssues(String issues) {

            }

            @Override
            public void onError(SDError error) {
				if (error !=null && error.getReason() != null ){
					if (error.getDetails() == null){
						dialogs.ShowErrorDialog(error.getReason());
					}else{
						Enumeration<String> element = error.getDetails().elements();
						while(element.hasMoreElements()){
							dialogs.ShowErrorDialog(element.nextElement());
						}
					}
				}else{
					dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				}
            }
        });
	}

}


