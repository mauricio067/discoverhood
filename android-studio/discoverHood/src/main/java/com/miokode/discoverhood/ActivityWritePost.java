package com.miokode.discoverhood;

import java.util.Enumeration;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.dialogs.DialogFragmentActivity;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationPOST;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.miokode.discoverhood.tools.MemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

@SuppressLint("ResourceAsColor")
public class ActivityWritePost extends DialogFragmentActivity {
	
	private EditText textoPublication;
    private ImageView user_pic;
    private double latitude = 0;
    private double longitude = 0;
    private SDAccount account;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    DisplayImageOptions options;
    private MemoryCache avatarCache;
    private String userID;
    
    
    
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_post_event);
    	
	    ((DiscoverHoodApp)getApplicationContext()).setesN(false);	
    	
    	account = ((DiscoverHoodApp)getApplication()).getUserDataAccount();
        avatarCache = ((DiscoverHoodApp)getApplication()).getAvatarMemoryCache();
    	Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
		
    	textoPublication = (EditText)findViewById(R.id.info_publication);
		
		
		
		if (savedInstanceState != null) {
            
        	String text = savedInstanceState.getString("text");
            textoPublication.setText(text);
		}
		else {
			String textPost = textoPublication.getText().toString().toString();
		}
		
    	user_pic=(ImageView)findViewById(R.id.user_p);

        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(this);
        
        if(account.getAvatar()!=null){
        	
        	String url = account.getAvatar().getPictureUrl().trim();
            imageLoader.displayImage(url, user_pic, options);
        }
        			
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
        	userID = extras.getString("userID");
        }else{
        	userID = null;
        }
		
        
        textoPublication.setOnFocusChangeListener(new OnFocusChangeListener() {     
        	@Override
    		public void onFocusChange(View v, boolean hasFocus) {
    			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    		    imm.showSoftInput(textoPublication, InputMethodManager.SHOW_IMPLICIT);
    		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    			if (hasFocus){
    				textoPublication.setHint("");
    			}
    			else {
    				textoPublication.setHint(getResources().getString(R.string.email));
    			}
            }
    	   });
        
        TextView CancelPost= (TextView)findViewById(R.id.cancel_post);
		CancelPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	finish();
            	 overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
            }
		});
		
		final TextView SendPost= (TextView)findViewById(R.id.send_post);
		SendPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				SendPost.setEnabled(false);
		  		((DiscoverHoodApp) getApplication()).postSended(true);
            	getMyLocation();
            	String textPost = textoPublication.getText().toString().toString().trim();
            	if(textPost.length()==0) dialogs.ShowErrorDialog(getResources().getString(R.string.emptypost));
            	else{
            		enviarDatosAlServer();
            	}
            
            }
		});
   }
  
    public void onSaveInstanceState(Bundle bundle) {
	     super.onSaveInstanceState(bundle);
	     bundle.putString("text",textoPublication.getText().toString() );
    }
    
    private void getMyLocation(){
    	
		 latitude = ((DiscoverHoodApp) this.getApplication()).getLatitude();
		 longitude = ((DiscoverHoodApp) this.getApplication()).getLongitude();
	}
 
    
	private void enviarDatosAlServer(){
		 dialogs.showLoadingProgressDialog(getString(R.string.sending_post));
		 final SDPublication publication;
		 if (userID != null) {
			 publication = new SDPublication( textoPublication.getText().toString(), latitude, longitude, userID);
		 }else{
			 publication = new SDPublication( textoPublication.getText().toString(), latitude, longitude);
		 }
		  new SDPublicationPOST(SD_Url,  SD_Key, publication,  new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dialogs.dismissProgressDialog();
				onBackPressed();
				//finish();			
            	overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
		    }


			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				((TextView)findViewById(R.id.cancel_post)).setEnabled(true);
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));

				 }
			}		 
		 }); 
	 }

		
	
}


