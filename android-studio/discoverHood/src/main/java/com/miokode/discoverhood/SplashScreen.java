package com.miokode.discoverhood;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;

import com.facebook.AppEventsLogger;
import com.miokode.discoverhood.gcm.GcmIntentService;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.account.SDAccountGETMy;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.services.DaemonForTasks;
import com.miokode.discoverhood.services.DaemonMessages;
import com.miokode.discoverhood.tools.ConnectionDialogs;
import com.miokode.discoverhood.tools.LoginTools;
import com.miokode.discoverhood.tools.NotificationsManager;
import com.miokode.discoverhood.tools.SessionManagement;

public class SplashScreen extends  FragmentActivity {

    private static int SPLASH_TIME_OUT = 1000;
    private SessionManagement generalSession;
    private ConnectionDialogs dialogs;
    private String SD_url = null;
	private SDAccount account = null;
	private LoginTools LoginHelper;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_splash);
       //((DiscoverHoodApp)getApplication()).setSplashScreen(this);

		showHashKey();

        generalSession = new SessionManagement(getApplicationContext()); 
        dialogs = new ConnectionDialogs(this);
        SD_url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();

    }

	private void showHashKey() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.miokode.discoverhood",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	@Override
    protected void onResume() {
        super.onResume();
        LoginHelper = new LoginTools(this);
        //start();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
        new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
	           	if (!LoginHelper.isActiveGPS()){
	           		LoginHelper.ActivateGPS();
	           	}else{
	           		start();
	           	}
           }
       }, SPLASH_TIME_OUT);
    }
    
    
    private void start(){
            if (generalSession.isLoggedIn() && generalSession.isFacebookLoggedIn()) {
            	HashMap<String, String> user = generalSession.getUserDetails();
                String SD_KEY = user.get(SessionManagement.SD_KEY);
                LoginHelper.setSDKey(SD_KEY);
                ((DiscoverHoodApp) SplashScreen.this.getApplication()).setServerKey(SD_KEY);
       		  	Login();
        	}else{
                Intent i = new Intent(SplashScreen.this, ActivityMain.class);
				Bundle extras = getIntent().getExtras();
				if(extras!=null){
					i.putExtras(extras);
				}
                startActivity(i);
                finish();
        	}
    }
    
    public void startNextActivity(){
	   	if (account!=null){ 
  			if(account.getActive()){

  				generalSession.createLoginSession(((DiscoverHoodApp) this.getApplication()).getServerKey(), account.getFacebook_login() );

				Bundle extras = getIntent().getExtras();
				if(extras!=null && extras.containsKey(NotificationsManager.EXTRA_NOTIF_TYPE)){
					startActivity(GcmIntentService.getIntentForNotification(this, extras.getString("ID"), extras.getInt(NotificationsManager.EXTRA_NOTIF_TYPE)));
				}else {
					Intent i = new Intent(this, ActivityMenu.class);
					startActivity(i);
				}
  			}else{
  				this.startActivity(new Intent(this, ActivityValidateCode.class));
  			}
  			this.overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
  			this.finish();
	    }
		
    }
    
	private void guardarUsuarioApp( SDAccount account){
		((DiscoverHoodApp) this.getApplication()).setUserDataAccount(account);
		this.account = account;
	}
	
    //***********************************************************
  	//******************** Server connection  *******************
  	//***********************************************************
  	private void Login(){
  			dialogs.showLoadingProgressDialog(getString(R.string.connecting));
  			 String SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
  			 LoginHelper.setSDKey(SD_Key);
  			 new SDAccountGETMy(SD_url, SD_Key, new SDConnectionResponseListener(){

  				@Override
  				public void onSuccess(String response, Object resultElement) {
  					dialogs.dismissProgressDialog();
  					SDAccount account = (SDAccount) resultElement;
  					guardarUsuarioApp(account);
  					startNextActivity();
  				}

  				@Override
  				public void onIssues(String issues) {
  					dialogs.dismissProgressDialog();
  					dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
  				}

  				@Override
  				public void onError(SDError error) {
  					dialogs.dismissProgressDialog();
  					 if ( error!=null&& error.getCode() == 401 ) {
  						 if (account!=null && account.getFacebook_login()){
  					        	dialogs.ShowErrorDialogIteractive(getString(R.string.server_connection_error),  new DialogInterface.OnClickListener() {	
  					     			public void onClick(DialogInterface dialog, int which) {
  					     				dialog.dismiss();	  	
  					     				SplashScreen.this.finish();
  					     		    }
  					     		});
  						 }else{
					        	dialogs.ShowErrorDialogIteractive(getString(R.string.invalid_user_pass),  new DialogInterface.OnClickListener() {	
					     			public void onClick(DialogInterface dialog, int which) {
					     				dialog.dismiss();	  	
					                    Intent i = new Intent(SplashScreen.this, ActivityMain.class);
					                    startActivity(i);
					                    finish();
					     				SplashScreen.this.finish();
					     		    }
					     		});
  						 }
  					 }else{
				         dialogs.ShowErrorDialogIteractive(getString(R.string.server_connection_error),  new DialogInterface.OnClickListener() {	
				     			public void onClick(DialogInterface dialog, int which) {
				     				dialog.dismiss();	 
				                    Intent i = new Intent(SplashScreen.this, ActivityMain.class);
				                    startActivity(i);
				                    finish();
				     				SplashScreen.this.finish();
				     		    }
				     	 });
  					 }
  				}	 	 
  			 }); 
  		 }
  	
}
