package com.miokode.discoverhood.Tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Mauricio on 17/02/2015.
 */
public class FacebookPictureAsync extends AsyncTask<Void,Void,Void> {
    private String idUserFacebook;
    private  Bitmap picture = null;
    private FacebookPictureAsyncCallback callback;

    public FacebookPictureAsync(String idUserFacebook, FacebookPictureAsyncCallback callback) {
        this.idUserFacebook = idUserFacebook;
        this.callback = callback;
    }


    @Override
    protected Void doInBackground(Void... voids) {

        InputStream inputStream = null;
        final String nomimg ="https://graph.facebook.com/"+idUserFacebook+"/picture?type=large";
        URL imageURL = null;
        try {
            imageURL = new URL( nomimg);

		} catch (Exception e) {
			e.printStackTrace();
		}
        try {
            HttpURLConnection connection = (HttpURLConnection) imageURL.openConnection();
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects( true );
            connection.connect();
            inputStream = connection.getInputStream();
            //img_value.openConnection().setInstanceFollowRedirects(true).getInputStream()
            picture = BitmapFactory.decodeStream(inputStream);

        } catch (IOException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        callback.onSuccess(picture);

    }


}
