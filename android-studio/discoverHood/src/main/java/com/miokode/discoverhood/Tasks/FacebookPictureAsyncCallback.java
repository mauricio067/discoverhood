package com.miokode.discoverhood.Tasks;

import android.graphics.Bitmap;

/**
 * Created by Mauricio on 17/02/2015.
 */
public abstract class FacebookPictureAsyncCallback {
    abstract public Void onSuccess(Bitmap picture);
}
