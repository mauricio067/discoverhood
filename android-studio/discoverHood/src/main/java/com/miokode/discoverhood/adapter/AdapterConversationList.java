package com.miokode.discoverhood.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.tools.DateTools;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.tools.MemoryCache;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.conversation.SDConversation;
import com.miokode.discoverhood.restful.SDMessage;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class AdapterConversationList extends BaseAdapter  {
	 
    private ArrayList<SDConversation> items;
    private ArrayList<SDConversation> itemsoriginal;
    private LayoutInflater mInflater;
    private SDAccount account;
    private Activity context;    
    private SDConversationArrayFilter filter;
    private MemoryCache avatarCache;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    DisplayImageOptions options;
    
    public AdapterConversationList(Activity context, ArrayList<SDConversation> items) {
    	this.context = context;
    	this.items = items;
    	this.itemsoriginal = items;
    	this.filter = new SDConversationArrayFilter();
    	avatarCache = ((DiscoverHoodApp)context.getApplication()).getAvatarMemoryCache();
        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(context);
    	mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	account = ((DiscoverHoodApp)context.getApplication()).getUserDataAccount();

    }
    public Object getItems(){
    	return items;
    }

    public void addConversation(final SDConversation item) {
    	itemsoriginal.add(item);
        notifyDataSetChanged();
    }
    public void clearConversations(){
    	items.clear();
    	notifyDataSetChanged();
    }
    
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public SDConversation getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_conversation, null);
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.nameContact);
            holder.date = (TextView)convertView.findViewById(R.id.datesend);
            holder.message = (TextView)convertView.findViewById(R.id.mensaje);
            holder.pic = (ImageView)convertView.findViewById(R.id.imageContact);          
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        SDConversation conversa = items.get(position);

       SDMessage mens = conversa.getLastMsg();    
        
        SDAuthor author;
        author = conversa.getPeople().get(mens.getAuthor());
        Iterator<SDAuthor> listautores = conversa.getPeople().values().iterator();
        


        while (listautores.hasNext()){
        	SDAuthor aut = listautores.next();

        	if (!(aut.getId().equalsIgnoreCase(this.account.getId()))) {
        		author = aut;
        	}
        }
        
        SDConversation conv = items.get(position);
        java.lang.System.out.println(conv.getPeople());
        HashMap<String, SDAuthor> lista;
        lista = conv.getPeople();

        holder.name.setText( author.getName() );   
        holder.date.setText(DateTools.getformatdate(mens.getCreated()));
        
        holder.message.setText( mens.getText() );
        if (author.getAvatar() != null ){
			String url = author.getAvatar().getPictureUrl().trim();
            imageLoader.displayImage(url, holder.pic, options);
		}
        
        return convertView;
    }


    public static class ViewHolder {
    	public TextView name;
    	public TextView date;
    	public TextView message;
    	public ImageView pic;
    }	
    
    public Filter getFilter() {
        return filter;
        
    }
    public void setFilter(CharSequence s)  {
    	//Log.d("nada", String.valueOf(s));
    	filter.filter(s);
    	
    }
    /**
     * Clase para filtrar la lista de conversaciones
     * 
     * @author Fernando Anthony Ristano
     */
    public class SDConversationArrayFilter extends Filter {
	        @Override
	        protected FilterResults performFiltering(CharSequence constraint) {
	        	//Log.d("nada", "entra");
	        	//Log.d("nadara", String.valueOf(constraint));
	            FilterResults Result = new FilterResults();
	            if(constraint.length() == 0 ){
	                Result.values = itemsoriginal;
	                Result.count = itemsoriginal.size();
	                return Result;
	            }

	            ArrayList<SDConversation> Filtered_Conv = new ArrayList<SDConversation>();
	            String filterString = constraint.toString().toLowerCase();
	
	            SDMessage lastMsgs;
	            
	            for(int i = 0; i<itemsoriginal.size(); i++){	
	            	lastMsgs = itemsoriginal.get(i).getLastMsg();
	            	
	                SDAuthor author;
	                author = itemsoriginal.get(i).getPeople().get(lastMsgs.getAuthor());
	                Iterator<SDAuthor> listautores = itemsoriginal.get(i).getPeople().values().iterator();
	                
	                while (listautores.hasNext()){
	                	SDAuthor aut = listautores.next();

	                	if (!(aut.getId().equalsIgnoreCase(account.getId()))) {
	                		author = aut;
	                	}
	                }
	            	if (author.getName().toLowerCase().contains(filterString.toLowerCase()))
	            		Filtered_Conv.add( itemsoriginal.get(i) );
	            }
	            Result.values = Filtered_Conv;
	            Result.count = Filtered_Conv.size();
	
	            return Result;
	        }
	
	        @Override
	        protected void publishResults(CharSequence constraint, FilterResults results) {
	        	items = (ArrayList<SDConversation>) results.values;
	            notifyDataSetChanged();
	        }
    }
    
}
