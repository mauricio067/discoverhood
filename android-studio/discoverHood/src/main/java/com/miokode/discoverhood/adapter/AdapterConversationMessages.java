package com.miokode.discoverhood.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.ActivityMenu;
import com.miokode.discoverhood.tools.DateTools;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.fragment.FragmentOtherUserProfile;
import com.miokode.discoverhood.fragment.FragmentUserProfile;
import com.miokode.discoverhood.tools.MemoryCache;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.conversation.SDConversation;
import com.miokode.discoverhood.restful.SDMessage;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class AdapterConversationMessages extends android.widget.BaseAdapter  {	 
    private SDConversation conversation;
    private ArrayList<SDMessage> items;
    private LayoutInflater mInflater;
    private Activity context;    
    private SDConversationArrayFilter filter;
    private MemoryCache avatarCache;
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private DisplayImageOptions options;
    private SDAccount account;
    
    public AdapterConversationMessages(Activity context, SDConversation conversation) {
    	this.context = context;
    	this.conversation = conversation;
    	this.items = new ArrayList<SDMessage>();
    	this.filter = new SDConversationArrayFilter();
    	avatarCache = ((DiscoverHoodApp)context.getApplication()).getAvatarMemoryCache();
        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(context);
    	mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.account = ((DiscoverHoodApp) context.getApplication()).getUserDataAccount();
       // ((DiscoverHoodApp) context.getApplicationContext()).setActualConversation(items, conversation);

    }
    public SDMessage getLastMessage(){
    	if (items.size()== 0){
    		return null;
    	}
    	else {
    	return items.get(items.size()-1);
    	}
    }
    public void addFromBottom(SDMessage msg){
    	items.add(items.size(), msg);
    }
    public void addMessage(final SDMessage item) {
    	items.add(item);
        notifyDataSetChanged();
    }

    public void addMessages(ArrayList<SDMessage> itm) {
    	items.addAll(itm);
        notifyDataSetChanged();
    }
    
    public void addFromTop(SDMessage item, int pos){
    	items.add(pos, item);
        notifyDataSetChanged();
    }
    
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public SDMessage getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_conversation, null);
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.nameContact);
            holder.date = (TextView)convertView.findViewById(R.id.datesend);
            holder.message = (TextView)convertView.findViewById(R.id.mensaje);
            holder.pic = (ImageView)convertView.findViewById(R.id.imageContact);          
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        final SDMessage mens = items.get(position);
		        		
        SDAuthor author = conversation.getPeople().get(mens.getAuthor());
        holder.name.setText( author.getName() );
        Date fecha =  mens.getCreated();
        holder.date.setText(DateTools.getformatdate(fecha));
        holder.message.setText( mens.getText() );
        
        if (author.getAvatar() != null ){
			String url = author.getAvatar().getPictureUrl().trim();
            imageLoader.displayImage(url, holder.pic, options);
		}
        else {
        	holder.pic.setImageResource(R.drawable.photo_icon);
        }
        
        
        
        holder.pic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
           	 SDMessage p = mens;     	   
               if(conversation.getPeople().get(p.getAuthor()).getId().compareTo(account.getId())==0){
    		 	   		Fragment fragment = new FragmentUserProfile();
             	   ((ActivityMenu)context).switchContent(fragment);

                }
                else
                	conversation.getPeople().get(p.getAuthor());
             	   gotoUserProfile(conversation.getPeople().get(p.getAuthor()).getId(),conversation.getPeople().get(p.getAuthor()));
             
                }
             });
        
        
        return convertView;
    }

   private void gotoUserProfile(String userID, SDAuthor aut){
    	
    	((DiscoverHoodApp) context.getApplication()).ClearOtherUserPublications();
    	FragmentOtherUserProfile fragment = new FragmentOtherUserProfile();
    	fragment.setuserData(aut);
	 	Bundle args= new Bundle();
	    args.putString("userID", userID);
	  
        fragment.setArguments(args);	
 	    ((ActivityMenu)context).switchContent(fragment);
    }

    public static class ViewHolder {
    	public TextView name;
    	public TextView date;
    	public TextView message;
    	public ImageView pic;
    }	
            
    
    public Filter getFilter() {
        return filter;
    }
    
    /**
     * Clase para filtrar la lista de conversaciones
     * 
     * @author Fernando Anthony Ristano
     */
    public class SDConversationArrayFilter extends Filter {
	        @Override
	        protected FilterResults performFiltering(CharSequence constraint) {
	            FilterResults Result = new FilterResults();
	            if(constraint.length() == 0 ){
	                Result.values = items;
	                Result.count = items.size();
	                return Result;
	            }
	
	            String filterString = constraint.toString().toLowerCase();
	            HashMap<String, SDAuthor> people = conversation.getPeople();
	            ArrayList<SDMessage> filterableMsgs = new ArrayList<SDMessage>();
	            
	            for(int i = 0; i<items.size(); i++){	
	            	//Busco textos parecidos
	            	if ( items.get(i).getText().toLowerCase().contains(filterString) ||  people.get( items.get(i).getAuthor()).getName().toLowerCase().contains(filterString) )
	            		filterableMsgs.add(items.get(i));
	            }
	            Result.values = filterableMsgs;
	            Result.count = filterableMsgs.size();
	
	            return Result;
	        }
	
	        @Override
	        protected void publishResults(CharSequence constraint, FilterResults results) {
	        	items = (ArrayList<SDMessage>) results.values;
	            notifyDataSetChanged();
	        }
    }
    
}
