package com.miokode.discoverhood.adapter;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.miokode.discoverhood.ActivityCommentPost;
import com.miokode.discoverhood.ActivityEnlargeImage;
import com.miokode.discoverhood.ActivityEnlargeMap;
import com.miokode.discoverhood.ActivityMenu;
import com.miokode.discoverhood.tools.ConnectionDialogs;
import com.miokode.discoverhood.tools.DateTools;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.fragment.FragmentOtherUserProfile;
import com.miokode.discoverhood.fragment.FragmentUserProfile;
import com.miokode.discoverhood.tools.MemoryCache;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.favourites.SDFavoritesPOSTAdd;
import com.miokode.discoverhood.restful.favourites.SDFavoritesPOSTRemove;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationCheckIn;
import com.miokode.discoverhood.restful.publication.SDPublicationImage;
import com.miokode.discoverhood.restful.publication.SDPublicationPOSTshare;
import com.miokode.discoverhood.restful.publication.SDPublicationPOSTtoggleSmile;
import com.miokode.discoverhood.restful.publication.SDPublicationShared;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@SuppressLint("ResourceAsColor")
public class AdapterExpandableListFavorites extends BaseExpandableListAdapter {
	private ConnectionDialogs dialogs;
    private String SD_Url;
	private String SD_Key;
    private Activity activity;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    DisplayImageOptions options;
//    private MiniImageLoader mil;
    private MemoryCache imageCache=new MemoryCache();
    private List<Calendar> _listDataHeader;
    private HashMap<Calendar, List<SDPublication>> _listDataChild;
    private int lastExpandedGroupPosition;
    ExpandableListView expandableList;
    /*se utilizan para recuperar valores*/
 //   private consult_publication consultapublicaciones;
    private List<SDPublication> nuevasPublicaciones;
    private LayoutInflater mInflater;
    private MemoryCache avatarCache;
    private SDAccount account;
	private TextView shares;
    private View cv;
    private double latitude;
    private double longitude;
	private ArrayList<String> publicationsSmiled;
	private ProgressDialog proD = null;
    private ProgressBar spinner = null;

	
	
    public void removeitem(int groupPosition, int childPosition){
    	this._listDataChild.get(this.getGroup(groupPosition)).remove(childPosition);
    	notifyDataSetChanged();
    }
    
    public AdapterExpandableListFavorites(Activity context, List<Calendar> listDataHeader,
    		HashMap<Calendar, List<SDPublication>> listChildData) {
		this.dialogs = new ConnectionDialogs(context);
        this.SD_Url = ((DiscoverHoodApp) context.getApplication()).getServerUrl();
        this.SD_Key = ((DiscoverHoodApp) context.getApplication()).getServerKey();
        this.activity = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(context);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        avatarCache = ((DiscoverHoodApp)context.getApplication()).getAvatarMemoryCache();
        this.account = ((DiscoverHoodApp) context.getApplication()).getUserDataAccount();
        getMyLocation();
        publicationsSmiled = ((DiscoverHoodApp)context.getApplication()).getPublicationsSmiled();  


        //    consultapublicaciones = (consult_publication) new consult_publication();

    }
 
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    
    
    
    /*Estas variables estan definidas para que funcione bien el codigo*/
	private ArrayList<SDPublication> mData;

    
    
    @SuppressLint("ResourceAsColor")
	@Override
    public View getChildView(int groupPosition, final int childPosition,
        boolean isLastChild, View convertView, ViewGroup parent) {
        ImageView picture=null;
        final ViewHolder holder;
        ImageView avatar = null;

        
        
        /*necesario para el buen funcionamiento*/
        mData =(ArrayList<SDPublication>) this._listDataChild.get(this._listDataHeader.get(groupPosition));
        final int pos = childPosition;
        int position = childPosition;
        SDPublication post=mData.get(position);
        SDPublicationShared postShared = null;
        final double postLatitude = post.getLatitud();
        final double postLongitude = post.getLongitud();
        LayoutInflater infalInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final SDPublication posteo = (SDPublication) getChild(groupPosition,childPosition);
       
/*Se debe cambiar de aca en adelante, no olvidar setear la variable cv = convertView 
 * despues de setear a convertView asi no tira nullpointer al momento de smiles coments o shares*/        
        holder = new ViewHolder();
        
        if(post instanceof SDPublicationShared){
            	convertView=mInflater.inflate(R.layout.item_post_shared, null);
        	
       	    	ImageView pic = (ImageView) convertView.findViewById(R.id.image_post_shared);
        	
       	    	postShared = (SDPublicationShared)post;     
           	    post = postShared.getSharedPublication(); 
        		holder.otherUname = (TextView) convertView.findViewById(R.id.other_name);
           	    holder.otherUname.setText(postShared.getSharedPublication().getAuthor().getName());
           	    holder.otherUname.setOnClickListener(new OnClickListener() {
                 @Override
                 public void onClick(View v) {
                	 SDPublicationShared p = (SDPublicationShared) mData.get(pos);     	   
                    if(p.getSharedPublication().getAuthor().getId().compareTo(account.getId())==0){
         		 	   		Fragment fragment = new FragmentUserProfile();
                  	   ((ActivityMenu)activity).switchContent(fragment);

                     }
                     else
                  	   gotoUserProfile(p.getSharedPublication().getAuthor().getId(),p.getSharedPublication().getAuthor());
                     }
                  });

   			 if(!(post instanceof SDPublicationImage) && !(post instanceof SDPublicationCheckIn)){
        	    	pic.setVisibility(View.GONE);
        	    	spinner = (ProgressBar) convertView.findViewById(R.id.progressBarShared); 
        	    	spinner.setVisibility(View.INVISIBLE);
   			 
   			 } else {
 				   spinner = (ProgressBar) convertView.findViewById(R.id.progressBarShared); 

 			 }
        }
   		else{
        
   			if(post instanceof SDPublicationImage || post instanceof SDPublicationCheckIn){   	
   				convertView=mInflater.inflate(R.layout.item_feed_post_im, null);    
   				spinner = (ProgressBar) convertView.findViewById(R.id.progressBarPost); 

   			}
   			else if (post.getTo()!=null){
        	convertView=mInflater.inflate(R.layout.item_post_to_user, null); 
        	
        	holder.otherUname = (TextView) convertView.findViewById(R.id.other_name);
        	holder.otherUname.setText(post.getTo().getName());
        	
        	holder.otherUname.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                	SDPublication postt = mData.get(pos);
                	gotoUserProfile(postt.getTo().getId(),postt.getAuthor());
                }
                
            });
        	
        } 
        else{   	
        		convertView=mInflater.inflate(R.layout.item_feed_post, null);
        }   
   		}
        /*seteo utlima informacion de la publicacion*/
        if (((DiscoverHoodApp) activity.getApplication()).isTogglePublicationsSmiled(post.getId())) {
     	   if (post.getIsmile()) {
     		   post.setIsmile(false);
     		   post.decreaseSmiles();
     	   }
     	   else {
     		   post.setIsmile(true);
     		   post.increaseSmiles();
     	   }
        }
        
        cv = convertView ;             
        holder.user_name = (TextView) convertView.findViewById(R.id.profile_name);
        holder.date = (TextView) convertView.findViewById(R.id.post_time);
        holder.distance = (TextView) convertView.findViewById(R.id.post_distance);
        holder.post = (TextView) convertView.findViewById(R.id.user_post);
        holder.smi = (TextView) convertView.findViewById(R.id.smile);
        holder.smis = (TextView) convertView.findViewById(R.id.smiles);
        holder.com = (TextView) convertView.findViewById(R.id.comment);
        holder.sha = (TextView) convertView.findViewById(R.id.share);
        holder.shas = (TextView) convertView.findViewById(R.id.shares);
        holder.addToFavorites = (TextView)convertView.findViewById(R.id.addTOfavorites);
        holder.trSmile = (TableRow) convertView.findViewById(R.id.tableRow_smile); 
        holder.trShare = (TableRow) convertView.findViewById(R.id.tableRow_share); 
        holder.trComment = (TableRow) convertView.findViewById(R.id.tableRow_comment); 
		avatar = (ImageView)convertView.findViewById(R.id.post_user_image);

		
		avatar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	SDPublication postt = mData.get(pos);
            	if( postt.getAuthor().getId().compareTo(account.getId())==0){
   		 	   		Fragment fragment = new FragmentUserProfile();
            	   ((ActivityMenu)activity).switchContent(fragment);}

            	else
            		gotoUserProfile(postt.getAuthor().getId(), postt.getAuthor());
            }

        });
    
        			
       if( post.getAuthor().getId().compareTo(account.getId())==0){
    	   holder.sha.setText(null);
           final ImageView isha = (ImageView) convertView.findViewById(R.id.i_share);
           isha.setImageDrawable(null);
       }
       else{
    	   
    	   	holder.trShare.setOnClickListener(new OnClickListener() {
               @Override
               public void onClick(View v) {
            	    SDPublication postt = mData.get(pos);
             	   	shares = (TextView) cv.findViewById(R.id.shares);

                   	if(postt instanceof SDPublicationShared){
                       	
                        shares.setText(((SDPublicationShared) postt).getSharedPublication().getSharesCounter()+" "+activity.getResources().getString(R.string.Shares));
                        sharePost( ((SDPublicationShared) postt).getSharedPublication(), pos);
                   	}else{
                        shares.setText(postt.getSharesCounter()+" "+activity.getResources().getString(R.string.Shares));
                        sharePost(postt, pos);
                   	}

               } 
           });
       }
    	   
       
       holder.user_name.setOnClickListener(new OnClickListener() {
           @Override
           public void onClick(View v) {
        	              	   
        	   SDPublication postt = mData.get(pos);
        	   
               if( postt.getAuthor().getId().compareTo(account.getId())==0){
   		 	   		Fragment fragment = new FragmentUserProfile();
            	   ((ActivityMenu)activity).switchContent(fragment);

               }
               else
            	   gotoUserProfile(postt.getAuthor().getId(),postt.getAuthor());
               }
           
       });

       
       final TextView Ssmiles = (TextView) convertView.findViewById(R.id.smiles);
       final TextView Ssmile = (TextView) convertView.findViewById(R.id.smile);
       holder.trSmile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            	SDPublication postti = mData.get(pos);
                SDPublicationPOSTtoggleSmile.smileClicked(postti,holder.trSmile,Ssmiles,Ssmile, (DiscoverHoodApp) activity.getApplication(),
                        new Runnable(){
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });

            }
         	 
        }); 
       
          
       TextView tv = (TextView) convertView.findViewById(R.id.comments);

       tv.setText(post.getCommentsCounter()+" "+activity.getResources().getString(R.string.Comments));   

       ((DiscoverHoodApp)activity.getApplicationContext()).setIV(tv);
        holder.trComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	SDPublication postt = mData.get(pos);
            	SDPublicationShared Sp;
            	if (postt instanceof SDPublicationShared) {
           	    	Sp = (SDPublicationShared)postt;     
               	    postt = Sp.getSharedPublication(); 
               	    //((DiscoverHoodApp) activity.getApplicationContext()).setCurrentPublication(postt);
            	}
            	((DiscoverHoodApp) activity.getApplicationContext()).setCurrentPublication(postt);

            	Intent inte = new Intent(activity, ActivityCommentPost.class);
            	inte.putExtra("publicationID", postt.getId());
            	activity.startActivity(inte);
            	activity.overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
         }
        });

        if (post.iComment()){
            TextView Scomments = (TextView) convertView.findViewById(R.id.comment);
            Scomments.setTextColor(Color.MAGENTA);
        }
        
        /*Seteo de colores*/
        if (post.isIshared()){
        	holder.sha.setTextColor(Color.MAGENTA);
        }
        else {
        	holder.sha.setTextColor(Color.WHITE);
        }
        
        if (post.getIsmile()) {
        	holder.smi.setTextColor(Color.MAGENTA);
        }
        else {
        	holder.smi.setTextColor(Color.WHITE);
        }
        
        
        holder.addToFavorites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	
        		 SDPublication pub =  mData.get(pos);
        		 TextView AddToFavorites = (TextView)v.findViewById(R.id.addTOfavorites);
        		if (activity.getString(R.string.Add_to_Favorites) == AddToFavorites.getText() ){
        			 AddToFavorites.setText(R.string.Remove_Favorite);
        			 addPublicationToFavorites(pub);

        		 }
        		else {
        			 AddToFavorites.setText(R.string.Add_to_Favorites);
        			 removePublicationToFavorites(pub);                		

        		}
            	
            	if (((SDPublication)mData.get(pos)).getFavorite()){
            		
            	}else{
            	}
            	
            }
        });

  
    long now = System.currentTimeMillis();
    CharSequence time = DateUtils.getRelativeTimeSpanString(now, DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_NO_NOON, 0);
    
    holder.user_name.setText(post.getAuthor().getName());
    holder.date.setText(time);
    if( post.getAuthor().getId().compareTo(account.getId())==0)
    	holder.distance.setText(null);
    else{
    	double distance = getDistance(post.getLatitud(), post.getLongitud());
    	holder.distance.setText(distance+" "+activity.getResources().getString(R.string.km));
    }
    holder.smis.setText(post.getSmilesCounter()+" "+activity.getResources().getString(R.string.Smiles));
    holder.shas.setText(post.getSharesCounter()+" "+activity.getResources().getString(R.string.Shares));
    holder.post.setText(post.getText());
    holder.date.setText(DateTools.getTimeAgo(activity, post.getCreated()));

    
    if (((SDPublication)post).getFavorite()){
    	holder.addToFavorites.setText(R.string.Remove_Favorite);
    }
    else {
    	holder.addToFavorites.setText(R.string.Add_to_Favorites);
    }
    	
            
	if(post instanceof SDPublicationCheckIn){
		
		final SDPublicationCheckIn P=(SDPublicationCheckIn) post;
		final String url = P.getMapImage().getPictureUrl().trim();           
		picture = (ImageView)convertView.findViewById((postShared!=null)? R.id.image_post_shared : R.id.picture_image);
	    if(picture!=null) {
            imageLoader.displayImage(url, picture, options,new ImageLoadingListener() {

                @Override
                public void onLoadingFailed(String imageUri, View view,
                                            FailReason failReason) {
                    // TODO Auto-generated method stub

                }
                @Override
                public void onLoadingComplete(String imageUri, View view,
                                              Bitmap loadedImage) {
                    if(spinner != null){
                        spinner.setVisibility(View.INVISIBLE);
                    }
                }
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    // TODO Auto-generated method stub

                }
                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    // TODO Auto-generated method stub

                }
            });
			 picture.setOnClickListener(new OnClickListener() {
               public void onClick(View view) {
            	   	Intent i= new Intent(activity, ActivityEnlargeMap.class);
	                i.putExtra("latitude", P.getLatitud());
	                i.putExtra("longitude", P.getLongitud());
	            	activity.startActivity(i);
            }
		});
		}
	        
	}
	else{
		 if(post instanceof SDPublicationImage){
			
			SDPublicationImage P=(SDPublicationImage) post;
			final String url = P.getPicture().getPictureUrl().trim();
			picture = (ImageView)convertView.findViewById((postShared!=null)? R.id.image_post_shared : R.id.picture_image);
			if (picture!=null){
                imageLoader.displayImage(url, picture, options,new ImageLoadingListener() {

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {
                        // TODO Auto-generated method stub

                    }
                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {
                        if(spinner != null){
                            spinner.setVisibility(View.INVISIBLE);
                        }
                    }
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        // TODO Auto-generated method stub

                    }
                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        // TODO Auto-generated method stub

                    }
                });
				final View  thumbPic = (ImageView)convertView.findViewById( (postShared!=null)? R.id.image_post_shared : R.id.picture_image);
			    thumbPic.setOnClickListener(new OnClickListener() {
	            public void onClick(View view) {
	            	
	            	SDPublication postt = mData.get(pos);
	            	((DiscoverHoodApp) activity.getApplication()).setCurrentPublication(postt);
	            	Intent i= new Intent(activity, ActivityEnlargeImage.class);
	                i.putExtra("url", url);
	            	activity.startActivity(i);
	           }
			});
			}
			
		 }
	}
	

	if (postShared!=null) {
		if (postShared.getAuthor().getAvatar() != null ){
			avatar = (ImageView)convertView.findViewById(R.id.post_user_image);
			String url = postShared.getAuthor().getAvatar().getPictureUrl().trim();
            imageLoader.displayImage(url, avatar, options);
		}
		holder.user_name.setText(postShared.getAuthor().getName());
        holder.date.setText(DateTools.getTimeAgo(activity, postShared.getCreated()));
	}else{
		if (((SDPublication) post).getAuthor().getAvatar() != null ){
			avatar = (ImageView)convertView.findViewById(R.id.post_user_image);
			String url = ((SDPublication) post).getAuthor().getAvatar().getPictureUrl().trim();
            imageLoader.displayImage(url, avatar, options);
		}
	}
	
    			
	return convertView;  
    }

    public static class ViewHolder {
        public TableRow trComment;
		public TableRow trShare;
		public TableRow trSmile;
		public TextView user_name;
        public ImageView user_pic;
        public TextView date;
        public TextView distance;
        public TextView post;
        public TextView smi;
        public TextView smis;
        public TextView com;
        public TextView coms;
        public TextView sha;
        public TextView shas;
        public ImageView pic;
        public TextView addToFavorites;   
        public TextView otherUname;
    }
    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }


    @Override
    public Object getGroup(int groupPosition) {
    	Calendar fg = (Calendar) _listDataHeader.get(groupPosition);
    	return fg;


    	//String fechag = String.valueOf(fg.get(Calendar.YEAR));
    	//fechag = String.valueOf(fg.get(Calendar.MONTH))+ " / " +fechag ;
        //return fechag;
    }
    public  Object getposgroup(int Pos){
    	return _listDataHeader.get(Pos);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
    	Calendar fechapost =(Calendar)getGroup(groupPosition);

    	SimpleDateFormat dateFormat = new SimpleDateFormat("MM / yyyy", Locale.US);
    	String headerTitle = dateFormat.format(fechapost.getTime());
    	
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_bar_favorites_group_month_year, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
       // lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        
        if (groupPosition % 2 ==0) {
        	lblListHeader.setBackgroundColor(Color.parseColor("#a0a2a4"));
        	lblListHeader.setTextColor(Color.parseColor("#524e57"));
        }
        else {
        	lblListHeader.setBackgroundColor(Color.parseColor("#524e57"));        	
        	lblListHeader.setTextColor(Color.parseColor("#a0a2a4"));
        }
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
    public void Get_more_publication(){

    }
    /*
     * agrego mas publicaciones al final 
    */
    public void addMoreItemsFinal(int groupPosition, List<SDPublication> nuevasPublicaciones){
        	
        List<SDPublication> publicacionesEnUnMes= _listDataChild.get(getGroup(groupPosition));
    	Calendar fechapost =(Calendar)getGroup(groupPosition);
    	String fechag = String.valueOf(fechapost.get(Calendar.YEAR));
        String mesAnio = String.valueOf(fechapost.get(Calendar.MONTH))+ " / " +fechag ;
        if (publicacionesEnUnMes.isEmpty()) {
       //     nuevasPublicaciones = consultapublicaciones.getFirstpublications(mesAnio);
            publicacionesEnUnMes.addAll(nuevasPublicaciones);
        }
        else {
       //     nuevasPublicaciones = consultapublicaciones.getNextPublications(mesAnio,mesAnio);
            publicacionesEnUnMes.addAll(nuevasPublicaciones);
        }
    }
    
    
    public void addListaItemFinal(int groupPosition, List<SDPublication> nuevasPublicaciones){
    	
        List<SDPublication> publicacionesEnUnMes= _listDataChild.get(getGroup(groupPosition));
    	Calendar fechapost =(Calendar)getGroup(groupPosition);
    	String fechag = String.valueOf(fechapost.get(Calendar.YEAR));
        String mesAnio = String.valueOf(fechapost.get(Calendar.MONTH))+ " / " +fechag ;
        if (publicacionesEnUnMes.isEmpty()) {
 //           nuevasPublicaciones = consultapublicaciones.getFirstpublications(mesAnio);
        	
            publicacionesEnUnMes.addAll(nuevasPublicaciones);
        }
        else {
           // nuevasPublicaciones = consultapublicaciones.getNextPublications(mesAnio,mesAnio);
            publicacionesEnUnMes.addAll(nuevasPublicaciones);
        }
    }
    public void replaceitems(int groupPosition,List<SDPublication> Publicaciones){
    	List<SDPublication> pubmes = _listDataChild.get(getGroup(groupPosition));

    	pubmes.clear();
    	pubmes.addAll(Publicaciones);
		notifyDataSetChanged();
    }
    /*
     * agrego mas publicaciones al principio 
    */
    public void addMoreItemsPrincipio(String mesAnio){
        List<SDPublication> publicacionesEnUnMes= _listDataChild.get(mesAnio);
    }
    
    public boolean isEmptyGroup(int groupPosition) {
    	return (_listDataChild.get((Calendar)getGroup(groupPosition)).isEmpty());
    }


	 /**
	  * Agrega a favorito una publicacion
	  * 
	  * @param pub
	  * 	Publicacion a agregar a favoritos
	  */
	 private void addPublicationToFavorites(SDPublication pub){
		 dialogs.showLoadingProgressDialog(activity.getResources().getString(R.string.addingtofavoritos));
		 new SDFavoritesPOSTAdd(SD_Url, SD_Key, pub.getId(), new SDConnectionResponseListener(){
				@Override
				public void onSuccess(String response, Object resultElement) {
					dialogs.dismissProgressDialog();
				}

				@Override
				public void onIssues(String issues) {
					dialogs.dismissProgressDialog();
					dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error) + issues);
				}

				@Override
				public void onError(SDError error) {
					dialogs.dismissProgressDialog();
					 if (error.getReason() != null ){
						 if (error.getDetails() == null){
							 dialogs.ShowErrorDialog(error.getReason());
						 }else{
							 Enumeration<String> element = error.getDetails().elements();
							 while(element.hasMoreElements()){
								 dialogs.ShowErrorDialog(element.nextElement());
							 }
						 }
					 }else{
						 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
					 }
				}	 	 
			 });  
	 }
	 
	 

	 /**
	  * Quita de favoritos una publicacion dada
	  * 
	  * @param pub
	  * 	Publicacion que sera retirada de favoritos
	  */
	 private void removePublicationToFavorites(SDPublication pub){
		 dialogs.showLoadingProgressDialog(activity.getResources().getString(R.string.removingtofavoritos));
		 new SDFavoritesPOSTRemove(SD_Url, SD_Key, pub.getId(), new SDConnectionResponseListener(){
			@Override
			public void onSuccess(String response, Object resultElement) {
				
				Toast.makeText(activity, activity.getResources().getString(R.string.publication_removed_from_favorites), Toast.LENGTH_LONG).show();
				
				dialogs.dismissProgressDialog();
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error) + issues);
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
				 }
			}	 	 
		 });  
	 }	 
	
	 
	    public void ShowErrorDialog( String msg ){   	
	    		final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
	    		alertDialog.setTitle("ERROR");    	   		
	    		alertDialog.setMessage(msg);
	    		alertDialog.show();
	    }
	    
	    public void showLoadingProgressDialog() {
			this.proD = ProgressDialog.show(activity, "", activity.getResources().getString(R.string.retrieving_publications), true, false);
		}
		
		public void dismissProgressDialog(){
			if (this.proD != null ) {
				this.proD.dismiss();
			}
		}
	 
	 
	 
	 
	 
	 private void gotoUserProfile(String userID, SDAuthor aut){
	    	
	    	((DiscoverHoodApp) activity.getApplication()).ClearOtherUserPublications();
	    	FragmentOtherUserProfile fragment = new FragmentOtherUserProfile();
	    	fragment.setuserData(aut);
		 	Bundle args= new Bundle();
		    args.putString("userID", userID);
		  
	        fragment.setArguments(args);	
	 	    ((ActivityMenu)activity).switchContent(fragment);
	    }
	 

	 private void sharePost(SDPublication publi, final int position){
		 
		 final SDPublication pu = publi;
						 
		  new SDPublicationPOSTshare(SD_Url,  SD_Key, pu.getId(),  latitude, longitude, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
                pu.increaseShares();
                pu.setIshared(true);
                Toast.makeText(activity, activity.getResources().getString(R.string.publication_shared), Toast.LENGTH_LONG).show();
                notifyDataSetChanged();
		    }

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
				 }
			}		 
		 }); 
	 }
	 
	 private void getMyLocation(){
	    	
		 latitude = ((DiscoverHoodApp) activity.getApplication()).getLatitude();
		 
		 longitude = ((DiscoverHoodApp) activity.getApplication()).getLongitude();
		 
	}
	 

     
	    public double getDistance(double post_lat, double post_lon){
	        
	    	if (post_lat == 0 && post_lon == 0) return 0;
	    	    	
	    	Location locationA = new Location("MyLocation");

	    	locationA.setLatitude(latitude);
	    	
	    	locationA.setLongitude(longitude);

	    	Location locationB = new Location("OtherLocation");

	    	locationB.setLatitude(post_lat);
	    	locationB.setLongitude(post_lon);

	    	double distance = locationA.distanceTo(locationB);
	    	
	    	BigDecimal bd = new BigDecimal(distance/1000);
	        bd = bd.setScale(1, 6);
	                
	        distance = bd.doubleValue();
	        	
	    	return distance;
	    }

	    
	 
	 
	 
	 
	 
	 
}