package com.miokode.discoverhood.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.miokode.discoverhood.ActivityCommentPost;
import com.miokode.discoverhood.ActivityEnlargeImage;
import com.miokode.discoverhood.ActivityEnlargeMap;
import com.miokode.discoverhood.ActivityMenu;
import com.miokode.discoverhood.restful.publication.SDPublicationDeletePOST;
import com.miokode.discoverhood.restful.publication.SDPublicationPOSTtoggleSmile;
import com.miokode.discoverhood.tools.ConnectionDialogs;
import com.miokode.discoverhood.tools.DateTools;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.fragment.FragmentOtherUserProfile;
import com.miokode.discoverhood.fragment.FragmentUserProfile;
import com.miokode.discoverhood.tools.MemoryCache;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.favourites.SDFavoritesPOSTAdd;
import com.miokode.discoverhood.restful.favourites.SDFavoritesPOSTRemove;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationCheckIn;
import com.miokode.discoverhood.restful.publication.SDPublicationImage;
import com.miokode.discoverhood.restful.publication.SDPublicationPOSTshare;
import com.miokode.discoverhood.restful.publication.SDPublicationShared;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Enumeration;

@SuppressLint("NewApi")
public class AdapterLisItemFeed extends android.widget.BaseAdapter {
    private ConnectionDialogs dialogs;
    private String SD_Url;
    private String SD_Key;
    private ArrayList<SDPublication> mData;
    private LayoutInflater mInflater;
    private SDAccount account;
    private final Activity activity;
    private MemoryCache avatarCache;
    private MemoryCache imageCache;
    //    private ImageLoader Iloader;
//    private ImageLoader AvatarLoader;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    DisplayImageOptions options;
    private View cv;
    private double latitude;
    private double longitude;
    private ProgressDialog proD = null;
    private TextView shares;
    ProgressBar spinner = null;

    public AdapterLisItemFeed(Activity context, ArrayList<SDPublication> mData) {
        super();
        this.dialogs = new ConnectionDialogs(context);
        this.SD_Url = ((DiscoverHoodApp) context.getApplication()).getServerUrl();
        this.SD_Key = ((DiscoverHoodApp) context.getApplication()).getServerKey();
        this.account = ((DiscoverHoodApp) context.getApplication()).getUserDataAccount();
        this.activity = context;
        this.mData = mData;
        avatarCache = ((DiscoverHoodApp) context.getApplication()).getAvatarMemoryCache();
        imageCache = ((DiscoverHoodApp) context.getApplication()).getGalleryMemoryCache();
//        AvatarLoader = new ImageLoader(context, 200, avatarCache);
//        Iloader = new ImageLoader(activity, 300, imageCache);
//        getMyLocation();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .diskCacheExtraOptions(480, 800, null)
                .denyCacheImageMultipleSizesInMemory()
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .writeDebugLogs()
                .build();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

        imageLoader.init(config);
    }


    public void addItem(SDPublication item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void addFromTop(SDPublication item, int pos) {
        mData.add(pos, item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public SDPublication getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public String getDistance(double lat1, double long1) {

        double lat2 = latitude;
        double long2 = longitude;

        /*
        double degToRad = Math.PI / 180.0;
        double phi1 = lat1 * degToRad;
        double phi2 = lat2 * degToRad;
        double lam1 = long1 * degToRad;
        double lam2 = long2 * degToRad;


        double distance = (6371.01 * Math.acos(Math.sin(phi1) * Math.sin(phi2) + Math.cos(phi1) * Math.cos(phi2) * Math.cos(lam2 - lam1)));
        */

        float[] res = new float[10];
        Location.distanceBetween(lat1,long1,lat2,long2,res);

        return String.format("%.2f", res[0]/1000);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        getMyLocation();
        final ViewHolder holder;
        ImageView avatar = null;
        final int pos = position;
        SDPublication post = mData.get(position);
        SDPublicationShared postShared = null;

        cv = convertView;
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        holder = new ViewHolder();


        if (post instanceof SDPublicationShared) {
            convertView = mInflater.inflate(R.layout.item_post_shared, null);

            ImageView pic = (ImageView) convertView.findViewById(R.id.image_post_shared);

            postShared = (SDPublicationShared) post;
            post = postShared.getSharedPublication();
            holder.otherUname = (TextView) convertView.findViewById(R.id.other_name);
            holder.otherUname.setText(postShared.getSharedPublication().getAuthor().getName());
            holder.otherUname.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    SDPublicationShared p = (SDPublicationShared) mData.get(pos);
                    if (p.getSharedPublication().getAuthor().getId().compareTo(account.getId()) == 0) {
                        Fragment fragment = new FragmentUserProfile();
                        ((ActivityMenu) activity).switchContent(fragment);

                    } else
                        gotoUserProfile(p.getSharedPublication().getAuthor().getId(), p.getSharedPublication().getAuthor());
                }
            });

            if (!(post instanceof SDPublicationImage) && !(post instanceof SDPublicationCheckIn)) {
                pic.setVisibility(View.GONE);
                spinner = (ProgressBar) convertView.findViewById(R.id.progressBarShared);
                spinner.setVisibility(View.INVISIBLE);

            } else {
                spinner = (ProgressBar) convertView.findViewById(R.id.progressBarShared);
                spinner.setVisibility(View.INVISIBLE);
//       				   Iloader.setSpinner(spinner);
            }
        } else {

            if (post instanceof SDPublicationImage || post instanceof SDPublicationCheckIn) {
                convertView = mInflater.inflate(R.layout.item_feed_post_im, null);
                spinner = (ProgressBar) convertView.findViewById(R.id.progressBarPost);
                spinner.setVisibility(View.INVISIBLE);
//    				Iloader.setSpinner(spinner);

            } else if (post.getTo() != null) {
                convertView = mInflater.inflate(R.layout.item_post_to_user, null);

                holder.otherUname = (TextView) convertView.findViewById(R.id.other_name);
                holder.otherUname.setText(post.getTo().getName());

                holder.otherUname.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SDPublication postt = mData.get(pos);
                        gotoUserProfile(postt.getTo().getId(), postt.getAuthor());

                    }

                });

            } else {
                convertView = mInflater.inflate(R.layout.item_feed_post, null);
            }
        }
            
            /*seteo utlima informacion de la publicacion*/
        if (((DiscoverHoodApp) activity.getApplication()).isTogglePublicationsSmiled(post.getId())) {
            if (post.getIsmile()) {
                post.setIsmile(false);
                post.decreaseSmiles();
            } else {
                post.setIsmile(true);
                post.increaseSmiles();
            }
        }


        holder.user_name = (TextView) convertView.findViewById(R.id.profile_name);
        holder.date = (TextView) convertView.findViewById(R.id.post_time);
        holder.distance = (TextView) convertView.findViewById(R.id.post_distance);
        holder.post = (TextView) convertView.findViewById(R.id.user_post);
        holder.smi = (TextView) convertView.findViewById(R.id.smile);
        holder.smis = (TextView) convertView.findViewById(R.id.smiles);
        holder.com = (TextView) convertView.findViewById(R.id.comment);
        holder.sha = (TextView) convertView.findViewById(R.id.share);
        holder.shas = (TextView) convertView.findViewById(R.id.shares);
        holder.addToFavorites = (TextView) convertView.findViewById(R.id.addTOfavorites);
        holder.trSmile = (TableRow) convertView.findViewById(R.id.tableRow_smile);
        holder.trShare = (TableRow) convertView.findViewById(R.id.tableRow_share);
        holder.trComment = (TableRow) convertView.findViewById(R.id.tableRow_comment);
        avatar = (ImageView) convertView.findViewById(R.id.post_user_image);


        avatar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SDPublication postt = mData.get(pos);
                if (postt.getAuthor().getId().compareTo(account.getId()) == 0) {
                    Fragment fragment = new FragmentUserProfile();
                    ((ActivityMenu) activity).switchContent(fragment);
                } else
                    gotoUserProfile(postt.getAuthor().getId(), postt.getAuthor());
            }

        });


        final boolean isRealAuthor = post.getAuthor().getId().compareTo(account.getId()) == 0;
        if (isRealAuthor) {
            holder.sha.setText(null);
            final ImageView isha = (ImageView) convertView.findViewById(R.id.i_share);
            isha.setImageDrawable(null);
        }
        final boolean isAuthor = isRealAuthor || (postShared!=null && postShared.getAuthor().getId().equals(account.getId()));
        if(isAuthor){
            //se configura el swipe para eliminar la publicación
            ((SwipeLayout)convertView.findViewById(R.id.swipeLayout)).setShowMode(SwipeLayout.ShowMode.PullOut);
            final SDPublication finalPost = postShared!=null? postShared : post;
            convertView.findViewById(R.id.bt_delete_post).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    eliminarPublicacion(finalPost);
                }
            });
            convertView.findViewById(R.id.layDeletePost).setVisibility(View.VISIBLE);
        } else {
            convertView.findViewById(R.id.layDeletePost).setVisibility(View.GONE);
            holder.trShare.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    SDPublication postt = mData.get(pos);
                    shares = (TextView) cv.findViewById(R.id.shares);

                    if (postt instanceof SDPublicationShared) {

                        shares.setText(((SDPublicationShared) postt).getSharedPublication().getSharesCounter() + " " + activity.getResources().getString(R.string.Shares));
                        sharePost(((SDPublicationShared) postt).getSharedPublication(), pos);
                    } else {
                        shares.setText(postt.getSharesCounter() + " " + activity.getResources().getString(R.string.Shares));
                        sharePost(postt, pos);
                    }

                }
            });
        }


        holder.user_name.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                SDPublication postt = mData.get(pos);

                if (postt.getAuthor().getId().compareTo(account.getId()) == 0) {
                    Fragment fragment = new FragmentUserProfile();
                    ((ActivityMenu) activity).switchContent(fragment);

                } else
                    gotoUserProfile(postt.getAuthor().getId(), postt.getAuthor());
            }

        });


        final TextView Ssmiles = (TextView) convertView.findViewById(R.id.smiles);
        final TextView Ssmile = (TextView) convertView.findViewById(R.id.smile);
        holder.trSmile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                SDPublication postti = mData.get(pos);
                SDPublicationPOSTtoggleSmile.smileClicked(postti,holder.trSmile, Ssmiles, Ssmile, (DiscoverHoodApp) activity.getApplication(),
                        new Runnable(){
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });

            }

        });


        TextView tv = (TextView) convertView.findViewById(R.id.comments);

        tv.setText(post.getCommentsCounter() + " " + activity.getResources().getString(R.string.Comments));

        ((DiscoverHoodApp) activity.getApplicationContext()).setIV(tv);

        TextView ctv = (TextView) convertView.findViewById(R.id.comment);

        ((DiscoverHoodApp) activity.getApplicationContext()).setTextViewColorC(ctv);

        holder.trComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SDPublication postt = mData.get(pos);
                SDPublicationShared Sp;
                if (postt instanceof SDPublicationShared) {
                    Sp = (SDPublicationShared) postt;
                    postt = Sp.getSharedPublication();
                    //((DiscoverHoodApp) activity.getApplicationContext()).setCurrentPublication(postt);
                }
                ((DiscoverHoodApp) activity.getApplicationContext()).setCurrentPublication(postt);

                Intent inte = new Intent(activity, ActivityCommentPost.class);
                inte.putExtra("publicationID", postt.getId());
                activity.startActivity(inte);
                activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                notifyDataSetChanged();
            }
        });

        if (post.iComment()) {
            TextView Scomments = (TextView) convertView.findViewById(R.id.comment);
            Scomments.setTextColor(Color.MAGENTA);
        }

            
            /*Seteo de colores*/
        if (post.isIshared()) {
            holder.sha.setTextColor(Color.MAGENTA);
        } else {
            holder.sha.setTextColor(Color.WHITE);
        }

        if (post.getIsmile()) {
            holder.smi.setTextColor(Color.MAGENTA);
        } else {
            holder.smi.setTextColor(Color.WHITE);
        }


        holder.addToFavorites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                SDPublication pub = mData.get(pos);
                TextView AddToFavorites = (TextView) v.findViewById(R.id.addTOfavorites);
                if (activity.getString(R.string.Add_to_Favorites) == AddToFavorites.getText()) {
                    AddToFavorites.setText(R.string.Remove_Favorite);
                    addPublicationToFavorites(pub);

                } else {
                    AddToFavorites.setText(R.string.Add_to_Favorites);
                    removePublicationToFavorites(pub);

                }

                if (((SDPublication) mData.get(pos)).getFavorite()) {

                } else {
                }

            }
        });


        holder.user_name.setText(post.getAuthor().getName());
        SDPublication temPubl = (postShared != null) ? postShared : post;


        if (temPubl.getAuthor().getId().compareTo(account.getId()) == 0)
            holder.distance.setVisibility(View.INVISIBLE);
        else {
            String distance = getDistance(temPubl.getLatitud(), temPubl.getLongitud());
            holder.distance.setText(distance + " " + activity.getResources().getString(R.string.km));
        }

        holder.smis.setText(post.getSmilesCounter() + " " + activity.getResources().getString(R.string.Smiles));
        holder.shas.setText(post.getSharesCounter() + " " + activity.getResources().getString(R.string.Shares));
        holder.post.setText(post.getText());
        holder.date.setText(DateTools.getTimeAgo(activity, post.getCreated()));


        if (((SDPublication) post).getFavorite()) {
            holder.addToFavorites.setText(R.string.Remove_Favorite);
        } else {
            holder.addToFavorites.setText(R.string.Add_to_Favorites);
        }


        if (post instanceof SDPublicationCheckIn) {

            final SDPublicationCheckIn P = (SDPublicationCheckIn) post;
            final String url = P.getMapImage().getPictureThumbnailUrl().trim();
            holder.picture = (SimpleDraweeView) convertView.findViewById((postShared != null) ? R.id.image_post_shared : R.id.picture_image);
            if (holder.picture != null) {
                if (spinner != null) {
                    spinner.setVisibility(View.VISIBLE);
                }

                holder.picture.setScaleType(ImageView.ScaleType.CENTER_CROP);
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setControllerListener(new BaseControllerListener<ImageInfo>(){
                            @Override
                            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                                super.onFinalImageSet(id, imageInfo, animatable);
                                if (spinner != null) {
                                    spinner.setVisibility(View.INVISIBLE);
                                }
                            }
                        })
                        .setUri(Uri.parse(url)).build();
                holder.picture.setController(controller);

                /*
                imageLoader.loadImage(url, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        holder.picture.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        holder.picture.setImageBitmap(loadedImage);
                        if (spinner != null) {
                            spinner.setVisibility(View.INVISIBLE);
                        }
                        notifyDataSetChanged();
                    }
                });
                */

                holder.picture.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        Intent i = new Intent(activity, ActivityEnlargeMap.class);
                        i.putExtra("latitude", P.getLatitud());
                        i.putExtra("longitude", P.getLongitud());
                        activity.startActivity(i);
                    }
                });
            }

        }
        if (post instanceof SDPublicationImage) {

                SDPublicationImage P = (SDPublicationImage) post;
                final String url = P.getPicture().getPictureThumbnailUrl().trim();
                final String urlBig = P.getPicture().getPictureUrl().trim();
                holder.picture = (SimpleDraweeView) convertView.findViewById((postShared != null) ? R.id.image_post_shared : R.id.picture_image);
                if (holder.picture != null) {
                    if (spinner != null) {
                        spinner.setVisibility(View.VISIBLE);
                    }
                    holder.picture.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setControllerListener(new BaseControllerListener<ImageInfo>(){
                                @Override
                                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                                    super.onFinalImageSet(id, imageInfo, animatable);
                                    if (spinner != null) {
                                        spinner.setVisibility(View.INVISIBLE);
                                    }
                                }
                            })
                            .setUri(Uri.parse(url)).build();
                    holder.picture.setController(controller);
                    /*
                    imageLoader.loadImage(url, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                            holder.picture.setImageBitmap(loadedImage);
                            if (spinner != null) {
                                spinner.setVisibility(View.INVISIBLE);
                            }
                            notifyDataSetChanged();
                        }
                    });
                    */
                    final View thumbPic = convertView.findViewById((postShared != null) ? R.id.image_post_shared : R.id.picture_image);
                    thumbPic.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {

                            SDPublication postt = mData.get(pos);
                            ((DiscoverHoodApp) activity.getApplication()).setCurrentPublication(postt);
                            Intent i = new Intent(activity, ActivityEnlargeImage.class);
                            i.putExtra("url", urlBig);
                            activity.startActivity(i);
                        }
                    });
                }

            }



        if (postShared != null) {
            if (postShared.getAuthor().getAvatar() != null) {
                avatar = (ImageView) convertView.findViewById(R.id.post_user_image);
                String url = postShared.getAuthor().getAvatar().getPictureUrl().trim();
                imageLoader.displayImage(url, avatar, options);
            }
            holder.user_name.setText(postShared.getAuthor().getName());
            holder.date.setText(DateTools.getTimeAgo(activity, postShared.getCreated()));
        } else {
            if (((SDPublication) post).getAuthor().getAvatar() != null) {
                avatar = (ImageView) convertView.findViewById(R.id.post_user_image);
                String url = ((SDPublication) post).getAuthor().getAvatar().getPictureUrl().trim();
                imageLoader.displayImage(url, avatar, options);
            }
        }


        return convertView;
    }

    private void eliminarPublicacion(final SDPublication post) {
        String server = ((DiscoverHoodApp) activity.getApplication()).getServerUrl();
        String serverKey = ((DiscoverHoodApp) activity.getApplication()).getServerKey();
        final ProgressDialog pDialog = ProgressDialog.show(activity,null,activity.getString(R.string.eliminando),true,false);
        new SDPublicationDeletePOST(server, serverKey, post.getId(), new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {
                mData.remove(post);
                notifyDataSetChanged();
                pDialog.dismiss();
            }

            @Override
            public void onIssues(String issues) {
                pDialog.dismiss();
                Toast.makeText(activity,R.string.error_servidor_no_disponible,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(SDError error) {
                pDialog.dismiss();
                Toast.makeText(activity,R.string.error_servidor_no_disponible,Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Cambia la pantalla al perfil del usuario indicado
     *
     * @param userID
     */
    private void gotoUserProfile(String userID, SDAuthor aut) {

        ((DiscoverHoodApp) activity.getApplication()).ClearOtherUserPublications();
        FragmentOtherUserProfile fragment = new FragmentOtherUserProfile();
        fragment.setuserData(aut);
        Bundle args = new Bundle();
        args.putString("userID", userID);

        fragment.setArguments(args);
        ((ActivityMenu) activity).switchContent(fragment);
    }


    public static class ViewHolder {
        public TableRow trComment;
        public TableRow trShare;
        public TableRow trSmile;
        public TextView user_name;
        public ImageView user_pic;
        public SimpleDraweeView picture;
        public TextView date;
        public TextView distance;
        public TextView post;
        public TextView smi;
        public TextView smis;
        public TextView com;
        public TextView coms;
        public TextView sha;
        public TextView shas;
        public ImageView pic;
        public TextView addToFavorites;
        public TextView otherUname;
    }


    private void addPublicationToFavorites(SDPublication pub) {
        dialogs.showLoadingProgressDialog(activity.getResources().getString(R.string.addingtofavoritos));
        new SDFavoritesPOSTAdd(SD_Url, SD_Key, pub.getId(), new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {

                Toast toast = Toast.makeText(activity, activity.getResources().getString(R.string.publication_added_to_favorites), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                dialogs.dismissProgressDialog();
            }

            @Override
            public void onIssues(String issues) {
                dialogs.dismissProgressDialog();
                dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error) + issues);
            }

            @Override
            public void onError(SDError error) {
                dialogs.dismissProgressDialog();
                if (error.getReason() != null) {
                    if (error.getDetails() == null) {
                        dialogs.ShowErrorDialog(activity.getResources().getString(R.string.alreadyisfavorite));
                    } else {
                        Enumeration<String> element = error.getDetails().elements();
                        while (element.hasMoreElements()) {
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                } else {
                    dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
                }
            }
        });
    }


    private void sharePost(SDPublication publi, final int position) {

        final SDPublication pu = publi;

        new SDPublicationPOSTshare(SD_Url, SD_Key, pu.getId(), latitude, longitude, new SDConnectionResponseListener() {

            @Override
            public void onSuccess(String response, Object resultElement) {
                pu.increaseShares();
                pu.setIshared(true);
                Toast toast = Toast.makeText(activity, activity.getResources().getString(R.string.publication_shared), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                notifyDataSetChanged();
            }

            @Override
            public void onIssues(String issues) {
                dialogs.dismissProgressDialog();
                dialogs.ShowErrorDialog(issues);
            }

            @Override
            public void onError(SDError error) {
                dialogs.dismissProgressDialog();
                if (error.getReason() != null) {
                    if (error.getDetails() == null) {
                        dialogs.ShowErrorDialog(activity.getResources().getString(R.string.notShareAgain));
                    } else {
                        Enumeration<String> element = error.getDetails().elements();
                        while (element.hasMoreElements()) {
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                } else {
                    dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
                }
            }
        });
    }


    /**
     * Quita de favoritos una publicacion dada
     *
     * @param pub Publicacion que sera retirada de favoritos
     */
    private void removePublicationToFavorites(SDPublication pub) {
        dialogs.showLoadingProgressDialog(activity.getResources().getString(R.string.removingtofavoritos));
        new SDFavoritesPOSTRemove(SD_Url, SD_Key, pub.getId(), new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {


                Toast toast = Toast.makeText(activity, activity.getResources().getString(R.string.publication_removed_from_favorites), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                dialogs.dismissProgressDialog();
            }

            @Override
            public void onIssues(String issues) {
                dialogs.dismissProgressDialog();
                dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error) + issues);
            }

            @Override
            public void onError(SDError error) {
                dialogs.dismissProgressDialog();
                if (error.getReason() != null) {
                    if (error.getDetails() == null) {
                        dialogs.ShowErrorDialog(activity.getResources().getString(R.string.isnotfavorite));
                    } else {
                        Enumeration<String> element = error.getDetails().elements();
                        while (element.hasMoreElements()) {
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                } else {
                    dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
                }
            }
        });
    }


    public void ShowErrorDialog(String msg) {
        final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setTitle("ERROR");
        alertDialog.setMessage(msg);
        alertDialog.show();
    }

    public void showLoadingProgressDialog() {
        this.proD = ProgressDialog.show(activity, "", activity.getResources().getString(R.string.retrieving_publications), true, false);
    }

    public void dismissProgressDialog() {
        if (this.proD != null) {
            this.proD.dismiss();
        }
    }


    public void getMyLocation() {
        if (latitude == 0 && longitude == 0) {
            latitude = ((DiscoverHoodApp) activity.getApplication()).getLatitude();

            longitude = ((DiscoverHoodApp) activity.getApplication()).getLongitude();
        }
    }

}
