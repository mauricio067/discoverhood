package com.miokode.discoverhood.adapter;

import java.util.List;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miokode.discoverhood.ActivityShowPublication;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.restful.SDPushService;
import com.miokode.discoverhood.tools.MemoryCache;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.tools.NotificationsManager;
import com.miokode.discoverhood.tools.consult_friend;
import com.miokode.discoverhood.restful.publication.SDPublicationNotification;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class AdapterListDialogRecentActivity extends BaseAdapter{
	 
    private Context context;
    private List<SDPublicationNotification> items;
    private consult_friend consulta_amigos;
    private MemoryCache avatarCache;

    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private DisplayImageOptions options;
    private TextView noPending;

 
    public AdapterListDialogRecentActivity(Context context, List<SDPublicationNotification> items) {
        this.context = context;
        this.items = items;
        consulta_amigos =  (consult_friend) new consult_friend();
    	avatarCache = ((DiscoverHoodApp)context.getApplicationContext()).getAvatarMemoryCache();
        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(context);
        Bitmap imagenmostrar;
        imagenmostrar = BitmapFactory.decodeFile("@drawable/photo_icon");

    }
    public void setnoPendingRequest(TextView nopending){
    	noPending = nopending;
    	if (this.noPending == null){
    	}
    	else {
    	}
    	
    }
    @Override
    public int getCount() {
        return this.items.size();
    }
 
    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (items.size()==0){
        	noPending.setVisibility(View.VISIBLE);        	
        }
        else {
        	
        	noPending.setVisibility(View.GONE);
        	
        }
        // Create a new view into the list.
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_recent_activity, parent, false);
 
        // Set data into the view.
        ImageView friendImage= (ImageView) rowView.findViewById(R.id.imagefriend);
        TextView user = (TextView) rowView.findViewById(R.id.name);        
        final SDPublicationNotification publ = (SDPublicationNotification) this.items.get(position);
        

		switch(publ.getAction()){
		case SMILE: {
	        user.setText(Html.fromHtml("<font color=\"#652d90\">" + publ.getActioner().getName() + 
	                "</font> <font color=\"#515150\">"+ context.getString(R.string.new_smiles)+ "</font>"));			
		}
		break;
		
		case COMMENT: {
	        user.setText(Html.fromHtml("<font color=\"#652d90\">" + publ.getActioner().getName() + 
	                "</font> <font color=\"#515150\">"+ context.getString(R.string.new_comments)+ "</font>"));
		}
	    break;
		
		case SHARED:{
	        user.setText(Html.fromHtml("<font color=\"#652d90\">" + publ.getActioner().getName() + 
	                "</font> <font color=\"#515150\">"+ context.getString(R.string.new_shares)+ "</font>"));
		}		
		case UNSMILE:{
	        user.setText(Html.fromHtml("<font color=\"#652d90\">" + publ.getActioner().getName() + 
	                "</font> <font color=\"#515150\">"+ context.getString(R.string.new_unsmile)+ "</font>"));
		}
		break;
		
		default:
		break;
		}

        
        if (publ.getActioner().getAvatar() != null ){
			String url = publ.getActioner().getAvatar().getPictureUrl().trim();
            imageLoader.displayImage(url, friendImage, options);
		}


        final LinearLayout mainLayout = (LinearLayout) rowView.findViewById(R.id.recentactivitysee);
        if (!publ.isReaded()) {
        	//Log.d("recent", String.valueOf(publ.isReaded()));
            mainLayout.setBackgroundColor(context.getResources().getColor(R.color.notificationnew));
        	
        }        
        
        rowView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String postid = publ.getId();
                if(!publ.isReaded()) {
                    setReaded(mainLayout, postid);
                    publ.setRead();
                }
            	Intent i = new Intent(context.getApplicationContext(), ActivityShowPublication.class);
        		i.putExtra("ID", publ.getPublication().getId());
        		context.startActivity(i);
            }
	    });
        
        //Este codigo es para automaticamente marcar como leidas las publicaciones al verlas
    /*    if (!publ.isReaded()){
        	publ.setRead();
	    	CancelNotification(context, publ.getId());
	    	notifyDataSetChanged();
        }
        */
        return rowView;
    }

    private void setReaded(LinearLayout mainLayout,String postid) {
        mainLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
        SDPushService.setReaded((DiscoverHoodApp) context.getApplicationContext(), NotificationsManager.ACTION_SCS, postid);
        final DiscoverHoodApp app = (DiscoverHoodApp) context.getApplicationContext();
        app.setCantpublicationNotifications(app.getCantpublicationNotifications()-1);
        notifyDataSetChanged();
    }

    public static void CancelNotification(Context ctx, String notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId.hashCode());
    }
    
    
    
    
    
	
}