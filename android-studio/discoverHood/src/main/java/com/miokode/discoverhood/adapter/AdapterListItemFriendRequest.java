package com.miokode.discoverhood.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.tools.ConnectionDialogs;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.tools.MemoryCache;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.friends.SDFriend;
import com.miokode.discoverhood.restful.friends.SDFriendPOSTProcess;
import com.miokode.discoverhood.restful.friends.SDFriendPOSTProcess.Action;
import com.miokode.discoverhood.restful.friends.SDFriendRequester;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.Enumeration;
import java.util.List;

public class AdapterListItemFriendRequest extends BaseAdapter {
    private ConnectionDialogs dialogs;
    private String SD_Url;
    private String SD_Key;

    private Activity context;
    private List items;
    private MemoryCache avatarCache;

    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private DisplayImageOptions options;
    private TextView noPending;
    private AdapterListItemFriendNearest friend_list;

    public void setnoPendingRequest(TextView nopending) {
        noPending = nopending;
    }

    public AdapterListItemFriendRequest(Activity context, List items) {
        this.context = context;
        this.items = items;
        SD_Url = ((DiscoverHoodApp) context.getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) context.getApplication()).getServerKey();
        this.dialogs = new ConnectionDialogs(context);
        avatarCache = ((DiscoverHoodApp) context.getApplicationContext()).getAvatarMemoryCache();
        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(context);

    }

//    public AdapterListItemFriendRequest(Context context, ArrayList<SDFriendRequester> items) {
//        this.context = context;
//        this.items = items;
//    }

    public void setlistfriendlist(AdapterListItemFriendNearest adapt) {
        friend_list = adapt;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Create a new view into the list.
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_friend_request, parent, false);
        this.noPending.setVisibility(View.GONE);
        // Set data into the view.
        ImageView friendImage = (ImageView) rowView.findViewById(R.id.imagefriend);
        TextView nameFriend = (TextView) rowView.findViewById(R.id.friend_name);
        TextView distanciaFriend = (TextView) rowView.findViewById(R.id.distanciafriend);

        final SDFriendRequester fri = (SDFriendRequester) this.items.get(position);

        if (fri.getRequester().getAvatar() != null) {
            String url = fri.getRequester().getAvatar().getPictureUrl().trim();
            imageLoader.displayImage(url, friendImage, options);
        }


        // friendImage.setImageBitmap(fri.getImagen());
        // if (fri.getRequester().getAvatar() != null)

        distanciaFriend.setText("0 km away from");

        distanciaFriend.setVisibility(View.GONE);
        //	double distance = getDistance(post.getLatitud(), post.getLongitud());
        //holder.distance.setText(distance+" "+activity.getResources().getString(R.string.km));

        nameFriend.setText(fri.getRequester().getName());

        final int post = position;
        Button acceptRequest = (Button) rowView.findViewById(R.id.acceptfriend);
        acceptRequest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                processRequest(((SDFriendRequester) items.get(post)).getId(), Action.ACCEPT, post, fri);

            }
        });


        Button notnowRequest = (Button) rowView.findViewById(R.id.notnowfriend);
        notnowRequest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                processRequest(((SDFriendRequester) items.get(post)).getId(), Action.REJECT, post, fri);
            }
        });


        return rowView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private void processRequest(String requestID, Action action, int post, final SDFriendRequester fri) {
        dialogs.showLoadingProgressDialog(context.getResources().getString(R.string.Sendinginformation));
        final int posti = post;
        new SDFriendPOSTProcess(SD_Url, SD_Key, requestID, action, new SDConnectionResponseListener() {

            @Override
            public void onSuccess(String response, Object resultElement) {
				items.remove(posti);

                //((DiscoverHoodApp) context.getApplicationContext()).deleteFriendRequest(((SDFriendRequester) items.get(posti)).getId());
                final DiscoverHoodApp app = (DiscoverHoodApp) context.getApplicationContext();
                app.setCantfriendRequestNotifications(app.getCantfriendRequestNotifications()-1);
                notifyDataSetChanged();
                SDFriend amigo = new SDFriend(fri.getId(), fri.getUpdated(), fri.getRequester().getName(), fri.getRequester().getAvatar());

                if (friend_list != null) {
                    friend_list.addfriend(amigo);
                    friend_list.notifyDataSetChanged();
                }
                dialogs.dismissProgressDialog();
            }

            @Override
            public void onIssues(String issues) {
                dialogs.dismissProgressDialog();
                dialogs.ShowErrorDialog(context.getResources().getString(R.string.server_connection_error) + issues);
            }

            @Override
            public void onError(SDError error) {
                dialogs.dismissProgressDialog();
                if (error.getReason() != null) {
                    if (error.getDetails() == null) {
                        dialogs.ShowErrorDialog(error.getReason());
                    } else {
                        Enumeration<String> element = error.getDetails().elements();
                        while (element.hasMoreElements()) {
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                } else {
                    dialogs.ShowErrorDialog(context.getResources().getString(R.string.server_connection_error));
                }
            }
        });

    }

}
