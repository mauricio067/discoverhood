package com.miokode.discoverhood.adapter.items;

import java.text.SimpleDateFormat;
import android.graphics.drawable.Drawable;

public class ItemFeed {
	
	static SimpleDateFormat FORMATTER = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
	private String user_name;
	private CharSequence date;
	private String distance;
	private Drawable image;
	private String post;
	private int smiles;
	private int comments;
	private int shares;
	
	public ItemFeed(String name, CharSequence date, String dist, Drawable drawable, String p, int smi, int com, int sha){
		user_name=name; 
		this.date=date;
		distance=dist;
		image=drawable;
		post=p;
		smiles=0;
		comments=0;
		shares=0;
	}
	
	public String getUserName() {
		return this.user_name;
	}

	public String getDistance() {
		return this.distance;
	}

	public String getPost() {
		return this.post;
	}

	public CharSequence getDate() {
		return date;
	}

	public Drawable getImage(){
		return this.image;
	}

	public int getNroSmiles(){
		return this.smiles;
	}
	
	public int getNroComments(){
		return this.comments;
	}
	
	public int getNroShares(){
		return this.shares;
	}

	public void setUserName(String un) {
		this.user_name = un.trim();
	}

	public void setDistance(String d) {
		this.distance = d.trim();
	}

	public void setPost(String post) {
		this.post = post.trim();
	}

	public void setDate(CharSequence date) {
		this.date = date;
	}
		
	public void setImage(Drawable image){
		this.image = image;
	}
	
	public void setNroSmiles(int s){
		this.smiles = s;
	}
	
	public void setNroComments(int c){
		this.comments = c;
	}
	
	public void setNroShares(int s){
		this.shares = s;
	}
	
	
}