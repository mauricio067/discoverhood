package com.miokode.discoverhood.adapter.items;

public class ItemMenu {
	
	private String title;
	
	public ItemMenu(String title){
		this.title=title;
	}
	
	public String getTitle(){
		return title;
	}
	
	public void setTitle(String title){
		this.title=title;
	}

}
