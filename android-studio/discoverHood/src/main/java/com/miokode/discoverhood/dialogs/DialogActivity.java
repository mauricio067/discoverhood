package com.miokode.discoverhood.dialogs;

import android.app.Activity;

import com.miokode.discoverhood.tools.ConnectionDialogs;
import com.miokode.discoverhood.DiscoverHoodApp;

public class DialogActivity extends Activity {
	protected ConnectionDialogs dialogs;
	protected String SD_Url;
	protected String SD_Key;	
	
	public DialogActivity( ){
		super();
		this.dialogs = new ConnectionDialogs(this);
		SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
	}
}
