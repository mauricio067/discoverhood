package com.miokode.discoverhood.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.miokode.discoverhood.tools.ConnectionDialogs;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;

public class DialogFragment extends SherlockFragment {
	protected ConnectionDialogs dialogs;
	protected String SD_Url;
	protected String SD_Key;
	public DialogFragment( ){
		super();
	}


	
	
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.dialogs = new ConnectionDialogs(this.getActivity());
        this.SD_Url = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerUrl();
        this.SD_Key = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerKey();
	}

    /*PARA EL ACTIONBAR MENU*/
    private LinearLayout contentFrame, contentFrameFriendIcon,contentFrameGeoIcon,contentFrameMessage;
    private ImageView imageFI, imageGI, imageFM;
    private String cf, cffi, cfgi, cfm;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		super.onCreateView(inflater, container, savedInstanceState);
		
    //Va para todos los que tienen actionbar, es para poder seleccionar un estio predefinido al action bar
		
    	
    	final com.actionbarsherlock.app.ActionBar ab = getSherlockActivity().getSupportActionBar();
    	/* no necesito programar el boton de menú */
	    ab.setDisplayShowHomeEnabled(true);
	    ab.setDisplayShowTitleEnabled(false);  
	    View view = inflater.inflate(R.layout.titlebar_actionbar,null);
	    ab.setCustomView(view);
	    ab.setDisplayShowCustomEnabled(true);
    	contentFrame = (LinearLayout) getActivity().findViewById(R.id.contframe);
    	contentFrameFriendIcon = (LinearLayout) getActivity().findViewById(R.id.content_layout_friendicon);
    	contentFrameGeoIcon = (LinearLayout) getActivity().findViewById(R.id.content_layout_geoicon);
    	contentFrameMessage = (LinearLayout) getActivity().findViewById(R.id.content_layout_messageicon);
    	contentFrame.setVisibility(View.VISIBLE);
    	contentFrameFriendIcon.setVisibility(View.INVISIBLE);
    	contentFrameGeoIcon.setVisibility(View.INVISIBLE);
    	contentFrameMessage.setVisibility(View.INVISIBLE);
    	
    	imageFI = (ImageView) getActivity().findViewById(R.id.imageView_fi);
    	imageGI =(ImageView) getActivity().findViewById(R.id.imageView_gi);
    	imageFM =(ImageView) getActivity().findViewById(R.id.imageView_mi);
    	
    	this.cantconversationNotifications = ((DiscoverHoodApp) getSherlockActivity().getApplicationContext()).getCantconversationNotifications();
    	this.cantfriendRequestNotifications  = ((DiscoverHoodApp) getSherlockActivity().getApplicationContext()).getCantfriendRequestNotifications();
    	this.cantpublicationNotifications = ((DiscoverHoodApp) getSherlockActivity().getApplicationContext()).getCantpublicationNotifications();
    	
		/*seteo la imagen inferior*/

    	this.contentFrameFriendIcon.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
        		contentFrame.setVisibility(View.VISIBLE);
        		contentFrameFriendIcon.setVisibility(View.INVISIBLE);	
            	imageFI.setVisibility(View.INVISIBLE);
            	cf = "true";
            	cffi = "false";
	        }
    	});
    	
    	this.contentFrameGeoIcon.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
        		contentFrame.setVisibility(View.VISIBLE);
        		contentFrameGeoIcon.setVisibility(View.INVISIBLE);
        		imageGI.setVisibility(View.INVISIBLE);
            	cf = "true";
            	cfgi = "false";	 
	        }
    		
    	});
    	
    	this.contentFrameMessage.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
        		contentFrame.setVisibility(View.VISIBLE);
        		contentFrameMessage.setVisibility(View.INVISIBLE);
            	imageFM.setVisibility(View.INVISIBLE);
            	cf = "true";
            	cfm = "false";	    
	        }
    	}); 
    	

    	RelativeLayout framegeneral= (RelativeLayout) getActivity().findViewById(R.id.content_frame_general);
    	framegeneral.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
        		contentFrame.setVisibility(View.VISIBLE);
        		
        		contentFrameFriendIcon.setVisibility(View.INVISIBLE);	
            	imageFI.setVisibility(View.INVISIBLE);
        		contentFrameMessage.setVisibility(View.INVISIBLE);
            	imageFM.setVisibility(View.INVISIBLE);
        		contentFrameGeoIcon.setVisibility(View.INVISIBLE);
        		imageGI.setVisibility(View.INVISIBLE);
        		
            	cf = "true";
            	cffi = "false";
            	cfm = "false";	    
            	cfgi = "false";	 

	        }
    		
    	});
    	
    	if (cf == null) {
    	contentFrame.setVisibility(View.VISIBLE);
    	cf = "true";
    	}
    	else {
    		if (cf == "true"){
    	    	contentFrame.setVisibility(View.VISIBLE);
    		}
    		else{
    	    	contentFrame.setVisibility(View.INVISIBLE);
    		}
    	}
    	
    	if (cffi == null) {
    	contentFrameFriendIcon.setVisibility(View.INVISIBLE);
    	cffi = "false";
    	}
    	else {
    		if (cffi == "true"){
    			contentFrameFriendIcon.setVisibility(View.VISIBLE);
    	    	imageFI.setVisibility(View.VISIBLE);
    	    	System.out.println("AHORA SE QUE ENTRA POR ACA!!!!!");

    		}
    		else{
    			contentFrameFriendIcon.setVisibility(View.INVISIBLE);
    		}
    	}
    	if (cfgi == null) {
    	contentFrameGeoIcon.setVisibility(View.INVISIBLE);
    	cfgi = "false";
    	}
    	else {
    		if (cfgi == "true"){
    			contentFrameGeoIcon.setVisibility(View.VISIBLE);
    	    	imageGI.setVisibility(View.VISIBLE);

    		}
    		else{
    			contentFrameGeoIcon.setVisibility(View.INVISIBLE);
    		}
    	}
    	if (cfm == null) {
    	contentFrameMessage.setVisibility(View.INVISIBLE);
    	cfm = "false";
    	}
    	else {
    		if (cfm == "true"){
    			contentFrameMessage.setVisibility(View.VISIBLE);
    	    	imageFM.setVisibility(View.VISIBLE);

    		}
    		else{
    			contentFrameMessage.setVisibility(View.INVISIBLE);
    		}
    	}
    	
    	
    	//Aca defino todos los onclick de la barra de menus.   

	    ImageView friendIcon = (ImageView) view.findViewById(R.id.friends_icon);

	    friendIcon.setOnClickListener(new View.OnClickListener() {
	        @SuppressWarnings("null")
			@Override
	        public void onClick(View view) {
	        	if (contentFrameFriendIcon.getVisibility() == View.VISIBLE){
	        		contentFrame.setVisibility(View.VISIBLE);
	        		contentFrameFriendIcon.setVisibility(View.INVISIBLE);	
	            	imageFI.setVisibility(View.INVISIBLE);
	            	cf = "true";
	            	cffi = "false";
	            }
	        	else {
	        	//	contentFrame.setVisibility(View.INVISIBLE);
	        		contentFrameFriendIcon.setVisibility(View.VISIBLE);
	        		contentFrameGeoIcon.setVisibility(View.INVISIBLE);
	        		contentFrameMessage.setVisibility(View.INVISIBLE);
	            	imageFI.setVisibility(View.VISIBLE);
	            	imageGI.setVisibility(View.INVISIBLE);
	            	imageFM.setVisibility(View.INVISIBLE);
	            	cf = "false";
	            	cffi = "true";
	            	cfgi = "false";
	            	cfm = "false";
	            	
	        	}

	        	
	        }
	    });


	    ImageView mailIcon = (ImageView) view.findViewById(R.id.mail_icon);
	    mailIcon.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
	        	if (contentFrameMessage.getVisibility() == View.VISIBLE){
	        		contentFrame.setVisibility(View.VISIBLE);
	        		contentFrameMessage.setVisibility(View.INVISIBLE);
	            	imageFM.setVisibility(View.INVISIBLE);
	            	cf = "true";
	            	cfm = "false";	            	
	        	}
	        	else {
	        	//	contentFrame.setVisibility(View.INVISIBLE);
	        		contentFrameFriendIcon.setVisibility(View.INVISIBLE);
	        		contentFrameGeoIcon.setVisibility(View.INVISIBLE);
	        		contentFrameMessage.setVisibility(View.VISIBLE);
	            	imageFI.setVisibility(View.INVISIBLE);
	            	imageGI.setVisibility(View.INVISIBLE);
	            	imageFM.setVisibility(View.VISIBLE);
	            	cf = "false";
	            	cffi = "false";
	            	cfgi = "false";
	            	cfm = "true";	            	
	        	}
	            // ...
	        }
	    });
	    // la flor esta en la playa, en la parte de la rambla.

	    ImageView geoIcon = (ImageView) view.findViewById(R.id.geo_icon);
	    geoIcon.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
	            // ...
	        	System.out.println("MUESTRO / OCULTO PUBLICACIONES");
	        	if (contentFrameGeoIcon.getVisibility() == View.VISIBLE){
	        		contentFrame.setVisibility(View.VISIBLE);
	        		contentFrameGeoIcon.setVisibility(View.INVISIBLE);
	        		imageGI.setVisibility(View.INVISIBLE);
	            	cf = "true";
	            	cfgi = "false";	       	        		
	        	}
	        	else {
	        	//	contentFrame.setVisibility(View.INVISIBLE);
	        		contentFrameFriendIcon.setVisibility(View.INVISIBLE);
	        		contentFrameGeoIcon.setVisibility(View.VISIBLE);
	        		contentFrameMessage.setVisibility(View.INVISIBLE);
	            	imageFI.setVisibility(View.INVISIBLE);
	            	imageGI.setVisibility(View.VISIBLE);
	            	imageFM.setVisibility(View.INVISIBLE);
	            	cf = "false";
	            	cffi = "false";
	            	cfgi = "true";
	            	cfm = "false";		            	
	        	}
	        }
	    });
		
		
	    

		((DiscoverHoodApp) this.getActivity().getApplicationContext()).setActionbarActive(this);
		
		  Friend = (LinearLayout)view.findViewById(R.id.friendlayout);
		  Mail = (LinearLayout)view.findViewById(R.id.maillayout);
		  Geo = (LinearLayout)view.findViewById(R.id.geolayout);
		  FriendCant = (TextView)view.findViewById(R.id.friend_icon_cant);
		  MailCant= (TextView)view.findViewById(R.id.mail_icon_cant);
		  GeoCant = (TextView)view.findViewById(R.id.geo_icon_cant);
		
		  setactionbar();
	    
		return super.onCreateView(inflater, container, savedInstanceState);
	}
    
		 private LinearLayout Friend;
		 private LinearLayout Mail;
		 private LinearLayout Geo;
		 private TextView FriendCant;
		 private TextView MailCant;
		 private TextView GeoCant;
		 
		 private int cantfriendRequestNotifications;
		 private int cantpublicationNotifications;
		 private int cantconversationNotifications;    
		 
	    public void setactionbar(){
	    	if (this.getActivity() != null){
		    	this.getActivity().runOnUiThread(new Runnable() {
		            @Override
		            public void run() {
		                // This code will always run on the UI thread, therefore is safe to modify UI elements.
		       		 	if (cantfriendRequestNotifications == 0) {
		       		 		Friend.setVisibility(View.GONE);
		       		 	}else{
		       		 		Friend.setVisibility(View.VISIBLE);
		       		 		FriendCant.setText(String.valueOf(cantfriendRequestNotifications));
		       		 	}
		    		 
		       		 	if (cantconversationNotifications == 0) {
		       		 		Mail.setVisibility(View.GONE);
		       		 	}else{
		       		 		Mail.setVisibility(View.VISIBLE);
		       		 		MailCant.setText(String.valueOf(cantconversationNotifications));
		       		 	}
		    		 
		       		 	if (cantpublicationNotifications == 0) {
		       		 		Geo.setVisibility(View.GONE);
		       		 	}else{
		       		 		Geo.setVisibility(View.VISIBLE);
		       		 		GeoCant.setText(String.valueOf(cantpublicationNotifications));
		       		 	}
		            }
		        });	
	    	}
	    }
	    
	    

		public int getCantfriendRequestNotifications() {
			return cantfriendRequestNotifications;
		}

		public void setCantfriendRequestNotifications(int cantfriendRequestNotifications) {
			this.cantfriendRequestNotifications = cantfriendRequestNotifications;
		}

		public int getCantpublicationNotifications() {
			return cantpublicationNotifications;
		}

		public void setCantpublicationNotifications(int cantpublicationNotifications) {
			this.cantpublicationNotifications = cantpublicationNotifications;
		}

		public int getCantconversationNotifications() {
			return cantconversationNotifications;
		}

		public void setCantconversationNotifications(int cantconversationNotifications) {
			this.cantconversationNotifications = cantconversationNotifications;
		}
		
		
	
	
	
	
	
	
	
	
	
}
	

