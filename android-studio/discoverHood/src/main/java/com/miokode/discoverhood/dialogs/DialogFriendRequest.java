package com.miokode.discoverhood.dialogs;

import java.util.ArrayList;
import java.util.Enumeration;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.adapter.AdapterListDialogFriendRequest;
import com.miokode.discoverhood.adapter.AdapterListItemFriendRequest;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.friends.SDFriendGETRequestsReceived;
import com.miokode.discoverhood.restful.friends.SDFriendNotification;
import com.miokode.discoverhood.restful.friends.SDFriendRequester;
import com.miokode.discoverhood.tools.ConnectionDialogs;

public class DialogFriendRequest extends SherlockFragment {
    private FragmentActivity fa;

	private ListView listView, listViewFriendRequest; 
	private  ArrayList<SDFriendNotification> listNotify;
	private ArrayList<SDFriendRequester> listFriendRequest;
    private View rootView;
    private ConnectionDialogs dialogs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dialogs = new ConnectionDialogs(this.getActivity());
    }

    public DialogFriendRequest() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_friend_request, container, false);
        listNotify =  (ArrayList<SDFriendNotification>)((DiscoverHoodApp) this.getActivity().getApplication()).getFriendRequestNotifications();
        listFriendRequest =  (ArrayList<SDFriendRequester>)((DiscoverHoodApp) this.getActivity().getApplication()).getFriendRequests();
	    fa = super.getActivity();
	    //solicitudes aceptadas AdapterListDialogFriendRequest
	    listView = (ListView) rootView.findViewById(android.R.id.list);
	    //solicitudes pedidas (AdapterListItemFriendRequest )
	    listViewFriendRequest = (ListView) rootView.findViewById(R.id.listfriendrequest);

	    AdapterListDialogFriendRequest adapt = new AdapterListDialogFriendRequest(fa, listNotify);
        this.listView.setAdapter(adapt);
        //((DiscoverHoodApp) this.getActivity().getApplication()).setListDialogFriendRequest(adapt);
        //((DiscoverHoodApp) this.getActivity().getApplication()).setListItemFriendRequest(adapterfr);

        loadFriendRequests();
        return rootView;
    }


    public void loadFriendRequests() {
        rootView.findViewById(R.id.friends_layLoadingRequests).setVisibility(View.VISIBLE);
        final DiscoverHoodApp app = (DiscoverHoodApp) getActivity().getApplication();
        final TextView nopending = (TextView) rootView.findViewById(R.id.nopendingrequests);
        nopending.setVisibility(View.GONE);
        new SDFriendGETRequestsReceived(app.getServerUrl(),app.getServerKey(), new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {
                try {
                    ArrayList<SDFriendRequester> requesters = (ArrayList<SDFriendRequester>) resultElement;
                    if (requesters!=null) {
                        listFriendRequest = requesters;
                        AdapterListItemFriendRequest adapterfr = new AdapterListItemFriendRequest(fa,listFriendRequest);
                        nopending.setVisibility(View.VISIBLE);
                        adapterfr.setnoPendingRequest(nopending);
                        listViewFriendRequest.setAdapter(adapterfr);
                        app.setCantfriendRequestNotifications(listFriendRequest.size());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                rootView.findViewById(R.id.friends_layLoadingRequests).setVisibility(View.GONE);
            }

            @Override
            public void onIssues(String issues) {

            }

            @Override
            public void onError(SDError error) {
                if (error !=null && error.getReason() != null ){
                    if (error.getDetails() == null){
                        dialogs.ShowErrorDialog(error.getReason());
                    }else{
                        Enumeration<String> element = error.getDetails().elements();
                        while(element.hasMoreElements()){
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                }else{
                    dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
                }
                rootView.findViewById(R.id.friends_layLoadingRequests).setVisibility(View.GONE);
            }
        });
    }
	
}
