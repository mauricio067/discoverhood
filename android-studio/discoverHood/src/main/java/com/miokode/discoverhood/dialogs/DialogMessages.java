package com.miokode.discoverhood.dialogs;

import java.util.Enumeration;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.miokode.discoverhood.ActivityMenu;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.fragment.FragmentCreateNewConversation;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.adapter.AdapterListDialogMessages;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.conversation.SDConversationNotification;
import com.miokode.discoverhood.restful.conversation.SDConversationsNotificationsGETAll;
import com.miokode.discoverhood.tools.ConnectionDialogs;

public class DialogMessages extends Fragment{
    private FragmentActivity fa;
    private TextView nopending;
	private ListView listView;
	private  List<SDConversationNotification> messages;
    private View rootView;
    private ConnectionDialogs dialogs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dialogs = new ConnectionDialogs(this.getActivity());
    }

    public DialogMessages() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_mesages, container, false);
	    fa = super.getActivity();
	    
	    nopending = (TextView) rootView.findViewById(R.id.nopendingmessages);
        loadNewMessages();
        
        TextView writeAMessage = (TextView) rootView.findViewById(R.id.write_mesage);
        
        writeAMessage.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
            	FragmentCreateNewConversation mContent = new FragmentCreateNewConversation();
          	   ((ActivityMenu)fa).switchContent(mContent);

			}
        	
        });

        return rootView;
    }

    public void loadNewMessages() {
        final FragmentActivity activity = getActivity();
        if (activity!=null) {
            nopending.setVisibility(View.GONE);
            rootView.findViewById(R.id.layLoadingMessages).setVisibility(View.VISIBLE);
            final DiscoverHoodApp app = (DiscoverHoodApp) activity.getApplication();
            new SDConversationsNotificationsGETAll(app.getServerUrl(), app.getServerKey(), new SDConnectionResponseListener() {
                @Override
                public void onSuccess(String response, Object resultElement) {
                    if(resultElement!=null) {
                        messages = (List<SDConversationNotification>) resultElement;

                        listView = (ListView) rootView.findViewById(android.R.id.list);
                        AdapterListDialogMessages Adapter = new AdapterListDialogMessages(fa, messages);
                        nopending.setVisibility(View.VISIBLE);
                        Adapter.setnopending(nopending);
                        Adapter.setFragment(getFragmentManager());
                        //((DiscoverHoodApp) getActivity().getApplication()).setListDialogMessages(Adapter);
                        listView.setAdapter(Adapter);
                        int cant = 0;
                        for(SDConversationNotification cn : messages){
                            if(!cn.isReaded()) cant++;
                        }
                        app.setCantconversationNotifications(cant);
                    }
                    rootView.findViewById(R.id.layLoadingMessages).setVisibility(View.GONE);
                }

                @Override
                public void onIssues(String issues) {

                }

                @Override
                public void onError(SDError error) {
                    if (error !=null && error.getReason() != null ){
                        if (error.getDetails() == null){
                            dialogs.ShowErrorDialog(error.getReason());
                        }else{
                            Enumeration<String> element = error.getDetails().elements();
                            while(element.hasMoreElements()){
                                dialogs.ShowErrorDialog(element.nextElement());
                            }
                        }
                    }else{
                        dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
                    }
                    rootView.findViewById(R.id.layLoadingMessages).setVisibility(View.GONE);
                }
            });
        }
    }


}
