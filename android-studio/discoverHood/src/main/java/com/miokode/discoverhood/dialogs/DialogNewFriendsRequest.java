package com.miokode.discoverhood.dialogs;


import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.miokode.discoverhood.adapter.items.ItemNewFriendRequest;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.adapter.AdapterLisItemNewFriendRequest;


public class DialogNewFriendsRequest extends Activity{
	
	private AdapterLisItemNewFriendRequest adapter;
		
	private ArrayList<ItemNewFriendRequest> requests=new ArrayList<ItemNewFriendRequest>();
	
	 private LayoutInflater inflater;
	    private SherlockFragmentActivity sfa;
	
	    private ListView list;
	    private Dialog listDialog;
	    
	    
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       // showdialog();
	        setContentView(R.layout.dialog_mesages);
	        
	    }
	 
	    private void showdialog()
	    {
	        listDialog = new Dialog(this);
	        LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        addRequests();   
	        adapter = new AdapterLisItemNewFriendRequest(this, requests);        
	        View v = li.inflate(R.layout.dialog_mesages, null, false);
	        listDialog.setContentView(v);	         
	        listDialog.show();
	    }
	    
	private void addRequests() {
		requests.add(new ItemNewFriendRequest("Lorena Vinueza"));
		requests.add(new ItemNewFriendRequest("Carla Gomez"));		    	
   }
	
}
