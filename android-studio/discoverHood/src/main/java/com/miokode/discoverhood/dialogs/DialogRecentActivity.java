package com.miokode.discoverhood.dialogs;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.adapter.AdapterListDialogRecentActivity;
import com.miokode.discoverhood.gcm.GcmIntentService;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDPushService;
import com.miokode.discoverhood.restful.publication.SDPublicationNotification;
import com.miokode.discoverhood.restful.publication.SDPublicationNotificationGETAll;
import com.miokode.discoverhood.tools.ConnectionDialogs;

public class DialogRecentActivity extends Fragment {

	private ListView listView;
	private  List<SDPublicationNotification> notify = null;
    private FragmentActivity fa;
    private View view;
    private ConnectionDialogs dialogs;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dialogs = new ConnectionDialogs(this.getActivity());
    }

    public DialogRecentActivity() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_recent_activity, container,false);
	    fa = super.getActivity();
	    view.setOnFocusChangeListener(new OnFocusChangeListener() {          
    	    public void onFocusChange(View v, boolean hasFocus) {

    	        if(hasFocus) {

                }
            }
    	});    	

	    listView = (ListView) view.findViewById(android.R.id.list);
        loadRecentActivity();
        //((DiscoverHoodApp) getActivity().getApplicationContext()).setAdapterRecentActivity(adapter);
        ((DiscoverHoodApp) getActivity().getApplication()).setDialogRecentActivity(this);
        return view;
    }

    public void setNotificationsReaded(){
        if (notify!=null) {
            final DiscoverHoodApp app = (DiscoverHoodApp) getActivity().getApplication();
            final ArrayList<String> notifs = new ArrayList<String>();
            for(SDPublicationNotification n : notify){
                if(!n.isReaded())
                    notifs.add(n.getId());
            }
            new SDPushService(app.getServerUrl(), app.getServerKey(), null, notifs, null, null, new SDConnectionResponseListener() {
                @Override public void onSuccess(String response, Object resultElement) { }
                @Override public void onIssues(String issues) { }
                @Override public void onError(SDError error) { } });
            app.setCantpublicationNotifications(0);
            GcmIntentService.clearStaticIds();
        }
    }

    public void loadRecentActivity() {
        final TextView nopending = (TextView)view.findViewById(R.id.noninformation);
        nopending.setVisibility(View.GONE);
        final DiscoverHoodApp app = (DiscoverHoodApp) getActivity().getApplication();
        view.findViewById(R.id.layLoadingNotifications).setVisibility(View.VISIBLE);
        new SDPublicationNotificationGETAll(app.getServerUrl(), app.getServerKey(), new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {
                if(resultElement!=null){

                    notify = (List<SDPublicationNotification>) resultElement;

                    AdapterListDialogRecentActivity adapter = new AdapterListDialogRecentActivity(fa, notify);
                    nopending.setVisibility(View.VISIBLE);
                    adapter.setnoPendingRequest(nopending);
                    listView.setAdapter(adapter);

                    int cant = 0;
                    for(SDPublicationNotification nt : notify){
                        if(!nt.isReaded()) cant++;
                    }
                    app.setCantpublicationNotifications(cant);
                }
                view.findViewById(R.id.layLoadingNotifications).setVisibility(View.GONE);
            }

            @Override
            public void onIssues(String issues) {

            }

            @Override
            public void onError(SDError error) {
                if (error !=null && error.getReason() != null ){
                    if (error.getDetails() == null){
                        dialogs.ShowErrorDialog(error.getReason());
                    }else{
                        Enumeration<String> element = error.getDetails().elements();
                        while(element.hasMoreElements()){
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                }else{
                    dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
                }
                view.findViewById(R.id.layLoadingNotifications).setVisibility(View.GONE);
            }
        });

    }
}
