package com.miokode.discoverhood.fragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.ActivityMenu;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.adapter.AdapterConversationMessages;
import com.miokode.discoverhood.dialogs.DialogSherlockListFragment;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.conversation.SDConversation;
import com.miokode.discoverhood.restful.conversation.SDConversationGETAllMgs;
import com.miokode.discoverhood.restful.conversation.SDConversationGETAllNewMgs;
import com.miokode.discoverhood.restful.conversation.SDConversationGETByID;
import com.miokode.discoverhood.restful.conversation.SDConversationPOSTAddMessage;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDMessage;

import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

public class FragmentConversation extends DialogSherlockListFragment {
   // private ListView listView;
    private PullToRefreshListView listView;
    private String conversationID;
    private FragmentActivity fa;
    private EditText message;
    private ArrayList<SDMessage> Messages;
    private ArrayList<SDMessage> AllMessages;
    private String oldestMessagesID = null;
    private String oldestOtherUserMessageId = "";
    private ArrayList<String> generaloldestMessages;
    private boolean loading = false;
    private SDConversation conversation;
    private AdapterConversationMessages adapter;
    private TextView cancel;
    private Timer myTimer =null;
	private SDAccount account;
	private MyTimerTask myTask = null;
	

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {
    	this.setRetainInstance(true);
    	super.onCreateView(inflater, container, savedInstanceState);
		final View fragmentView = (ViewGroup) inflater.inflate(R.layout.screen_conversation, container, false);
	    fa = super.getActivity();
	    account = ((DiscoverHoodApp)fa.getApplicationContext()).getUserDataAccount();
	    // listView = (ListView) fragmentView.findViewById(R.id.listMessages);
	   if (message == null) {
	    message = (EditText) fragmentView.findViewById(R.id.writeMessage);}
	    ImageView imagen = new ImageView(fa);
	    imagen.setImageResource(R.drawable.photo_icon);
	
	    Messages = new ArrayList<SDMessage>();
	    AllMessages = new ArrayList<SDMessage>();
	    generaloldestMessages = new ArrayList<String>();
	    
	    if (savedInstanceState != null && savedInstanceState.containsKey("conversationid")){
	    	conversationID = savedInstanceState.getString("conversationid");
	    }
	    
	    message.setOnFocusChangeListener(new OnFocusChangeListener() {  
	    	@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus){
					message.setHint("");
				}
				else {
					message.setHint(getResources().getString(R.string.write_here));
				}
				
	        }
		   });
	    
       // buttonsend.setOnClickListener(this);
	   
        final Button  buttonsend = (Button) fragmentView.findViewById(R.id.sendMessage);
	       buttonsend.setOnClickListener(new View.OnClickListener() {
	           public void onClick(View v) {
	        	   message.setText(message.getText().toString().replaceAll("[\n\r]",""));
	        	   
                  if (message.getText().toString().replaceAll(" ", "").length() > 0) {
      				SDMessage msg = new SDMessage(message.getText().toString());
      				msg.setAuthor(account.getId());
      				msg.setCreated(new Date());
      				
      				addNewMessage(msg);
      				

                	    sendMessage(message.getText().toString(), conversationID);
        				message.setText("");


                  }
                  else {
                	  dialogs.ShowErrorDialog(getResources().getString(R.string.youmustaddtext)); //Quizas que no habilite el boton seria suficiente
                  }
	           }
	        });
	       

	    if (conversation != null){
	    	setConversation(conversation);
	    }
	    cancel = (TextView) fragmentView.findViewById(R.id.cancel_message);

	    cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	//fa.finish();
   			 ((ActivityMenu) getActivity()).switchContentWithoutBack(new FragmentListConversation());

            //	fa.onBackPressed();
		
			}
		});
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    myTask = new MyTimerTask();
        myTimer = new Timer();
        myTimer.schedule(myTask, 6000, 6000);

	    
	    
	    
	    
	    
	    
	    return fragmentView;
	}
    
    @Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);		
		listView = (PullToRefreshListView) getListView();

		((PullToRefreshListView)listView).setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
				final FragmentActivity activity = getActivity();
				if (activity!=null) {
					if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
                        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }
                    else {
                        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    }
					getMessages(oldestMessagesID, true);
					listView.postDelayed(new Runnable() {
                      @Override
                       public void run() {
                           listView.onRefreshComplete();
                       }
                    }, 2000);
				}
			}
        });
	    if (adapter == null){
	    adapter = new AdapterConversationMessages(fa, conversation);         		
        this.listView.setAdapter(adapter);
		FragmentActivity activity = getActivity();
		if (activity!=null) {
			if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}
			else {
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}
		}
		getMessages(null, true);
	    }
	    else {
	        this.listView.setAdapter(adapter);

	    }
    
    }
		
	public String getConversationID() {
		return conversationID;
	}

	public void setConversationID(String conversationID) {
		this.conversationID = conversationID;
	}
	
	public void setConversation(SDConversation conversation) {
		this.conversation = conversation;
	}

	 private void addNewMessage(SDMessage msg){
		 adapter.addMessage(msg);
	 }
	 	 
	 private void addMessages(ArrayList<SDMessage> messages) {
			ListIterator<SDMessage> listItr = messages.listIterator();
			adapter.addMessages(messages);

			while(listItr.hasNext()){
				SDMessage msg = listItr.next();
				AllMessages.add(msg);
				oldestMessagesID = msg.getId();
				generaloldestMessages.add(oldestMessagesID);
				
				if (!msg.getAuthor().equals(account.getId()))
					oldestOtherUserMessageId = msg.getId();
			}
			
			adapter.notifyDataSetChanged();
        	listView.post(new Runnable() {
                @Override
                public void run() {
                    // Select the last row so it will scroll into view...
                	listView.setSelection(adapter.getCount() - 1);
                }
            });
	     }


		private void addOldestMessages(ArrayList<SDMessage> messages) {
			ListIterator<SDMessage> listItr = messages.listIterator();
			while(listItr.hasNext()){
				SDMessage msg = listItr.next();
				if (generaloldestMessages.isEmpty() || !generaloldestMessages.contains(msg.getId())){
					oldestMessagesID = msg.getId();
					generaloldestMessages.add(oldestMessagesID);
					AllMessages.add(msg);
					adapter.addMessage(msg);
				}
			}
			loading = false;
			adapter.notifyDataSetChanged();
	     }
		 		
		private void addNewestMessages(ArrayList<SDMessage> messages) {
			ListIterator<SDMessage> listItr = messages.listIterator();
			int i = 0;
			while(listItr.hasNext()){
				SDMessage msg = (SDMessage)listItr.next();
				if (generaloldestMessages.isEmpty() || !generaloldestMessages.contains(msg.getId())){
					oldestMessagesID = msg.getId();
					AllMessages.add(msg);
					adapter.addFromTop(msg,i);
					i++;
				}
			}
			adapter.notifyDataSetChanged();
		}
	    private void addNewestMessagestofinish(ArrayList<SDMessage> Messages){
			ListIterator<SDMessage> listItr = Messages.listIterator();
			while(listItr.hasNext()){
				SDMessage msg = (SDMessage)listItr.next();
					oldestMessagesID = msg.getId();
					AllMessages.add(AllMessages.size(), msg);
					adapter.addFromBottom(msg);
					if (msg.getAuthor() != account.getId())
						oldestOtherUserMessageId = msg.getId();
			}
			adapter.notifyDataSetChanged();
		}

		
	//***********************************************************
	//******************** Server connection  *******************
	//***********************************************************
	/**
	 * Obtiene el listado de mensajes, nuevos o antiguos
	 * @param lastID Ultimo ID de mensaje
	 * @param showDialog Determina si mostrar el dialogo de carga general o no
	 */
		
		
	 private void getMessages(final String lastID, final boolean showDialog){
		 if(lastID==null && showDialog) dialogs.showLoadingProgressDialog(getString(R.string.retrieving_messages)); 
		 new SDConversationGETAllMgs(SD_Url,  SD_Key, conversationID, lastID, new SDConnectionResponseListener(){
			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(String response, Object resultElement) {
				Messages = (ArrayList<SDMessage>) resultElement;
				if(lastID == null && showDialog && Messages.size()>0){ 
					addMessages(Messages);
					dialogs.dismissProgressDialog();
				}else{
					if(lastID == null && !showDialog && Messages.size()>0){
					    addNewestMessages(Messages);
			 		}else{	
						//list.removeFooterView(footerView);
			 			addOldestMessages(Messages);
			 		}
		 		}
				requestOrientation();
			}
			
			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(getString(R.string.error_servidor_no_disponible )+ issues);
				requestOrientation();
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				requestOrientation();
			}	 	 
		 });  
	 }
	 
	 
	 
	 
	 
		/**
		 * Obtiene el listado de mensajes,los mas nuevos dependiendo del firstId
		 * @param firstId primer ID de mensaje
		 * @param showDialog Determina si mostrar el dialogo de carga general o no
		 */
	 private void getFirstMessages(final String firstId, final boolean showDialog){
		 if(firstId==null && showDialog) dialogs.showLoadingProgressDialog(getString(R.string.retrieving_messages)); 
		 new SDConversationGETAllNewMgs(SD_Url,  SD_Key, conversationID, firstId, new SDConnectionResponseListener(){
			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(String response, Object resultElement) {
				Messages = (ArrayList<SDMessage>) resultElement;
				if(firstId == null && showDialog && Messages.size()>0){ 
					addMessages(Messages);
					dialogs.dismissProgressDialog();
				}else{
					    addNewestMessagestofinish(Messages);

		 		}
				requestOrientation();
			}
			
			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
			//	dialogs.ShowErrorDialog(getString(R.string.error_servidor_no_disponible )+ issues);
				requestOrientation();
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						// dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
						//	 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
				//	 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				requestOrientation();
			}	 	 
		 });  
	 }

	private void requestOrientation() {
		final FragmentActivity activity = getActivity();
		if(activity!=null)activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
	}


	/**
	  * Envia un nuevo mensaje al server
	  * @param text
	  * @param conversationID
	  */
	 private void sendMessage(String text, String conversationID){
		 //dialogs.showLoadingProgressDialog(getString(R.string.sending_message));
		 new SDConversationPOSTAddMessage(SD_Url, SD_Key, text, conversationID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				SDMessage msg = (SDMessage) resultElement;
				dialogs.dismissProgressDialog();
			}

			@Override
			public void onIssues(String issues) {
			//	dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
			}

			@Override
			public void onError(SDError error) {
			//	 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 });  
		 
	 }
	 
	 private void getConversation(String conversationID){
		 dialogs.showLoadingProgressDialog(getString(R.string.retrieving_conversation));
		 new SDConversationGETByID(SD_Url, SD_Key, conversationID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				conversation = (SDConversation) resultElement;
				setConversation(conversation);
				dialogs.dismissProgressDialog();
				requestOrientation();
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(getString(R.string.retrieving_conversation) + issues);
				requestOrientation();
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.retrieving_conversation));
				 }
				requestOrientation();
			}	 	 
		 });  
		 
	 }

	 
	 class MyTimerTask extends TimerTask {
		  public void run() {
			  // ERROR
			// hTextView.setText("Impossible");
			 // how update TextView in link below  
	                 // http://android.okhelp.cz/timer-task-timertask-run-cancel-android-example/
			  final FragmentActivity activity = getActivity();
			  if (activity!=null) {
				  if (adapter.getLastMessage()!= null){
                      if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
                          activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                      }
                      else {
                          activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                      }

                      if (oldestOtherUserMessageId.isEmpty()){
                          getFirstMessages(adapter.getLastMessage().getId(), true);
                      }
                      else {
                          getFirstMessages(oldestOtherUserMessageId, true);

                      }
                  }
			  }
			  Log.d("Discoverhood","entro el timmer para las tareas");
		  }
		}

    @Override
	    public void onStop() { 
	    	if (myTimer != null) {
		    myTimer.cancel();
		    }
	        super.onStop();
	    }
	    
	    @Override
	    public void onDestroy(){
	    	if (myTimer != null) {
		    myTimer.cancel();}
	    	super.onDestroy();
	    	
	    }
	    @Override
	    public void onDetach(){
	    	if (myTimer != null) {
		    myTimer.cancel();}
	    	super.onDetach();
	    	
	    }
	    
	    


	    
	    
	    
	    /**
* Clase interna destinada a ejecutar cada cierto tiempo un metodo que checkea la ubicacion
*/
	 /*
private class checkMessages extends TimerTask {

private Context activity;
private boolean hasLocation;

public CheckLocation(Context context){
	activity = context;
	hasLocation = ((DiscoverHoodApp)fa.getApplicationContext()).CanGetLocation();
}

public void run() {
    if (!hasLocation) {
        
    } else {
    	timer.cancel(); 
    	finish();
   	 	overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 

    	//startActivity(new Intent(activity, ActivityMenu.class));
    }
}
}
	 */
	 
	 

	 
	 
}


