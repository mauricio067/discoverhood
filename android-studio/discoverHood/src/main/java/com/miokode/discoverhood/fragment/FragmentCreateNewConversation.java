package com.miokode.discoverhood.fragment;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.miokode.discoverhood.ActivityMenu;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.adapter.AdapterFriendListSelect;
import com.miokode.discoverhood.dialogs.DialogFragment;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.conversation.SDConversation;
import com.miokode.discoverhood.restful.conversation.SDConversationPOSTAdd;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.friends.SDFriend;
import com.miokode.discoverhood.restful.friends.SDFriendGETAll;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class FragmentCreateNewConversation extends DialogFragment {

    private ListView listView;
    private FragmentActivity fa;
    private EditText inputsearch, message;
    private List usuariosAniadidos = new ArrayList();
    private TextView cancel;
    private AdapterFriendListSelect adapter;
    private ArrayList<SDFriend> friendList;
    private SDFriend pers = null;
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private DisplayImageOptions options;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);

        super.onCreateView(inflater, container, savedInstanceState);
        final View fragmentView = inflater.inflate(R.layout.screen_create_new_conversation, container, false);
        fa = super.getActivity();
        listView = (ListView) fragmentView.findViewById(R.id.list_friend_to_filter);

        inputsearch = (EditText) fragmentView.findViewById(R.id.SearchBox);
        message = (EditText) fragmentView.findViewById(R.id.writeMessage);
        cancel = (TextView) fragmentView.findViewById(R.id.cancel_message);

        inputsearch.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(inputsearch, InputMethodManager.SHOW_IMPLICIT);
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                if (hasFocus) {
                    inputsearch.setHint("");
                } else {
                    inputsearch.setHint(getResources().getString(R.string.search));
                }
            }
        });
        message.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    message.setHint("");
                } else {
                    message.setHint(getResources().getString(R.string.write_here));
                }
            }
        });

        final ImageView imagencontacto = (ImageView) fragmentView.findViewById(R.id.imagenContacto);
        final TextView nombreContacto = (TextView) fragmentView.findViewById(R.id.nombreContacto);

        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(getActivity());

        final LinearLayout user = (LinearLayout) fragmentView.findViewById(R.id.contentUser);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
//	            	inputsearch.setText("ej ñldjkf weopiurf aoifuq wefakeñiotu zxclkgbj erplsdfj qweioy");
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                pers = (SDFriend) listView.getItemAtPosition(arg2);
                listView.setVisibility(View.INVISIBLE);
                inputsearch.setVisibility(View.INVISIBLE);

                if (pers.getAvatar() != null) {
                    String url = pers.getAvatar().getPictureUrl().trim();
                    imageLoader.displayImage(url, imagencontacto, options);
                }

                nombreContacto.setText(pers.getName());
                //listView.setVisibility(View.INVISIBLE);
                user.setVisibility(View.VISIBLE);

            }
        });
        if (pers != null) {
            //inputsearch.setVisibility(View.INVISIBLE);
            user.setVisibility(View.VISIBLE);

            if (pers.getAvatar() != null) {
                String url = pers.getAvatar().getPictureUrl().trim();
                imageLoader.displayImage(url, imagencontacto, options);
            }
            nombreContacto.setText(pers.getName());
        }
        final Button buttonsend = (Button) fragmentView.findViewById(R.id.sendMessage);
        buttonsend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                message.setText(message.getText().toString().replaceAll("[\n\r]", ""));

                if (message.getText().toString().replaceAll(" ", "").length() > 0) {
                    if (pers != null) {
                        createConversation(pers.getId(), message.getText().toString());
                    } else {
                        dialogs.ShowErrorDialog(fa.getApplication().getString(R.string.youmustaddfriend));
                    }
                } else {
                    dialogs.ShowErrorDialog(fa.getApplication().getString(R.string.youmustaddtext));
                }
            }
        });

        final Button deleteUser = (Button) fragmentView.findViewById(R.id.deleteuser);
        deleteUser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                user.setVisibility(View.INVISIBLE);
                pers = null;
                inputsearch.setText("");
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                inputsearch.setVisibility(View.VISIBLE);
                listView.setVisibility(View.VISIBLE);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fa.onBackPressed();

            }
        });

	    /*seteo de fuentes*/
        Typeface face = Typeface.createFromAsset(fa.getAssets(), "fonts/arial.ttf");
        cancel.setTypeface(face);
        inputsearch.setTypeface(face);
        nombreContacto.setTypeface(face);
        message.setTypeface(face);
        buttonsend.setTypeface(face);

        if (friendList != null) {
            addFriends(friendList);
        } else {
            if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
            getFriends();
        }

        inputsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                CharSequence s = inputsearch.getText();
                adapter.getFilter().filter(s);
                adapter.notifyDataSetChanged();
                inputsearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) fa.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(inputsearch, InputMethodManager.SHOW_IMPLICIT);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });
        fragmentView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                user.setVisibility(View.INVISIBLE);
                return false;
            }
        });
        return fragmentView;
    }

    private void addFriends(ArrayList<SDFriend> list) {
        adapter = new AdapterFriendListSelect(fa, list);
        listView.setAdapter(adapter);
    }

    //***********************************************************
    //******************** Server connection  *******************
    //***********************************************************

    /**
     * Obtiene el listado de amigos
     */
    private void getFriends() {
        dialogs.showLoadingProgressDialog(getString(R.string.retrieving_friends));
        new SDFriendGETAll(SD_Url, SD_Key, new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {
                friendList = (ArrayList<SDFriend>) resultElement;
                addFriends(friendList);
                dialogs.dismissProgressDialog();
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }

            @Override
            public void onIssues(String issues) {
                dialogs.dismissProgressDialog();
                dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }

            @Override
            public void onError(SDError error) {
                dialogs.dismissProgressDialog();
                if (error.getReason() != null) {
                    if (error.getDetails() == null) {
                        dialogs.ShowErrorDialog(error.getReason());
                    } else {
                        Enumeration<String> element = error.getDetails().elements();
                        while (element.hasMoreElements()) {
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                } else {
                    dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
                }
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }
        });

    }

    /**
     * Crea una nueva conversacion con un nuevo mensaje inicial
     *
     * @param toUserID Usuario de destino
     * @param text     Texto del mensdaje
     */
    private void createConversation(String toUserID, String text) {
        dialogs.showLoadingProgressDialog(getString(R.string.creating_new_conversation));
        new SDConversationPOSTAdd(SD_Url, SD_Key, toUserID, text, new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {
                SDConversation conversation = (SDConversation) resultElement;
                dialogs.dismissProgressDialog();
                FragmentConversation mContent = new FragmentConversation();
                mContent.setConversationID(conversation.getId());
                mContent.setConversation(conversation);

                ((ActivityMenu) fa).switchContentWithoutBack(mContent);

        	/*	getFragmentManager()
        		.beginTransaction()
        		.replace(R.id.content_frame, mContent)
        		.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)     
        		.commit();*/
            }

            @Override
            public void onIssues(String issues) {
                dialogs.dismissProgressDialog();
                dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
            }

            @Override
            public void onError(SDError error) {
                dialogs.dismissProgressDialog();
                if (error.getReason() != null) {
                    if (error.getDetails() == null) {
                        dialogs.ShowErrorDialog(error.getReason());
                    } else {
                        Enumeration<String> element = error.getDetails().elements();
                        while (element.hasMoreElements()) {
                            dialogs.ShowErrorDialog(element.nextElement());
                        }
                    }
                } else {
                    dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
                }
            }
        });

    }
}

