package com.miokode.discoverhood.fragment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.ActivityMenu;
import com.miokode.discoverhood.ActivityMenuBase;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.dialogs.DialogFragment;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.account.SDAccountPOSTEdit;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;


public class FragmentEditUserProfile extends DialogFragment {
	private static int TAKE_PIC = 1;
	private static int SELECT_PIC = 2;
	private String name = " ";
	private AlertDialog ImageDialog =null;
	private GregorianCalendar born = null;
	private Bitmap picture = null;
	static final int DATE_DIALOG_ID = 999;
    private View fragmentView;
	private TextView ug, bd, genre;
	private EditText nickname, firstname, lastname, pass1, pass2, mail,fn, ln;
	private ImageView image;
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private DisplayImageOptions options;
	private String date;
	private SDAccount account;
	private Bitmap avatarImage; 

	
	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {
	   // setRetainInstance(true);
		super.onCreateView(inflater, container, savedInstanceState);
	    fragmentView = (ViewGroup) inflater.inflate(R.layout.screen_edit_user_profile, container, false);
        this.account = ((DiscoverHoodApp) this.getActivity().getApplication()).getUserDataAccount();
		Typeface face = Typeface.createFromAsset(fragmentView.getContext().getAssets(),"fonts/arial.ttf");
		Button signup = (Button)  fragmentView.findViewById(R.id.sign_up);
		TextView pouallr = (TextView)fragmentView.findViewById(R.id.pou_all_rigths);
        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(getActivity());

        nickname = (EditText) fragmentView.findViewById(R.id.user_nickname);       
        firstname = (EditText) fragmentView.findViewById(R.id.user_first_name);
        lastname = (EditText)fragmentView.findViewById(R.id.user_last_name);
		pass1 = (EditText) fragmentView.findViewById(R.id.user_password);
     	pass2 = (EditText) fragmentView.findViewById(R.id.user_password_confirm);
     	mail = (EditText) fragmentView.findViewById(R.id.user_email);
     	genre = (TextView) fragmentView.findViewById(R.id.user_genre);
     	bd = (TextView) fragmentView.findViewById(R.id.user_birthday);
     	image = (ImageView) fragmentView.findViewById(R.id.user_pic);
     	
     	int imageResource = R.drawable.photo_icon;
     	Drawable noImage = getResources().getDrawable(imageResource);
     	image.setImageDrawable(noImage);
	    nickname.setText(account.getNickname());
     	firstname.setText(account.getFirstname());
     	lastname.setText(account.getLastname());
		mail.setText(account.getEmail());
		genre.setText(account.getGender());
		
		nickname.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.showSoftInput(nickname, InputMethodManager.SHOW_IMPLICIT);
			    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				if (hasFocus){
					nickname.setHint("");
					nickname.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
				}
				else {
					nickname.setHint(getResources().getString(R.string.nickname));
					nickname.setBackgroundColor(getResources().getColor(R.color.white));
				}
	        }
	  	});  	  
		firstname.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.showSoftInput(firstname, InputMethodManager.SHOW_IMPLICIT);
			    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

				if (hasFocus){
					firstname.setHint("");
					firstname.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
				}
				else {
					firstname.setHint(getResources().getString(R.string.first_name));
					firstname.setBackgroundColor(getResources().getColor(R.color.white));
				}
	        }
	  	}); 
		
		
		
		lastname.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.showSoftInput(lastname, InputMethodManager.SHOW_IMPLICIT);
			    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

				if (hasFocus){
					lastname.setHint("");
					lastname.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
				}
				else {
					lastname.setHint(getResources().getString(R.string.last_name));
					lastname.setBackgroundColor(getResources().getColor(R.color.white));
				}
	        }
	    });  
		mail.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.showSoftInput(mail, InputMethodManager.SHOW_IMPLICIT);
			    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

			 	if (hasFocus){
					mail.setHint("");
					mail.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
				}
				else {
					mail.setHint(getResources().getString(R.string.email));
					mail.setBackgroundColor(getResources().getColor(R.color.white));
				}
		   }
		});  
		pass1.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.showSoftInput(pass1, InputMethodManager.SHOW_IMPLICIT);
			    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

				if (hasFocus){
					pass1.setHint("");
					pass1.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
				}
				else {
					pass1.setHint(getResources().getString(R.string.password));
					pass1.setBackgroundColor(getResources().getColor(R.color.white));
				}
		      }
		});  
		pass2.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.showSoftInput(pass2, InputMethodManager.SHOW_IMPLICIT);
			    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

				if (hasFocus){
					pass2.setHint("");
					pass2.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
					
				}
				else {
					pass2.setHint(getResources().getString(R.string.confirm_password));
					pass2.setBackgroundColor(getResources().getColor(R.color.white));
				}
		       }
		});	  
		if (account.getGender().contentEquals("M")) {
			genre.setText(R.string.masculino);
		}
		else {
			genre.setText(R.string.femenino);
		}
		           
		DateFormat format = DateFormat.getDateInstance(1);
		format.setTimeZone(TimeZone.getTimeZone("gmt")); 
		born = new GregorianCalendar();
		
	
		String fDate = new SimpleDateFormat("d MMM yyyy").format( account.getBirthdayNotNull());
		
		born.setTime(account.getBirthdayNotNull());
		date = format.format( account.getBirthdayNotNull().getTime() );
		
		//bd.setText(date);
		
		bd.setText(fDate);
		
		bd.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				showDatePicker();
 			}
    	});
		
		
	    pass1.setTypeface(face);
	    pass2.setTypeface(face);
	    mail.setTypeface(face);
	    bd.setTypeface(face);
	    nickname.setTypeface(face);
        firstname.setTypeface(face);
        lastname.setTypeface(face);
		genre .setTypeface(face);
	    pouallr.setTypeface(face);
	    signup.setTypeface(face);
	    
	    genre.setOnClickListener(new View.OnClickListener() {
	           public void onClick(View v) {
	   	    	  final CharSequence[] items = {getResources().getString(R.string.masculino), getResources().getString(R.string.femenino)};  	 
		    	   AlertDialog.Builder builder = new AlertDialog.Builder(fragmentView.getContext());
		    	   builder.setTitle(getResources().getString(R.string.genre));
		    	   builder.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
		    	      public void onClick(DialogInterface dialog, int item) {
		    	      	  genre.setText(items[item]);    	    	
		    	          dialog.cancel();
		    	      }
		    	   });
		      	   AlertDialog alert = builder.create();
		    	   alert.show();   
	           }
	           
	    });
	    
		
		genre.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				
				if (hasFocus){
		   	    	  final CharSequence[] items = { getResources().getString(R.string.masculino), getResources().getString(R.string.femenino)};  	 
			    	   AlertDialog.Builder builder = new AlertDialog.Builder(fragmentView.getContext());
			    	   builder.setTitle(getResources().getString(R.string.genre));
			    	   builder.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
			    	      public void onClick(DialogInterface dialog, int item) {
			    	      	  genre.setText(items[item]);    	    	
			    	          dialog.cancel();
			    	      }
			    	   });
			      	   AlertDialog alert = builder.create();
			    	   alert.show();   
				}
				else {
				}
			}
		});
	    
	    signup.setOnClickListener(new View.OnClickListener() {
	           public void onClick(View v) {
	          	   if ( dataVerification() ) { 
   	             	    if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
	               		    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
	          	    	}
	          		    else {		     
	          		       getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
	          		    }		

	         			enviarDatosAlServer();
	          		 
	         	   }  
	           }
	    });

	    if(account.getAvatar()!=null) imageLoader.displayImage(account.getAvatar().getPictureUrl(), image, options);
	    image.setOnClickListener(new View.OnClickListener() {
	           public void onClick(View v) {
	        	   getPhoto();
	           }
	    });
	    
	    if (account.getFacebook_login()){
			pass1.setEnabled(false);
	     	pass2.setEnabled(false);
	     	mail.setEnabled(false);
	    }
	    
	    return fragmentView;
 	}
	
	public void setBirthday(CharSequence bdate){
		bd.setText(bdate);
	}

  
	public void guardarUsuarioApp( SDAccount account){
		((DiscoverHoodApp) this.getActivity().getApplication()).setUserDataAccount(account);
		((DiscoverHoodApp) this.getActivity().getApplication()).updateFragmentLeftMenu(account);
		
	}
	//***********************************************************
	//********************* Checking data  **********************
	//***********************************************************
    public boolean dataVerification(){  	
    	 Boolean exito = true;
    	 String errors = "";
    
    	 int nn = nickname.getText().toString().trim().length();
    	 int n= firstname.getText().toString().trim().length();
    	 int l= lastname.getText().toString().trim().length(); 
    	 int p1= pass1.getText().toString().trim().length();
    	 int p2= pass2.getText().toString().trim().length();
    	 int m= mail.getText().toString().trim().length();
    	 int g= genre.getText().toString().compareTo(getResources().getString(R.string.genre));
    	 int b= bd.getText().toString().compareTo(getResources().getString(R.string.birthday));
    	 
    	 
    	 //nickname
    	 if (nn!=0 && (nn<2 || nn>20)){
    		 nickname.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 //errors = errors + getString(R.string.nickname_verification_error) + "\n";
    		 exito = false;
    	 }
    	 //firstname
    	 if (n==0 || n<2 || n>50){
    		 firstname.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.firstname_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 firstname.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
    	 //lastname
    	 if (l==0 || l<2 || l>50){
    		 lastname.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.lastname_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 lastname.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
    	 //password
    	 if ( (p1!=0 && p2==0) || (p1==0 && p2!=0) ){
    		 pass1.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 pass2.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.password_verification_error) + "\n";
    		 exito = false;
    	 }else if(p1!=0 && p2!=0){
	    	 if (pass1.getText().toString().compareTo(pass2.getText().toString())!=0){
	    		 pass1.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
	    		 pass2.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
	    		 errors = errors + getString(R.string.password_mismatch_error) + "\n";
	    		 exito = false;
	    	 }else if(p1<6 || p1>20){
	    		 pass1.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
	    		 pass2.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
	    		 errors = errors + getString(R.string.password_verification_error) + "\n";
	    		 exito = false;
	    	 }else{
	    		 pass1.setBackgroundColor(getResources().getColor(R.color.white));
	    		 pass2.setBackgroundColor(getResources().getColor(R.color.white));
	    	 }
    	 }
    	 //email
    	 if (m==0){
    		 mail.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.email_verification_error) + "\n";
    		 exito = false;
    	 }else if (!checkEmail(mail.getText().toString().trim())) {
    		 mail.setBackgroundColor(getResources().getColor(R.color.focus_background));
    		 errors = errors + getString(R.string.invalid_mail_error) + "\n";
    		 exito = false;
    	 }else{
    		 mail.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
    	//genre
    	 if (g==0){
    		 genre.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.genre_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 genre.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
     	//birthday
    	 if (b==0){
    		 bd.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.birthday_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 bd.setBackgroundColor(getResources().getColor(R.color.white));
    	 }

    	 if (!exito){
    		 dialogs.ShowErrorDialog(errors);
    	 }
    	 return exito;
     }
   
    public static boolean checkEmail (String email) {
        Pattern p = Pattern.compile("[-\\w\\.]+@\\w+\\.\\w+");
        Matcher m = p.matcher(email);
        return m.matches();
    }
    
    public boolean equalPass(String s1, String s2){
    	if (s1.length()==0 && s2.length()==0) 
    		return false; 
    	else 
    		return s1.compareTo(s2)==0;
    }

	 

	//***********************************************************
	//************************* Dialogs  ************************
	//***********************************************************
	 private void showDatePicker() {
		  DatePickerFragment date = new DatePickerFragment();
		  Calendar calender = Calendar.getInstance();
		  Bundle args = new Bundle();
		  /*
		  args.putInt("year", calender.get(Calendar.YEAR));
		  args.putInt("month", calender.get(Calendar.MONTH));
		  args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
		 */ 
		  args.putInt("year", born.get(Calendar.YEAR));
		  args.putInt("month", born.get(Calendar.MONTH));
		  args.putInt("day", born.get(Calendar.DAY_OF_MONTH));
		  
		  date.setArguments(args);
		  date.setCallBack(ondate);
		  date.show(getActivity().getSupportFragmentManager(), " ");
	}
	OnDateSetListener ondate = new OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			DateFormat format = DateFormat.getDateInstance(1);
			format.setTimeZone(TimeZone.getDefault()); 
			born = new GregorianCalendar(year, monthOfYear, dayOfMonth);
			date = format.format( born.getTime() );
			bd.setText(date); 
		}
	};
	 
	public void chsdooseGenre(View v){
		final CharSequence[] items = {getResources().getString(R.string.masculino), getResources().getString(R.string.femenino)};  	 
		AlertDialog.Builder builder = new AlertDialog.Builder(fragmentView.getContext());
		builder.setTitle(getResources().getString(R.string.genre));
		builder.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int item) {
		    	genre.setText(items[item]);    	    	
		        dialog.cancel();
		    }
		});
		AlertDialog alert = builder.create();
		alert.show();
	}	 
	///////////////////////////
	/// Obtengo la foto
	///////////////////////////
	public void getPhoto(){  	   	
		ImageDialog = new AlertDialog.Builder(this.getActivity())
		.setTitle(getString(R.string.obtener_foto))
		.setMessage(getString(R.string.foto_option))
		.setPositiveButton(getString(R.string.galeria), new AlertDialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				fromGallery();
			}
		})
		.setNegativeButton(getString(R.string.camara), new AlertDialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				fromCamera();
			}
		})
		.setNeutralButton(getString(R.string.cancel_take_photo), new AlertDialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				ImageDialog.dismiss();
			}
		})
		.create();
		ImageDialog.show();   
	}
		
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	
	  	    if (resultCode != Activity.RESULT_OK) {
		       return;
		    }else{
			if (requestCode == TAKE_PIC) {
				
				if (data != null) {
					if (data.hasExtra("data")) { 
						ImageView iv = (ImageView)fragmentView.findViewById(R.id.user_pic); 
	    				picture = (Bitmap) data.getParcelableExtra("data");
	    				avatarImage = Bitmap.createScaledBitmap(picture, 400, 400, true);
	                    iv.setImageBitmap(avatarImage);
	    		 				
					}
				}else{
				
					ImageView iv = (ImageView)fragmentView.findViewById(R.id.user_pic); 
				    picture = BitmapFactory.decodeFile(name);
    				avatarImage = Bitmap.createScaledBitmap(picture, 400, 400, true);
				    iv.setImageBitmap(avatarImage);

				    new MediaScannerConnectionClient() {
						private MediaScannerConnection msc = null; {
							msc = new MediaScannerConnection(getActivity().getApplicationContext(), this); msc.connect();
						}
						public void onMediaScannerConnected() { 
							msc.scanFile(name, null);
						}
						public void onScanCompleted(String path, Uri uri) { 
							msc.disconnect();
						} 
					};				
				}
				} else if (requestCode == SELECT_PIC){
					Uri selectedImage = data.getData();
					InputStream is;
					try {
						is= getActivity().getContentResolver().openInputStream(selectedImage);
						BufferedInputStream bis = new BufferedInputStream(is);
						BitmapFactory.Options options=new BitmapFactory.Options();
						options.inSampleSize = 2;
						picture=BitmapFactory.decodeStream(bis,null,options);
						ImageView iv = (ImageView)fragmentView.findViewById(R.id.user_pic); 
			    		iv.setImageBitmap(picture);
			    		
					}catch (FileNotFoundException e) {}
		    	}
		   }
	   	   	    			 
	  }
	private void fromGallery(){
		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
		int code = SELECT_PIC;	
		startActivityForResult(i, code);  	
	}
		
	private void fromCamera(){
		Intent i= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri output = Uri.fromFile(new File(name));
		int code = TAKE_PIC;
		startActivityForResult(i, code);
	}
		
	
	//***********************************************************
	//******************** Server connection  *******************
	//***********************************************************
	/**
	 * Send data to server. 
	 */
	private void enviarDatosAlServer(){
		 dialogs.showLoadingProgressDialog(getString(R.string.saving_changes)); 	
		 final SDAccount editedAccount = new SDAccount( firstname.getText().toString().trim(), lastname.getText().toString().trim(), (genre.getText().toString().compareTo(getString(R.string.masculino)) == 0)? "M": "F" , mail.getText().toString().trim(), pass1.getText().toString(), born.getTime(), picture );
		 editedAccount.setNickname(nickname.getText().toString().trim());

		 new SDAccountPOSTEdit(SD_Url,  SD_Key, account, editedAccount, new SDConnectionResponseListener(){
			@Override
			public void onSuccess(String response, Object resultElement) {
				guardarUsuarioApp((SDAccount) resultElement);
				dialogs.dismissProgressDialog();
				((ActivityMenuBase)getActivity()).refreshFragmentLeftMenu("", nickname.getText().toString().trim());
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
				dialogs.ShowInfoDialog(getString(R.string.changes_saved), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						((ActivityMenu) getActivity()).switchContent(new FragmentGeneralFeeds());
					}
				});

			}
			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(issues);
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				 getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}
		 	 
		 }); 
	 }

	 
		 
}
