package com.miokode.discoverhood.fragment;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.TimeZone;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.miokode.discoverhood.ActivityEnlargeAvatar;
import com.miokode.discoverhood.ActivityWritePost;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.adapter.AdapterLisItemFeed;
import com.miokode.discoverhood.dialogs.DialogSherlockListFragment;
import com.miokode.discoverhood.restful.account.SDAccount;
import com.miokode.discoverhood.restful.account.SDAccountGET;
import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.friends.SDFriendPOSTSendRequest;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationsGETAllByUser;
import com.miokode.discoverhood.tools.ConnectionDialogs;
import com.miokode.discoverhood.tools.DateTools;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.miokode.discoverhood.tools.MemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

public class FragmentOtherUserProfile extends DialogSherlockListFragment {
	
	private ConnectionDialogs dialogs;
	private String SD_Url;
	private String SD_Key;
	private AdapterLisItemFeed adapter;
	private ArrayList<SDPublication> Publicaciones;
	private String userID = null;
	private MemoryCache imageCache;
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private DisplayImageOptions options;
    @SuppressWarnings("unused")
	private SDAccount userData;
    //private SDAuthor Author;
    private View mTop;
    private PullToRefreshListView list;
    @SuppressWarnings("unused")
	private ImageView avatar;
    private String userAvatar;
	private View footerView;
    private ArrayList<String> oldestPublications;
    private TextView nonpubli;
	private String oldestPublicationID = null;
	private ArrayList<SDPublication> UserPublications;
	private boolean loading = false;
	@SuppressWarnings("unused")
	private String newestPublicationID;
	private int index;
	private View view;
	private Context activity;
   
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        setRetainInstance(true);
		super.onCreateView(inflater, container, savedInstanceState);
		view = inflater.inflate(R.layout.screen_friend_profile, container, false);
		activity = getActivity();
		LinearLayout WritePost= (LinearLayout) view.findViewById(R.id.write_post);
	 	this.nonpubli = (TextView) view.findViewById(R.id.noninformation);

		WritePost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
          			Intent intent = new Intent(getActivity(), ActivityWritePost.class);
          			intent.putExtra("userID", userID);
          			startActivity(intent);
          			getActivity().overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
            }
        });		
		return view ;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);	
		this.dialogs = new ConnectionDialogs(this.getActivity());
		SD_Url = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerUrl();
	    SD_Key = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerKey();
	    mTop = getLayoutInflater(savedInstanceState).inflate(R.layout.item_friend_profile, null);
	    footerView = getLayoutInflater(savedInstanceState).inflate(R.layout.item_footer, null, false);
		oldestPublications = ((DiscoverHoodApp) getActivity().getApplication()).getOtherUserOldestPublications(); 
		imageCache = ((DiscoverHoodApp)getActivity().getApplication()).getAvatarMemoryCache();
        boolean posted = ((DiscoverHoodApp) this.getActivity().getApplication()).isPostSended();
		
	    TextView invite = (TextView) mTop.findViewById(R.id.invite);
	    invite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	//send friendship request
                AlertDialog.Builder alt_bld = new AlertDialog.Builder(view.getContext());
                alt_bld.setMessage(getResources().getString(R.string.consultsendinvitation))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yesoption), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    // Action for 'Yes' Button
                    	inviteFriend();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.nooption), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
                AlertDialog alert = alt_bld.create();
                // Title for AlertDialog
                alert.setTitle(getResources().getString(R.string.titleconsultsendinvitation));
                // Icon for AlertDialog
                alert.show();

            }
        });	    	  
	
		list = (PullToRefreshListView) getListView();      

	    ((PullToRefreshListView)list).setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
            	if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
            	}
            	else {		     
            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
            	}		

            	getPublications(null, false);
                    list.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                list.onRefreshComplete();
                            }
                    }, 2000);
            }
        });
	    
	    list.setOnScrollListener(new OnScrollListener() {           
			@Override
	    	public void onScrollStateChanged(AbsListView view, int scrollState) {
	        	if (scrollState == SCROLL_STATE_IDLE) {
	    		        if (list.getLastVisiblePosition() >= list.getCount()-1) {
	    		        	if(!loading){
	    		        		list.addFooterView(footerView, null, false);
	    		        		loading=true;
	    		        		if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
	    		        		    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
	    		        		}
	    		        		else {		     
	    		        		    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
	    		        		}		

	    		        		getPublications(oldestPublicationID, false);
	    		        	}
	    			}
	    		}
	        }
	         
	        @Override
	        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount){
	        	
	        }
	    });
	    
	    if(savedInstanceState!=null && !posted){
		    nonpubli.setVisibility(View.GONE);
	    	int listPosition = ((DiscoverHoodApp) getActivity().getApplication()).getListPosition();
	  		UserPublications = ((DiscoverHoodApp) getActivity().getApplication()).getOtherUserPublications();
	    	ArrayList<SDPublication> pub = new ArrayList<SDPublication>();
		    adapter = new AdapterLisItemFeed(getActivity(),pub);
		 	list.setAdapter(adapter);
		 	if(UserPublications.size()>5 && UserPublications.size()-1>listPosition){	 			
		 		UserPublications.subList(listPosition+1, UserPublications.size()).clear();
		 		oldestPublications.subList(listPosition+1, oldestPublications.size()).clear();
		 		oldestPublicationID = oldestPublications.get(oldestPublications.size()-1);
		 	}
		 	
		 	loadAllPublications();
	    }
	  	else{
	  		((DiscoverHoodApp) this.getActivity().getApplication()).postSended(false);
	    	if (UserPublications != null){
		    nonpubli.setVisibility(View.GONE);
	    	ArrayList<SDPublication> pub = new ArrayList<SDPublication>();
	        adapter = new AdapterLisItemFeed(getActivity(),pub);
	 	    list.setAdapter(adapter);
	 	    loadAllPublications();
	    	}
	    	else {
	        		((DiscoverHoodApp) getActivity().getApplication()).ClearOtherUserPublications();
	    			if (userID == null){
	    				this.userID = this.getArguments().getString("userID");
	    				this.userAvatar = this.getArguments().getString("urlAvatar");
	    			    getUserProfile(userID);
	    			}else 
	    				LoadPosts();
	    	}
    	
	    }
	}
	
	public void onPause(){
		super.onPause();
		index = list.getFirstVisiblePosition();
  		((DiscoverHoodApp) this.getActivity().getApplication()).setListPosition(index);
	}
	
	public void onResume(){
		super.onResume();
		if (adapter != null) {
		adapter.notifyDataSetChanged();
		}
        boolean posted = ((DiscoverHoodApp) this.getActivity().getApplication()).isPostSended();

        if(posted){
        
        	((DiscoverHoodApp) this.getActivity().getApplication()).postSended(false);
        				        
        	((DiscoverHoodApp) getActivity().getApplication()).ClearOtherUserPublications();
			if (userID == null){
				this.userID = this.getArguments().getString("userID");
				this.userAvatar = this.getArguments().getString("urlAvatar");
			    getUserProfile(userID);
			}else 
				LoadPosts();
    	}
	}
	
	//***********************************************************
	//******************** Others process  **********************
	//***********************************************************
	
	private void LoadPosts(){
		UserPublications = ((DiscoverHoodApp) getActivity().getApplication()).getOtherUserPublications();
		Publicaciones = new ArrayList<SDPublication>();
		adapter = new AdapterLisItemFeed(getActivity(), Publicaciones);
		list.setAdapter(adapter);
		if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
		    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
		}
		else {		     
		    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
		}		

		getPublications(null, true);
	}
	
	
	private void addPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		while(listItr.hasNext()){
			SDPublication publ = (SDPublication)listItr.next();
			oldestPublicationID = publ.getId();
			oldestPublications.add(oldestPublicationID);
			UserPublications.add(publ);
			adapter.addItem(publ);
		}
		adapter.notifyDataSetChanged();
	}

	private void loadAllPublications(){
		Iterator<SDPublication> it = UserPublications.iterator();
		while(it.hasNext()){
			SDPublication publ = it.next();
			oldestPublicationID = publ.getId();
			adapter.addItem(publ);
		}
        adapter.notifyDataSetChanged();
	}

	private void addOldestPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		while(listItr.hasNext()){
			SDPublication publ = (SDPublication)listItr.next();
			if (oldestPublications.isEmpty() || !oldestPublications.contains(publ.getId())){
				oldestPublicationID = publ.getId();
				oldestPublications.add(oldestPublicationID);
				UserPublications.add(publ);
				adapter.addItem(publ);
			}
		}
		loading = false;
		adapter.notifyDataSetChanged();
     }
	
	private void addNewestPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		int i = 0;
		while(listItr.hasNext()){
			SDPublication publ = (SDPublication)listItr.next();
			if (oldestPublications.isEmpty() || !oldestPublications.contains(publ.getId())){
				newestPublicationID = publ.getId();
				UserPublications.add(publ);
				adapter.addFromTop(publ,i);
				i++;
			}
		}
		adapter.notifyDataSetChanged();
		list.onRefreshComplete();
	}
	
	 
	private void updateUserData( SDAccount userData ){
		this.userData = userData;
		DateFormat format = DateFormat.getDateInstance(1);
		format.setTimeZone(TimeZone.getTimeZone("gmt")); 
	    TextView online_offline = (TextView)mTop.findViewById(R.id.online_offline);;

        options = ImageLoaderHelper.getOptions();
        imageLoader = ImageLoaderHelper.getInstance(getActivity());
		ImageView avatar = (ImageView)mTop.findViewById(R.id.post_user_image);
		TextView Fdistance = (TextView)mTop.findViewById(R.id.friend_distance);
		TextView name = (TextView) mTop.findViewById(R.id.profile_name);
		TextView edad = (TextView) mTop.findViewById(R.id.profile_age);
		TextView born=(TextView)mTop.findViewById(R.id.profile_birthday);

		if(userData.isFriend())
			mTop.findViewById(R.id.invite).setVisibility(View.GONE);

		if(this.userData.getAvatar()!=null) {
			//Log.d("[Avatar]", this.userData.getAvatar().toString());
            imageLoader.displayImage(userData.getAvatar().getPictureUrl(), avatar, options, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String s, View view) {
					Log.e("discovertest","started");
				}

				@Override
				public void onLoadingFailed(String s, View view, FailReason failReason) {
					Log.e("discovertest","failed");
				}

				@Override
				public void onLoadingComplete(String s, View view, Bitmap bitmap) {
					Log.e("discovertest","completed");
				}

				@Override
				public void onLoadingCancelled(String s, View view) {
					Log.e("discovertest","canceled");
				}
			});
	    	userAvatar = userData.getAvatar().getPictureUrl();
		    avatar.setOnClickListener(new OnClickListener() {
	            public void onClick(View view) {
	            		Intent i= new Intent(getActivity(), ActivityEnlargeAvatar.class);
	            		i.putExtra("url",userAvatar);
	            		getActivity().startActivity(i);
	            }
			});			

		}
	
	    online_offline.setText(getString( userData.isOnline()? R.string.online : R.string.offline));
        Double d = userData.getDistance();
        BigDecimal bd = new BigDecimal(d/1000);
        bd = bd.setScale(1, 6);     
        d = bd.doubleValue();
        
	    Fdistance.setText( Double.toString(d)+" "+getString(R.string.km));
	    name.setText(userData.getName());
		if (!userData.hideDate()) {
			final Date birthday = userData.getBirthdayNotNull();
			edad.setText(getString(R.string.Age_edad) + " " + DateTools.getAge(birthday));
			born.setText(format.format(birthday));
		}else{
			born.setText("");
			edad.setText("");
		}

		list.addHeaderView(mTop);
	}
	
	
	//***********************************************************
	//******************** Server connection  *******************
	//***********************************************************
	/**
	 * Obtiene la informacion del usuario
	 * @param userID
	 */
	private void getUserProfile(String userID){
		 dialogs.showLoadingProgressDialog(getString(R.string.loading_user_information));
		 new SDAccountGET(SD_Url, SD_Key, userID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				updateUserData( (SDAccount) resultElement );
				dialogs.dismissProgressDialog();
				LoadPosts();
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 });
	 }
	
	
	private void getPublications(final String lastID, final boolean showDialog){
		if(lastID==null && showDialog) dialogs.showLoadingProgressDialog(getString(R.string.retrieving_publications));
		
		 new SDPublicationsGETAllByUser(SD_Url, SD_Key, userID, lastID,  new SDConnectionResponseListener(){

			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(String response, Object resultElement) {

				Publicaciones = (ArrayList<SDPublication>) resultElement;
				if(lastID == null && showDialog && Publicaciones.size()>0){ 
   				    nonpubli.setVisibility(View.GONE);
					addPublications(Publicaciones);
					dialogs.dismissProgressDialog();
				}
				else {
					  if(lastID == null && !showDialog && Publicaciones.size()>0){
					  addNewestPublications(Publicaciones);
			 		}else{	
						list.removeFooterView(footerView);
			 			addOldestPublications(Publicaciones);
			 		}
					  if (adapter.isEmpty()){
						  nonpubli.setVisibility(View.GONE);
					  }
					  else {
						  nonpubli.setVisibility(View.GONE);
					  }
		 		}
				requestOrientation();
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
				requestOrientation();
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				requestOrientation();
			}	 	 
		 });  
		 
	 }

	private void requestOrientation() {
		final FragmentActivity activity = getActivity();
		if(activity!=null)
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
	}

	/**
	  * Envia solicitud de amistad
	  */
	 private void inviteFriend( ){
	  	dialogs.showLoadingProgressDialog(getString(R.string.sending_frienship_request));
		new SDFriendPOSTSendRequest(SD_Url, SD_Key, userID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				
				Toast toast = Toast.makeText(activity, activity.getResources().getString(R.string.friend_request_sent), Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				dialogs.dismissProgressDialog();
				((TextView) mTop.findViewById(R.id.invite)).setVisibility(View.GONE);
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 });  
	}
	 public SDAuthor Author;
	 public void setuserData (SDAuthor ud){
		 this.Author = ud;
	 }
	 
}


