package com.miokode.discoverhood.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.ActivityImagePost;
import com.miokode.discoverhood.ActivityMenu;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.dialogs.DialogFragment;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.publication.SDPublicationImage;
import com.miokode.discoverhood.restful.publication.SDPublicationsGETMyImages;
import com.miokode.discoverhood.tools.ImageLoaderHelper;
import com.miokode.discoverhood.tools.MemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import java.util.TimeZone;

public class FragmentPhotoGallery extends DialogFragment {

    public static int mSelected = 0;
    private GridView gridpics;
    private View v = null;
    public static ArrayList<String> mStrings;
    private ImageLoadAdapter adapter;
    private MemoryCache memoryCache;
    private String SD_Url;
    private String SD_Key;
    private ProgressDialog proD = null;
    private ArrayList<SDPublicationImage> Iposts;
    private ArrayList<SDPublicationImage> Iswipe;
    private Bitmap LastestPicTaken;
    private TextView nopending;

    @Override
    public void onSaveInstanceState(Bundle safe) {
        super.onSaveInstanceState(safe);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.screen_user_pictures, container, false);
        nopending = (TextView) v.findViewById(R.id.noninformation);
        setRetainInstance(true);
        final com.actionbarsherlock.app.ActionBar ab = getSherlockActivity().getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        ImageView photo = (ImageView) v.findViewById(R.id.photo);
        photo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), ActivityImagePost.class));
                getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });

        TextView photo_text = (TextView) v.findViewById(R.id.photo_text);
        photo_text.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), ActivityImagePost.class));
                getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SD_Url = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerKey();
        LastestPicTaken = ((DiscoverHoodApp) this.getActivity().getApplication()).getPictureTaken();
        ((DiscoverHoodApp) getActivity().getApplication()).setPictureTaken(null);
        boolean posted = ((DiscoverHoodApp) this.getActivity().getApplication()).isPostSended();

        DateFormat format = DateFormat.getDateInstance(1);
        format.setTimeZone(TimeZone.getDefault());
        gridpics = (GridView) v.findViewById(R.id.pics_grid);
        memoryCache = ((DiscoverHoodApp) getActivity().getApplication()).getGalleryMemoryCache();
        mStrings = new ArrayList<String>();
        Iswipe = ((DiscoverHoodApp) getActivity().getApplication()).getImagesSwipe();
        if (savedInstanceState == null && posted) {
            ArrayList<SDPublicationImage> pi = new ArrayList<SDPublicationImage>();
            adapter = new ImageLoadAdapter(getActivity(), pi);
            gridpics.setAdapter(adapter);
            gridpics.setSelection(mSelected);
            nopending.setVisibility(View.GONE);
            loadPublications(Iswipe);
        } else {
            ((DiscoverHoodApp) getActivity().getApplication()).clearImagesSwipe();
            ((DiscoverHoodApp) this.getActivity().getApplication()).postSended(false);

            if (Iposts != null) {
                ArrayList<SDPublicationImage> postaux = new ArrayList<SDPublicationImage>();
                adapter = new ImageLoadAdapter(getActivity(), postaux);
                gridpics.setAdapter(adapter);
                gridpics.setSelection(mSelected);
                addPublications(Iposts);
            } else {
                ((DiscoverHoodApp) getActivity().getApplication()).ClearImagePublications();
                Iposts = new ArrayList<SDPublicationImage>();
                adapter = new ImageLoadAdapter(getActivity(), Iposts);
                gridpics.setAdapter(adapter);
                gridpics.setSelection(mSelected);
                if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                } else {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                getMyPublicacions();
            }
        }
    }


    public void ShowErrorDialog(String msg) {
        final AlertDialog alertDialog = new AlertDialog.Builder(this.getActivity()).create();
        alertDialog.setTitle(getString(R.string.error));
        alertDialog.setMessage(msg);
        alertDialog.show();
    }

    public void showLoadingProgressDialog() {
        this.proD = ProgressDialog.show(this.getActivity(), "", getString(R.string.retrieving_images), true, false);

    }

    public void dismissProgressDialog() {
        if (this.proD != null) {
            this.proD.dismiss();
        }
    }

    private void getMyPublicacions() {
        showLoadingProgressDialog();
        new SDPublicationsGETMyImages(SD_Url, SD_Key, new SDConnectionResponseListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(String response, Object resultElement) {

                Iposts = (ArrayList<SDPublicationImage>) resultElement;
                if (!Iposts.isEmpty()) {
                    SDPublicationImage firstOne = Iposts.get(0);
                    String url = firstOne.getPicture().getPictureUrl();
                    memoryCache.put(url, LastestPicTaken);
                    addPublications(Iposts);
                }
                if (adapter.isEmpty()) {
                    nopending.setVisibility(View.VISIBLE);
                } else {
                    nopending.setVisibility(View.GONE);
                }
                dismissProgressDialog();
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }

            @Override
            public void onIssues(String issues) {
                dismissProgressDialog();
                ShowErrorDialog(getString(R.string.server_connection_error) + issues);
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }

            @Override
            public void onError(SDError error) {
                dismissProgressDialog();
                if (error.getReason() != null) {
                    if (error.getDetails() == null) {
                        ShowErrorDialog(error.getReason());
                    } else {
                        Enumeration<String> element = error.getDetails().elements();
                        while (element.hasMoreElements()) {
                            ShowErrorDialog(element.nextElement());
                        }
                    }
                } else {
                    ShowErrorDialog(getString(R.string.server_connection_error));
                }
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }
        });

    }

    private void addPublications(ArrayList<SDPublicationImage> publications) {
        ListIterator<SDPublicationImage> listItr = publications.listIterator();
        DateFormat format = DateFormat.getDateInstance(1);
        format.setTimeZone(TimeZone.getDefault()); //Seteo al timezone del usuario
        while (listItr.hasNext()) {
            SDPublicationImage publ = listItr.next();
            if (mStrings.isEmpty() || !mStrings.contains(publ.getPicture().getPictureUrl())) {
                Iswipe.add(publ);
                String url = publ.getPicture().getPictureUrl();
                mStrings.add(url);
                adapter.addItem(publ);
            }
        }
        adapter.notifyDataSetChanged();

    }

    private void loadPublications(ArrayList<SDPublicationImage> publications) {
        ListIterator<SDPublicationImage> listItr = publications.listIterator();
        DateFormat format = DateFormat.getDateInstance(1);
        format.setTimeZone(TimeZone.getDefault()); //Seteo al timezone del usuario
        while (listItr.hasNext()) {
            SDPublicationImage publ = listItr.next();
            if (mStrings.isEmpty() || !mStrings.contains(publ.getPicture().getPictureUrl())) {
                String url = publ.getPicture().getPictureUrl();
                mStrings.add(url);
                adapter.addItem(publ);
            }
        }
        adapter.notifyDataSetChanged();

    }


    private class ImageLoadAdapter extends BaseAdapter implements OnClickListener {

        private Activity activity;
        private ArrayList<SDPublicationImage> data;
        private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
        private DisplayImageOptions options;

        public ImageLoadAdapter(Activity a, ArrayList<SDPublicationImage> d) {

            activity = a;
            data = d;
            options = ImageLoaderHelper.getOptions();
            imageLoader = ImageLoaderHelper.getInstance(getActivity());
        }

        public int getCount() {
            return data.size();

        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public void addItem(SDPublicationImage pi) {
            data.add(pi);
            notifyDataSetChanged();

        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            ImageView imageView;

            if (convertView == null) {

                imageView = new ImageView(activity);

                imageView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 130));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

                } else {
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }

                imageView.setPadding(2, 2, 2, 2);

            } else {
                imageView = (ImageView) convertView;
            }

            SDPublicationImage post = (SDPublicationImage) data.get(position);
            String url = post.getPicture().getPictureThumbnailUrl();
            imageLoader.displayImage(url, imageView, options);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((DiscoverHoodApp) getActivity().getApplication()).postSended(true);
                    mSelected = (Integer) view.getTag();
                    notifyDataSetChanged();
                    mStrings.clear();
                    FragmentSwipeGallery fragment = new FragmentSwipeGallery();
                    Bundle args = new Bundle();
                    args.putInt("Iselected", mSelected);
                    fragment.setArguments(args);
                    ((ActivityMenu) activity).getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_frame, fragment)
                            .addToBackStack(getTag())
                            .commit();
                }
            });
            try {
                imageView.setTag(position);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return imageView;
        }

        @Override
        public void onClick(View arg0) {
        }
    }
}

		

		 
	
