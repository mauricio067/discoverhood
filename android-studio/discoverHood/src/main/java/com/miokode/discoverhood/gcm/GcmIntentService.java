package com.miokode.discoverhood.gcm;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.miokode.discoverhood.ActivityMain;
import com.miokode.discoverhood.ActivityNotificationBridge;
import com.miokode.discoverhood.ActivityShowPublication;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.SplashScreen;
import com.miokode.discoverhood.tools.NotificationsManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by darkosmoljo on 22/09/14.
 */
public class GcmIntentService extends IntentService {

    private static final String TAG = "GcmIntentService";

    public static final int NOTIFICATION_ID = 1;
    public static final String NEW_MESSAGES = "gcm.newmessages",NEW_PUB_NOTIFS="gc.newpubnotifs",NEW_FRIEND_REQUEST="gcm.newfriendreq";
    public static String EXTRA_NOTIF_TYPE = "NotifType";

    public GcmIntentService() {
        super("GcmIntentService");
    }
    private static ArrayList<Integer> ids=new ArrayList<Integer>();

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        Log.e("messageType", messageType);
        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            	processMessage(extras.getString("message"),extras.getString("request"),extras.getString("type"));
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void processMessage(String message, String auxId, String type) {
        if (message!=null) {
            try {
                sendNotification(message,auxId,Integer.valueOf(type));
            } catch (NumberFormatException e) {
                Log.w("DiscoverHood","Tipo de notificación inválida");
            }
        }
    }

    public static Intent getIntentForNotification(Context ctx, String id, int type){
        Intent intent=null;

        if (isProcessRuning(ctx)) {
            switch (type){
                case NotificationsManager.ACTION_SCS:
                    intent = new Intent(ctx, ActivityShowPublication.class);
                    intent.putExtra("ID", id);
                    break;
                case NotificationsManager.MESSAGE:
                    intent = new Intent(ctx, ActivityNotificationBridge.class);
                    intent.putExtra("M", String.valueOf(NotificationsManager.MESSAGE));
                    break;
                case NotificationsManager.FRIENDREQUEST:
                case NotificationsManager.NEWFRIEND:
                    intent = new Intent(ctx, ActivityNotificationBridge.class);
                    intent.putExtra("FR", String.valueOf(NotificationsManager.FRIENDREQUEST));
                    break;
            }
        } else {
            intent = new Intent(ctx, SplashScreen.class);
            switch (type){
                case NotificationsManager.ACTION_SCS:
                    intent.putExtra("ID", id);
                    break;
                case NotificationsManager.MESSAGE:
                    intent.putExtra("M", NotificationsManager.MESSAGE);
                    break;
                case NotificationsManager.FRIENDREQUEST:
                case NotificationsManager.NEWFRIEND:
                    intent.putExtra("FR", NotificationsManager.FRIENDREQUEST);
                    break;
            }
        }
        if(intent!=null)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    private void sendNotification(String msg, String auxId, int type) {
        final int notifId = String.valueOf(msg + auxId + type).hashCode();
        if(ids.contains(notifId))
            return;

        ids.add(notifId);

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = getIntentForNotification(this, auxId, type);
        intent.putExtra(EXTRA_NOTIF_TYPE,type);

        PendingIntent pendIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.logo_white_)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
                .setAutoCancel(true);

        builder.setContentIntent(pendIntent);
        notificationManager.notify(type==NotificationsManager.MESSAGE?type:notifId, builder.build());

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (type){
            case NotificationsManager.ACTION_SCS:
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(NEW_PUB_NOTIFS));
                break;
            case NotificationsManager.MESSAGE:
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(NEW_MESSAGES));
                break;
            case NotificationsManager.FRIENDREQUEST:
            case NotificationsManager.NEWFRIEND:
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(NEW_FRIEND_REQUEST));
                break;
        }
    }


    public static boolean isProcessRuning(Context ctx) {
        final DiscoverHoodApp app = (DiscoverHoodApp) ctx.getApplicationContext();
        return app!=null && app.getServerKey()!=null && app.getServerUrl()!=null && app.getUserDataAccount()!=null;
    }

    public static void clearStaticIds(){
        if(ids!=null) ids.clear();
    }

}
