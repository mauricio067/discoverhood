package com.miokode.discoverhood.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDGcmRegistrationPOST;

/**
 * Created by matias on 25/08/2015.
 */
public class GcmUtils {
    private static final String SENDER_ID="1000724144029";

    private Context ctx;
    private final String GCM_PREFERENCES="GcmPreferences";
    private final String REGISTRATION_ID="RegistrationId";
    private final String VERSION_OF_ID="VersionOfId";

    public GcmUtils(Context ctx) {
        this.ctx = ctx;
    }

    /**
     * Verifica si se posee un <b>registrationId</b> de GCM y si no es así se obtiene uno y se lo envia al servidor.
     * Si ya se obtuvo un <b>registrationId</b> pero aún no se subió al servidor, se intenta subirlo nuevamente.
     * Si se actualizó la aplicación (el version code es distinto) se vuelve a solicitar un registration id para evitar el error que GCM provoca al recibir un mensaje cuando se está actualizando.
     */
    public void configureGcmIfNecessary(){
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... strings) {
                SharedPreferences pref = ctx.getSharedPreferences(GCM_PREFERENCES, Context.MODE_PRIVATE);
                String registrationId = pref.getString(REGISTRATION_ID, "");
                int version = pref.getInt(VERSION_OF_ID,0);
                int currentVersion = getCurrentVersion();
                final String sent_to_server = getSentToServerKey();
                if(registrationId.length()==0 || version!= currentVersion) {
                    try {
                        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(ctx);
                        gcm.unregister();//nos aseguramos de que no queden reg id asociados
                        registrationId = gcm.register(SENDER_ID);
                        pref.edit().putString(REGISTRATION_ID, registrationId)
                                .putBoolean(sent_to_server, false)
                                .putInt(VERSION_OF_ID,currentVersion).commit();
                        Log.d("GCM Utils", "New Registration Id");
                        return registrationId;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else if(!pref.getBoolean(sent_to_server,false)){
                    return registrationId;
                }
                return "";
            }

            @Override
            protected void onPostExecute(String registrationId) {
                if(registrationId.length()>0){
                    sendRegistrationToServer(registrationId);
                }
            }
        }.execute();;
    }

    private String getSentToServerKey() {
        DiscoverHoodApp appData = (DiscoverHoodApp) ctx.getApplicationContext();
        String SENT_TO_SERVER = "SentToServer";
        return SENT_TO_SERVER + (appData.getUserDataAccount()!=null?appData.getUserDataAccount().getId():"");
    }

    private int getCurrentVersion() {
        try {
            PackageInfo pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Se envía al servidor privado el <b>registrationId</b> recibido de {@link GoogleCloudMessaging#register(String...)} si hay un usuario logueado.
     * @param registrationId recibido de GCM
     */
    private void sendRegistrationToServer(final String registrationId) {
        Log.d("GCM Utils","Sending Registration Id to Server");
        DiscoverHoodApp appData = (DiscoverHoodApp) ctx.getApplicationContext();
        String url = appData.getServerUrl();
        String key = appData.getServerKey();
        new SDGcmRegistrationPOST(url, key, new SDConnectionResponseListener() {
            @Override
            public void onSuccess(String response, Object resultElement) {
                ctx.getSharedPreferences(GCM_PREFERENCES, Context.MODE_PRIVATE).edit().putBoolean(getSentToServerKey(),true).commit();
            }

            @Override
            public void onIssues(String issues) {

            }

            @Override
            public void onError(SDError error) {

            }
        },registrationId, false);
    }

    /**
     * Se establece como 'No guardado' el registration id para poder subirlo cuando se loguee otro usuario. No se da de baja en GCM, se seguiran recibiendo mensajes.
     */
    public void unregister(){
        DiscoverHoodApp appData = (DiscoverHoodApp) ctx.getApplicationContext();
        final String url = appData.getServerUrl();
        final String key = appData.getServerKey();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(ctx);
                    gcm.unregister();
                    SharedPreferences pref = ctx.getSharedPreferences(GCM_PREFERENCES, Context.MODE_PRIVATE);
                    String regId = pref.getString(REGISTRATION_ID, "");
                    if (regId.length()>0) {
                        new SDGcmRegistrationPOST(url, key, new SDConnectionResponseListener() {
                            @Override
                            public void onSuccess(String response, Object resultElement) {

                            }

                            @Override
                            public void onIssues(String issues) {

                            }

                            @Override
                            public void onError(SDError error) {

                            }
                        }, regId, true);
                        pref.edit().putString(REGISTRATION_ID, "").putBoolean(getSentToServerKey(), false).commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
