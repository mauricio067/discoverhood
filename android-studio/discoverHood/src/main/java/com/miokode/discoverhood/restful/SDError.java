package com.miokode.discoverhood.restful;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

public class SDError {
	private int code;
	private String reason = null;
	private Dictionary<String, String> details = null;
	
	
	public SDError(int code, String reason) {
		this.code = code;
		this.reason = reason;
	}
	
	public SDError(int code, String reason, Dictionary<String, String> details) {
		this.code = code;
		this.reason = reason;
		this.details = details;
	}

	public SDError(String json ) {
		try{
			
			JSONObject obj = new JSONObject(json);
			this.code = obj.getInt("error");
			this.reason = obj.getString("reason");			 
			JSONObject objDetails = obj.getJSONObject("details");
			this.details = new Hashtable<String, String>();
	        Iterator<?> keys = objDetails.keys();
	        while( keys.hasNext() ){
	            String key = (String)keys.next();
	            if( objDetails.get(key) instanceof String ){
	            	this.details.put( key, (String)objDetails.get(key) );
	            }
	        }
		 } catch (JSONException e) {
			e.printStackTrace();
		 }

	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Dictionary<String, String> getDetails() {
		return details;
	}
	public void setDetails(Dictionary<String, String> details) {
		this.details = details;
	}

}
