package com.miokode.discoverhood.restful;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by matias on 03/09/2015.
 */
public class SDGcmRegistrationPOST extends SDConnection {

    private static final String URL_SERVICE_REGISTER = "createregid";
    private static final String URL_SERVICE_UNREGISTER = "unregisterregid";
    private final String registrationId;
    private final String urlService;

    public SDGcmRegistrationPOST(String server, String key, SDConnectionResponseListener respuestaListener, String registrationId, boolean unregister) {
        super(server, key, respuestaListener);
        this.registrationId = registrationId;
        this.urlService = unregister ? URL_SERVICE_UNREGISTER : URL_SERVICE_REGISTER;
        execute();
    }

    @Override
    protected void execute() {
        JSONObject json = new JSONObject();
        try {
            json.put("regid",registrationId);
            postJSON(urlService,json,respuestaListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void processResponse(String result) {
        if(result!=null){
            Log.d("GCM Post","Result: "+result);
            respuestaListener.onSuccess(result,null);
        }else{
            respuestaListener.onError(null);
        }
    }
}
