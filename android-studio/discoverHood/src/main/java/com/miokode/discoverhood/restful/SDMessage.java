package com.miokode.discoverhood.restful;

import org.json.JSONObject;

public class SDMessage extends SDObject {
	private String text;
	private String author;
	private boolean readed;

	
	/**
	 * Constructor solo utilizado para preparar el objeto antes de enviarlo al server
	 * 
	 * @param publication
	 */
	public SDMessage(String texto) {
		super();
		this.text = texto;
	}
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDMessage(JSONObject json){
		super(json);
		this.readed = getValueFromJSONBoolean("readed", true, json);
		this.text = getValueFromJSONString("text", "", json);
		//Para salvar espacio, solo se muestra el id del author, ya que el dato completo se encuentra en el campo people de la conversacion
		this.author = getValueFromJSONString("author", "", json);
		
		/*try {
			JSONObject objAvatar = json.getJSONObject("author");
			this.author = new SDAuthor(objAvatar);
		} catch (JSONException e) {
			this.author = null;
			e.printStackTrace();
		}*/
	}

	public void setAuthor(String autor){
		this.author = autor;
	}
	public String toString(){
		return "id: " + this.id + " text " +this.text;
	}
	
	/////////////////////////////
	// Getters and setters
	/////////////////////////////
	public String getText() {
		return text;
	}

	public String getAuthor() {
		return author;
	}

	public boolean isReaded() {
		return readed;
	}
	
	
}
