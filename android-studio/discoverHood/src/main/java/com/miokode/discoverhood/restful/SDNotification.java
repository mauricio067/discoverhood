package com.miokode.discoverhood.restful;

import java.util.Date;

import org.json.JSONObject;

public class SDNotification extends SDObject {
	private Date readedTime;
	private boolean readed;

	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDNotification(JSONObject json){
		super(json);
		this.readedTime = getValueFromJSONDate("readedTime", new Date(), json);
		this.readed = getValueFromJSONBoolean("readed", false, json);
	}
		
	/////////////////////////////
	// Getters and setters
	/////////////////////////////
	public Date getReadedTime() {
		return readedTime;
	}

	public boolean isReaded() {
		return readed;
	}
	
	public void setRead() {
		this.readed = true;
	}
	
}

