package com.miokode.discoverhood.restful;

import java.util.ArrayList;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDNotificationPOSTsetReaded extends SDConnection {
	private ArrayList<SDNotification> notifications;
	public enum NOTIFICATION_TYPE { PUBLICATION, CONVERSATION, FRIENDSHIP };
	private NOTIFICATION_TYPE notifType;
	
	public SDNotificationPOSTsetReaded(String server, String key, NOTIFICATION_TYPE notifType, ArrayList<SDNotification> notifications, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.notifications = notifications;
		this.notifType = notifType;
		execute();
	}

	@Override
	protected void execute() {
		JSONObject objup = new JSONObject();
		try {
			JSONArray objarr = new JSONArray() ;
			ListIterator<SDNotification> listItr = notifications.listIterator();
			int count = 0;
			while(listItr.hasNext()){
				SDNotification notif = listItr.next();
				JSONObject obj = new JSONObject();
		        obj.put("item"+count, notif.getId() );
		        objarr.put(obj);
				count++;
			}
			objup.put("notifications", objarr);
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		String url = "";
		switch (notifType) {
			case PUBLICATION:
				url = "publication/notifications/setreaded";
				break;
			case CONVERSATION:
				url = "conversation/notifications/setreaded";
				break;
			case FRIENDSHIP:
				url = "friendships/notifications/setreaded";
				break;
		}
		
		this.rpc = url;
		this.postJSON(url, objup, respuestaListener); 
		
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}


}