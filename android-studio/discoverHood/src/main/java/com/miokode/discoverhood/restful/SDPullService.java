package com.miokode.discoverhood.restful;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.miokode.discoverhood.restful.friends.SDFriendNotification;
import com.miokode.discoverhood.restful.friends.SDFriendRequester;
import com.miokode.discoverhood.restful.publication.SDPublicationNotification;

public class SDPullService extends SDConnection {
	
	public SDPullService(String server, String key, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		execute();
	}

	@Override
	protected void execute() {   
		this.rpc = "pullservice";
		this.get("pullservice", respuestaListener); 
	}
	

	@Override
	protected void processResponse( String result ) {
		////Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		
		if (!serverResponse.getMultipleItems()) {
			SDPullResponse reponse = new SDPullResponse();
			
			JSONObject resp = serverResponse.getItemResponse();
			
			try{
				JSONObject notifications = resp.getJSONObject("notifications");
				////Log.d("[notifications]", notifications.toString());
				JSONObject obj; 
				try {		
					obj = notifications.getJSONObject("publications");
					JSONArray items = obj.getJSONArray("items");
					ArrayList<SDPublicationNotification> publiArray = new ArrayList<SDPublicationNotification>();
					for(int n = 0; n < items.length(); n++) {
						publiArray.add( new SDPublicationNotification(items.getJSONObject(n)) );
					}
					reponse.setPublicationsNotifications(publiArray);
				} catch (JSONException e) {
					//Log.d("[jsonRetrieveError]", "publications notifications not found");
				}
				/*try {		
					obj = notifications.getJSONObject("messages");
					JSONArray items = obj.getJSONArray("items");
					ArrayList<SDConversationNotification> convArray = new ArrayList<SDConversationNotification>();
					for(int n = 0; n < items.length(); n++) {
						convArray.add( new SDConversationNotification(items.getJSONObject(n)) );
					}
					reponse.setConversationsNotifications(convArray);
				} catch (JSONException e) {
					//Log.d("[jsonRetrieveError]", "messages notifications not found");
				}*/
				try {		
					obj = notifications.getJSONObject("friends");
					JSONArray items = obj.getJSONArray("items");
					ArrayList<SDFriendNotification> friendArray = new ArrayList<SDFriendNotification>();
					for(int n = 0; n < items.length(); n++) {
						friendArray.add( new SDFriendNotification(items.getJSONObject(n)) );
					}
					reponse.setFriendshipsNotifications(friendArray);
				} catch (JSONException e) {
					//Log.d("[jsonRetrieveError]", "friends notifications not found");
				}
			} catch (JSONException e) {
				//Log.d("[jsonRetrieveError]", "notifications not found");
			}

			try{
				JSONObject obj = resp.getJSONObject("friendships_requests");
				JSONArray items = obj.getJSONArray("items");
				ArrayList<SDFriendRequester> friendRequestArray = new ArrayList<SDFriendRequester>();
				for(int n = 0; n < items.length(); n++) {
					friendRequestArray.add( new SDFriendRequester(items.getJSONObject(n)) );
				}
				reponse.setfrienshipsRequest(friendRequestArray);
			} catch (JSONException e) {
				//Log.d("[jsonRetrieveError]", "friendships_requests not found");
			}	
			respuestaListener.onSuccess(result, reponse);
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}