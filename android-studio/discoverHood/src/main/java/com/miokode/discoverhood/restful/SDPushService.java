package com.miokode.discoverhood.restful;

import java.util.ArrayList;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.tools.NotificationsManager;

public class SDPushService extends SDConnection {
	private ArrayList<String> publications;
	private ArrayList<String> publNotif ;
	private ArrayList<String> friendNotif ;
	private ArrayList<String> convNotif ;

	public static void setReaded(DiscoverHoodApp app,int notifType,String id){
		final ArrayList<String> ids = new ArrayList<String>();
		ids.add(id);
		new SDPushService(app.getServerUrl(), app.getServerKey(), null, notifType == NotificationsManager.ACTION_SCS ? ids : null,
                notifType == NotificationsManager.FRIENDREQUEST ? ids : null,
                notifType == NotificationsManager.MESSAGE ? ids : null,
                new SDConnectionResponseListener() {
                    @Override public void onSuccess(String response, Object resultElement) { }
                    @Override public void onIssues(String issues) { }
                    @Override public void onError(SDError error) { }
                });
	}
	
	public SDPushService(String server, String key, ArrayList<String> publications, ArrayList<String> publNotif, ArrayList<String> friendNotif, ArrayList<String> convNotif, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publications = publications;
		this.publNotif = publNotif;
		this.friendNotif = friendNotif;
		this.convNotif = convNotif;
		execute();
	}

	@Override
	protected void execute() {   
	    JSONObject obj = new JSONObject();
		boolean send = false;
    	try {
    		if (publications!= null && publications.size()>0){
				obj.put("toggleSmiles", processArrays(publications) );
				send = true;
			}
    		if (publNotif!= null && publNotif.size()>0){
				obj.put("notifpublications", processArrays(publNotif) );
				send = true;
			}
    		if (convNotif!= null && convNotif.size()>0){
				obj.put("notifmessages", processArrays(convNotif) );
				send = true;
			}
    		if (friendNotif!= null && friendNotif.size()>0){
				obj.put("notiffriendships", processArrays(friendNotif) );
				send = true;
			}
    		
    	} catch (JSONException e) {
    		//e.printStackTrace();
    	}
    	////Log.d("[pushsalida]", obj.toString());
		this.rpc = "pushservice";
		if(send)
			this.postJSON("pushservice", obj, respuestaListener);
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

	private JSONArray processArrays(ArrayList<String> array){
		JSONArray objarr = null;
		try {
			objarr = new JSONArray() ;
			ListIterator<String> listItr = array.listIterator();
			int count = 0;
			while(listItr.hasNext()){
				String publID = listItr.next();
				JSONObject obj = new JSONObject();
		        obj.put("item"+count, publID );
		        objarr.put(obj);
				count++;
			}
    	} catch (JSONException e) {
    		//e.printStackTrace();
    	}
		return objarr;
	}

}
