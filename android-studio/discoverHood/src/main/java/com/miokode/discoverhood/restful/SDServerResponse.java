package com.miokode.discoverhood.restful;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


public class SDServerResponse {
	private ArrayList<String> errores;
	//private ArrayList<String> items_responce;
	//private ArrayList<String> HATEOAS;
	private Boolean multipleItems = false; 
	private JSONArray items_response;
	private JSONObject item_response;
	private JSONObject HATEOAS;
	
	public SDServerResponse(String json){
			errores = new ArrayList<String>();
			item_response = null;
			items_response = null;
			HATEOAS = null;
			
			JSONObject obj = null;
			JSONArray objItems = null;
			try {
				obj = new JSONObject(json);
				//Determina si contenien arreglo o elemento simple
				try {
					objItems = obj.getJSONArray("items");
					multipleItems = true;
					items_response = objItems;
				}catch (JSONException e) {
					//e.printStackTrace();
					//Log.d("[jsonRetrieveError]", "items not found");
					multipleItems = false;
					item_response = obj;
				}
//				objItems = obj.getJSONArray("_items");
						
			/*			
				JSONObject objItem = obj.getJSONObject("item0");
				if (objItem.getString("status").compareTo("ERR") == 0 ){
					objAr = objItem.getJSONArray("issues");
					for(int n = 0; n < objAr.length(); n++) {
						errores.add(objAr.getString(n));
					}
				}
				*/
				
				
				//Process HATEOAS
//				items_response = obj.getJSONArray("_items");
//				HATEOAS = obj.getJSONObject("_links");
				/*multipleItems = false;
				item_response = obj;
				*/
				
			 } catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			 }
	}

	public Boolean getMultipleItems(){
		return multipleItems;
	}
	
	public ArrayList<String> getErrores() {
		return errores;
	}

	public JSONObject getItemResponse(){
		return item_response;
	}
	
	public JSONArray getItemsResponse() {
		return items_response;
	}

	public JSONObject getHATEOAS() {
		return HATEOAS;
	}

	
	
	
}
