package com.miokode.discoverhood.restful;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDTools {

	/**
	 * Procesa una fecha enviada por el servidor con un formato especifico
	 * @param fecha
	 * 		String con la fecha a parsear
	 * @return
	 * 		Date con la fecha correspondiente, null en caso de que el server no mande bien los datos
	 */
	public static Date processDateString(String fecha){
	    fecha.replaceAll("GMT", "");
	    SimpleDateFormat dateFormat = new SimpleDateFormat("c, d MMM yyyy H:m:s", Locale.US);
	    dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	    try {
	        return dateFormat.parse(fecha);
	    } catch (ParseException e) {
	    	//Log.d("[processDateString]", "Date parse error");
	    }
	    return null;
	}
	
	/**
	 * Dado un Date con una fecha, devuelve el String con el formato esperado por el server
	 * @param fecha
	 * @return
	 * 		String con el formato necesario
	 */
	public static String convertDate2String(Date fecha){
		SimpleDateFormat dateFormat = new SimpleDateFormat("c, d MMM yyyy H:m:s", Locale.US); 
		return dateFormat.format(fecha.getTime()) + " GMT";
	}
	
	/**
	 * Procesa los mensajes de error.
	 * Todo tienen la siguiente forma:
	 *     {'error' : 400, 'reason': 'No enougth data received', 'details' : [] }
	 * 
	 */
	public static ArrayList<String> procesarJSonErrorUnicoObjeto(String json){
		ArrayList<String> errores = new ArrayList<String>();
		JSONObject obj = null;
		JSONArray objAr = null;
		try {
			obj = new JSONObject(json.toString());
			JSONObject objItem = obj.getJSONObject("item0");
			if (objItem.getString("status").compareTo("ERR") == 0 ){
				objAr = objItem.getJSONArray("issues");
				for(int n = 0; n < objAr.length(); n++) {
					errores.add(objAr.getString(n));
				}
			}
		 } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return errores;
		 }
		return errores;
		
	}

    public static String getCantSmile(int cant){
    	return("2 smiles");
    }
    public static String getCantCommentCounter(int cant){
    	return("2 comments");
    }

}
