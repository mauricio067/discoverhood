package com.miokode.discoverhood.restful.account;

import java.util.Date;

import org.json.JSONObject;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import com.miokode.discoverhood.restful.SDImage;
import com.miokode.discoverhood.restful.SDObject;


/**
* @author Fernando Anthony Ristaño
*/
public class SDAccount extends SDObject {
	private String nickname;
	private String firstname;
	private String lastname;
	private String gender;
	private String email;
	private String password;
	private Date birthday;
	private boolean active;
    private boolean facebook_login;
	private boolean isFriend;
    private String facebook_code;
    
    private boolean online;
    private double distance;

	private SDImage avatar;
	private Bitmap bitmapAvatar;
	private boolean hideDate =false;
	
	
	/**
	 * Constructor solo utilizado para preparar el objeto antes de enviarlo al server con datos de facebook
	 *
	 * */	 
	public SDAccount( String firstname, String lastname, String gender, String email, Date birthday, Bitmap avatar, String facebook_code){
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.gender = gender;
		this.email = email;
		this.birthday = birthday;
		this.bitmapAvatar = avatar;
		this.facebook_login = true;
		this.facebook_code = facebook_code;
	}
	
	/**
	 * Constructor solo utilizado para preparar el objeto antes de enviarlo al server
	 *
	 * */	 
	public SDAccount( String firstname, String lastname, String gender, String email, String password, Date birthday, Bitmap avatar ){
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.gender = gender;
		this.email = email;
		this.password = password;
		this.birthday = birthday;
		this.bitmapAvatar = avatar;
		this.facebook_login = false;
	}
	
	

	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDAccount(JSONObject json){
		super(json);
		this.nickname = getValueFromJSONString("nickname", null, json);
		this.firstname = getValueFromJSONString("firstname", "", json);
		this.lastname = getValueFromJSONString("lastname", "", json);
		this.email = getValueFromJSONString("email","", json);
		this.facebook_login = getValueFromJSONBoolean("fb", false, json);
		this.gender = getValueFromJSONString("gender", "M", json);
		this.active = getValueFromJSONBoolean("active", false, json);
		this.birthday = getValueFromJSONDate("birthday", null, json);
		this.online = getValueFromJSONBoolean("online", false, json);
		this.distance = getValueFromJSONDouble("distance", 0, json);
		this.isFriend = getValueFromJSONBoolean("friend", false, json);
		this.hideDate = getValueFromJSONBoolean("vdate",false,json);
		
		try {
			JSONObject objAvatar = json.getJSONObject("avatar");
			this.avatar = new SDImage(objAvatar);
		} catch (Exception e) {
			e.printStackTrace();
			this.avatar = null;
		}
				
	}
	
	
	public String toString() {
		return "Id:[" + this.getId() + "] firstname:[" + this.getFirstname() + "] Lastname:[" + this.getLastname() + "] email:[" + this.getEmail() + "] active: " + this.getActive();
	}
	
	/////////////////////////////////
	// Getters and setters
	/////////////////////////////////
	public String getName(){
		if (nickname != null) {
			return this.nickname;
		}else{
			return this.firstname + " " + this.lastname;
		}
	}
	
	public String getNickname() {
		return nickname;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getGender() {
		return gender;
	}

	public String getEmail() {
		return email;
	}

	public String getFacebookCode() {
		return facebook_code;
	}

	public Date getBirthdayNotNull() {
		if(birthday==null)
			return new Date();
		else
			return birthday;
	}

	public void setHideDate(boolean hideDate) {
		this.hideDate = hideDate;
	}

	public boolean hideDate() {
		return hideDate;
	}

	public boolean getActive() {
		return active;
	}

	public boolean getFacebook_login() {
		return facebook_login;
	}

	public String getPassword() {
		return password;
	}

	public Bitmap getBitMapAvatar() {
		return bitmapAvatar;
	}
	
	public SDImage getAvatar() {
		return avatar;
	}

	public boolean isOnline() {
		return online;
	}


	public double getDistance() {
		return distance;
	}

	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}



	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}



	public void setLastname(String lastname) {
		this.lastname = lastname;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public boolean isFriend() {
		return isFriend;
	}

	public void setIsFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

	public void setBitmapAvatar(Bitmap bitmapAvatar) {
		this.bitmapAvatar = bitmapAvatar;
	}

//    public String getAvatar_id() {
//		return avatar_id;
//	}
//
//	public String getAvatarUrl() {
//		return avatarUrl;
//	}
//
//	public String getAvatarMD5() {
//		return avatarMD5;
//	}
//	
//    public String getAvatarThumbnailUrl() {
//		return avatarThumbnailUrl;
//	}
//
//	public String getAvatarThumbnailMD5() {
//		return avatarThumbnailMD5;
//	}
	
	
	
	
}



