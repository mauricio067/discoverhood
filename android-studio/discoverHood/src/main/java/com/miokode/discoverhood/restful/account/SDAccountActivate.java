package com.miokode.discoverhood.restful.account;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;

public class SDAccountActivate extends SDConnection {
	private Integer validationCode;
	
	public SDAccountActivate(String server, String key, Integer validationCode, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.validationCode = validationCode;
		execute();
	}

	@Override
	protected void execute() {
    	JSONObject obj = new JSONObject();
    	try {
	        obj.put("validation_code", validationCode );
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}    
		this.rpc = "activate_account";
		this.postJSON("activate_account", obj, respuestaListener);		
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

}
