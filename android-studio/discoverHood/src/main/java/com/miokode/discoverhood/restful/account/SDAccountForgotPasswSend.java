package com.miokode.discoverhood.restful.account;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;

public class SDAccountForgotPasswSend extends SDConnection {
	private String email;
	
	public SDAccountForgotPasswSend(String server, String key, String email, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.email = email;
		execute();
	}

	@Override
	protected void execute() {
        JSONObject obj = new JSONObject();
    	try {
	        obj.put("email", email );
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "forgot_pass";
		this.postJSON("forgot_pass", obj, respuestaListener);
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

	@Override
	protected void processError( Integer errorCode, String errorMessage, String result ) {
		//Log.d("[ONERROR]", errorCode.toString() + errorMessage.toString());
		try {
			JSONObject obj = new JSONObject(result);
			respuestaListener.onError(new SDError(errorCode, obj.getString("reason")));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respuestaListener.onError(new SDError(errorCode,"Data retrieve error"));
		}
	}
}
