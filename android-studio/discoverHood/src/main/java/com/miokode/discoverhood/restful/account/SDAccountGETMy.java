package com.miokode.discoverhood.restful.account;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDServerResponse;

public class SDAccountGETMy extends SDConnection {

	public SDAccountGETMy(String server, String key, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		execute();
	}

	@Override
	protected void execute() {
		this.get("account", respuestaListener);
	}

	@Override
	protected void processResponse( String result ) {
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		SDAccount account = null;
		if (!serverResponse.getMultipleItems()) {
			//Log.i("[account info]", serverResponse.getItemResponse().toString() );
			account = new SDAccount(serverResponse.getItemResponse());
			respuestaListener.onSuccess(result, account);
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}
}
