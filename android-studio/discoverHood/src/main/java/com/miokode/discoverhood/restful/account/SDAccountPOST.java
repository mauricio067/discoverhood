package com.miokode.discoverhood.restful.account;

import android.graphics.Bitmap;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDTools;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

public class SDAccountPOST extends SDConnection {
    private SDAccount account;

    public SDAccountPOST(String server, String key, SDAccount account, SDConnectionResponseListener respuestaListener) {
        super(server, key, respuestaListener);
        this.account = account;
        execute();
    }

    @Override
    protected void execute() {
        MultipartEntityBuilder entity = MultipartEntityBuilder.create();
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        String newFirsname = account.getFirstname().trim();
        String newLastname = account.getLastname().trim();
        String newPassword = account.getPassword().trim();
        try {
            newFirsname = new String(newFirsname.getBytes("UTF-8"), "ISO-8859-1");
            newLastname = new String(newLastname.getBytes("UTF-8"), "ISO-8859-1");
            newPassword = new String(newPassword.getBytes("UTF-8"), "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        entity.addTextBody("firstname", newFirsname);
        entity.addTextBody("lastname", newLastname);
        entity.addTextBody("password", newPassword);
        entity.addTextBody("email", account.getEmail().trim());
        entity.addTextBody("gender", account.getGender());
        entity.addTextBody("birthday", SDTools.convertDate2String(account.getBirthdayNotNull()));
        if(account.hideDate())
            entity.addTextBody("vdate","true");
        if (account.getBitMapAvatar() != null) {
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            account.getBitMapAvatar().compress(Bitmap.CompressFormat.PNG, 90, bao);
            byte[] data = bao.toByteArray();
            entity.addBinaryBody("avatar", data, ContentType.DEFAULT_BINARY, "avatar_" + account.getFirstname() + "_" + account.getLastname() + ".png");
        }
        if (account.getFacebook_login()) {
            entity.addTextBody("fb_code", account.getFacebookCode());
        }
        this.rpc = "create_account";
        this.postMultipart("create_account", entity, respuestaListener);
    }

    @Override
    protected void processResponse(String result) {
        respuestaListener.onSuccess(result, null);
    }


}