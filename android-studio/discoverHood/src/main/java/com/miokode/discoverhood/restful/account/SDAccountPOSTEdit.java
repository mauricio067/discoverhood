package com.miokode.discoverhood.restful.account;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import android.graphics.Bitmap;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDServerResponse;
import com.miokode.discoverhood.restful.SDTools;

public class SDAccountPOSTEdit extends SDConnection {
	private SDAccount edited;
	private SDAccount original;
	
	public SDAccountPOSTEdit(String server, String key, SDAccount original, SDAccount edited, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.original = original;
		this.edited = edited;
		execute();
	}

	@Override
	protected void execute() {
        MultipartEntityBuilder entity = MultipartEntityBuilder.create();
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        String newFirsname = edited.getFirstname().trim();
        String newLastname = edited.getLastname().trim();
        String newPassword = edited.getPassword().trim();
        String newNickname = edited.getNickname().trim();
        try {
            newFirsname = new String(newFirsname.getBytes("UTF-8"), "ISO-8859-1");
            newLastname = new String(newLastname.getBytes("UTF-8"), "ISO-8859-1");
            newPassword = new String(newPassword.getBytes("UTF-8"), "ISO-8859-1");
            newNickname = new String(newNickname.getBytes("UTF-8"), "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (edited.getNickname()!=null && edited.getNickname().length()>0 ){
        	entity.addTextBody("nickname",newNickname);
        }
        if (edited.getFirstname()!=null && edited.getFirstname().length()>0 && edited.getFirstname().compareTo(original.getFirstname()) != 0){
        	entity.addTextBody("firstname",newFirsname);
        }
        if (edited.getLastname()!=null && edited.getLastname().length()>0 && edited.getLastname().compareTo(original.getLastname()) != 0){
        	entity.addTextBody("lastname", newLastname);
        }

     	if (!edited.getFacebook_login()){
     		if (edited.getPassword()!=null && edited.getPassword().length()>0)
     			entity.addTextBody("password", newPassword);
            if (edited.getEmail()!=null && edited.getEmail().length()>0 && edited.getEmail().compareTo(original.getEmail()) != 0){
            	entity.addTextBody("email",edited.getEmail());
            }
     	}
     	
     	if (edited.getGender().compareTo(original.getGender()) != 0)
     		entity.addTextBody("gender", edited.getGender());
        
        Date dateOriginal = original.getBirthdayNotNull();
        Date dateEdited = edited.getBirthdayNotNull();
        if (dateOriginal.compareTo(dateEdited) != 0){
    		entity.addTextBody("birthday", SDTools.convertDate2String(edited.getBirthdayNotNull()));
        }

        if (edited.getBitMapAvatar() != null){ 
	        ByteArrayOutputStream bao = new ByteArrayOutputStream();
	        edited.getBitMapAvatar().compress(Bitmap.CompressFormat.PNG, 90, bao);
	        byte[] data = bao.toByteArray();
	        entity.addBinaryBody("avatar", data, ContentType.DEFAULT_BINARY,"avatar_"+edited.getFirstname()+"_"+edited.getLastname()+".png");
        }
        
		this.rpc = "account/edit";
		this.postMultipart("account/edit", entity, respuestaListener);
        
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		SDAccount account = null;
		if (!serverResponse.getMultipleItems()) {
			account = new SDAccount(serverResponse.getItemResponse());
			respuestaListener.onSuccess(result, account);
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}