package com.miokode.discoverhood.restful.account;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;

public class SDAccountRestorePass extends SDConnection {
	private Integer validation_code;
	private String password;
	private String email;
	
	public SDAccountRestorePass(String server, String key, String email, Integer validation_code, String password, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.validation_code = validation_code;
		this.password = password;
		this.email = email;
		execute();
	}

	@Override
	protected void execute() {
		JSONObject obj = new JSONObject();
    	try {
    		obj.put("email", this.email );
	        obj.put("validation_code", this.validation_code );
	   		obj.put("password", this.password );
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "restore_pass";
		this.postJSON("restore_pass", obj, respuestaListener);
		
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

}