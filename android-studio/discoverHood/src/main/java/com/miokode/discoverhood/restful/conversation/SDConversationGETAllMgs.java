package com.miokode.discoverhood.restful.conversation;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDMessage;
import com.miokode.discoverhood.restful.SDServerResponse;

public class SDConversationGETAllMgs extends SDConnection {
	private ArrayList<SDMessage> messages ;
	private String lastMessageID;
	private String conversationID;
	
	public SDConversationGETAllMgs(String server, String key, String conversationID, String lastMessageID, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.messages = new ArrayList<SDMessage>();
		this.lastMessageID = lastMessageID;
		this.conversationID = conversationID;
		execute();
	}


	@Override
	protected void execute() {		
		if (lastMessageID == null){
			this.get("conversation/" + conversationID + "/messages", respuestaListener);
		}else{
			this.get("conversation/" + conversationID + "/messages/" + lastMessageID, respuestaListener);
		}
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		if (messages == null) messages =  new ArrayList<SDMessage>();
		SDServerResponse serverResponse = new SDServerResponse(result.toString());

		
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					messages.add( new SDMessage(items.getJSONObject(n)) );
				}
				respuestaListener.onSuccess(result, messages);
			 } catch (JSONException e) {
					//e.printStackTrace();
					//Log.d("[jsonRetrieveError]",  "messages not found");
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}

}