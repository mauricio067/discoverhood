package com.miokode.discoverhood.restful.conversation;

import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDMessage;
import com.miokode.discoverhood.restful.SDNotification;

import org.json.JSONException;
import org.json.JSONObject;

public class SDConversationNotification extends SDNotification {
	private SDAuthor sendedby;
	private SDMessage message;
	private String conversationID;

	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDConversationNotification(JSONObject json){
		super(json);
		this.conversationID = getValueFromJSONString("conversation", "", json);
		try {
			this.message = new SDMessage(json.getJSONObject("message"));
			this.sendedby = new SDAuthor(json.getJSONObject("sendedby"));
		} catch (JSONException e) {
			//e.printStackTrace();
		}
	}
	
	public String toString(){
		return "id: " + this.id ;
	}

	/////////////////////////////
	// Getters and setters
	/////////////////////////////
	public SDAuthor getSendedby() {
		return sendedby;
	}

	public String getConversationID() {
		return conversationID;
	}

	public SDMessage getMessage() {
		return message;
	}
	
	
}
