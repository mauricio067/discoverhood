package com.miokode.discoverhood.restful.conversation;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class SDConversationPOSTAdd extends SDConnection {
    private String toUserID;
    private String text;

    public SDConversationPOSTAdd(String server, String key, String toUserID, String text, SDConnectionResponseListener respuestaListener) {
        super(server, key, respuestaListener);
        this.toUserID = toUserID;
        this.text = text;
        execute();
    }

    @Override
    protected void execute() {
        JSONObject obj = new JSONObject();
        String newText = text;
        try {
            newText = new String(text.getBytes("UTF-8"), "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            obj.put("to", toUserID);
            obj.put("msg", newText);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.rpc = "startNewConversation";
        this.postJSON("startNewConversation", obj, respuestaListener);
    }

    @Override
    protected void processResponse(String result) {
        //Log.d("[ONRESPONSE]",  result.toString());
        SDServerResponse serverResponse = new SDServerResponse(result.toString());
        if (!serverResponse.getMultipleItems()) {
            respuestaListener.onSuccess(result, new SDConversation(serverResponse.getItemResponse()));
        } else {
            respuestaListener.onError(new SDError(0, "Data retrieve error"));
        }
    }

}