package com.miokode.discoverhood.restful.conversation;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDMessage;
import com.miokode.discoverhood.restful.SDServerResponse;

import java.io.UnsupportedEncodingException;

public class SDConversationPOSTAddMessage extends SDConnection {
	private String text;
	private String conversationID;
	
	public SDConversationPOSTAddMessage(String server, String key, String text, String conversationID, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.text = text;
		this.conversationID = conversationID;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
        String newText = text;
        try {
            newText = new String(text.getBytes("UTF-8"), "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    	try {
	        obj.put("msg", newText );
	        obj.put("conversation", conversationID );
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "conversation/newMessage";
		this.postJSON("conversation/newMessage", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (!serverResponse.getMultipleItems()) {
			respuestaListener.onSuccess( result, new SDMessage(serverResponse.getItemResponse()) );
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}

}