package com.miokode.discoverhood.restful.favourites;

import com.miokode.discoverhood.restful.SDObject;
import com.miokode.discoverhood.restful.publication.SDPublication;

import org.json.JSONException;
import org.json.JSONObject;

public class SDFavorite extends SDObject {
	private SDPublication publication;
	
	/**
	 * Constructor solo utilizado para preparar el objeto antes de enviarlo al server
	 * 
	 * @param publication
	 */
	public SDFavorite( SDPublication publication) {
		super();
		this.publication = publication;
	}
		

	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDFavorite(JSONObject json){
		super(json);
		try{ 
			this.publication = new SDPublication(json.getJSONObject("publication"));
		} catch (JSONException e) {
			this.publication = null;
			e.printStackTrace();
		};
	}
	
	
	///////////////////////////
	//// Getters and setters
	///////////////////////////

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public SDPublication getPublication() {
		return publication;
	}
	public void setPublication(SDPublication publication) {
		this.publication = publication;
	}
	
}
