package com.miokode.discoverhood.restful.favourites;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.publication.SDPublication;
import com.miokode.discoverhood.restful.publication.SDPublicationCheckIn;
import com.miokode.discoverhood.restful.publication.SDPublicationImage;
import com.miokode.discoverhood.restful.SDServerResponse;

public class SDFavoritesGETList extends SDConnection {
	private ArrayList<SDPublication> publications ;
	private Integer year;
	private Integer month;
	
	public SDFavoritesGETList(String server, String key, Integer year, Integer month,  SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		System.out.println(year);
		System.out.println(month);
		System.out.println(year);
		System.out.println(month);
		this.publications = new ArrayList<SDPublication>();
		this.year = year;
		this.month = month;
		execute();
	}


	@Override
	protected void execute() {
		this.get("getFavorites/" + this.year.toString() + "/" + this.month.toString(), respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		if (publications == null) publications =  new ArrayList<SDPublication>();
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					JSONObject objItem = items.getJSONObject(n);
					String tipo = objItem.getString("type");
					if (tipo.compareTo("image") == 0) {
						publications.add( new SDPublicationImage(items.getJSONObject(n)) );
					}else if (tipo.compareTo("checkin") == 0) {
						publications.add( new SDPublicationCheckIn(items.getJSONObject(n)) );
					}else{
						publications.add( new SDPublication(items.getJSONObject(n)) );
					}
				}
				respuestaListener.onSuccess(result, publications);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}
}
