package com.miokode.discoverhood.restful.favourites;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;

public class SDFavoritesGETListDates extends SDConnection {
	private List<Calendar> dateGroups;
	
	public SDFavoritesGETListDates(String server, String key, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.dateGroups = new ArrayList<Calendar>(); 
		execute();
	}


	@Override
	protected void execute() {
		this.get("getListofDates", respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 	
		try {
			JSONObject obj = new JSONObject(result);
	        Iterator<?> keys = obj.keys();
	        JSONArray objitem;
	        while( keys.hasNext() ){
	            String key = (String)keys.next();
	           // //Log.d("[key]", key);
	            Calendar fechapublicacion;
	         //   if( obj.get(key) instanceof String ){
	            	objitem = obj.getJSONArray(key);
	            	////Log.d("[arrayitemshots]", objitem.toString());
	            	for(int n = 0; n < objitem.length(); n++) {
	            		//creo las fechas (mes año) y las agrego en GroupDate
	            		fechapublicacion = Calendar.getInstance();

	            		fechapublicacion.set(Integer.parseInt(key.toString()), objitem.getInt(n)-1, 14);
	            		////Log.d("[elgregorio]", fechapublicacion.toString());
	            		dateGroups.add(fechapublicacion);
					}
	           // }
	        }


	        respuestaListener.onSuccess(result, dateGroups);
		} catch (JSONException e) {
			e.printStackTrace();
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}	
	}

}
