package com.miokode.discoverhood.restful.favourites;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;

public class SDFavoritesPOSTRemove extends SDConnection {
	private String publicationID;
	
	public SDFavoritesPOSTRemove(String server, String key, String publicationID, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publicationID = publicationID;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("publication", publicationID );	        
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "removeFavorite";
		this.postJSON("removeFavorite", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

}