package com.miokode.discoverhood.restful.friends;

import com.miokode.discoverhood.restful.SDImage;
import com.miokode.discoverhood.restful.SDTools;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class SDFriend {
	private String id;
	private Date since;
	private String name;
	private SDImage avatar;
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDFriend(JSONObject json){
		this.id = getValueFromJSONString("_id", "", json);
	    this.since =  getValueFromJSONDate("since", new Date(), json);
	    this.name = getValueFromJSONString("name", "", json);
	    try {
			this.avatar = new SDImage(json.getJSONObject("avatar"));
		} catch (JSONException e) {
			e.printStackTrace();
		} 

	}

	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto.
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 * 
	 */
	private String getValueFromJSONString( String JsonTag, String defaultValue, JSONObject json ){
		try{ 
			return json.getString(JsonTag); 
		} catch (JSONException e) {
			e.printStackTrace();
			return defaultValue; 
		}
	}
	
	public SDFriend(String id, Date since, String name,SDImage avatar){
		this.id = id;
		this.since = since;
		this.name = name;
		this.avatar = avatar;
	}
	
	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 */
	protected Date getValueFromJSONDate( String JsonTag, Date defaultValue, JSONObject json ){
		try{ 
			return SDTools.processDateString(json.getString(JsonTag));
		} catch (JSONException e) {
			e.printStackTrace();
			return defaultValue;
		}
	}

	public String getId() {
		return id;
	}

	public Date getSince() {
		return since;
	}

	public String getName() {
		return name;
	}

	public SDImage getAvatar() {
		return avatar;
	}
	
	
}
