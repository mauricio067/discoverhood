package com.miokode.discoverhood.restful.friends;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDServerResponse;

public class SDFriendGETAll extends SDConnection {
	private ArrayList<SDFriend> friends ;

	
	public SDFriendGETAll(String server, String key, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.friends = new ArrayList<SDFriend>();
		execute();
	}


	@Override
	protected void execute() {
		this.get("friends", respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 

		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					friends.add( new SDFriend(items.getJSONObject(n)) );
				}
				respuestaListener.onSuccess(result, friends);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}

}