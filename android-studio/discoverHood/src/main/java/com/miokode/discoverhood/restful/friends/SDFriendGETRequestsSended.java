package com.miokode.discoverhood.restful.friends;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDServerResponse;

public class SDFriendGETRequestsSended extends SDConnection {
	private ArrayList<SDFriendRequested> requesterd ;

	
	public SDFriendGETRequestsSended(String server, String key, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.requesterd = new ArrayList<SDFriendRequested>();
		execute();
	}


	@Override
	protected void execute() {
		this.get("friendshipsrequestssended", respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					requesterd.add( new SDFriendRequested(items.getJSONObject(n)) );
				}
				respuestaListener.onSuccess(result, requesterd);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}
