package com.miokode.discoverhood.restful.friends;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;

public class SDFriendPOSTDelete extends SDConnection {
	private String friendID;
	
	public SDFriendPOSTDelete(String server, String key, String friendID, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.friendID = friendID;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("friend", friendID );	        
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "deletefriendship";
		this.postJSON("deletefriendship", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

}