package com.miokode.discoverhood.restful.friends;

import org.json.JSONException;
import org.json.JSONObject;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDServerResponse;

public class SDFriendPOSTProcess extends SDConnection {
	public enum Action { ACCEPT, REJECT};
	private Action action;
	private String requestID;
	
	public SDFriendPOSTProcess(String server, String key, String requestID, Action action, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.action = action;
		this.requestID = requestID;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("request", requestID );	   
	        switch (this.action) {
	            case ACCEPT:
	            	obj.put("action", "accept" );
	                break;
	                    
	            case REJECT:
	            	obj.put("action", "reject" );	
	                break;
	        }
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "processfriendshiprequest";
		this.postJSON("processfriendshiprequest", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		if (result.length() == 0) {
			respuestaListener.onSuccess(result, null);
		}else{
			SDServerResponse serverResponse = new SDServerResponse(result.toString());
			if (!serverResponse.getMultipleItems()) {
				JSONObject objItem = serverResponse.getItemResponse();
				respuestaListener.onSuccess(result, new SDFriend(objItem));
			}else{
				respuestaListener.onError(new SDError(0,"Data retrieve error"));
			}
		}
	}

}