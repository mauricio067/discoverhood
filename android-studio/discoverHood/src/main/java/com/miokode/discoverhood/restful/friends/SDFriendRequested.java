package com.miokode.discoverhood.restful.friends;

import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDObject;

import org.json.JSONException;
import org.json.JSONObject;

public class SDFriendRequested extends SDObject {
	private SDAuthor requested;
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDFriendRequested(JSONObject json){
		super(json);
		try {
			this.requested = new SDAuthor(json.getJSONObject("requested"));
		} catch (JSONException e) {
			e.printStackTrace();
		} 
	}

	public SDAuthor getRequested(){
		return requested;
	}
	
	
}
