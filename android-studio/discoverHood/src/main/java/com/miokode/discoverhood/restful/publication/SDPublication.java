package com.miokode.discoverhood.restful.publication;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDObject;

public class SDPublication extends SDObject {
	private String text;
	private double latitude;
	private double longitude;
	private Integer smilesCounter;
	private Integer commentsCounter;
	private Integer sharesCounter;
	private SDAuthor author;
	private boolean favorite;
	private boolean ismile;
	private boolean ishared;
	private boolean iComment;
	private SDAuthor to;
	
	
		
	/**
	 * Constructor solo utilizado para preparar el objeto antes de enviarlo al server
	 * 
	 * @param publication
	 */
	public SDPublication(String texto, double latitude, double longitude) {
		super();
		this.text = texto;
		this.latitude = latitude;
		this.longitude = longitude;
		this.smilesCounter=this.sharesCounter=this.commentsCounter=0;
		this.to = null;
	}

	/**
	 * Constructor solo utilizado para preparar el objeto antes de enviarlo al server
	 * Pensada para crear una publicacion en el feed de otro usuario
	 * 
	 * @param to
	 * 		ID del user donde postear la publicacion
	 */
	public SDPublication(String texto, double latitude, double longitude, String to) {
		super();
		this.text = texto;
		this.latitude = latitude;
		this.longitude = longitude;
		this.smilesCounter=this.sharesCounter=this.commentsCounter=0;
		this.to = new SDAuthor(to);
		////Log.d("[publication created]", "lat: " + Double.toString(latitude)+" longitude: " + Double.toString(longitude));
	}
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDPublication(JSONObject json){
		super(json);
		this.text = getValueFromJSONString("text", "", json);
		this.latitude = getValueFromJSONDouble("latitude", (double) 0, json );
		this.longitude = getValueFromJSONDouble("longitude", (double) 0, json );
		this.smilesCounter = getValueFromJSONInteger("smilesCounter", 0, json);
		this.commentsCounter = getValueFromJSONInteger("commentsCounter", 0, json);
		this.sharesCounter = getValueFromJSONInteger("sharesCounter", 0, json);
		this.favorite = getValueFromJSONBoolean("favorite", false, json);
		this.ismile = getValueFromJSONBoolean("ismile", false, json);
		this.ishared = getValueFromJSONBoolean("ishared", false, json);
		this.iComment = getValueFromJSONBoolean("iComment", false, json);
		this.author = null;
		try {
			JSONObject objAuthor = json.getJSONObject("author");
			this.author = new SDAuthor(objAuthor);
		} catch (JSONException e) {
			//Log.d("[jsonRetrieveError]", "author not found");
			//e.printStackTrace();
		}
		this.to = null;
		try {
			JSONObject objTo = json.getJSONObject("to");
			this.to = new SDAuthor(objTo);
		} catch (JSONException e) {
			//Log.d("[jsonRetrieveError]", "to not found");
			//e.printStackTrace();
		}
		////Log.d("[publication server]", "lat: " + Double.toString(latitude)+" longitude: " + Double.toString(longitude));
	}
	
	public String toString(){
		return "id: " + this.id + " texto " +this.text;
	}
	
	/////////////////////////////
	// Getters and setters
	/////////////////////////////
	public String getText() {
		return this.text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public double getLatitud() {
		return latitude;
	}
	public void setLatitud(double latitud) {
		this.latitude = latitud;
	}
	public double getLongitud() {
		return longitude;
	}
	public void setLongitud(double longitud) {
		this.longitude = longitud;
	}
	public int getSmilesCounter() {
		return smilesCounter;
	}
	public void setSmilesCounter(int smilesCounter) {
		this.smilesCounter = smilesCounter;
	}
	public int getCommentsCounter() {
		return commentsCounter;
	}
	public void setCommentsCounter(int commentsCounter) {
		this.commentsCounter = commentsCounter;
	}
	public int getSharesCounter() {
		return sharesCounter;
	}
	public void setSharesCounter(int sharesCounter) {
		this.sharesCounter = sharesCounter;
	}	
	public SDAuthor getAuthor() {
		return this.author;
	}
	public SDAuthor getTo() {
		return this.to;
	}
	public boolean isCrossPublication(){
		return this.to != null;
	}
	public boolean getFavorite() {
		return this.favorite;
	}
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}
    public void increaseSmiles(){
    	smilesCounter++;
    }
    public void increaseComments(){
    	commentsCounter++;
    }
    public void increaseShares(){
    	sharesCounter++;
    }
    public void decreaseSmiles(){
    	--smilesCounter;
    }
    public void decreaseComments(){
    	--commentsCounter;
    }
    public void decreaseShares(){
    	--sharesCounter;
    }
    public boolean getIsmile() {
		return this.ismile;
	}
    public boolean iComment(){
    	return this.iComment;
    }
    
    public void setIsmile(boolean b){
    	this.ismile=b;
    }
	public boolean isIshared() {
		return ishared;
	}
	public void setIshared(boolean ishared) {
		this.ishared = ishared;
	}
	public void setIComment(boolean icomment) {
		this.iComment = icomment;
	}
	
}
