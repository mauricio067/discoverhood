package com.miokode.discoverhood.restful.publication;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by matias on 08/09/2015.
 */
public class SDPublicationDeletePOST extends SDConnection {
    private String publicationID;

    public SDPublicationDeletePOST(String server, String key, String publicationID,  SDConnectionResponseListener respuestaListener) {
        super(server, key, respuestaListener);
        this.publicationID = publicationID;
        execute();
    }

    @Override
    protected void execute() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("publication", publicationID );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.rpc = "deletepublication";
        this.postJSON("deletepublication", obj, respuestaListener);
    }

    @Override
    protected void processResponse( String result ) {
        //Log.d("[ONRESPONSE]",  result.toString());
        respuestaListener.onSuccess(result, null);
    }
}
