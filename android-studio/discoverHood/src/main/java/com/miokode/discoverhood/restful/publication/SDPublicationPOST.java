package com.miokode.discoverhood.restful.publication;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import android.graphics.Bitmap;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;

public class SDPublicationPOST extends SDConnection {
	private SDPublication publication;
	
	public SDPublicationPOST(String server, String key, SDPublication publication, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publication = publication;
		execute();
	}

	@Override
	protected void execute() {
        
        MultipartEntityBuilder entity = MultipartEntityBuilder.create();
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE); 

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        if (publication instanceof SDPublicationImage){
        	entity.addTextBody("type", "image");
        	((SDPublicationImage) publication).getMapPicture().compress(Bitmap.CompressFormat.PNG, 90, bao);
	        byte[] data = bao.toByteArray();
            entity.addBinaryBody("image", data, ContentType.DEFAULT_BINARY,"picture_.png");
        }else if (publication instanceof SDPublicationCheckIn ){
        	entity.addTextBody("type", "checkin");
          	((SDPublicationCheckIn) publication).getMapPicure().compress(Bitmap.CompressFormat.PNG, 90, bao);
	        byte[] data = bao.toByteArray();
            entity.addBinaryBody("image", data, ContentType.DEFAULT_BINARY,"picture_.png");
        	
        }else{
        	entity.addTextBody("type", "text");
        }
        String newText =  publication.getText();
        try {
            newText = new String( publication.getText().getBytes( "UTF-8"),"ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        entity.addTextBody("text", newText);
     	entity.addTextBody("latitude", String.valueOf(publication.getLatitud()));
        entity.addTextBody("longitude", String.valueOf(publication.getLongitud()));

        
        if (publication.isCrossPublication()) {
        	entity.addTextBody("to", String.valueOf(publication.getTo().getId()));
        }
		this.rpc = "create_publication";
		this.postMultipart("create_publication", entity, respuestaListener);
		
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
		/*SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					JSONObject objItem = items.getJSONObject(n);
					String tipo = objItem.getString("type");
					if (tipo.compareTo("image") == 0) {
						publication = new SDPublicationImage(items.getJSONObject(n)) ;
					}else if (tipo.compareTo("checkin") == 0) {
						publication = new SDPublicationCheckIn(items.getJSONObject(n)) ;
					}else{
						publication = new SDPublication(items.getJSONObject(n)) ;
					}
				}
				respuestaListener.onSuccess(result, publication);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}*/
	}

}
