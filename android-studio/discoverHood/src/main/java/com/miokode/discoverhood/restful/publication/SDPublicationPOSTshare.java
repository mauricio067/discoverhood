package com.miokode.discoverhood.restful.publication;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;

public class SDPublicationPOSTshare extends SDConnection {
	private String publicationID;
	private double latitude;
	private double longitude;
	
	public SDPublicationPOSTshare(String server, String key, String publicationID, double latitude, double longitude,  SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publicationID = publicationID;
		this.latitude = latitude;
		this.longitude = longitude;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("publication", publicationID );	   
	        obj.put("latitude", latitude );	
	        obj.put("longitude", longitude );	
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "publication/share";
		this.postJSON("publication/share", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}
}