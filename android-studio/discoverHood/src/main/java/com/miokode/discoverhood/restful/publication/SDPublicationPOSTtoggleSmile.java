package com.miokode.discoverhood.restful.publication;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.widget.TableRow;
import android.widget.TextView;

import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.R;
import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.tools.ConnectionDialogs;

import java.util.Enumeration;

public class SDPublicationPOSTtoggleSmile extends SDConnection {
	private String publicationID;
	
	public SDPublicationPOSTtoggleSmile(String server, String key, String publicationID,  SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publicationID = publicationID;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("publication", publicationID );	   
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "toggleSmile";
		this.postJSON("toggleSmile", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

	/**
	 * Procesa el marcado de smile y envia al servidor la peticion
	 *  @param post
	 * @param cantS
	 * @param S
	 * @param runnable
	 */
	public static void smileClicked(SDPublication post,final TableRow rowSmile, final TextView cantS, final TextView S, final DiscoverHoodApp app, final Runnable runnable){
		final SDPublication publicacion = post;
        rowSmile.setBackgroundResource(R.color.focus_background);
        rowSmile.setClickable(false);
		new SDPublicationPOSTtoggleSmile(app.getServerUrl(),  app.getServerKey(), publicacion.getId(),  new SDConnectionResponseListener(){
			@Override
			public void onSuccess(String response, Object resultElement) {
                rowSmile.setBackgroundResource(R.color.transparente);
                rowSmile.setClickable(true);
				if(publicacion.getIsmile()){
					TextView cant = cantS;
					publicacion.decreaseSmiles();
					cant.setText(Integer.toString(publicacion.getSmilesCounter())+" "+app.getResources().getString(R.string.Smiles));
					TextView ST = S;
					ST.setTextColor(Color.DKGRAY);
					publicacion.setIsmile(false);

				}else{
					TextView cant = cantS;
					publicacion.increaseSmiles();
					cant.setText(Integer.toString(publicacion.getSmilesCounter())+" " + app.getResources().getString(R.string.Smiles));;
					TextView ST = S;
					ST.setTextColor(Color.MAGENTA);
					publicacion.setIsmile(true);
				}
				if(runnable !=null) runnable.run();
			}

			@Override
			public void onIssues(String issues) { }

			@Override
			public void onError(SDError error) {
                rowSmile.setBackgroundResource(R.color.transparente);
                rowSmile.setClickable(true);
				ConnectionDialogs dialogs = new ConnectionDialogs(app);
				if (error.getReason() != null ){
					if (error.getDetails() == null){
						dialogs.ShowErrorDialog(error.getReason());
					}else{
						Enumeration<String> element = error.getDetails().elements();
						while(element.hasMoreElements()){
							dialogs.ShowErrorDialog(element.nextElement());
						}
					}
				}else{
					dialogs.ShowErrorDialog(app.getResources().getString(R.string.server_connection_error));
				}
			}

		});
	}
}