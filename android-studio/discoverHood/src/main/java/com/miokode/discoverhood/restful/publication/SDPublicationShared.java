package com.miokode.discoverhood.restful.publication;

import org.json.JSONException;
import org.json.JSONObject;

public class SDPublicationShared extends SDPublication {
	private SDPublication shared_publication;

	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDPublicationShared(JSONObject json){
		super(json);
		try {
			JSONObject objclass = json.getJSONObject("shared_publication");
			String tipo = objclass.getString("type");
			if (tipo.compareTo("image") == 0) {
				this.shared_publication = new SDPublicationImage(objclass);
			}else if (tipo.compareTo("checkin") == 0) {
				this.shared_publication = new SDPublicationCheckIn(objclass);
			}else{
				this.shared_publication = new SDPublication(objclass);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public SDPublication getSharedPublication(){
		return shared_publication;
	}

}
