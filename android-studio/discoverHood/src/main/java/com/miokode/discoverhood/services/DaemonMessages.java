package com.miokode.discoverhood.services;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import com.miokode.discoverhood.DiscoverHoodApp;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.conversation.SDConversationNotification;
import com.miokode.discoverhood.restful.conversation.SDConversationsNotificationsGETAll;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDPushService;
import com.miokode.discoverhood.tools.NotificationsManager;


public class DaemonMessages extends Service implements LocationListener{
	
	private NotificationsManager ns;
	private updateNotificationAsyncTask NotificationTask;
	private String SD_Key;
	private String SD_Url;
	private boolean startDaemon = false;
    private ArrayList<String> notificationsConversationRead = new ArrayList<String>();
	
		
	@Override
    public IBinder onBind(Intent intent) {
        return null;
    }
		
	
    public class LocalBinder extends Binder {
    	DaemonMessages getService() {
            return DaemonMessages.this;
        }
    }
    
    
    @SuppressLint("NewApi")
	 @Override
	 public void onStart(Intent intent, int startId) {

		Bundle bundle = intent.getExtras();
		//Log.d("[Daemon]", bundle.toString());
		SD_Key = bundle.getString("SD_Key");
		SD_Url  = bundle.getString("SD_url");
		startDaemon = bundle.getBoolean("startDN");
		
		if(startDaemon)	initDaemon(SD_Key, SD_Url);
		else stopSelf();
		
	 }
	 
	
	@SuppressLint("NewApi")
	@Override
	 public int onStartCommand(Intent intent, int flags, int startId) {
	   
		Bundle bundle = intent.getExtras();
		SD_Key = bundle.getString("SD_Key");
		SD_Url  = bundle.getString("SD_url");
		startDaemon = bundle.getBoolean("startDN");

		if(startDaemon)	initDaemon(SD_Key, SD_Url);
		else stopSelf();
				 
		return START_NOT_STICKY;
	 }
	 
	 	 
	 public void onDestroy() {
		 super.onDestroy();
	}
	 
	 
	@SuppressLint("NewApi")
	private void initDaemon(String SD_Key, String SD_Url){
		this.SD_Key = SD_Key;
		this.SD_Url = SD_Url;
      
        ns = new NotificationsManager(this);
		
		NotificationTask = new updateNotificationAsyncTask();
			
		if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
            NotificationTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
       } else {
           NotificationTask.execute();
       }
	}
	 
	 
	class updateNotificationAsyncTask extends AsyncTask <Void, Void, Void> {
		 
	        @Override
	        protected Void doInBackground(Void... params) {
     	
	        	doPush();
	        	
	        	return null;
	        }

	        protected void onPostExecute(Boolean result){
	        	stopSelf();
	        }       
	 }
	 
	 private void doPull(){

		 new SDConversationsNotificationsGETAll(SD_Url, SD_Key, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {

				ArrayList<SDConversationNotification> notifArray = ((ArrayList<SDConversationNotification>) resultElement);

				if(notifArray.size()>0)	{
					ns.manageMessageNotifications(notifArray);
				}
				else {
					((DiscoverHoodApp) getApplicationContext()).setCantconversationNotifications(0);
				}
			}

			@Override
			public void onIssues(String issues) {
			}

			@Override
			public void onError(SDError error) {
			}	 	 
		 });  
	}
	 
		 
	private void doPush(){

    	notificationsConversationRead  = ((DiscoverHoodApp)getApplicationContext()).getNotificationsConversationRead();
		
		 new SDPushService(SD_Url, SD_Key, null, null, null, notificationsConversationRead, new SDConnectionResponseListener(){
			@Override
			public void onSuccess(String response, Object resultElement) {	
				notificationsConversationRead.clear();
				doPull();
			}

			@Override
			public void onIssues(String issues) {
				doPull();
			}

			@Override
			public void onError(SDError error) {
				doPull();
			}	 	 
		 });  
	}
	 
	
	public void addNotificationsConversationRead(String notifID) {
		 notificationsConversationRead.add(notifID);
	}
	 
	 
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}

	public void releaseBind(){
	    unbindService((ServiceConnection) this);
	}
	 
}
