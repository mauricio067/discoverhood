package com.miokode.discoverhood.tools;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.miokode.discoverhood.R;

public class ConnectionDialogs {
	private ProgressDialog dialogContainer;
	private Context context;
	
	public ConnectionDialogs( Context context ){
		this.context = context;
	}
	
    public void ShowErrorDialogIteractive( String msg, DialogInterface.OnClickListener OKlistener ){   	
		final Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("ERROR");    	   		
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton("OK", OKlistener);
		try {
			alertDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    public void ShowErrorDialog( String msg ){   	
		final Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("ERROR");    	   		
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {	
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();	  		
		    }
		});
		try {
			alertDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void ShowInfoDialog(String msg){
		ShowInfoDialog(msg,null);
	}
    
	public void ShowInfoDialog( String msg, DialogInterface.OnClickListener onAceptListener){
		final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setTitle("");    	   		
		dialog.setMessage(msg);
		dialog.setPositiveButton(R.string.aceptar,onAceptListener);
		try {
			dialog.create().show();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	    
	public void showLoadingProgressDialog(String text) {
		try {
			this.dialogContainer = ProgressDialog.show(context, "", text, true, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void dismissProgressDialog(){
		if (this.dialogContainer != null && this.dialogContainer.isShowing()) {
			try {
				this.dialogContainer.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
