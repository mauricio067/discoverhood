package com.miokode.discoverhood.tools;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by matias on 02/09/2015.
 */
public class ExceptionHandlerUtils {

    private static final String URL_SERVIDOR_LOGS = "http://matiaskoch.besaba.com/discoverhood/";
    private Context ctx;

    private Thread.UncaughtExceptionHandler androidDefaultUEH;

    public static void instantiate(Context ctx){
        new ExceptionHandlerUtils(ctx).uploadErrors();
    }

    private ExceptionHandlerUtils(Context ctx) {
        this.ctx = ctx;
        androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(handler);
    }

    private Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            String trace = getStackTrace(ex);
            PackageInfo pInfo = null;
            try {
                pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
                String textFile = "Version: "+pInfo.versionName+"\n";
                textFile += "Fecha: "+new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date())+"\n";
                textFile += "Exception: \n"+trace;
                writeToFile(textFile);
                uploadErrors();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            androidDefaultUEH.uncaughtException(thread, ex);
        }
    };

    private void uploadErrors() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File dir = new File(ctx.getFilesDir(),"logs");
                if(dir.exists()){
                    File[] files = dir.listFiles();
                    for(File f : files){
                        if(uploadLogFile(f)) f.delete();
                    }
                }
            }
        }).start();
    }

    private boolean uploadLogFile(File file){
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        String Tag="fSnd";
        try
        {
            FileInputStream fileInputStream = new FileInputStream(file);
            Log.d(Tag, "Starting Http File Sending to URL");
            // Open a HTTP connection to the URL
            HttpURLConnection conn = (HttpURLConnection)new URL(URL_SERVIDOR_LOGS + "/uploadlog.php").openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(lineEnd); dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + file.getName() +"\"" + lineEnd);
            dos.writeBytes(lineEnd);
            Log.d(Tag,"Headers are written");
            // create a buffer of maximum size
            int bytesAvailable = fileInputStream.available();

            int maxBufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);
            byte[ ] buffer = new byte[bufferSize];

            // read file and write it into form...
            int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0)
            {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable,maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0,bufferSize);
            }
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            fileInputStream.close();
            dos.flush();
            Log.d(Tag,"File Sent, Response: "+String.valueOf(conn.getResponseCode()));
            InputStream is = conn.getInputStream();
            int ch;
            StringBuffer b =new StringBuffer();
            while( ( ch = is.read() ) != -1 ){ b.append( (char)ch ); }
            String s=b.toString();
            Log.i("Response",s);
            dos.close();
            return s!=null && s.equals("1");
        }
        catch (MalformedURLException ex) {
            Log.e(Tag, "URL error: " + ex.getMessage(), ex);
        }
        catch (IOException ioe) {
            Log.e(Tag, "IO error: " + ioe.getMessage(), ioe);
        }
        return false;
    }

    private void writeToFile(String data) {
        try {
            data = data.replaceAll("\n","\r\n");
            File dir = new File(ctx.getFilesDir(),"logs");
            dir.mkdirs();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    new FileOutputStream(new File(dir,"log" +
                            new SimpleDateFormat("yyyy-MM-dd HH mm").format(new Date())+".txt")));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String getStackTrace(Throwable ex) {
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        return writer.toString();
    }
}
