package com.miokode.discoverhood.tools;

import android.content.Context;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by Mauricio on 17/02/2015.
 */
public class ImageLoaderHelper {
    private static com.nostra13.universalimageloader.core.ImageLoader imageLoader = null;
    private static DisplayImageOptions options;

    public static com.nostra13.universalimageloader.core.ImageLoader getInstance(Context context){
        if(imageLoader == null){
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .diskCacheExtraOptions(800, 800, null)
                    .denyCacheImageMultipleSizesInMemory()
                    .denyCacheImageMultipleSizesInMemory()
                    .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                    .memoryCacheSize(2 * 1024 * 1024)
                    .diskCacheSize(50 * 1024 * 1024)
                    .diskCacheFileCount(100)
                    .writeDebugLogs()
                    .build();
            imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
            imageLoader.init(config);
        }

        return imageLoader;
    }
    public static DisplayImageOptions getOptions(){
        if(options == null){
            options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
        }
        return options;
    }

}
