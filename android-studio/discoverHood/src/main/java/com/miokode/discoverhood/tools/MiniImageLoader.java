package com.miokode.discoverhood.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

public class MiniImageLoader {

    private MemoryCache memoryCache; 
    private FileCache fileCache;
    private int imageSize;

    
    public MiniImageLoader(Context context, int imageSize, MemoryCache memoryCache){
    	this.memoryCache=memoryCache;
    	fileCache = new FileCache(context);
        this.imageSize=imageSize;
    }
          
    public void ShowImage(String url, ImageView imageView){
    	   	
    	Bitmap bitmap = memoryCache.get(url);
        
        if(bitmap==null){
        	bitmap = getBitmap(url);
            memoryCache.put(url, bitmap);
        }
    	
            imageView.setImageBitmap(getBitmap(url));
	}

	private Bitmap getBitmap(String url){
        File f=fileCache.getFile(url);
        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;
        
        try {
             
            Bitmap bitmap=null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();
                      
            OutputStream os = new FileOutputStream(f);
                        
            Utils.CopyStream(is, os);
             
            os.close();
            conn.disconnect();
                         
            bitmap = decodeFile(f);
             
            return bitmap;
             
        } catch (Throwable ex){
           ex.printStackTrace();
           if(ex instanceof OutOfMemoryError)
               memoryCache.clear();
           return null;
        }
    }
 
    private Bitmap decodeFile(File f){
        try {
             
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o);
            stream1.close();
             
            final int REQUIRED_SIZE=imageSize;
             
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2 < REQUIRED_SIZE || height_tmp/2 < REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }
             
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
             
        } catch (FileNotFoundException e) {
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    

    private static class Utils {
        public static void CopyStream(InputStream is, OutputStream os){
            final int buffer_size=1024;
            try {
                 
                byte[] bytes=new byte[buffer_size];
                for(;;){
                    
                  int count=is.read(bytes, 0, buffer_size);
                  if(count==-1)
                      break;
                   
                  os.write(bytes, 0, count);
                }
            }
            catch(Exception ex){}
        }
    }
}
        