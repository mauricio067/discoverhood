package com.miokode.discoverhood.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by matias on 08/09/2015.
 */
public class Utils {

    /**
     * Lee el archivo <b>path</b> y genera un Bitmap asegurando que no sea mas grande que <b>reqWidth</b> ni <b>reqHeight</b>
     * @param path ruta al archivo de imagen
     * @param reqWidth ancho máximo que puede tener la imagen
     * @param reqHeight ancho mínimo que puede tener la imagen
     * @return Bitmap con la imagen redimensionada
     */
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight)
    { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;//RGB_565;
        int inSampleSize = 1;
        if(width>height){
            int aux = reqHeight;
            reqHeight = reqWidth;
            reqWidth = aux;
        }

        if (height > reqHeight)
        {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth)
        {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static Bitmap redimensionarImagen(Bitmap bitmap,int newWidth,int newHeight) {
        if(bitmap!=null && (bitmap.getWidth()>newWidth || bitmap.getHeight()>newHeight)){
            float scaleHeight = (float)newHeight / (float)bitmap.getHeight();
            float scaleWidth = (float)newWidth / (float)bitmap.getWidth();
            float scale = Math.min(scaleHeight, scaleWidth);
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth()*scale), (int) (bitmap.getHeight()*scale),true);
        }
        return bitmap;
    }
}
