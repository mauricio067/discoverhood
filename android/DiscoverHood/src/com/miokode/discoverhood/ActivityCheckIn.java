package com.miokode.discoverhood;

import java.util.Enumeration;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDPublicationCheckIn;
import com.miokode.discoverhood.restful.SDPublicationPOST;


public class ActivityCheckIn extends DialogFragmentActivity { 
	private GoogleMap map;
	private SupportMapFragment mMapFragment;
	private EditText post;
	private Bitmap MapCapture;
	private double latitude;
	private double longitude;
    private ImageView user_pic;
    private SDAccount account;
    private ImageLoader il;
    private MemoryCache avatarCache;
  
    
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_checkin);
       
	    ((DiscoverHoodApp)getApplicationContext()).setesN(false);	
    	
    	account = ((DiscoverHoodApp)getApplication()).getUserDataAccount();
        
        avatarCache = ((DiscoverHoodApp)getApplication()).getAvatarMemoryCache();
		
        user_pic = (ImageView)findViewById(R.id.user_p);
        
		il = new ImageLoader(this, 180, avatarCache);
        
        if(account.getAvatar()!=null){
        	String url = account.getAvatar().getPictureUrl().trim();
        	il.DisplayImage(url, user_pic);
        }
        			
		post=(EditText)findViewById(R.id.post);	

		if (savedInstanceState != null) {
        	String text = savedInstanceState.getString("text");
            post.setText(text);
		}
		
		post.setOnFocusChangeListener(new OnFocusChangeListener() {          
    		public void onFocusChange(View v, boolean hasFocus) {
    			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    		    imm.showSoftInput(post, InputMethodManager.SHOW_IMPLICIT);
    		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    			if (hasFocus){
    				post.setHint("");
    			}else{
    				post.setHint(getResources().getString(R.string.email));
    			}
            }
    	});
		
		TextView CancelPost= (TextView)findViewById(R.id.cancel_post);
		CancelPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	 onBackPressed();
            	 overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
            }
		});
		
		TextView SendPost = (TextView)findViewById(R.id.send_post);
		SendPost.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		  		((DiscoverHoodApp) getApplication()).postSended(true);
		  		SnapshotReadyCallback callback = new SnapshotReadyCallback() {
                    Bitmap bitmap;
                    @Override
                    public void onSnapshotReady(Bitmap snapshot) {
                    	bitmap = snapshot;
                    	try {
                    		MapCapture=bitmap;
                    		enviarDatosAlServer(MapCapture);
                    	} catch (Exception e) {
                           e.printStackTrace();
                    	}
                    }
				};
            map.snapshot(callback);
			}
		});
		ShowMyLocationMap();
    }
    
    
    public void onSaveInstanceState(Bundle bundle) {
	     super.onSaveInstanceState(bundle);
	     bundle.putString("text",post.getText().toString() );
    }
            	     
	private void ShowMyLocationMap(){
			 latitude = ((DiscoverHoodApp) this.getApplication()).getLatitude();
			 longitude = ((DiscoverHoodApp) this.getApplication()).getLongitude();
			
			 //mMapFragment = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map));
			 //map = mMapFragment.getMap();
			 
		      android.support.v4.app.FragmentManager myFragmentManager = getSupportFragmentManager();
	          SupportMapFragment mySupportMapFragment = (SupportMapFragment)myFragmentManager.findFragmentById(R.id.map);
	          
	     	  map = mySupportMapFragment.getMap();
	             if(map!=null)
	            map.setMyLocationEnabled(true);
			 
			 map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			 LatLng MyLocation=new LatLng(latitude,longitude);
			 map.addMarker(new MarkerOptions().position(MyLocation).title(this.getResources().getString(R.string.mylocation)));
			 map.moveCamera(CameraUpdateFactory.newLatLngZoom(MyLocation, 16));
			 map.setOnCameraChangeListener(new OnCameraChangeListener() {
				    @Override
					public void onCameraChange(CameraPosition arg0) {
						 if (arg0.zoom > 12){
							 map.animateCamera(CameraUpdateFactory.zoomTo(arg0.zoom ));
						 }
						 else{
							 map.animateCamera(CameraUpdateFactory.zoomTo(12));

						 }
					}
			 
			 });
			 
			 
	}

	private void enviarDatosAlServer(Bitmap MapCapture){
		 dialogs.showLoadingProgressDialog(getString(R.string.sending_post));
		 final SDPublicationCheckIn publication = new SDPublicationCheckIn(post.getText().toString(), latitude, longitude, MapCapture);
		 new SDPublicationPOST(SD_Url,  SD_Key, publication,  new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dialogs.dismissProgressDialog();
				finish();
				overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 }); 
	 }
	 
}
	
	
	
	
	

