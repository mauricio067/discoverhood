package com.miokode.discoverhood;

import android.support.v4.app.FragmentActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;


public class ActivityEnlargeAvatar extends FragmentActivity{

	private MemoryCache m;
    private String urlImage;
        
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	    
    	setContentView(R.layout.screen_avatar);
    	
	    ((DiscoverHoodApp)getApplicationContext()).setesN(false);	
    	
    	Intent intent = getIntent();
	    urlImage = intent.getStringExtra("url");
	    
	    m=((DiscoverHoodApp)getApplication()).getAvatarMemoryCache();
	  	           
	    MTImageView l_image = (MTImageView) findViewById(R.id.photo); 
	    l_image.setImageBitmap(m.get(urlImage));
       
	    TextView Xclose = (TextView) findViewById(R.id.close); 
       	Xclose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	finish();
            	
      	    }
        });
      
    }
    
       
}
