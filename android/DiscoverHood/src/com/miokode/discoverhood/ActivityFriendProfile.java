package com.miokode.discoverhood;

import java.util.ArrayList;
import android.app.ListActivity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityFriendProfile extends ListActivity{

	private MyCustomAdapter mAdapter;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        setContentView(R.layout.screen_friend_profile);
                     
        mAdapter = new MyCustomAdapter();
               
		final ListView list = getListView();

        LayoutInflater inflater = LayoutInflater.from(this);
        
        View mTop = inflater.inflate(R.layout.item_friend_profile, null);

        list.addHeaderView(mTop);
        
        addFeeds();
        
        setListAdapter(mAdapter);
           
        Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
        TextView pouallr= (TextView)findViewById(R.id.pou_all_rigths);
        TextView wap = (TextView) findViewById(R.id.text_post);

        pouallr.setTypeface(face);
        wap.setTypeface(face);

    }
 
    
    private void addFeeds() {
    	
    	long now = System.currentTimeMillis();
    	
    	CharSequence time = DateUtils.getRelativeTimeSpanString(now, DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_NO_NOON, 0);
	
    	mAdapter.addItem(new ItemFeed("nombre y apellido", time, "km away from you", getResources().getDrawable(R.drawable.discoverhood_logo), "Caminando...", 0, 0, 0));
		
    	mAdapter.addItem(new ItemFeed("nombre y apellido", time, "km away from you", getResources().getDrawable(R.drawable.discoverhood_logo), "Caminando...", 0, 0, 0));

    	mAdapter.addItem(new ItemFeed("nombre y apellido", time, "km away from you", getResources().getDrawable(R.drawable.discoverhood_logo), "Caminando...", 0, 0, 0));
    
	}


	private class MyCustomAdapter extends BaseAdapter {
 
        private ArrayList<ItemFeed> mData = new ArrayList<ItemFeed>();
        private LayoutInflater mInflater;
 
        public MyCustomAdapter() {
               mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
 
        public void addItem(final ItemFeed item) {
            mData.add(item);
            notifyDataSetChanged();
        }
 
        @Override
        public int getCount() {
            return mData.size();
        }
 
        @Override
        public ItemFeed getItem(int position) {
            return mData.get(position);
        }
 
        @Override
        public long getItemId(int position) {
            return position;
        }
 
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
           
        	ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.screen_friend_profile, null);
                holder = new ViewHolder();
                holder.user_name = (TextView)convertView.findViewById(R.id.profile_name);
                holder.date = (TextView)convertView.findViewById(R.id.online_offline);
                holder.distance = (TextView)convertView.findViewById(R.id.profile_birthday);
                holder.post = (TextView)convertView.findViewById(R.id.user_post);
                holder.smi = (TextView)convertView.findViewById(R.id.smile);
                holder.smis = (TextView)convertView.findViewById(R.id.smiles);
                holder.com = (TextView)convertView.findViewById(R.id.comment);
                holder.coms = (TextView)convertView.findViewById(R.id.comments);
                holder.sha = (TextView)convertView.findViewById(R.id.share);
                holder.shas = (TextView)convertView.findViewById(R.id.shares);
                holder.pic = (ImageView)convertView.findViewById(R.id.post_user_image);
                holder.pic_i = (ImageView)convertView.findViewById(R.id.i_smile);
                holder.pic_c = (ImageView)convertView.findViewById(R.id.i_comment);
                holder.pic_s = (ImageView)convertView.findViewById(R.id.i_share);

                convertView.setTag(holder);
            
            } else {
                holder = (ViewHolder)convertView.getTag();
              }
             
            holder.user_name.setText(mData.get(position).getUserName());
            holder.date.setText(mData.get(position).getDate());
            holder.distance.setText(mData.get(position).getDistance());
            holder.post.setText(mData.get(position).getPost());
            holder.smis.setText(mData.get(position).getNroSmiles()+" Smiles");
            holder.coms.setText(mData.get(position).getNroComments()+" Comments");
            holder.shas.setText(mData.get(position).getNroShares()+" Shares");
            holder.pic.setImageDrawable(getResources().getDrawable(R.drawable.discoverhood_logo));
            holder.pic_i.setImageDrawable(getResources().getDrawable(R.drawable.smile));
            holder.pic_c.setImageDrawable(getResources().getDrawable(R.drawable.comment));
            holder.pic_s.setImageDrawable(getResources().getDrawable(R.drawable.share));
          

            return convertView;
        }
    }
 
    public static class ViewHolder {
    	public TextView user_name;
    	
    	public TextView date;
    	
    	public TextView distance;
    	
    	public TextView post;
    	
    	public TextView add_fav;
    	
    	public TextView smi;
    	
    	public TextView smis;
    	
    	public TextView com;
    	
    	public TextView coms;
    	
    	public TextView sha;
    	
    	public TextView shas;
    	
    	public ImageView pic;
    	
    	public ImageView pic_i;
    	
    	public ImageView pic_c;
    	
    	public ImageView pic_s;
    	

    }
}