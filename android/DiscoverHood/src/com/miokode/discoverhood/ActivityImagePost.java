package com.miokode.discoverhood;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Enumeration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDPublication;
import com.miokode.discoverhood.restful.SDPublicationImage;
import com.miokode.discoverhood.restful.SDPublicationPOST;

@SuppressLint("CutPasteId")
public class ActivityImagePost extends FragmentActivity {
	
	private ProgressDialog proD = null;
	private String SD_Url;
	private String SD_Key;	
	private static int TAKE_PIC=1;
	private static int SELECT_PIC=2;
	private String name=" ";
    private EditText textoPublication;
    private ImageView user_pic;
    private ImageView image;
    private AlertDialog ImageDialog;
    private Bitmap picture = null;
    private double latitude;
    private double longitude;
    private SDAccount account;
    private ImageLoader il;
    private MemoryCache avatarCache;
	private Bitmap avatarImage;
	private String url;
    
	
    @SuppressLint("ResourceAsColor")
	protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);

    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_take_a_pic_post);
    	   
	    ((DiscoverHoodApp)getApplicationContext()).setesN(false);	

    	SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
    	SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
    	account = ((DiscoverHoodApp)getApplication()).getUserDataAccount();
        avatarCache = ((DiscoverHoodApp)getApplication()).getAvatarMemoryCache();
       
        textoPublication=(EditText)findViewById(R.id.post);

        user_pic = (ImageView)findViewById(R.id.user_p);
        
        il = new ImageLoader(this, 200, avatarCache);
      
        if (savedInstanceState != null) {
        	
        	picture = savedInstanceState.getParcelable("picture"); 
        	
        	String text = savedInstanceState.getString("text");
            textoPublication.setText(text);
            
            String url = savedInstanceState.getString("urlAvatar");
            il.DisplayImage(url, user_pic);
                      
            if(((DiscoverHoodApp)getApplication()).getInstanceBitmap()!=null){
         		avatarImage =  ((DiscoverHoodApp)getApplication()).getInstanceBitmap();
			    ImageView iv = (ImageView)findViewById(R.id.picture);
			    iv.setImageBitmap(avatarImage);
         	}
        }			
        else{
                
	        if(account.getAvatar()!=null){
	         	url = account.getAvatar().getPictureUrl().trim();
	         
	         	il.DisplayImage(url, user_pic);
	        }
        }
        
        @SuppressWarnings("unused")
		Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");

		
        image=(ImageView)findViewById(R.id.picture);
        image.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
             takeApicture();	
           
            }
         });
        
        textoPublication.setOnFocusChangeListener(new OnFocusChangeListener() {          
    		public void onFocusChange(View v, boolean hasFocus) {
    			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    		    imm.showSoftInput(textoPublication, InputMethodManager.SHOW_IMPLICIT);
    		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    			if (hasFocus){
    				textoPublication.setHint("");
    			}
    			else {
    				textoPublication.setHint(getResources().getString(R.string.email));
    			}
            }
    	   });
		TextView CancelPost= (TextView)findViewById(R.id.cancel_post);
		CancelPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	 onBackPressed();
            	 overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
            }
		});
		
		
		TextView SendPost= (TextView)findViewById(R.id.send_post);
		SendPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            		if(picture == null) ShowErrorDialog(getString(R.string.post_without_pic));
            		else{
        		  		((DiscoverHoodApp) getApplication()).postSended(true);
        		  		getMyLocation();
                    	enviarDatosAlServer(picture);
            		}
           }
            });
		  
	}
   
   public void onSaveInstanceState(Bundle bundle) {
	     super.onSaveInstanceState(bundle);
	     bundle.putString("text",textoPublication.getText().toString() );
	     bundle.putString("urlAvatar",url);
	     ((DiscoverHoodApp)getApplication()).setInstanceBitmap(avatarImage);
	     bundle.putParcelable("picture", (Parcelable)picture);
	     //Log.d("aferlegusta", "guardo los datos");
	}
 
      
      
   private void takeApicture(){
       	ImageDialog = new AlertDialog.Builder(this)
        .setTitle(getString(R.string.obtener_foto))
        .setMessage(getString(R.string.foto_option))
        .setPositiveButton(getString(R.string.galeria), new AlertDialog.OnClickListener() {
        	
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				fromGallery();
			}
		})
        .setNegativeButton(getString(R.string.camara), new AlertDialog.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	fromCamera();
	        }
        })
        
       .setNeutralButton(getString(R.string.cancel_take_photo), new AlertDialog.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	ImageDialog.dismiss();
	        	onBackPressed();
           	    overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
	        }
        })
        .create();
        ImageDialog.show();
          
   }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	
	  	    if (resultCode != Activity.RESULT_OK) {
		       return;
		    }else{
			if (requestCode == TAKE_PIC) {
				
				if (data != null) {
					if (data.hasExtra("data")) { 
						ImageView iv = (ImageView)findViewById(R.id.picture); 
	    				picture = (Bitmap) data.getParcelableExtra("data");
	    				avatarImage = Bitmap.createScaledBitmap(picture, 300, 300, true);
	                    iv.setImageBitmap(avatarImage);
	                    ((DiscoverHoodApp)this.getApplication()).setPictureTaken(picture);
	    		 				
					}
				}else{
				
				    ImageView iv = (ImageView)findViewById(R.id.picture);
				    picture = BitmapFactory.decodeFile(name);
    				avatarImage = Bitmap.createScaledBitmap(picture, 300, 300, true);
				    iv.setImageBitmap(avatarImage);
                    ((DiscoverHoodApp)this.getApplication()).setPictureTaken(picture);

				    new MediaScannerConnectionClient() {
						private MediaScannerConnection msc = null; {
							msc = new MediaScannerConnection(getApplicationContext(), this); msc.connect();
						}
						public void onMediaScannerConnected() { 
							msc.scanFile(name, null);
						}
						public void onScanCompleted(String path, Uri uri) { 
							msc.disconnect();
						} 
					};				
				}
				} else if (requestCode == SELECT_PIC){
					Uri selectedImage = data.getData();
					InputStream is;
					try {
						is= getContentResolver().openInputStream(selectedImage);
						BufferedInputStream bis = new BufferedInputStream(is);
						BitmapFactory.Options options=new BitmapFactory.Options();
						options.inSampleSize = 3;
						picture = BitmapFactory.decodeStream(bis,null,options);
						avatarImage = picture;
	    				ImageView iv = (ImageView)findViewById(R.id.picture);
			    		iv.setImageBitmap(picture);
	                    ((DiscoverHoodApp)this.getApplication()).setPictureTaken(picture);
			    		
					}catch (FileNotFoundException e) {}
		    	}
		   }
	   	   	    			 
	  }

	private void fromGallery(){
	   Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
	   int code = SELECT_PIC;
	   startActivityForResult(i, code);
	   
	}


	@SuppressWarnings("unused")
	private void fromCamera(){
	  Intent i= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	  Uri output = Uri.fromFile(new File(name));
	  int code = TAKE_PIC;
	  startActivityForResult(i, code);
	  
	}
 
	
	
	 private void getMyLocation(){
	    	
		 latitude = ((DiscoverHoodApp) this.getApplication()).getLatitude();
		 longitude = ((DiscoverHoodApp) this.getApplication()).getLongitude();
	}
	

	public void ShowErrorDialog( String msg ){   	
 		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
 		alertDialog.setTitle(getString(R.string.error));    	   		
 		alertDialog.setMessage(msg);
 		alertDialog.show();
	}
 
 
	public void showLoadingProgressDialog() {
		this.proD = ProgressDialog.show(this, "", getString(R.string.sending_post), true, false);
	}
	
	
	public void dismissProgressDialog(){
		if (this.proD != null ) {
			this.proD .dismiss();
		}
	}
 	
	private void enviarDatosAlServer(Bitmap picture){
		 showLoadingProgressDialog();
	 
		 final SDPublication publication = new SDPublicationImage( textoPublication.getText().toString(), latitude, longitude, picture);
		 
		  new SDPublicationPOST(SD_Url,  SD_Key, publication,  new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dismissProgressDialog();
								
				finish();
				
			}

			@Override
			public void onIssues(String issues) {
				 dismissProgressDialog();
				 ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				 dismissProgressDialog();
				 ActivityImagePost.this.proD.dismiss();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	
 	 
		 }); 
	 }
}





