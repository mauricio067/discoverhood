package com.miokode.discoverhood;

import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.AppEventsLogger;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDAccountGETMy;
import com.miokode.discoverhood.restful.SDAccountPOST;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;

public class ActivityMain extends FragmentActivity implements LocationListener {

	protected ConnectionDialogs dialogs;
	protected String SD_url = null;
	protected String SD_Key = null;
	private SDAccount account = null;
	private LocationManager lm;
	private Location location = null;
	private boolean canGetLocation = false;
	private boolean isGPSEnabled = false;
	private boolean isNetworkEnabled = false;
	private EditText txtPass, txtUserMail;
	private String SD_Url;
	private Activity test;
	// PARA FACEBOOOK!!!!
	private static final int SPLASH = 0;
	private static final int SELECTION = 1;
	private static final int FRAGMENT_COUNT = SELECTION + 1;
	private Fragment[] fragments = new Fragment[FRAGMENT_COUNT];
	private boolean isResumed = false;
	private GraphUser user;
	private Activity act;
	private LoginButton loginFBButton;
	private Session fbSession;
	private SessionManagement generalSession;
	private Builder alertDialog;
	private boolean showDialog = false;

	private final String PENDING_ACTION_BUNDLE_KEY = "com.miokode.discoverhood:PendingAction";

	private PendingAction pendingAction = PendingAction.NONE;

	private enum PendingAction {
		NONE, POST_PHOTO, POST_STATUS_UPDATE
	}

	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback statusCallback = new SessionStatusCallback();

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		generalSession = new SessionManagement(getApplicationContext());
		dialogs = new ConnectionDialogs(this);
		SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.screen_main);
		 try{
	            PackageInfo info = getPackageManager().getPackageInfo(
	                    "com.gogress.unremis", PackageManager.GET_SIGNATURES);
	            for (Signature signature : info.signatures) {
	                MessageDigest md = MessageDigest.getInstance("SHA");
	                md.update(signature.toByteArray());
	                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));

	            }
	        } catch (PackageManager.NameNotFoundException e) {
	        	e.printStackTrace();
	        } catch (NoSuchAlgorithmException e) {
	        	e.printStackTrace();
	        }
		((DiscoverHoodApp) this.getApplication()).setMainActivity(this);

		lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		ActivateGPS();
		getLocation();
		stopService(new Intent(this.getApplicationContext(),
				DaemonForTasks.class));
		stopService(new Intent(this.getApplicationContext(),
				DaemonMessages.class));

		uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);

		FragmentManager fm = getSupportFragmentManager();
		// fragments[SPLASH] = fm.findFragmentById(R.id.splashFragment);
		// fragments[SELECTION] = fm.findFragmentById(R.id.selectionFragment);

		FragmentTransaction transaction = fm.beginTransaction();
		for (int i = 0; i < fragments.length; i++) {
			transaction.hide(fragments[i]);
		}
		// transaction.commit();

		processFBbutton();

		if (generalSession.isLoggedIn() && !generalSession.isFacebookLoggedIn()) {
			HashMap<String, String> user = generalSession.getUserDetails();
			String SD_KEY = user.get(SessionManagement.SD_KEY);
			((DiscoverHoodApp) this.getApplication()).setServerKey(SD_KEY);
			Login(false);
		}

		txtPass = (EditText) findViewById(R.id.userpass);
		txtUserMail = (EditText) findViewById(R.id.usermail);
		/* aca definimos el tipo de fuente */
		Typeface face = Typeface
				.createFromAsset(getAssets(), "fonts/arial.ttf");
		Button logibot = (Button) findViewById(R.id.loginb);
		TextView npasw = (TextView) findViewById(R.id.newpass);
		TextView regist = (TextView) findViewById(R.id.registration);
		TextView termofuser = (TextView) findViewById(R.id.termofuse);
		TextView rightsde = (TextView) findViewById(R.id.rigths);

		txtPass.setTypeface(face);
		txtUserMail.setTypeface(face);
		logibot.setTypeface(face);
		npasw.setTypeface(face);
		regist.setTypeface(face);
		termofuser.setTypeface(face);
		rightsde.setTypeface(face);

		txtUserMail.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(txtUserMail, InputMethodManager.SHOW_IMPLICIT);
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				if (hasFocus) {
					txtUserMail.setHint("");
					txtUserMail.setBackgroundDrawable(getResources()
							.getDrawable(R.drawable.edittext_focus));

				} else {
					txtUserMail.setHint(getResources()
							.getString(R.string.email));
					txtUserMail.setBackgroundColor(getResources().getColor(
							R.color.white));

				}
			}
		});

		txtPass.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(txtPass, InputMethodManager.SHOW_IMPLICIT);
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				if (hasFocus) {
					txtPass.setHint("");
					txtPass.setBackgroundDrawable(getResources().getDrawable(
							R.drawable.edittext_focus));

				} else {
					txtPass.setHint(getResources().getString(R.string.password));
					txtPass.setBackgroundColor(getResources().getColor(
							R.color.white));

				}
			}
		});

		test = (Activity) this;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		restoreValue(savedInstanceState);
	}

	private void restoreValue(Bundle savedInstanceState) {
		if (savedInstanceState != null
				&& savedInstanceState.containsKey("SD_Key")) {
			SD_Key = savedInstanceState.getString("SD_Key");
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
		isResumed = false;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
		isResumed = true;
		// Call the 'activateApp' method to log an app event for use in
		// analytics and advertising reporting. Do so in
		// the onResume methods of the primary Activities that an app may be
		// launched into.
		AppEventsLogger.activateApp(this);
		updateUI();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private boolean ServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (DaemonForTasks.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private void startNextActivity() {
		if (account != null) {
			if ((((DiscoverHoodApp) this.getApplication()).CanGetLocation()) == false) {
				getLocation();
			} else {
				if (account.getActive()) {
					startNotificationService();
					startMessagesService();
					// Intent intent = new Intent(this,
					// DaemonGeolocalization.class);
					// startService(intent);
					generalSession.createLoginSession(((DiscoverHoodApp) this
							.getApplication()).getServerKey(), account
							.getFacebook_login());
					Intent i = new Intent(ActivityMain.this, ActivityMenu.class);
					startActivity(i);

				} else {
					startActivity(new Intent(ActivityMain.this,
							ActivityValidateCode.class));
				}
				overridePendingTransition(R.anim.slide_left_in,
						R.anim.slide_left_out);
			}
		} else {
			dialogs.ShowErrorDialog(getResources().getString(
					R.string.account_no_active));
		}
	}

	// ***********************************************************
	// ******************** Getting Location *******************
	// ***********************************************************

	private void getLocation() {
		try {
			isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
			isNetworkEnabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			if (!isGPSEnabled && !isNetworkEnabled) {
				if (showDialog) {
					// ActivateGPS();
					alertDialog.show();

				} else
					showDialog = true;

			} else {
				canGetLocation = true;
			}
			if (canGetLocation) {
				if (isNetworkEnabled) {
					lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
							0, 0, this);
					if (lm != null) {
						location = lm
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							((DiscoverHoodApp) this.getApplication())
									.setLatitude(location.getLatitude());
							((DiscoverHoodApp) this.getApplication())
									.setLongitude(location.getLongitude());
						}
					}
				} else {
					if (isGPSEnabled) {
						if (location == null) {
							lm.requestLocationUpdates(
									LocationManager.GPS_PROVIDER, 0, 0, this);
							if (lm != null) {
								location = lm
										.getLastKnownLocation(LocationManager.GPS_PROVIDER);
								if (location != null) {
									((DiscoverHoodApp) this.getApplication())
											.setLatitude(location.getLatitude());
									((DiscoverHoodApp) this.getApplication())
											.setLongitude(location
													.getLongitude());
								}
							}
						}
					}
				}

				if (location.getLatitude() != 0 && location.getLongitude() != 0) {
					canGetLocation = true;
					((DiscoverHoodApp) getApplication())
							.setCanGetLocation(canGetLocation);
				} else {
					canGetLocation = false;
					((DiscoverHoodApp) getApplication())
							.setCanGetLocation(canGetLocation);
				}
			} else {
				((DiscoverHoodApp) getApplication()).setLatitude(0);
				((DiscoverHoodApp) getApplication()).setLongitude(0);
				canGetLocation = false;
				((DiscoverHoodApp) getApplication())
						.setCanGetLocation(canGetLocation);
			}

		} catch (Exception e) {
			dialogs.ShowErrorDialog(getResources().getString(
					R.string.cannot_get_location));
			/*
			 * ((DiscoverHoodApp) getApplication()).setLatitude(0);
			 * ((DiscoverHoodApp) getApplication()).setLongitude(0);
			 * canGetLocation = false; ((DiscoverHoodApp)
			 * getApplication()).setCanGetLocation(canGetLocation);
			 */

		}

	}

	@Override
	public void onLocationChanged(Location location) {
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	private void ActivateGPS() {
		alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle(getString(R.string.gps_no_enabled));
		alertDialog.setMessage(getString(R.string.enable_gps));
		alertDialog.setPositiveButton(getString(R.string.accept_gps),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);
						dialog.cancel();
						finish();
					}
				});

		alertDialog.setNegativeButton(getString(R.string.no_accept_gps),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// alertDialog.show();
	}

	// ***********************************************************
	// ****************** Facebook process **********************
	// ***********************************************************

	private void processFBbutton() {
		// con estas 2 lineas pido permiso de datos.
		loginFBButton = (LoginButton) findViewById(R.id.loginButton1);
		// loginButton.setText("hola facebook");
		loginFBButton.setReadPermissions(Arrays.asList("user_location",
				"user_birthday", "user_likes", "user_subscriptions", "email"));
//		loginFBButton
//				.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
//					public void onUserInfoFetched(GraphUser usr) {
//						user = usr;
//						if (user != null) {
//							Date date = new Date();
//							if (user.asMap().get("locale").toString()
//									.contentEquals("es_LA")) {
//								DateFormat format = new SimpleDateFormat(
//										"MM/dd/yyyy");
//								try {
//									date = format.parse(user.getBirthday());
//								} catch (ParseException e) {
//									e.printStackTrace();
//								}
//							}
//							Bitmap imageprofile = getProfileImageFacebook(usr
//									.getId());
//							System.out.println(user.asMap().get("genre"));
//
//							account = new SDAccount(user.getFirstName(), user
//									.getLastName(),
//							/*
//							 * (user.asMap().get("genre").toString().compareTo(
//							 * "Male") == 0)? "M":
//							 */"F", user.asMap().get("email").toString(), date,
//									imageprofile,
//
//									usr.getId());
//
//							/* make the API call */
//							/*
//							 * RequestAsyncTask req = new Request( fbSession,
//							 * "/me/picture", null, HttpMethod.GET, new
//							 * Request.Callback() { public void
//							 * onCompleted(Response response) {
//							 * 
//							 * try {
//							 * 
//							 * GraphObject go = response.getGraphObject();
//							 * JSONObject jso = go.getInnerJSONObject();
//							 * JSONArray v = jso.getJSONArray("picture");
//							 * JSONObject j = v.getJSONObject(0);
//							 * 
//							 * } catch (JSONException e) { e.printStackTrace();
//							 * } Bitmap picture = null; } } ).executeAsync();
//							 */
//							imageprofile = null;
//							account.setPassword("lala2293954");
//
//						}
//						updateUI();
//						// It's possible that we were waiting for this.user to
//						// be populated in order to post a
//						// status update.
//						handlePendingAction();
//						if (account != null) {
//							((DiscoverHoodApp) getApplication())
//									SE CONECTO A FACEBOOK.setServerConnectionData(
//											account.getEmail(),
//											account.getFacebookCode());
//							Login(false);
//
//						}
//
//					}
//				});
	}

	private void updateUI() {
		Session session = Session.getActiveSession();
		boolean enableButtons = (user != null);
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (pendingAction != PendingAction.NONE
				&& (exception instanceof FacebookOperationCanceledException || exception instanceof FacebookAuthorizationException)) {
			new AlertDialog.Builder(ActivityMain.this)
					.setTitle("R.string.cancelled")
					.setMessage("R.string.permission_not_granted")
					.setPositiveButton("R.string.ok", null).show();
			pendingAction = PendingAction.NONE;
		} else if (state == SessionState.OPENED_TOKEN_UPDATED) {
			handlePendingAction();
		}
		updateUI();
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session seession, SessionState state,
				Exception exception) {
			onSessionStateChange(fbSession, state, exception);
			fbSession = seession;
		}
	};

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall,
				Exception error, Bundle data) {
			 Log.d("HelloFacebook", String.format("Error: %s",
			 error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall,
				Bundle data) {
			Log.d("HelloFacebook", "Success!");
			java.lang.System.out.println("SE CONECTO A FACEBOOK");
		}
	};

	private Bitmap getProfileImageFacebook(String idUserFacebook) {
		Bitmap picture = null;
		InputStream inputStream = null;

		try {
			inputStream = new URL("graph.facebook.com/" + idUserFacebook
					+ "/picture").openStream();
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

		picture = BitmapFactory.decodeStream(inputStream);
		return picture;
	}

	@SuppressWarnings("incomplete-switch")
	private void handlePendingAction() {
		PendingAction previouslyPendingAction = pendingAction;
		// These actions may re-set pendingAction if they are still pending, but
		// we assume they
		// will succeed.
		pendingAction = PendingAction.NONE;

		switch (previouslyPendingAction) {
		case POST_PHOTO:
			// postPhoto();
			break;
		case POST_STATUS_UPDATE:
			// postStatusUpdate();
			break;
		}
	}

	// ***********************************************************
	// ******************** Others process **********************
	// ***********************************************************

	private void showFragment(int fragmentIndex, boolean addToBackStack) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		for (int i = 0; i < fragments.length; i++) {
			if (i == fragmentIndex) {
				transaction.show(fragments[i]);
			} else {
				transaction.hide(fragments[i]);
			}
		}
		if (addToBackStack) {
			transaction.addToBackStack(null);
		}
		transaction.commit();
	}

	public void startUserRegistration(View v) {
		Intent i = new Intent(ActivityMain.this, ActivityRegister.class);
		startActivity(i);
		// finish();
		overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
	}

	public void getNewPassword(View v) {
		Intent i = new Intent(ActivityMain.this, ActivityNewPassword.class);
		startActivity(i);
		// finish();
		overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
	}

	public void seeTermsOfUse(View v) {
		startActivity(new Intent(ActivityMain.this, ActivityPrivacyTerms.class));
		// finish();
		overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
	}

	/*
	 * Realiza los chequeos estaticos de los campos y hace la conexion con el
	 * server
	 */
	public void LoginUser(View v) {
		if (this.DataVerification(txtUserMail.getText().toString(), txtPass
				.getText().toString())) {
			((DiscoverHoodApp) this.getApplication()).setServerConnectionData(
					txtUserMail.getText().toString(), txtPass.getText()
							.toString());
			Login(false);
		} else {
			dialogs.ShowErrorDialog(getResources().getString(
					R.string.invalidmail));
		}
	}

	/*
	 * Verifica que los campos esten completos y que el email tenga el formato
	 * correcto
	 */
	public boolean DataVerification(String email, String pass) {
		if (email.length() == 0 || pass.length() == 0 || !checkEmail(email)) {
			return false;
		} else
			return true;

	}

	/*
	 * Verifica que el email ingresado tenga el formato correcto
	 */
	public static boolean checkEmail(String email) {
		Pattern p = Pattern.compile("[-\\w\\.]+@\\w+\\.\\w+");
		Matcher m = p.matcher(email);
		return m.matches();
	}

	public void guardarUsuarioApp(SDAccount account) {
		((DiscoverHoodApp) this.getApplication()).setUserDataAccount(account);
		this.account = account;
	}

	// ***********************************************************
	// ************** Services initialization *******************
	// ***********************************************************

	private void startNotificationService() {
		final long REPEAT_TIME = 1000 * 20;
		AlarmManager service = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(this, DaemonForTasks.class);
		Bundle bundle = new Bundle();

		bundle.putString("SD_Key",
				((DiscoverHoodApp) this.getApplication()).getServerKey());

		bundle.putString("SD_url",
				((DiscoverHoodApp) this.getApplication()).getServerUrl());
		bundle.putBoolean("startDN", true);

		i.putExtras(bundle);
		PendingIntent pending = PendingIntent.getService(this, 0, i,
				PendingIntent.FLAG_UPDATE_CURRENT);
		Calendar cal = Calendar.getInstance();

		cal.add(Calendar.SECOND, 2);

		service.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				cal.getTimeInMillis(), REPEAT_TIME, pending);
	}

	private void startMessagesService() {
		final long REPEAT_TIME = 1000 * 20;
		AlarmManager service = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(this, DaemonMessages.class);

		Bundle bundle = new Bundle();
		bundle.putString("SD_Key",
				((DiscoverHoodApp) this.getApplication()).getServerKey());
		bundle.putString("SD_url",
				((DiscoverHoodApp) this.getApplication()).getServerUrl());

		bundle.putBoolean("startDN", true);

		i.putExtras(bundle);
		PendingIntent pending = PendingIntent.getService(this, 0, i,
				PendingIntent.FLAG_UPDATE_CURRENT);
		Calendar cal = Calendar.getInstance();

		cal.add(Calendar.SECOND, 2);

		service.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				cal.getTimeInMillis(), REPEAT_TIME, pending);

	}

	// ***********************************************************
	// ******************** Server connection *******************
	// ***********************************************************
	private void Login(final boolean fromRegister) {
		dialogs.showLoadingProgressDialog(getString(R.string.connecting));
		String SD_Key = ((DiscoverHoodApp) this.getApplication())
				.getServerKey();
		new SDAccountGETMy(SD_Url, SD_Key, new SDConnectionResponseListener() {

			@Override
			public void onSuccess(String response, Object resultElement) {
				dialogs.dismissProgressDialog();
				SDAccount account = (SDAccount) resultElement;
				guardarUsuarioApp(account);
				startNextActivity();
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				if (error != null && error.getCode() == 401) {
					if (account != null && account.getFacebook_login()) {
						if (fromRegister) {
							dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
						} else {
							registerFacebookUser();
						}
					} else {
						dialogs.ShowErrorDialog(getString(R.string.invalid_user_pass));
					}
				} else {
					dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				}
			}
		});
	}

	private void registerFacebookUser() {
		dialogs.showLoadingProgressDialog(getString(R.string.connecting));
		((DiscoverHoodApp) this.getApplication()).setServerConnectionData(
				account.getEmail(), account.getFacebookCode());
		String SD_Key = ((DiscoverHoodApp) this.getApplication())
				.getServerKey();
		new SDAccountPOST(SD_Url, SD_Key, account,
				new SDConnectionResponseListener() {

					@Override
					public void onSuccess(String response, Object resultElement) {
						dialogs.dismissProgressDialog();
						Login(true);
					}

					@Override
					public void onIssues(String issues) {
						dialogs.dismissProgressDialog();
						dialogs.ShowErrorDialog(issues);
					}

					@Override
					public void onError(SDError error) {
						dialogs.dismissProgressDialog();
						if (error.getReason() != null) {
							if (error.getDetails() == null) {
								dialogs.ShowErrorDialog(error.getReason());
							} else {
								Enumeration<String> element = error
										.getDetails().elements();
								while (element.hasMoreElements()) {
									dialogs.ShowErrorDialog(element
											.nextElement());
								}
							}
						} else {
							dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
						}
					}
				});
	}

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if (session.isOpened()) {
				// make request to the /me API
				Request.newMeRequest(session, new Request.GraphUserCallback() {

					// callback after Graph API response with user object
					@Override
					public void onCompleted(GraphUser usr, Response response) {
						
						user = usr;
						if (user != null) {
							Date date = new Date();
							if (user.asMap().get("locale").toString()
									.contentEquals("es_LA")) {
								DateFormat format = new SimpleDateFormat(
										"MM/dd/yyyy");
								try {
									date = format.parse(user.getBirthday());
								} catch (ParseException e) {
									e.printStackTrace();
								}
							}
							Bitmap imageprofile = getProfileImageFacebook(usr
									.getId());
							System.out.println(user.asMap().get("genre"));
 
							account = new SDAccount(user.getFirstName(), "kobylanski",
							/*
							 * (user.asMap().get("genre").toString().compareTo(
							 * "Male") == 0)? "M":
							 */"F", user.asMap().get("email").toString(), date,
									imageprofile,
 
									usr.getId());
 
							/* make the API call */
							/*
							 * RequestAsyncTask req = new Request( fbSession,
							 * "/me/picture", null, HttpMethod.GET, new
							 * Request.Callback() { public void
							 * onCompleted(Response response) {
							 * 
							 * try {
							 * 
							 * GraphObject go = response.getGraphObject();
							 * JSONObject jso = go.getInnerJSONObject();
							 * JSONArray v = jso.getJSONArray("picture");
							 * JSONObject j = v.getJSONObject(0);
							 * 
							 * } catch (JSONException e) { e.printStackTrace();
							 * } Bitmap picture = null; } } ).executeAsync();
							 */
							//imageprofile = null;
							account.setPassword("lala2293954");
 
						}
						updateUI();
						// It's possible that we were waiting for this.user to
						// be populated in order to post a
						// status update.
						handlePendingAction();
						if (account != null) {
							((DiscoverHoodApp) getApplication()).setServerConnectionData(
											account.getEmail(),
											account.getFacebookCode());
							Login(false);
 
						}
						
//						if (user != null) {
//							
//							Log.i("LoginFacebook", "Hello " + user.getName()
//									+ "!");
//							Log.i("LoginFacebook",
//									"Firstname " + user.getFirstName() + "!");
//							Log.i("LoginFacebook",
//									"Lastname " + user.getLastName() + "!");
//							Log.i("LoginFacebook",
//									"Birthday " + user.getBirthday() + "!");
//							Log.i("LoginFacebook",
//									"Email " + user.getProperty("email") + "!");
//						}

					}

				}).executeAsync();
			}
		}
	}

}
