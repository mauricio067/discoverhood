package com.miokode.discoverhood;

import com.slidingmenu.lib.SlidingMenu;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.WindowManager;




public class ActivityMenu extends ActivityMenuBase{
	
	private Fragment mContent, friendreq, geo, notify, frag;
	
	public ActivityMenu(){
		super(R.string.app_name);
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		//Log.d("[activityMenu]", "Created");
		super.onCreate(savedInstanceState);
		stopService(new Intent(this.getApplicationContext(),DaemonGeolocalization.class));	    
		((DiscoverHoodApp) this.getApplication()).setActivitMenu(this);

		startGeoService();

	   // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	    getSupportActionBar().setHomeButtonEnabled(true);
	    getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionBar_background)));
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
         
	    setContentView(R.layout.screen_menu_content_frame);
	    
	    boolean esN = ((DiscoverHoodApp) this.getApplication()).getesN();	
	    
	    if (savedInstanceState != null){
	    	mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
	    	((DiscoverHoodApp) this.getApplication()).setesN(true);	
	    }
	    	
    	Bundle extras = this.getIntent().getExtras();
    	
    	
    	if (esN && extras != null){
    		if (extras.containsKey("M"))
    			mContent = new FragmentListConversation();
    		if (extras.containsKey("FR"))
    			mContent = new FragmentFriend();
    	}
    	
	    if (mContent == null){
	    	mContent = new FragmentGeneralFeeds();
	    	((DiscoverHoodApp) this.getApplication()).setesN(true);	
	    }
    		    
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.content_frame, mContent).commit();
    	
    	
		setBehindContentView(R.layout.screen_menu_frame);
		FragmentLeftMenu FLM = new FragmentLeftMenu();
		((DiscoverHoodApp) this.getApplicationContext()).setFragmentLeftMenu(FLM);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame, FLM).commit();
		
		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		setSlidingActionBarEnabled(true);
		
		friendreq = new DialogFriendRequest();
		getSupportFragmentManager().beginTransaction()
		.replace(R.id.content_framefriendicon, friendreq).commit();

		geo = new DialogRecentActivity();
		getSupportFragmentManager().beginTransaction()
		.replace(R.id.content_framegeoicon, geo).commit();

		notify = new DialogMessages();
		getSupportFragmentManager().beginTransaction()
		.replace(R.id.content_framemessageicon, notify).commit();

		
		 if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
			getSlidingMenu().setBehindWidth(600);
		 }else{

			 int width = getScreenWidth();
		     getSlidingMenu().setBehindWidth(width-35);
		 }
		 
	}

	private void startGeoService(){
		Bundle bundle = new Bundle();
		 bundle.putString("SD_Key",((DiscoverHoodApp) this.getApplication()).getServerKey());
		 bundle.putString("SD_url", ((DiscoverHoodApp)this.getApplication()).getServerUrl());	    
		 
		 Intent intent = new Intent(this, DaemonGeolocalization.class);

		 intent.putExtras(bundle);	    		    
		 
		 this.startService(intent);
		 
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		getSupportFragmentManager().putFragment(outState, "mContent", mContent);
	}
	
	public void switchContent(Fragment fragment){
	   
		mContent = fragment;
		getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.content_frame, fragment)
		.commit();
		getSlidingMenu().showContent();
	}
	
	public void switchFragment(Fragment fragment){
		   
		mContent = fragment;
		
		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, Fragment.instantiate(ActivityMenu.this, fragment.getClass().getName()))
		.commitAllowingStateLoss(); 
		getSlidingMenu().showContent();
	}
			
		
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	/**
	 * Obtengo el ancho de la pantalla del dispositivo
	 */
	private int getScreenWidth(){
		
		int sw = 320;
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1){
				WindowManager wm = (WindowManager) getSystemService(ActivityMenu.WINDOW_SERVICE);
				Display display = wm.getDefaultDisplay();
				Point size = new Point();
				display.getSize(size);
				sw = size.x;
			} else{
    			Display display = getWindowManager().getDefaultDisplay(); 
    		  	sw = display.getWidth();
			}
			
		return sw;
  }

	
   public void startSearchingLocation(){
		startActivity(new Intent(ActivityMenu.this, ActivitySearchingLocation.class));
        overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );     

	}
}

