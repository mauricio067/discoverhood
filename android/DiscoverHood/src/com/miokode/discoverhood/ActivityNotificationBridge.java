package com.miokode.discoverhood;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class ActivityNotificationBridge extends Activity{
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_menu_frame);

    	Bundle extras = this.getIntent().getExtras();
    	       		
    	   	if(extras.containsKey("M")) {
    	   		Intent intente = new Intent(ActivityNotificationBridge.this, ActivityMenu.class);
    	   		
    	   		intente.putExtra("M", extras.getString("M"));
    	   		startActivity(intente);
    		}
    		
    	   	if(extras.containsKey("FR")){
    	   		Intent intente = new Intent(ActivityNotificationBridge.this, ActivityMenu.class);
    	   		
    	   		intente.putExtra("FR", extras.getString("FR"));
    	   		startActivity(intente);
    	   	}
	
    	}
}

