package com.miokode.discoverhood;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class ActivityPrivacyTerms extends Activity {
	 private TextView termCond, policyUse;
	 private ViewSwitcher switcher;
	 private View termsview, privacyview;
	
    @SuppressLint("CutPasteId")
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.screen_privacy_terms);
       
        if (customTitleSupported) {
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar_back);
        }
        termCond = (TextView) findViewById(R.id.user_genre);
        policyUse = (TextView) findViewById(R.id.post_distance);
        switcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        termsview=(View)findViewById(R.id.scrollView1);
        privacyview=(View)findViewById(R.id.scrollView2);
		policyUse.setTextColor(Color.parseColor("#cccccc"));

        Button backButton = (Button)findViewById(R.id.backbutton);
        backButton.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
           
        	  onBackPressed();
          }
        });
        
    /*aca definimos el tipo de fuente*/
        Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
	    TextView text1 = (TextView)findViewById(R.id.text_scroll1);
	    TextView text2 = (TextView)findViewById(R.id.text_scroll2);
	    TextView pouallr = (TextView)findViewById(R.id.pou_all_rigths);
	    
	    termCond.setTypeface(face);
	    policyUse.setTypeface(face);
	    text1.setTypeface(face);
	    text2.setTypeface(face);
	    pouallr.setTypeface(face);
	    
	    /*Solo para los botones*/
	   //backButton.setTypeface(face);    
         
	   termCond.setOnClickListener(new OnClickListener(){
   	    @Override
   	    public void onClick(View v){
   	    	if (switcher.getCurrentView() != termsview) {
		    		termCond.setTextColor(Color.parseColor("#FFFFFF"));
		    		policyUse.setTextColor(Color.parseColor("#cccccc"));
		       		policyUse.setTypeface(null, Typeface.NORMAL);
		       		termCond.setTypeface(null, Typeface.BOLD);
		    		switcher.showPrevious(); 
		        }
   	    	
   	    }
	   });
      
   
	   policyUse.setOnClickListener(new OnClickListener(){
	   	public void onClick(View v){
	   		if (switcher.getCurrentView() != privacyview){  
			   		termCond.setTextColor(Color.parseColor("#cccccc"));
			   		policyUse.setTextColor(Color.parseColor("#FFFFFF"));    		
			   		policyUse.setTypeface(null, Typeface.BOLD);
			   		termCond.setTypeface(null, Typeface.NORMAL);
			    	switcher.showNext();
	
		    }
	   	}
	   });
 }

     
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }


}