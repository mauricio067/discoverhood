package com.miokode.discoverhood;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDAccountGETMy;
import com.miokode.discoverhood.restful.SDAccountPOST;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;

public class ActivityRegister extends Activity {
	private ConnectionDialogs dialogs;
	private String SD_Url;
	private String name = " ";
	private int day,month, year;
	static final int DATE_DIALOG_ID = 999;
	private EditText firstname, lastname, pass1, pass2, mail;
	private TextView bd, genre;
	private static int TAKE_PIC=1;
	private static int SELECT_PIC=2;
	private GregorianCalendar born = null;
	private ImageView avatar;
	private AlertDialog ImageDialog =null;
	@SuppressWarnings("unused")
	private Drawable imagen=null;
	private Bitmap avatarImage; 
	private String birthday;
	private String ForM;
	@SuppressWarnings("unused")
	private Uri output;
    private Bitmap picture = null;


	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
        this.dialogs = new ConnectionDialogs(this);
        final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.screen_register);
        if (customTitleSupported) {
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar_back);
        }
                 
        name = Environment.getExternalStorageDirectory() + "/Discover_Hood_Avatar.png";
        firstname = (EditText) findViewById(R.id.user_first_name);   
        lastname = (EditText) findViewById(R.id.user_last_name);
		pass1 = (EditText)findViewById(R.id.user_password);
     	pass2 = (EditText)findViewById(R.id.user_password_confirm);
     	mail = (EditText)findViewById(R.id.user_email);
     	bd = (TextView)findViewById(R.id.user_birthday);
	    avatar = (ImageView) findViewById(R.id.user_pic);
        avatar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	getPhoto();	
           
            }
         });
	        	
     	genre = (TextView)findViewById(R.id.user_genre);
     	
        if (savedInstanceState != null) {
            super.onRestoreInstanceState(savedInstanceState);
            birthday = savedInstanceState.getString("birthday");
            ForM = savedInstanceState.getString("genre");
         	genre.setText(ForM);
         	bd.setText(birthday);
         	if(((DiscoverHoodApp)getApplication()).getInstanceBitmap()!=null){
         		avatarImage =  ((DiscoverHoodApp)getApplication()).getInstanceBitmap();
			    ImageView iv = (ImageView)findViewById(R.id.user_pic);
			    iv.setImageBitmap(avatarImage);
         	}
         				
        }else{
        	  birthday = getResources().getString(R.string.birthday);
              ForM = getResources().getString(R.string.genre);
           	  genre.setText(ForM);
           	  bd.setText(birthday);
        }
     	
     	
     	
     	
        Button backButton = (Button)findViewById(R.id.backbutton);
                    
	    Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
	    Button signup = (Button)  findViewById(R.id.sign_up);
	    TextView textpic = (TextView)findViewById(R.id.text_pic1);
	    TextView textview2 = (TextView)findViewById(R.id.post_distance);
	    TextView pouallr = (TextView)findViewById(R.id.pou_all_rigths);
	    pass1.setTypeface(face);
	    pass2.setTypeface(face);
	    mail.setTypeface(face);
	    bd.setTypeface(face);
	    firstname.setTypeface(face);
	    lastname.setTypeface(face);
	    genre.setTypeface(face);
	    signup.setTypeface(face);
	    textpic.setTypeface(face);
	    textview2.setTypeface(face);
	    pouallr.setTypeface(face);
        firstname.setTypeface(face);
        lastname.setTypeface(face);
  	  	backButton.setTypeface(face);   
  	  
        backButton.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
        	  onBackPressed();
          }
        });
                
                
        final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
                    
		bd.setOnClickListener(new OnClickListener() {
 			@SuppressWarnings("deprecation")
			@Override
		public void onClick(View v) {
 
		 showDialog(DATE_DIALOG_ID);
		}
 
		});
		
		bd.setOnFocusChangeListener(new OnFocusChangeListener() {          
			@SuppressWarnings("deprecation")
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.showSoftInput(bd, InputMethodManager.SHOW_IMPLICIT);
			    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				if (hasFocus){
					showDialog(DATE_DIALOG_ID);
					bd.setHint("");
				}
				else {
					
				}
			}
		});
		

		genre.setOnClickListener(new OnClickListener() {
 			@Override
		public void onClick(View v) {
 			chooseGenre(v);
		}
 		});
		
		
		genre.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				
				if (hasFocus){
		 			chooseGenre(v);
					genre.setHint("");
				}
				else {
				}
			}
		});
		
	  
	  firstname.setOnFocusChangeListener(new OnFocusChangeListener() {          
		public void onFocusChange(View v, boolean hasFocus) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.showSoftInput(firstname, InputMethodManager.SHOW_IMPLICIT);
		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
			if (hasFocus){
				firstname.setHint("");
				firstname.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
			}
			else {
				firstname.setHint(getResources().getString(R.string.first_name));
				firstname.setBackgroundColor(getResources().getColor(R.color.white));

			}
        }
  	  });  	  
	  
	  lastname.setOnFocusChangeListener(new OnFocusChangeListener() {          
		public void onFocusChange(View v, boolean hasFocus) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.showSoftInput(lastname, InputMethodManager.SHOW_IMPLICIT);
		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
			if (hasFocus){
				lastname.setHint("");
				lastname.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
			}
			else {
				lastname.setHint(getResources().getString(R.string.last_name));
				lastname.setBackgroundColor(getResources().getColor(R.color.white));
			}
        }
      });  
	  
	  mail.setOnFocusChangeListener(new OnFocusChangeListener() {          
		public void onFocusChange(View v, boolean hasFocus) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.showSoftInput(mail, InputMethodManager.SHOW_IMPLICIT);
		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
			if (hasFocus){
				mail.setHint("");
				mail.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
			}
			else {
				mail.setHint(getResources().getString(R.string.email));
				mail.setBackgroundColor(getResources().getColor(R.color.white));
			}
       }
   });  
	  
	  pass1.setOnFocusChangeListener(new OnFocusChangeListener() {          
		public void onFocusChange(View v, boolean hasFocus) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.showSoftInput(pass1, InputMethodManager.SHOW_IMPLICIT);
		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
			if (hasFocus){
				pass1.setHint("");
				pass1.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
			}
			else {
				pass1.setHint(getResources().getString(R.string.password));
				pass1.setBackgroundColor(getResources().getColor(R.color.white));
			}
		}
	  });
	  
	  pass2.setOnFocusChangeListener(new OnFocusChangeListener() {          
		public void onFocusChange(View v, boolean hasFocus) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.showSoftInput(pass2, InputMethodManager.SHOW_IMPLICIT);
		    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
			if (hasFocus){
				pass2.setHint("");
				pass2.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
			}
			else {
				pass2.setHint(getResources().getString(R.string.confirm_password));
				pass2.setBackgroundColor(getResources().getColor(R.color.white));

			}
		}
		});	  
		  
	}
	    	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    	onBackPressed();
	    	dialogs.dismissProgressDialog();
	    }
	    return super.onKeyDown(keyCode, event);
	}
		
   public void getCode(View v){
        startActivity(new Intent(this, ActivityValidateCode.class));
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
   }


   private void getPhoto(){  	   	
    	ImageDialog = new AlertDialog.Builder(this)
        .setTitle(getString(R.string.obtener_foto))
        .setMessage(getString(R.string.foto_option))
        .setPositiveButton(getString(R.string.galeria), new AlertDialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				fromGallery();
			}
        })

        .setNegativeButton(getString(R.string.camara), new AlertDialog.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	fromCamera();
	        }
        })
        
       .setNeutralButton(getString(R.string.cancel_take_photo), new AlertDialog.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	ImageDialog.dismiss();
	         }
        })
        .create();
        ImageDialog.show();
                       
    }
	
   
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	
	  	    if (resultCode != Activity.RESULT_OK) {
		       return;
		    }else{
			if (requestCode == TAKE_PIC) {
				
				if (data != null) {
					if (data.hasExtra("data")) { 
						ImageView iv = (ImageView)findViewById(R.id.user_pic); 
	    				picture = (Bitmap) data.getParcelableExtra("data");
	    				avatarImage = Bitmap.createScaledBitmap(picture, 400, 400, true);
	                    iv.setImageBitmap(avatarImage);
	    		 				
					}
				}else{
				
					ImageView iv = (ImageView)findViewById(R.id.user_pic); 
				    picture = BitmapFactory.decodeFile(name);
    				avatarImage = Bitmap.createScaledBitmap(picture, 400, 400, true);
				    iv.setImageBitmap(avatarImage);

				    new MediaScannerConnectionClient() {
						private MediaScannerConnection msc = null; {
							msc = new MediaScannerConnection(getApplicationContext(), this); msc.connect();
						}
						public void onMediaScannerConnected() { 
							msc.scanFile(name, null);
						}
						public void onScanCompleted(String path, Uri uri) { 
							msc.disconnect();
						} 
					};				
				}
				} else if (requestCode == SELECT_PIC){
					Uri selectedImage = data.getData();
					InputStream is;
					try {
						is= getContentResolver().openInputStream(selectedImage);
						BufferedInputStream bis = new BufferedInputStream(is);
						BitmapFactory.Options options=new BitmapFactory.Options();
						options.inSampleSize = 2;
						picture=BitmapFactory.decodeStream(bis,null,options);
						ImageView iv = (ImageView)findViewById(R.id.user_pic); 
			    		iv.setImageBitmap(picture);
			    		
					}catch (FileNotFoundException e) {}
		    	}
		   }
	   	   	    			 
	  }
    
    private void fromGallery(){
 	   Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
 	   int code = SELECT_PIC;
 	   startActivityForResult(i, code);
 	   
 	}


 	@SuppressWarnings("unused")
 	private void fromCamera(){
 	  Intent i= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 	  Uri output = Uri.fromFile(new File(name));
 	  int code = TAKE_PIC;
 	  startActivityForResult(i, code);
 	  
 	}
  
    
    public void onSaveInstanceState(Bundle bundle) {
	     super.onSaveInstanceState(bundle);
	     bundle.putString("birthday", birthday);
	     bundle.putString("genre", ForM);
	     ((DiscoverHoodApp)getApplication()).setInstanceBitmap(avatarImage);
	}
  
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
     super.onRestoreInstanceState(savedInstanceState);
     birthday = (savedInstanceState.getString("birthday"));
     ForM = (savedInstanceState.getString("genre"));
     
    }
  
    public void chooseGenre(View v){
    	final CharSequence[] items = {getResources().getString(R.string.masculino),getResources().getString(R.string.femenino)};  	 
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(getResources().getString(R.string.genre));
    	builder.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
    	    public void onClick(DialogInterface dialog, int item) {
    	    	ForM = items[item].toString();
    	    	genre.setText(items[item]);    	    	
    	        dialog.cancel();
    	    }
    	});
    	AlertDialog alert = builder.create();
    	alert.show();
    }

    
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
		   return new DatePickerDialog(this, datePickerListener,  year, month,day);
		}
		return null;
	}
 
	
	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
 		public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
		
 			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			DateFormat format = DateFormat.getDateInstance(1);
			format.setTimeZone(TimeZone.getDefault()); //Seteo al imezone del usuario
			born = new GregorianCalendar(selectedYear, selectedMonth, selectedDay);
		  // 	birthday = format.format(born.getTime());
			bd.setText(format.format( born.getTime()));
			((DiscoverHoodApp)getApplicationContext()).setBirthday(born);
		}
	};
          
	
    public void startNextActivity(){
    	startActivity(new Intent(this, ActivityValidateCode.class));
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);    
        finish();
    }  
   
    
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    
    ///////////////////////////////
    //// Verificacion y acciones
    ///////////////////////////////
    
    /*
     * Realiza los chequeos estaticos de los campos y hace la conexion con el server  
     */
    public void signUp(View v){ 	
  	   if ( this.dataVerification() ) { 
  			enviarDatosAlServer();
  	   }  
    }

    
    /*
     * Verifica que los campos esten completos y que el email tenga el formato correcto
     */
    public boolean dataVerification(){  	
    	 Boolean exito = true;
    	 String errors = "";
    
    	 int n= firstname.getText().toString().trim().length();
    	 int l= lastname.getText().toString().trim().length(); 
    	 int p1= pass1.getText().toString().trim().length();
    	 int p2= pass2.getText().toString().trim().length();
    	 int m= mail.getText().toString().trim().length();
    	 int g= genre.getText().toString().compareTo(getResources().getString(R.string.genre));
    	 int b= bd.getText().toString().compareTo(getResources().getString(R.string.birthday));
    	 
    	 //firstname
    	 if (n==0 || n<2 || n>50){
    		 firstname.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.firstname_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 firstname.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
    	 //lastname
    	 if (l==0 || l<2 || l>50){
    		 lastname.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.lastname_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 lastname.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
    	 //password
    	 if ( p1==0 && p2==0){
    		 pass1.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 pass2.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.password_verification_error) + "\n";
    		 exito = false;
    	 }else if (pass1.getText().toString().compareTo(pass2.getText().toString())!=0){
    		 pass1.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 pass2.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.password_mismatch_error) + "\n";
    		 exito = false;
    	 }else if(p1<6 || p1>20){
    		 pass1.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 pass2.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.password_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 pass1.setBackgroundColor(getResources().getColor(R.color.white));
    		 pass2.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
    	 //email
    	 if (m==0){
    		 mail.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.email_verification_error) + "\n";
    		 exito = false;
    	 }else if (!checkEmail(mail.getText().toString().trim())) {
    		 mail.setBackgroundColor(getResources().getColor(R.color.focus_background));
    		 errors = errors + getString(R.string.invalid_mail_error) + "\n";
    		 exito = false;
    	 }else{
    		 mail.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
    	//genre
    	 if (g==0){
    		 genre.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.genre_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 genre.setBackgroundColor(getResources().getColor(R.color.white));
    	 }
     	//birthday
    	 if (b==0){
    		 bd.setBackgroundColor( getResources().getColor(R.color.field_error_background) );
    		 errors = errors + getString(R.string.birthday_verification_error) + "\n";
    		 exito = false;
    	 }else{
    		 bd.setBackgroundColor(getResources().getColor(R.color.white));
    	 }

    	 if (!exito){
    		 dialogs.ShowErrorDialog(errors);
    	 }
    	 return exito;
     }
    
    
    /*
     * Verifica que el email ingresado tenga el formato correcto
     */
    public static boolean checkEmail (String email) {
        Pattern p = Pattern.compile("[-\\w\\.]+@\\w+\\.\\w+");
        Matcher m = p.matcher(email);
        return m.matches();
    }
    
    /*
     * Verifica que los pass ingresados sean iguales 
     */
    public boolean equalPass(String s1, String s2){
    	if (s1.length()==0 && s2.length()==0) 
    		return false; 
    	else 
    		return s1.compareTo(s2)==0;
    }
    
   
    

	public void guardarUsuarioApp( SDAccount account){
		((DiscoverHoodApp) this.getApplication()).setUserDataAccount(account);
	}
	
	//***********************************************************
	//******************** Server connection  *******************
	//***********************************************************
	private void enviarDatosAlServer(){
		 dialogs.showLoadingProgressDialog(getString(R.string.creating_account)); 
		 born = ((DiscoverHoodApp)getApplication()).getBirthday();
		 final SDAccount account = new SDAccount( firstname.getText().toString().trim(), lastname.getText().toString().trim(), (genre.getText().toString().compareTo(getResources().getString(R.string.masculino)) == 0)? "M": "F" , mail.getText().toString().trim(), pass1.getText().toString(), born.getTime(), picture);
		 ((DiscoverHoodApp) this.getApplication()).setServerConnectionData( mail.getText().toString().trim(), pass1.getText().toString() );
		 String SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
		 new SDAccountPOST(SD_Url,  SD_Key, account,  new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dialogs.dismissProgressDialog();
				LogueoPreValidate();
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 }); 
	 }
	
	 private void LogueoPreValidate(){
		 dialogs.showLoadingProgressDialog(getString(R.string.connecting));
		 String SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
		 new SDAccountGETMy(SD_Url, SD_Key, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				SDAccount account = (SDAccount) resultElement;
				guardarUsuarioApp(account);
				dialogs.dismissProgressDialog();
				startNextActivity();
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(getString(R.string.error_servidor_no_disponible));
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if ( error.getCode() == 401 ) {
					 dialogs.ShowErrorDialog(getString(R.string.not_permission_401));
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 }); 
	 }
}
