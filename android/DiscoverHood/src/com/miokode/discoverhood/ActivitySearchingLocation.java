package com.miokode.discoverhood;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.miokode.discoverhood.R;

public class ActivitySearchingLocation extends Activity{
	
	private Timer timer;
	private AlertDialog exitDialog;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_searching_location);
		    	
    	timer = new Timer();
        timer.scheduleAtFixedRate(new CheckLocation (this), 1*10*1000, 1*2*1000);  
        
        TextView cancelButton = (TextView)findViewById(R.id.cancel_gps);
        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	StopWaitingLocalization();
            }
          });
    }
		
	/**
	 * Clase interna destinada a ejecutar cada cierto tiempo un metodo que checkea la ubicacion
	 */
	private class CheckLocation extends TimerTask {
		
		private Context activity;
		private boolean hasLocation;
		
		public CheckLocation(Context context){
			activity = context;
			hasLocation = ((DiscoverHoodApp)getApplicationContext()).CanGetLocation()||(((DiscoverHoodApp)getApplicationContext()).getLatitude()!=0 && ((DiscoverHoodApp)getApplicationContext()).getLongitude()!=0);
		}
		
        public void run() {
            if (!hasLocation) {
                
            } else {
            	timer.cancel(); 
            	finish();
           	 	overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
            }
        }
	}
       
	
	/**
	 * Metodo que muetra un dialogo de alerta al cancelar la busqueda de la ubicacion
	 */
    private void StopWaitingLocalization(){  	   	
         exitDialog = new AlertDialog.Builder(this)
        .setTitle(getString(R.string.stop_waiting_localization_title))
        .setMessage(getString(R.string.stop_waiting_localization))
     
        .setPositiveButton(getString(R.string.yes_wait), new AlertDialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
			}
        })

        .setNegativeButton(getString(R.string.no_wait), new AlertDialog.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	if (((DiscoverHoodApp)getApplicationContext()).getActivityMain()!=null) 
	        		((DiscoverHoodApp)getApplicationContext()).getActivityMain().finish();
	        	if (((DiscoverHoodApp)getApplication()).getSplashScreen()!=null)
	        		((DiscoverHoodApp)getApplication()).getSplashScreen().finish();
	        	
	        	((DiscoverHoodApp)getApplicationContext()).getActivityMenu().finish();
	           	
	        	finish();
	        }
        })
       
       .create();
       exitDialog.show();
                       
    }
        
    
	@Override
	public void onBackPressed() {
		if (((DiscoverHoodApp)getApplicationContext()).getActivityMain()!=null) 
    		((DiscoverHoodApp)getApplicationContext()).getActivityMain().finish();
    	if (((DiscoverHoodApp)getApplication()).getSplashScreen()!=null)
    		((DiscoverHoodApp)getApplication()).getSplashScreen().finish();
    	((DiscoverHoodApp)getApplicationContext()).getActivityMenu().finish();
       	finish();
	}
	

}
