package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.Iterator;

import com.actionbarsherlock.app.SherlockListActivity;
import com.miokode.discoverhood.restful.SDPublication;
import com.miokode.discoverhood.restful.SDPublicationCheckIn;
import com.miokode.discoverhood.restful.SDPublicationImage;
import com.miokode.discoverhood.restful.SDPublicationNotification;
import com.miokode.discoverhood.restful.SDPublicationShared;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityShowPublication extends SherlockListActivity{
	
	private ListView list;
	private AdapterLisItemFeed adapter;
	private ArrayList<SDPublication> JustOnePublication;
	private ArrayList<SDPublicationNotification> PublicationsNotification;
	private SDPublication p;
	private ArrayList<SDPublication> UserPublications;
	private ArrayList<SDPublicationNotification> publicationNotifications;
	private ConnectionDialogs dialogs;

		
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	setContentView(R.layout.screen_publication);
    	
		publicationNotifications = ((DiscoverHoodApp) getApplication()).getPublicationNotifications();
	
    	Bundle extras= getIntent().getExtras(); 
		
		String id = extras.getString("ID");
		
		SDPublication publi = searchPublicationNotification(id);
		
		
		JustOnePublication = new ArrayList<SDPublication>();
		
	    adapter = new AdapterLisItemFeed(this, JustOnePublication);
		
		list = this.getListView();      
	    	           
 	    list.setAdapter(adapter);		   	
		  
		
		//Log.DEBUG("[Notificacion Imagen]", publi.ge)
				
		adapter.addItem(publi);
		adapter.notifyDataSetChanged();
		
		TextView closeM = (TextView)findViewById(R.id.close_publication);
		  closeM.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	finish();
	            	
	            }
	        });
	}
	
		
	private SDPublication searchPublicationNotification(String id){
		SDPublicationNotification pu = null;
		boolean is = false;
		Iterator<SDPublicationNotification> it = publicationNotifications.iterator();
		
		while(it.hasNext() && ! is){
			pu = (SDPublicationNotification)it.next();
			is = pu.getId().compareTo(id)==0;
		}
		
		return pu.getPublication(); 
	}
 }


