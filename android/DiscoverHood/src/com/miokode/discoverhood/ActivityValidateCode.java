package com.miokode.discoverhood;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDAccountActivate;
import com.miokode.discoverhood.restful.SDAccountActivateResend;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;

/**
 * Created by r on 06/09/13.
 */
public class ActivityValidateCode extends Activity implements LocationListener {
	private ConnectionDialogs dialogs;
	private String SD_Url;
	private String SD_Key;
	private EditText code;
	private LocationManager lm;  
    private Location location = null;
	private SessionManagement generalSession;
	private Builder alertDialog;
	private boolean showDialog = false;
	private boolean canGetLocation = false;
    private boolean isGPSEnabled = false; 
	private boolean isNetworkEnabled = false;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        generalSession = new SessionManagement(getApplicationContext()); 
        SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
        this.dialogs = new ConnectionDialogs(this);
        final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
       
        setContentView(R.layout.screen_validate_code);

        if (customTitleSupported) {
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar_back);
        }
        
        Button backButton = (Button)findViewById(R.id.backbutton);
        backButton.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
            //finish();
        	onBackPressed();
          }
        });

        code=(EditText)findViewById(R.id.enterCode);
        
        code.setOnFocusChangeListener(new OnFocusChangeListener() {          
				public void onFocusChange(View v, boolean hasFocus) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				    imm.showSoftInput(code, InputMethodManager.SHOW_IMPLICIT);
				    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
					if (hasFocus){
						code.setHint("");
						code.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
					}
					else {
						code.setHint(getResources().getString(R.string.enter_validation_code));
						code.setBackgroundColor(getResources().getColor(R.color.white));
	
					}
		        }
		});  	
        
        /*aca definimos el tipo de fuente*/
	    Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
	    Button resendcode = (Button)  findViewById(R.id.resend_code_b);
	    Button validatecod = (Button)  findViewById(R.id.validate_code_b);
	    TextView textvalidatecod = (TextView)findViewById(R.id.textValidateCode);
	    TextView pouallr= (TextView)findViewById(R.id.pou_all_rigths);
	    
	    
	    code.setTypeface(face);
	    resendcode.setTypeface(face);
	    validatecod.setTypeface(face);
	    textvalidatecod.setTypeface(face);
	    pouallr.setTypeface(face);
	
	    /*Solo para los botones*/
	    backButton.setTypeface(face);    

   }
		 
	
    public void GoToPrivacySettings(View v){
    	String cod = code.getText().toString();
    	if (cod.isEmpty()){
    		dialogs.ShowErrorDialog(getResources().getString(R.string.emptyvalidationcode));
    	}else{
    		enviarDatosAlServer();
    	}
    	
    }
  
    public void startNextActivity(){
	   SDAccount account = ((DiscoverHoodApp) this.getApplication()).getUserDataAccount();
       if (account!=null){ 
  				startNotificationService();
  				startMessagesService();
  				//Intent intent = new Intent(this, DaemonGeolocalization.class);
  		        //startService(intent);
  				generalSession.createLoginSession(((DiscoverHoodApp) this.getApplication()).getServerKey(), account.getFacebook_login() );
  				Intent i =new Intent(this, ActivityMenu.class);
  				startActivity(i);
  				overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
	   }

    }
    
    
    
    public void resendCode(View v){
    	reenviarValidationCodeAlServer();      	
    }
    
    public boolean checkCode (String codigo) {

        Pattern p = Pattern.compile("[0-9]*"); //Contemplar los 6 digitos

        Matcher m = p.matcher(codigo);

        return m.matches();

    }
    
   
    
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
  //***********************************************************
  	//************** Services initialization  *******************
  	//***********************************************************

  	private void startNotificationService(){
  		 	final long REPEAT_TIME = 1000 * 35;
  		    AlarmManager service = (AlarmManager) getSystemService(Context.ALARM_SERVICE);   
  		    Intent i = new Intent(this, DaemonForTasks.class);	   
  		    Bundle bundle = new Bundle();

  		    bundle.putString("SD_Key",((DiscoverHoodApp) this.getApplication()).getServerKey());

  		    bundle.putString("SD_url", ((DiscoverHoodApp)this.getApplication()).getServerUrl());	    
  		    bundle.putBoolean("startDN", true);

  		    i.putExtras(bundle);	    		    
  		    PendingIntent pending = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);	   
  		    Calendar cal = Calendar.getInstance();	   		    

  		    cal.add(Calendar.SECOND, 2);	  
  		    
  		    service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), REPEAT_TIME, pending);
  	 }
  	 
  	
  	private void startMessagesService(){
  	 	final long REPEAT_TIME = 1000 * 30;
  	    AlarmManager service = (AlarmManager) getSystemService(Context.ALARM_SERVICE);   
  	    Intent i = new Intent(this, DaemonMessages.class);	   
  	    Bundle bundle = new Bundle();

  	    bundle.putString("SD_Key",((DiscoverHoodApp) this.getApplication()).getServerKey());

  	    bundle.putString("SD_url", ((DiscoverHoodApp)this.getApplication()).getServerUrl());	    
  	    bundle.putBoolean("startDN", true);

  	    i.putExtras(bundle);	    		    
  	    PendingIntent pending = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);	   
  	    Calendar cal = Calendar.getInstance();	   		    

  	    cal.add(Calendar.SECOND, 1);	  
  	    
  	    service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), REPEAT_TIME, pending);
  	    
   }
	
	 private void getLocation() {
		 try {	
	            isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
	            isNetworkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	            if (!isGPSEnabled && !isNetworkEnabled) {       	
	                 if (showDialog){
	                  	//ActivateGPS();	
	            	    alertDialog.show();
	            	    
	                 }else showDialog =  true;
	                   	
	            } else {  
	            	canGetLocation = true;
	            }
	            if(canGetLocation){
	                if (isNetworkEnabled) {
	                    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	                    if (lm != null) {
	                    	location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);                    
	                    	if (location != null) {                               
	                        	((DiscoverHoodApp) this.getApplication()).setLatitude(location.getLatitude());             
	                            ((DiscoverHoodApp) this.getApplication()).setLongitude(location.getLongitude());
	                        }
	                    }
	                }
	                else {   if (isGPSEnabled) {
	                			if (location == null) {                				
	                				lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);    
	                				if (lm != null) {
	                					location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	                					if (location != null) {                   
	                						((DiscoverHoodApp) this.getApplication()).setLatitude(location.getLatitude());
	                						((DiscoverHoodApp) this.getApplication()).setLongitude(location.getLongitude());
	                					}
	                				}
	                			}
	                		}
	                }
	                
	                if(location.getLatitude()!=0 && location.getLongitude()!=0) {
	                	canGetLocation = true;
						((DiscoverHoodApp) getApplication()).setCanGetLocation(canGetLocation);
	                }
	                else{
	                	canGetLocation = false;
						((DiscoverHoodApp) getApplication()).setCanGetLocation(canGetLocation);
	                }
	             }
	            else {
	            	((DiscoverHoodApp) getApplication()).setLatitude(0);
					((DiscoverHoodApp) getApplication()).setLongitude(0);
					canGetLocation = false;
					((DiscoverHoodApp) getApplication()).setCanGetLocation(canGetLocation);
	            }
	 
	        } catch (Exception e) {
	        	dialogs.ShowErrorDialog(getResources().getString(R.string.cannot_get_location));
	        	onBackPressed();
	        }
	        
	    }
	
		 
	@Override
	public void onLocationChanged(Location location) {
	}
	
	@Override
	public void onProviderDisabled(String provider) {
		
	}
	
	@Override
	public void onProviderEnabled(String provider) {
	}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
				
	}

	 private void ActivateGPS(){
	     alertDialog = new AlertDialog.Builder(this);
	     alertDialog.setTitle(getString(R.string.gps_no_enabled));
	     alertDialog.setMessage(getString(R.string.enable_gps));     
	     alertDialog.setPositiveButton(getString(R.string.accept_gps), new DialogInterface.OnClickListener() {
	    	
	    	 public void onClick(DialogInterface dialog,int which) {
	             Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	             startActivity(intent);
	             dialog.cancel();
	             finish();
	         }
	     });

	     alertDialog.setNegativeButton(getString(R.string.no_accept_gps), new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int which) {
	        	 dialog.cancel();
	         }
	     });

	    // alertDialog.show();
	}

	 private void enviarDatosAlServer(){
		 dialogs.showLoadingProgressDialog(getString(R.string.checking_code));
		 
		 new SDAccountActivate(SD_Url,  SD_Key, Integer.parseInt(code.getText().toString()),  new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dialogs.dismissProgressDialog();
				startNextActivity();
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 }); 
	 }
	 
	 private void reenviarValidationCodeAlServer(){
		 dialogs.showLoadingProgressDialog(getString(R.string.resend_code));
		 
		 new SDAccountActivateResend(SD_Url,  SD_Key,  new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dialogs.dismissProgressDialog();
				dialogs.ShowInfoDialog(getString(R.string.validationemailresend));
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if ( error.getCode() == 401 ) {
					 dialogs.ShowErrorDialog(getString(R.string.not_permission_401));
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 }); 
	 }
	 
	 
}