package com.miokode.discoverhood;


import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDAccountActivate;
import com.miokode.discoverhood.restful.SDAccountActivateResend;
import com.miokode.discoverhood.restful.SDAccountGETMy;
import com.miokode.discoverhood.restful.SDAccountRestorePass;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityValidatePassword extends Activity{
  
	private ConnectionDialogs dialogs;
	private String SD_Url;
	private String SD_Key;
	private EditText pass1, pass2, code;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
        this.dialogs = new ConnectionDialogs(this);
        final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.screen_validate_code_password);
        
        if (customTitleSupported) {
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar_back);
        }
        
        Button backButton = (Button)findViewById(R.id.backbutton);
        backButton.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
           
           onBackPressed();
        	  
          }
        });
               
        code=(EditText)(EditText)findViewById(R.id.editValidateCodeText);
    	pass1=(EditText)(EditText)findViewById(R.id.editNewPassText);
    	pass2=(EditText)(EditText)findViewById(R.id.editNewPassRepText);
     
    	code.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.showSoftInput(code, InputMethodManager.SHOW_IMPLICIT);
			    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				if (hasFocus){
					code.setHint("");
					code.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
				}
				else {
					code.setHint(getResources().getString(R.string.editable_text_enter__validate_code));
					code.setBackgroundColor(getResources().getColor(R.color.white));

				}
	        }
		});  
    	pass1.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus){
					pass1.setHint("");
					pass1.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
				}
				else {
					pass1.setHint(getResources().getString(R.string.editable_text_enter__new_passw));
					pass1.setBackgroundColor(getResources().getColor(R.color.white));

				}
	        }
    	});  
    	pass2.setOnFocusChangeListener(new OnFocusChangeListener() {          
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus){
					pass2.setHint("");
					pass2.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_focus));
				}
				else {
					pass2.setHint(getResources().getString(R.string.editable_text_enter__rep_new_passwe));
					pass2.setBackgroundColor(getResources().getColor(R.color.white));

				}
	        }
    	});  

        /*aca definimos el tipo de fuente*/
	    Typeface face = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
	    Button changep = (Button)  findViewById(R.id.sendcodeb);
	    TextView copyr = (TextView)findViewById(R.id.pou_all_rigths);
	    
	    
	    code.setTypeface(face);
	    pass1.setTypeface(face);
	    pass2.setTypeface(face);
	    changep.setTypeface(face);
	    copyr.setTypeface(face);
	    
	    /*Solo para los botones*/
	   backButton.setTypeface(face);    
	    	
    	
    	
    }

    public void GoToUserSession(View v){
    	if (checkCode(code.getText().toString())){
    		if (equalPass( pass1.getText().toString(), pass2.getText().toString()) ) {
    			enviarDatosAlServer();
    		}else{
    			dialogs.ShowErrorDialog(getString(R.string.password_mismatch_error)); //TODO cambiar el texto y usar @string
    		}
    	}else{
    		dialogs.ShowErrorDialog(getString(R.string.invalidcode)); //TODO cambiar el texto y usar @string
    	}
    }
  
    public void startNextActivity(){
    	startActivity(new Intent(this, ActivityMenu.class));
    	overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }
    
    public boolean checkCode (String codigo) {
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(codigo);
       return m.matches();
    }

    public boolean equalPass(String s1, String s2){
    	return s1.compareTo(s2)==0;
    }
    
    public boolean dataVerification(String c, String s1, String s2){
    	 if(c.length()==0 || s1.length()==0 || s2.length()==0 || !checkCode(c)) return false;
    	 else return (s1.compareTo(s2)==0);
    }
 

    
    
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
    
	
	public void guardarUsuarioApp( SDAccount account){
		((DiscoverHoodApp) this.getApplication()).setUserDataAccount(account);
	}
	
	 private void enviarDatosAlServer(){
		 dialogs.showLoadingProgressDialog(getString(R.string.checking_data));
		 
		 new SDAccountRestorePass(SD_Url,  SD_Key, this.getIntent().getStringExtra("email"), Integer.parseInt(code.getText().toString()), pass1.getText().toString(), new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				dialogs.dismissProgressDialog();
				LogueoPreValidate();
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 }); 
	 }
	 private void LogueoPreValidate(){
		 dialogs.showLoadingProgressDialog(getString(R.string.connecting));
		 ((DiscoverHoodApp) this.getApplication()).setServerConnectionData(this.getIntent().getStringExtra("email"), pass1.getText().toString());
		 String SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
		 new SDAccountGETMy(SD_Url, SD_Key, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				SDAccount account = (SDAccount) resultElement;
				guardarUsuarioApp(account);
				dialogs.dismissProgressDialog();
				startNextActivity();
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if ( error.getCode() == 401 ) {
					 dialogs.ShowErrorDialog("Hubo un problema al regenerar la contraseña.");
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 }); 
	 }
}
