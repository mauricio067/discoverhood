package com.miokode.discoverhood;

import java.util.ArrayList;
import com.miokode.discoverhood.restful.*;

import android.app.Activity;
import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterComment extends ArrayAdapter<SDPublicationComment> {
	 
    
	private ArrayList<SDPublicationComment> mData = new ArrayList<SDPublicationComment>();
    private LayoutInflater mInflater;
    private final Activity context;    
    private int index;
    private MemoryCache avatarCache;
    private ImageLoader Iloader;
    
    
    public AdapterComment(Activity context,  ArrayList<SDPublicationComment> mData) {
        super(context, R.layout.screen_comment, mData);
    	this.context=context;
    	this.mData=mData;
    	avatarCache = ((DiscoverHoodApp)context.getApplication()).getAvatarMemoryCache();
		Iloader = new ImageLoader(context, 180, avatarCache);
    	mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final SDPublicationComment item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public SDPublicationComment getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        ViewHolder holder=null;
        final SDPublicationComment comment = mData.get(position);
   	
    	if (convertView==null) {
    		holder = new ViewHolder();
            convertView=mInflater.inflate(R.layout.item_comment, null);
                	            
            holder.user_pic=(ImageView)convertView.findViewById(R.id.u_avatar);
            holder.date=(TextView)convertView.findViewById(R.id.date_comment);
            holder.post=(TextView)convertView.findViewById(R.id.u_comment);
                     
            convertView.setTag(holder);
        
        } else{
        		holder = (ViewHolder)convertView.getTag();
          }
       
        long now = System.currentTimeMillis();
    	CharSequence time = DateUtils.getRelativeTimeSpanString(now, DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_NO_NOON, 0);
    	
        holder.date.setText(time);
        holder.post.setText( mData.get(position).getContent());
     
        if(comment.getAuthor().getAvatar()!=null)
           Iloader.DisplayImage(comment.getAuthor().getAvatar().getPictureUrl().trim(), holder.user_pic);
         
      /*  holder.user_pic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
         	   goToOtherUserProfile(comment);
            }
        });*/
       
        return convertView;
    }


    public static class ViewHolder {	
    	public ImageView user_pic;
    	public TextView date;	
    	public TextView post;
	
    }
  
  
  }

