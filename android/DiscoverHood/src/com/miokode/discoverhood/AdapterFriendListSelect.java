package com.miokode.discoverhood;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDFriend;

public class AdapterFriendListSelect extends BaseAdapter{
    private ArrayList<SDFriend> items;
    private LayoutInflater mInflater;
    private final Activity context;    
    private MemoryCache avatarCache;
    private ImageLoader AvatarLoader;
    private FriendsArrayFilter filter;
    
    public AdapterFriendListSelect(Activity context, ArrayList<SDFriend> items) {
    	this.context = context;
    	this.items = items;
    	avatarCache = ((DiscoverHoodApp)context.getApplication()).getAvatarMemoryCache();
    	AvatarLoader = new ImageLoader(context, 100, avatarCache);
    	this.filter = new FriendsArrayFilter(items);
    	mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final SDFriend item) {
    	items.add(item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       
    	ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_friend_list_select, null);
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.nombreContacto);
            holder.pic = (ImageView)convertView.findViewById(R.id.imagenContacto);
            convertView.setTag(holder);
        
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        SDFriend contacto = (SDFriend)items.get(position);
        holder.name.setText(contacto.getName());
        if (contacto.getAvatar() != null ){
			String url = contacto.getAvatar().getPictureUrl().trim();
			AvatarLoader.DisplayImage(url, holder.pic);
		}
        return convertView;
    }


    public static class ViewHolder {
    	public TextView name;
    	public TextView date;
    	public TextView message;
    	public ImageView pic;
    }	
       
    
    public Filter getFilter() {
        return filter;
    }
    
    /**
     * Clase para filtrar la lista de friends
     * 
     * @author Fernando Anthony Ristano
     */
    public class FriendsArrayFilter extends Filter {
    		private ArrayList<SDFriend> originalList;
    	
    		public FriendsArrayFilter(ArrayList<SDFriend> items){
    			super();
    			this.originalList = items;
    		}
	        @Override
	        protected FilterResults performFiltering(CharSequence constraint) {
	            FilterResults Result = new FilterResults();
	            if(constraint.length() == 0 ){
	                Result.values = originalList;
	                Result.count = originalList.size();
	                return Result;
	            }
	
	            String filterString = constraint.toString().toLowerCase();
	            
	            ArrayList<SDFriend> filterableFriends = new ArrayList<SDFriend>();
	            
	            for(int i = 0; i<originalList.size(); i++){	
	            	if ( originalList.get(i).getName().toLowerCase().contains(filterString) )
	            		filterableFriends.add(originalList.get(i));
	            }
	            Result.values = filterableFriends;
	            Result.count = filterableFriends.size();
	            return Result;
	        }
	
	        @Override
	        protected void publishResults(CharSequence constraint, FilterResults results) {
	        	items = (ArrayList<SDFriend>) results.values;
	            notifyDataSetChanged();
	        }
    }
    
    
}
