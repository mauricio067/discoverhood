package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.Enumeration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDFavoritesPOSTAdd;
import com.miokode.discoverhood.restful.SDFavoritesPOSTRemove;
import com.miokode.discoverhood.restful.SDPublication;
import com.miokode.discoverhood.restful.SDPublicationCheckIn;
import com.miokode.discoverhood.restful.SDPublicationImage;
import com.miokode.discoverhood.restful.SDPublicationPOSTshare;
import com.miokode.discoverhood.restful.SDPublicationShared;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@SuppressLint("NewApi")         
public class AdapterLisItemFeed extends android.widget.BaseAdapter {
	private ConnectionDialogs dialogs;
    private String SD_Url;
	private String SD_Key;
	private ArrayList<SDPublication> mData;
    private LayoutInflater mInflater;
    private SDAccount account;
    private final Activity activity;   
    private MemoryCache avatarCache;
    private MemoryCache imageCache;
//    private ImageLoader Iloader;
//    private ImageLoader AvatarLoader;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    DisplayImageOptions options;
    private View cv;
    private double latitude;
    private double longitude;
	private ProgressDialog proD = null;
	private TextView shares;
	ProgressBar spinner = null;
	    
    public AdapterLisItemFeed(Activity context,  ArrayList<SDPublication> mData) {
    	super();
		this.dialogs = new ConnectionDialogs(context);
        this.SD_Url = ((DiscoverHoodApp) context.getApplication()).getServerUrl();
        this.SD_Key = ((DiscoverHoodApp) context.getApplication()).getServerKey();
        this.account = ((DiscoverHoodApp) context.getApplication()).getUserDataAccount();
        this.activity = context;
        this.mData=mData;
        avatarCache = ((DiscoverHoodApp)context.getApplication()).getAvatarMemoryCache();
        imageCache = ((DiscoverHoodApp)context.getApplication()).getGalleryMemoryCache();
//        AvatarLoader = new ImageLoader(context, 200, avatarCache);
//        Iloader = new ImageLoader(activity, 300, imageCache);
//        getMyLocation();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
        .diskCacheExtraOptions(480, 800, null)
        .denyCacheImageMultipleSizesInMemory()
        .denyCacheImageMultipleSizesInMemory()
        .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
        .memoryCacheSize(2 * 1024 * 1024)
        .diskCacheSize(50 * 1024 * 1024)
        .diskCacheFileCount(100)
        .writeDebugLogs()
        .build();
        options = new DisplayImageOptions.Builder()
        .cacheInMemory(true) 
        .cacheOnDisk(true)
        .build();
		imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
		
		imageLoader.init(config);
    }
  
    
    public void addItem(SDPublication item) {
        mData.add(item);
        notifyDataSetChanged();
    }
    
    public void addFromTop(SDPublication item, int pos){
    	mData.add(pos, item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public SDPublication getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    
      
    public String getDistance(double lat1, double long1){
       	
   	    double lat2 = latitude;
   	    double long2 = longitude;
   	    
    	        double degToRad= Math.PI / 180.0;
    		        double phi1 = lat1 * degToRad;
    		        double phi2 = lat2 * degToRad;
    		        double lam1 = long1 * degToRad;
    		        double lam2 = long2 * degToRad;
    		 
    		        double distance = (6371.01 * Math.acos( Math.sin(phi1) * Math.sin(phi2) + Math.cos(phi1) * Math.cos(phi2) * Math.cos(lam2 - lam1) ));

		        	return String.format( "%.2f", distance);
     }
    
        
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        
        final ViewHolder holder;
        ImageView avatar = null;
        final int pos = position;
        SDPublication post=mData.get(position);
        SDPublicationShared postShared = null;
        final ImageView picture;
        cv = convertView; 
        mInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        holder = new ViewHolder();
        
        
            if(post instanceof SDPublicationShared){
                	convertView=mInflater.inflate(R.layout.item_post_shared, null); 
            	
           	    	ImageView pic = (ImageView) convertView.findViewById(R.id.image_post_shared);
            	
           	    	postShared = (SDPublicationShared)post;     
               	    post = postShared.getSharedPublication(); 
            		holder.otherUname = (TextView) convertView.findViewById(R.id.other_name);
               	    holder.otherUname.setText(postShared.getSharedPublication().getAuthor().getName());
               	    holder.otherUname.setOnClickListener(new OnClickListener() {
                     @Override
                     public void onClick(View v) {
                    	 SDPublicationShared p = (SDPublicationShared) mData.get(pos);     	   
                        if(p.getSharedPublication().getAuthor().getId().compareTo(account.getId())==0){
             		 	   		Fragment fragment = new FragmentUserProfile();
                      	   ((ActivityMenu)activity).switchContent(fragment);

                         }
                         else
                      	   gotoUserProfile(p.getSharedPublication().getAuthor().getId(),p.getSharedPublication().getAuthor());
                         }
                      });

       			 if(!(post instanceof SDPublicationImage) && !(post instanceof SDPublicationCheckIn)){
            	    	pic.setVisibility(View.GONE);
            	    	spinner = (ProgressBar) convertView.findViewById(R.id.progressBarShared); 
            	    	spinner.setVisibility(View.INVISIBLE);
            	    	
       			 }
       			 else {
       				   spinner = (ProgressBar) convertView.findViewById(R.id.progressBarShared); 
//       				   Iloader.setSpinner(spinner);
       			 }
            }
       		else{
            
       			if(post instanceof SDPublicationImage || post instanceof SDPublicationCheckIn){   	
       				convertView = mInflater.inflate(R.layout.item_feed_post_im, null);   
       				spinner = (ProgressBar) convertView.findViewById(R.id.progressBarPost); 
//    				Iloader.setSpinner(spinner);
       				
       			}
       			else if (post.getTo()!=null){
       				convertView=mInflater.inflate(R.layout.item_post_to_user, null); 
            	
       				holder.otherUname = (TextView) convertView.findViewById(R.id.other_name);
       				holder.otherUname.setText(post.getTo().getName());
            	
       				holder.otherUname.setOnClickListener(new OnClickListener() {
       					@Override
       					public void onClick(View v) {
       						SDPublication postt = mData.get(pos);
                    		gotoUserProfile(postt.getTo().getId(),postt.getAuthor());
                    	
       					}
                    
       				});
            	
       			  } 
       			else{   	
       					convertView=mInflater.inflate(R.layout.item_feed_post, null);
       			}   
       		}
            
            /*seteo utlima informacion de la publicacion*/
            if (((DiscoverHoodApp) activity.getApplication()).isTogglePublicationsSmiled(post.getId())) {
         	   if (post.getIsmile()) {
         		   post.setIsmile(false);
         		   post.decreaseSmiles();
         	   }
         	   else {
         		   post.setIsmile(true);
         		   post.increaseSmiles();
         	   }
            }
            
                                     
            holder.user_name = (TextView) convertView.findViewById(R.id.profile_name);
            holder.date = (TextView) convertView.findViewById(R.id.post_time);
            holder.distance = (TextView) convertView.findViewById(R.id.post_distance);
            holder.post = (TextView) convertView.findViewById(R.id.user_post);
            holder.smi = (TextView) convertView.findViewById(R.id.smile);
            holder.smis = (TextView) convertView.findViewById(R.id.smiles);
            holder.com = (TextView) convertView.findViewById(R.id.comment);
            holder.sha = (TextView) convertView.findViewById(R.id.share);
            holder.shas = (TextView) convertView.findViewById(R.id.shares);
            holder.addToFavorites = (TextView)convertView.findViewById(R.id.addTOfavorites);
            holder.trSmile = (TableRow) convertView.findViewById(R.id.tableRow_smile); 
            holder.trShare = (TableRow) convertView.findViewById(R.id.tableRow_share); 
            holder.trComment = (TableRow) convertView.findViewById(R.id.tableRow_comment); 
			avatar = (ImageView)convertView.findViewById(R.id.post_user_image);

			
			avatar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                	SDPublication postt = mData.get(pos);
                	if( postt.getAuthor().getId().compareTo(account.getId())==0){
       		 	   		Fragment fragment = new FragmentUserProfile();
                	   ((ActivityMenu)activity).switchContent(fragment);}

                	else
                		gotoUserProfile(postt.getAuthor().getId(), postt.getAuthor());
                }

            });
        
            			
           if( post.getAuthor().getId().compareTo(account.getId())==0){
        	   holder.sha.setText(null);
               final ImageView isha = (ImageView) convertView.findViewById(R.id.i_share);
               isha.setImageDrawable(null);
           }
           else{
        	   
        	   	holder.trShare.setOnClickListener(new OnClickListener() {
                   @Override
                   public void onClick(View v) {
                	    SDPublication postt = mData.get(pos);
                 	   	shares = (TextView) cv.findViewById(R.id.shares);

	                   	if(postt instanceof SDPublicationShared){
	                       	
	                        shares.setText(((SDPublicationShared) postt).getSharedPublication().getSharesCounter()+" "+activity.getResources().getString(R.string.Shares));
	                        sharePost( ((SDPublicationShared) postt).getSharedPublication(), pos);
	                   	}else{
	                        shares.setText(postt.getSharesCounter()+" "+activity.getResources().getString(R.string.Shares));
	                        sharePost(postt, pos);
	                   	}

                   } 
               });
           }
        	   
           
           holder.user_name.setOnClickListener(new OnClickListener() {
               @Override
               public void onClick(View v) {
            	              	   
            	   SDPublication postt = mData.get(pos);
            	   
                   if( postt.getAuthor().getId().compareTo(account.getId())==0){
       		 	   		Fragment fragment = new FragmentUserProfile();
                	   ((ActivityMenu)activity).switchContent(fragment);

                   }
                   else
                	   gotoUserProfile(postt.getAuthor().getId(),postt.getAuthor());
                   }
               
           });

           
           final TextView Ssmiles = (TextView) convertView.findViewById(R.id.smiles);
	       final TextView Ssmile = (TextView) convertView.findViewById(R.id.smile);
           holder.trSmile.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                	
                	SDPublication postti = mData.get(pos);
                
                	if(postti instanceof SDPublicationShared){
                		postti = ((SDPublicationShared) postti).getSharedPublication();
                	}
                	
                	((DiscoverHoodApp) activity.getApplicationContext()).togglePublicationsSmiled(postti.getId());
                  
                	if(postti.getIsmile()){        		
                	
                		postti.decreaseSmiles();
                		Ssmiles.setText(postti.getSmilesCounter()+" "+activity.getResources().getString(R.string.Smiles));
            			Ssmile.setTextColor(Color.WHITE);
            			postti.setIsmile(false);
            			
            		}else{
            			
            			postti.increaseSmiles();
           			    Ssmiles.setText(postti.getSmilesCounter()+" "+activity.getResources().getString(R.string.Smiles));
            			Ssmile.setTextColor(Color.MAGENTA);
            			postti.setIsmile(true);	
            			
            		}
                }
             	 
            }); 
           
              
           TextView tv = (TextView) convertView.findViewById(R.id.comments);
   
           tv.setText(post.getCommentsCounter()+" "+activity.getResources().getString(R.string.Comments));   

           ((DiscoverHoodApp)activity.getApplicationContext()).setIV(tv);
         
           TextView ctv = (TextView) convertView.findViewById(R.id.comment);
           
           ((DiscoverHoodApp)activity.getApplicationContext()).setTextViewColorC(ctv);
           
           holder.trComment.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                	SDPublication postt = mData.get(pos);
                	SDPublicationShared Sp;
                	if (postt instanceof SDPublicationShared) {
               	    	Sp = (SDPublicationShared)postt;     
                   	    postt = Sp.getSharedPublication(); 
                   	    //((DiscoverHoodApp) activity.getApplicationContext()).setCurrentPublication(postt);
                	}
                	((DiscoverHoodApp) activity.getApplicationContext()).setCurrentPublication(postt);

                	Intent inte = new Intent(activity, ActivityCommentPost.class);
                	inte.putExtra("publicationID", postt.getId());
                	activity.startActivity(inte);
                	activity.overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
                	 notifyDataSetChanged();
             }
            });

            if (post.iComment()){
	            TextView Scomments = (TextView) convertView.findViewById(R.id.comment);
	            Scomments.setTextColor(Color.MAGENTA);
            }

            
            /*Seteo de colores*/
            if (post.isIshared()){
            	holder.sha.setTextColor(Color.MAGENTA);
            }
            else {
            	holder.sha.setTextColor(Color.WHITE);
            }
            
            if (post.getIsmile()) {
            	holder.smi.setTextColor(Color.MAGENTA);
            }
            else {
            	holder.smi.setTextColor(Color.WHITE);
            }
            
            
            holder.addToFavorites.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                	
	        		 SDPublication pub =  mData.get(pos);
	        		 TextView AddToFavorites = (TextView)v.findViewById(R.id.addTOfavorites);
	        		if (activity.getString(R.string.Add_to_Favorites) == AddToFavorites.getText() ){
	        			 AddToFavorites.setText(R.string.Remove_Favorite);
	        			 addPublicationToFavorites(pub);

	        		 }
	        		else {
	        			 AddToFavorites.setText(R.string.Add_to_Favorites);
	        			 removePublicationToFavorites(pub);                		

	        		}
                	
                	if (((SDPublication)mData.get(pos)).getFavorite()){
                		
                	}else{
                	}
                	
                }
            });
    
      
       holder.user_name.setText(post.getAuthor().getName());
       SDPublication temPubl = (postShared!=null)?postShared:post;

       
        if( temPubl.getAuthor().getId().compareTo(account.getId())==0)
        	holder.distance.setVisibility(View.INVISIBLE);
        else{
        	String distance = getDistance(temPubl.getLatitud(), temPubl.getLongitud());
        	holder.distance.setText(distance+" "+activity.getResources().getString(R.string.km));
        }
        
        holder.smis.setText(post.getSmilesCounter()+" "+activity.getResources().getString(R.string.Smiles));
        holder.shas.setText(post.getSharesCounter()+" "+activity.getResources().getString(R.string.Shares));
        holder.post.setText(post.getText());
        holder.date.setText(DateTools.getTimeAgo(activity, post.getCreated()));

        
        if (((SDPublication)post).getFavorite()){
        	holder.addToFavorites.setText(R.string.Remove_Favorite);
        }
        else {
        	holder.addToFavorites.setText(R.string.Add_to_Favorites);
        }
        	
                
		if(post instanceof SDPublicationCheckIn){
			
			final SDPublicationCheckIn P=(SDPublicationCheckIn) post;
			final String url = P.getMapImage().getPictureThumbnailUrl().trim();           
			picture = (ImageView)convertView.findViewById((postShared!=null)? R.id.image_post_shared : R.id.picture_image);
		    if(picture!=null) {
		    	imageLoader.displayImage(url, picture, options,new ImageLoadingListener() {
		    	   
					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						// TODO Auto-generated method stub
						
					}
					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						if(spinner != null){
							spinner.setVisibility(View.INVISIBLE);
						}
					}
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// TODO Auto-generated method stub
						
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						// TODO Auto-generated method stub
						
					}
		    	});
//		    	Iloader.DisplayImage(url, picture);
				 picture.setOnClickListener(new OnClickListener() {
	               public void onClick(View view) {
	            	   	Intent i= new Intent(activity, ActivityEnlargeMap.class);
		                i.putExtra("latitude", P.getLatitud());
		                i.putExtra("longitude", P.getLongitud());
		            	activity.startActivity(i);
	            }
			});
			}
		        
		}
		else{
			 if(post instanceof SDPublicationImage){
				
				SDPublicationImage P=(SDPublicationImage) post;
				final String url = P.getPicture().getPictureThumbnailUrl().trim();
				picture = (ImageView)convertView.findViewById((postShared!=null)? R.id.image_post_shared : R.id.picture_image);
				if (picture!=null){
					imageLoader.displayImage(url, picture, options,new ImageLoadingListener() {
				    	   
						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							// TODO Auto-generated method stub
							
						}
						@Override
						public void onLoadingComplete(String imageUri, View view,
								Bitmap loadedImage) {
							if(spinner != null){
								spinner.setVisibility(View.INVISIBLE);
							}
						}
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							// TODO Auto-generated method stub
							
						}
						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							// TODO Auto-generated method stub
							
						}
			    	});
					final View  thumbPic = (ImageView)convertView.findViewById( (postShared!=null)? R.id.image_post_shared : R.id.picture_image);
				    thumbPic.setOnClickListener(new OnClickListener() {
		            public void onClick(View view) {
		            	
		            	SDPublication postt = mData.get(pos);
		            	((DiscoverHoodApp) activity.getApplication()).setCurrentPublication(postt);
		            	Intent i= new Intent(activity, ActivityEnlargeImage.class);
		                i.putExtra("url", url);
		            	activity.startActivity(i);
		           }
				});
				}
				
			 }
		}
		

		if (postShared!=null) {
			if (postShared.getAuthor().getAvatar() != null ){
				avatar = (ImageView)convertView.findViewById(R.id.post_user_image);
				String url = postShared.getAuthor().getAvatar().getPictureUrl().trim();
				imageLoader.displayImage(url, avatar, options);
			}
			holder.user_name.setText(postShared.getAuthor().getName());
	        holder.date.setText(DateTools.getTimeAgo(activity, postShared.getCreated()));
		}else{
			if (((SDPublication) post).getAuthor().getAvatar() != null ){
				avatar = (ImageView)convertView.findViewById(R.id.post_user_image);
				String url = ((SDPublication) post).getAuthor().getAvatar().getPictureUrl().trim();
				imageLoader.displayImage(url, avatar, options);
			}
		}
		
	    			
		return convertView;
     }
  
    
    /**
     * Cambia la pantalla al perfil del usuario indicado
     * @param userID
     */
    private void gotoUserProfile(String userID, SDAuthor aut){
    	
    	((DiscoverHoodApp) activity.getApplication()).ClearOtherUserPublications();
    	FragmentOtherUserProfile fragment = new FragmentOtherUserProfile();
    	fragment.setuserData(aut);
	 	Bundle args= new Bundle();
	    args.putString("userID", userID);
	  
        fragment.setArguments(args);	
 	    ((ActivityMenu)activity).switchContent(fragment);
    }

   
    public static class ViewHolder {
        public TableRow trComment;
		public TableRow trShare;
		public TableRow trSmile;
		public TextView user_name;
        public ImageView user_pic;
        public TextView date;
        public TextView distance;
        public TextView post;
        public TextView smi;
        public TextView smis;
        public TextView com;
        public TextView coms;
        public TextView sha;
        public TextView shas;
        public ImageView pic;
        public TextView addToFavorites;   
        public TextView otherUname;
    }
     
	
	 private void addPublicationToFavorites(SDPublication pub){
		 dialogs.showLoadingProgressDialog(activity.getResources().getString(R.string.addingtofavoritos));
		 new SDFavoritesPOSTAdd(SD_Url, SD_Key, pub.getId(), new SDConnectionResponseListener(){
				@Override
				public void onSuccess(String response, Object resultElement) {
					
					Toast toast = Toast.makeText(activity, activity.getResources().getString(R.string.publication_added_to_favorites), Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					dialogs.dismissProgressDialog();
				}

				@Override
				public void onIssues(String issues) {
					dialogs.dismissProgressDialog();
					dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error) + issues);
				}

				@Override
				public void onError(SDError error) {
					dialogs.dismissProgressDialog();
					 if (error.getReason() != null ){
						 if (error.getDetails() == null){
							 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.alreadyisfavorite));
						 }else{
							 Enumeration<String> element = error.getDetails().elements();
							 while(element.hasMoreElements()){
								 dialogs.ShowErrorDialog(element.nextElement());
							 }
						 }
					 }else{
						 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
					 }
				}	 	 
			 });  
	 }
	 
	 
	 private void sharePost(SDPublication publi, final int position){
		 
		 final SDPublication pu = publi;
						 
		  new SDPublicationPOSTshare(SD_Url,  SD_Key, pu.getId(),  latitude, latitude, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
                pu.increaseShares();
                pu.setIshared(true);
                Toast toast = Toast.makeText(activity, activity.getResources().getString(R.string.publication_shared), Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
                notifyDataSetChanged();
		    }

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(issues);
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.notShareAgain));
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
				 }
			}		 
		 }); 
	 }

	 
	 
	 /**
	  * Quita de favoritos una publicacion dada
	  * 
	  * @param pub
	  * 	Publicacion que sera retirada de favoritos
	  */
	 private void removePublicationToFavorites(SDPublication pub){
		 dialogs.showLoadingProgressDialog(activity.getResources().getString(R.string.removingtofavoritos));
		 new SDFavoritesPOSTRemove(SD_Url, SD_Key, pub.getId(), new SDConnectionResponseListener(){
			@Override
			public void onSuccess(String response, Object resultElement) {
				
				
				Toast toast =Toast.makeText(activity, activity.getResources().getString(R.string.publication_removed_from_favorites), Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				dialogs.dismissProgressDialog();
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error) + issues);
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.isnotfavorite));
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(activity.getResources().getString(R.string.server_connection_error));
				 }
			}	 	 
		 });  
	 }	 
	
	 
	    public void ShowErrorDialog( String msg ){   	
	    		final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
	    		alertDialog.setTitle("ERROR");    	   		
	    		alertDialog.setMessage(msg);
	    		alertDialog.show();
	    }
	    
	    public void showLoadingProgressDialog() {
			this.proD = ProgressDialog.show(activity, "", activity.getResources().getString(R.string.retrieving_publications), true, false);
		}
		
		public void dismissProgressDialog(){
			if (this.proD != null ) {
				this.proD.dismiss();
			}
		}
	 

	   public void getMyLocation(){
	     	
		  latitude = ((DiscoverHoodApp) activity.getApplication()).getLatitude();
		 
		  longitude = ((DiscoverHoodApp) activity.getApplication()).getLongitude();
		 
	   }
			
	}
