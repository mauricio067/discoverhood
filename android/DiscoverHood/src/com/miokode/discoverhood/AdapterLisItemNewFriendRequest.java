package com.miokode.discoverhood;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterLisItemNewFriendRequest extends ArrayAdapter<ItemNewFriendRequest> {
	 
    private ArrayList<ItemNewFriendRequest> mData = new ArrayList<ItemNewFriendRequest>();
    private LayoutInflater mInflater;
    private MemoryCache avatarCache;
    private ImageLoader AvatarLoader;
    
    private final Activity context;    
    
    public AdapterLisItemNewFriendRequest(Activity context,  ArrayList<ItemNewFriendRequest> mData) {
        super(context, R.layout.item_new_friend_request, mData);
    	this.context=context;
    	this.mData=mData;
    	avatarCache = ((DiscoverHoodApp)context.getApplication()).getAvatarMemoryCache();
    	AvatarLoader = new ImageLoader(context, 100, avatarCache);
    	mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final ItemNewFriendRequest item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public ItemNewFriendRequest getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_new_friend_request, null);
            holder = new ViewHolder();
            holder.user_name = (TextView)convertView.findViewById(R.id.friend_distance);
            holder.pic = (ImageView)convertView.findViewById(R.id.imagefriend);
            
            convertView.setTag(holder);
        
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        
        holder.user_name.setText(mData.get(position).getName());


        if (mData.get(position).getAvatarURL() != null ){
			String url = mData.get(position).getAvatarURL().trim();
			AvatarLoader.DisplayImage(url, holder.pic);
		}

        return convertView;
    }

           

    public static class ViewHolder {
    	public TextView user_name;
	    	
    	public ImageView pic;
		
    }
    
}

