package com.miokode.discoverhood;

import java.util.List;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDFriendNotification;
import com.miokode.discoverhood.restful.SDPublicationNotification;
import com.miokode.discoverhood.restful.SDFriendNotification.Result;

public class AdapterListDialogFriendRequest extends BaseAdapter {

    private Context context;
    private List<SDFriendNotification> items;
    private consult_friend consulta_amigos;
    private MemoryCache avatarCache;
    private ImageLoader AvatarLoader;
 
    public AdapterListDialogFriendRequest(Context context, List<SDFriendNotification> items) {
        this.context = context;
        this.items = items;
        consulta_amigos =  (consult_friend) new consult_friend();
        Bitmap imagenmostrar;
        imagenmostrar = BitmapFactory.decodeFile("@drawable/photo_icon");
    	avatarCache = ((DiscoverHoodApp)context.getApplicationContext()).getAvatarMemoryCache();
    	AvatarLoader = new ImageLoader(context, 100, avatarCache);
    }
 
    @Override
    public int getCount() {
        return this.items.size();
    }
 
    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	final int pos = position;
    	
        // Create a new view into the list.
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_new_friend_request, parent, false);
 
        // Set data into the view.
        ImageView friendImage= (ImageView) rowView.findViewById(R.id.imagefriend);
        TextView description = (TextView) rowView.findViewById(R.id.friend_distance);
        TextView distanciaFriend = (TextView) rowView.findViewById(R.id.distanciafriend);
        final SDFriendNotification fri = (SDFriendNotification) this.items.get(position);
       Result resultado = (Result) fri.getResult();
       
           description.setText(Html.fromHtml("<font color=\"#652d90\">" + fri.getSender().getName() + 
                                        "</font> <font color=\"#515150\">"+ context.getString(R.string.acceptfriend) + "</font>"));           

           if (!fri.isReaded()) {
              	//Log.d("recent", String.valueOf(fri.isReaded()));
//              	((LinearLayout)rowView.findViewById(R.id.recentactivitysee)).setBackgroundColor(context.getResources().getColor(R.color.notificationnew));
              	((LinearLayout)rowView.findViewById(R.id.newfriends)).setBackgroundColor(context.getResources().getColor(R.color.notificationnew));
              }        
           if (fri.getSender().getAvatar() != null ){
       			String url = fri.getSender().getAvatar().getPictureUrl().trim();
       			AvatarLoader.DisplayImage(url, friendImage);
       		}
        LinearLayout  ifri = (LinearLayout)rowView.findViewById(R.id.isfriendrequest);
        ifri.setVisibility(View.INVISIBLE);

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	String postid = fri.getId();

            	notifyDataSetChanged();
            	gotoUserProfile(fri.getSender().getId(),items.get(position).getSender());
            	        
            }
        });   
        		
        //Este codigo es para automaticamente marcar como leidas las publicaciones al verlas
        if (!fri.isReaded()){
        	fri.setRead();
        	((DiscoverHoodApp) context.getApplicationContext()).addNotificationsFriendRead(fri.getId());
        	CancelNotification(context, fri.getId());
        	((DiscoverHoodApp) context.getApplicationContext()).setCantfriendRequestNotifications(((DiscoverHoodApp) context.getApplicationContext()).getCantfriendRequestNotifications()-1);
        	((DiscoverHoodApp) context.getApplicationContext()).setCantNotify();
        	notifyDataSetChanged();
    	}
        
        return rowView;
    	
    }
	
    public static void CancelNotification(Context ctx, String notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId.hashCode());
    }

    /**
     * Cambia la pantalla al perfil del usuario indicado
     * @param userID
     */
    private void gotoUserProfile(String userID,SDAuthor ac){
    	FragmentOtherUserProfile fragment = new FragmentOtherUserProfile();
    	fragment.setuserData(ac);
	 	Bundle args= new Bundle();
	    args.putString("userID", userID);
        fragment.setArguments(args);	
 	    ((ActivityMenu)context).switchContent(fragment);
    }
	
}
