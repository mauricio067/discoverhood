package com.miokode.discoverhood;

import java.util.Enumeration;
import java.util.List;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDConversation;
import com.miokode.discoverhood.restful.SDConversationGETByID;
import com.miokode.discoverhood.restful.SDConversationNotification;
import com.miokode.discoverhood.restful.SDError;

public class AdapterListDialogMessages  extends BaseAdapter {

	private TextView nopending;
    private Context context;
    private List<SDConversationNotification> items;
    private consult_friend consulta_amigos;
    private MemoryCache avatarCache;
    private FragmentManager FA;
    public void setFragment(FragmentManager f){
    	FA =f;
    }
	protected String SD_Url;
	protected String SD_Key;
	private SDConversation conversation;
    
    private ImageLoader AvatarLoader;
	private ProgressDialog proD = null;
	private ConnectionDialogs dialogs;

 
    public AdapterListDialogMessages(Context context, List<SDConversationNotification> items) {
        this.context = context;
        this.items = items;
        consulta_amigos =  (consult_friend) new consult_friend();
        Bitmap imagenmostrar;
        imagenmostrar = BitmapFactory.decodeFile("@drawable/photo_icon");
		//this.dialogs = new ConnectionDialogs();
        this.SD_Url = ((DiscoverHoodApp) context.getApplicationContext()).getServerUrl();
        this.SD_Key = ((DiscoverHoodApp) context.getApplicationContext()).getServerKey();
		this.dialogs = new ConnectionDialogs(context);
		
    	avatarCache = ((DiscoverHoodApp)context.getApplicationContext()).getAvatarMemoryCache();
    	AvatarLoader = new ImageLoader(context, 100, avatarCache);

    }
 
    @Override
    public int getCount() {
        return this.items.size();
    }
 
    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        
        if (this.items.isEmpty()) {
        	this.nopending.setVisibility(View.VISIBLE);
        }
        else {
        	this.nopending.setVisibility(View.GONE);
        }

        // Create a new view into the list.
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_message, parent, false);
 
        // Set data into the view.
        ImageView friendImage= (ImageView) rowView.findViewById(R.id.imagefriend);
        TextView user = (TextView) rowView.findViewById(R.id.name);        
        TextView message = (TextView) rowView.findViewById(R.id.nameContact);
        TextView date = (TextView) rowView.findViewById(R.id.friend_distance);
        SDConversationNotification fri = (SDConversationNotification) this.items.get(position);

        user.setText(fri.getSendedby().getName());
        message.setText(fri.getMessage().getText());
        date.setText(DateTools.getformatdate(fri.getCreated()));
        
        if (!fri.isReaded()) {
        	((LinearLayout)rowView.findViewById(R.id.itemmesaje)).setBackgroundColor(context.getResources().getColor(R.color.notificationnew));
        }
        
        if (fri.getSendedby().getAvatar() != null ){
			String url = fri.getSendedby().getAvatar().getPictureUrl().trim();
			AvatarLoader.DisplayImage(url, friendImage);
		}
        
    //    if (fri.getSendedby().getAvatar() != null ){
	//		String url = fri.getSendedby().getAvatar().getPictureUrl().trim();
	//		AvatarLoader.DisplayImage(url, friendImage);
	//	}

        rowView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            	if (!((SDConversationNotification)items.get(pos)).isReaded()){
            		
	            	((SDConversationNotification)items.get(pos)).setRead();
	            	String postid = items.get(pos).getId();
	            	//Log.d("[notificacion mensajes]", postid);
	            	((DiscoverHoodApp) context.getApplicationContext()).addNotificationsConversationRead(postid);
	            	CancelNotification(context, postid);
//	            	((DiscoverHoodApp) context.getApplicationContext()).setCantconversationNotifications(((DiscoverHoodApp) context.getApplicationContext()).getCantconversationNotifications()-1);
	            //	((DiscoverHoodApp) context.getApplicationContext()).setCantNotify();
            	}
            	getConversation(items.get(pos).getConversationID());
            }
	    });
        //descripcion debe contener nombre mas descripcion.
        //friendImage.setImageBitmap(fri.getImagen());
        
        
        return rowView;
    }
	
    public static void CancelNotification(Context ctx, String notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId.hashCode());
    }
    
   public void setnopending(TextView nopending){
	   this.nopending = nopending;
   }
    
    

   public void ShowErrorDialog( String msg ){   	
   		final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
   		alertDialog.setTitle("ERROR");    	   		
   		alertDialog.setMessage(msg);
   		alertDialog.show();
   }
   
   public void showLoadingProgressDialog() {
		this.proD = ProgressDialog.show(context, "", context.getResources().getString(R.string.retrieving_publications), true, false);
	}
	
	public void dismissProgressDialog(){
		if (this.proD != null ) {
			this.proD.dismiss();
		}
	}
	
	 private void getConversation(final String conversationID){
		 dialogs.showLoadingProgressDialog(context.getResources().getString(R.string.retrieving_conversation));
		 new SDConversationGETByID(SD_Url, SD_Key, conversationID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				conversation = (SDConversation) resultElement;
            	FragmentConversation fc= new FragmentConversation();
            	fc.setConversationID(conversationID);
            	fc.setConversation(conversation);

            	notifyDataSetChanged();
	 	   		Fragment fragment = fc;
         	   ((ActivityMenu)context).switchContent(fragment);
        /*    	FA
        		.beginTransaction()
        		.addToBackStack(null)
        		.replace(R.id.content_frame, fc)
        		.commit();      */      

				dialogs.dismissProgressDialog();
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(context.getResources().getString(R.string.server_connection_error) + issues);
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(context.getResources().getString(R.string.server_connection_error));
				 }
			}	 	 
		 });  
		 
	 }
	
}