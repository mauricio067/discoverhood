package com.miokode.discoverhood;

import java.util.List;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDPublicationNotification;

public class AdapterListDialogRecentActivity extends BaseAdapter{
	 
    private Context context;
    private List<SDPublicationNotification> items;
    private consult_friend consulta_amigos;
    private MemoryCache avatarCache;

    private ImageLoader AvatarLoader;
    private TextView noPending;

 
    public AdapterListDialogRecentActivity(Context context, List<SDPublicationNotification> items) {
        this.context = context;
        this.items = items;
        consulta_amigos =  (consult_friend) new consult_friend();
    	avatarCache = ((DiscoverHoodApp)context.getApplicationContext()).getAvatarMemoryCache();
    	AvatarLoader = new ImageLoader(context, 100, avatarCache);
        Bitmap imagenmostrar;
        imagenmostrar = BitmapFactory.decodeFile("@drawable/photo_icon");

    }
    public void setnoPendingRequest(TextView nopending){
    	noPending = nopending;
    	if (this.noPending == null){
    	}
    	else {
    	}
    	
    }
    @Override
    public int getCount() {
        return this.items.size();
    }
 
    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (items.size()==0){
        	noPending.setVisibility(View.VISIBLE);        	
        }
        else {
        	
        	noPending.setVisibility(View.GONE);
        	
        }
        // Create a new view into the list.
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_recent_activity, parent, false);
 
        // Set data into the view.
        ImageView friendImage= (ImageView) rowView.findViewById(R.id.imagefriend);
        TextView user = (TextView) rowView.findViewById(R.id.name);        
        SDPublicationNotification publ = (SDPublicationNotification) this.items.get(position);
        

		switch(publ.getAction()){
		case SMILE: {
	        user.setText(Html.fromHtml("<font color=\"#652d90\">" + publ.getActioner().getName() + 
	                "</font> <font color=\"#515150\">"+ context.getString(R.string.new_smiles)+ "</font>"));			
		}
		break;
		
		case COMMENT: {
	        user.setText(Html.fromHtml("<font color=\"#652d90\">" + publ.getActioner().getName() + 
	                "</font> <font color=\"#515150\">"+ context.getString(R.string.new_comments)+ "</font>"));
		}
	    break;
		
		case SHARED:{
	        user.setText(Html.fromHtml("<font color=\"#652d90\">" + publ.getActioner().getName() + 
	                "</font> <font color=\"#515150\">"+ context.getString(R.string.new_shares)+ "</font>"));
		}		
		case UNSMILE:{
	        user.setText(Html.fromHtml("<font color=\"#652d90\">" + publ.getActioner().getName() + 
	                "</font> <font color=\"#515150\">"+ context.getString(R.string.new_unsmile)+ "</font>"));
		}
		break;
		
		default:
		break;
		}

        
        if (publ.getActioner().getAvatar() != null ){
			String url = publ.getActioner().getAvatar().getPictureUrl().trim();
			AvatarLoader.DisplayImage(url, friendImage);
		}
        

        if (!publ.isReaded()) {
        	//Log.d("recent", String.valueOf(publ.isReaded()));
        	((LinearLayout)rowView.findViewById(R.id.recentactivitysee)).setBackgroundColor(context.getResources().getColor(R.color.notificationnew));
        	
        }        
        
        rowView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String postid = items.get(position).getId();
            	items.get(position).setRead();

            	Intent i = new Intent(context.getApplicationContext(), ActivityShowPublication.class);
        		i.putExtra("ID", items.get(position).getId());
        		
        		context.startActivity(i);            	
            	

            }
	    });
        
        //Este codigo es para automaticamente marcar como leidas las publicaciones al verlas
    /*    if (!publ.isReaded()){
        	publ.setRead();
	    	CancelNotification(context, publ.getId());
	    	notifyDataSetChanged();
        }
        */
        return rowView;
    }
	
    public static void CancelNotification(Context ctx, String notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId.hashCode());
    }
    
    
    
    
    
	
}