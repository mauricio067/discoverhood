package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDAuthor;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDFriend;
import com.miokode.discoverhood.restful.SDFriendPOSTDelete;

public class AdapterListItemFriendNearest extends BaseAdapter {
	private ConnectionDialogs dialogs;
	private String SD_Url;
	private String SD_Key;	
    private Activity context;
    private List items;
    private List itemsOriginal;
    private consult_friend consulta_amigos;
    private MemoryCache avatarCache;
    private ImageLoader AvatarLoader;
    private TextView noPending;
    
    
    public void setnoPendingRequest(TextView nopending){
    	noPending = nopending;
    }
    public AdapterListItemFriendNearest(Activity context, List items) {
        this.context = context;
        this.items = items;
        this.itemsOriginal = items;
        SD_Url = ((DiscoverHoodApp) context.getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) context.getApplication()).getServerKey();
        this.dialogs = new ConnectionDialogs(context);
        consulta_amigos =  (consult_friend) new consult_friend();
    	avatarCache = ((DiscoverHoodApp)context.getApplication()).getAvatarMemoryCache();
    	AvatarLoader = new ImageLoader(context, 100, avatarCache);
    }

    public void addfriend(SDFriend friend){
    	items.add(friend);
    }
    
    @Override
    public int getCount() {
        return this.items.size();
    }
 
    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
 
        // Create a new view into the list.
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_nearest_friend, parent, false);
        if (this.noPending!= null){
        this.noPending.setVisibility(View.GONE);
        }
        // Set data into the view.
        ImageView friendImage= (ImageView) rowView.findViewById(R.id.imagefriend);
        TextView nameFriend = (TextView) rowView.findViewById(R.id.friend_name);
        TextView distanciaFriend = (TextView) rowView.findViewById(R.id.distanciafriend);
        
        final SDFriend fri = (SDFriend) this.items.get(position);
        
        distanciaFriend.setVisibility(View.GONE);

        nameFriend.setText(fri.getName());
    	
        nameFriend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
         	   goToOtherUserProfile(fri);

            }
        });
        
        final SDFriend friend = (SDFriend) items.get(position);
        final int post = position;
        TextView remove_friend = (TextView) rowView.findViewById(R.id.remove_friend);
        remove_friend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	deleteFriend( friend.getId(), post);
            	
            }
        });
        
        if (friend.getAvatar() != null ){
			String url = friend.getAvatar().getPictureUrl().trim();
			AvatarLoader.DisplayImage(url, friendImage);
		}
        
        friendImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
         	   goToOtherUserProfile(fri);

            }
        });
        
        return rowView;
    }
    
	
	 private void deleteFriend(String friendID,  int post ){
		 dialogs.showLoadingProgressDialog(context.getResources().getString(R.string.Sendinginformation));
		 final int posti = post;
		 new SDFriendPOSTDelete(SD_Url, SD_Key, friendID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				 items.remove(posti);
				 notifyDataSetChanged();
				 dialogs.dismissProgressDialog();
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(context.getResources().getString(R.string.server_connection_error) + issues);
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(context.getResources().getString(R.string.server_connection_error));
				 }
			}	 	 
		 });  
		 
	 }
	 
	 private void goToOtherUserProfile(SDFriend friend){
		   
	      FragmentOtherUserProfile fragment = new FragmentOtherUserProfile();
	 	  Bundle args= new Bundle();
	      args.putString("userID", friend.getId());
	      args.putString("urlAvatar", friend.getAvatar().getPictureUrl());
	      fragment.setArguments(args);
	      SDAuthor autor = new SDAuthor(friend.getId());
	      autor.setAvatar(friend.getAvatar());
	      fragment.setuserData(autor);
	      
	 	   ((ActivityMenu)context).getSupportFragmentManager()
	         .beginTransaction()
	        	.replace(R.id.content_frame, fragment)
	        	.addToBackStack(null)
	        	.commit();
	    }
    
	 public void filter(CharSequence cs){
		 SDFriend fri;
		 if (cs.toString().isEmpty()){
			 this.items = this.itemsOriginal;
		 }
		 else {
			 String cp = cs.toString().toLowerCase(); 
			 items = new ArrayList<SDFriend>();
			 Iterator itera = this.itemsOriginal.iterator();
			 while (itera.hasNext()){
				 fri = (SDFriend)itera.next();
				 if (fri.getName().toLowerCase().contains(cp)){
					 items.add(fri);
				 }
			 }
		 }
		 this.notifyDataSetChanged();
		 
	 }
    
}