package com.miokode.discoverhood;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public abstract class BaseActivity extends Activity {
	protected OnBackPressedListener onBackPressedListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }
    
    @Override
    public void onBackPressed() {
    	//Log.d("back","APRETAMOS EL BOTON BACK!");
     //   if (onBackPressedListener != null)
           // onBackPressedListener.doBack();
      //  else
          //  super.onBackPressed();
    }
}