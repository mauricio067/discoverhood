package com.miokode.discoverhood;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

public class ConnectionDialogs {
	private ProgressDialog dialogContainer;
	private Context context;
	
	public ConnectionDialogs( Context context ){
		this.context = context;
	}
	
    public void ShowErrorDialogIteractive( String msg, DialogInterface.OnClickListener OKlistener ){   	
		final Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("ERROR");    	   		
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton("OK", OKlistener);
		alertDialog.show();

	}
    
    public void ShowErrorDialog( String msg ){   	
		final Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("ERROR");    	   		
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {	
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();	  		
		    }
		});
		alertDialog.show();
	}
    
	public void ShowInfoDialog( String msg ){   	
		final AlertDialog dialog = new AlertDialog.Builder(context).create();
		dialog.setTitle("");    	   		
		dialog.setMessage(msg);
		dialog.show();
    }
	    
	public void showLoadingProgressDialog(String text) {
		this.dialogContainer = ProgressDialog.show(context, "", text, true, false);
	}
	
	public void dismissProgressDialog(){
		if (this.dialogContainer != null ) {
			this.dialogContainer.dismiss();
		}
	}
	
}
