package com.miokode.discoverhood;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDPullResponse;
import com.miokode.discoverhood.restful.SDPullService;
import com.miokode.discoverhood.restful.SDPushService;


public class DaemonForTasks extends Service implements LocationListener{
	
	private NotificationsManager ns;
	private updateNotificationAsyncTask NotificationTask;
	private String SD_Key;
	private String SD_Url;
	private boolean startDaemon = false;
	private ArrayList<String> publicationsSmiled; 
	private ArrayList<String> notificationsPublicationRead = new ArrayList<String>();
	private ArrayList<String> notificationsFriendRead = new ArrayList<String>();
	
	
	@Override
    public IBinder onBind(Intent intent) {
        return null;
    }
		
	
    public class LocalBinder extends Binder {
    	DaemonForTasks getService() {
            return DaemonForTasks.this;
        }
    }

	
	@SuppressLint("NewApi")
	 @Override
	 public void onStart(Intent intent, int startId) {

		Bundle bundle = intent.getExtras();
		SD_Key = bundle.getString("SD_Key");
		SD_Url  = bundle.getString("SD_url");
		startDaemon = bundle.getBoolean("startDN");
		
		if(startDaemon)	initDaemon(SD_Key, SD_Url);
		else stopSelf();
	 }
	 
	
	@SuppressLint("NewApi")
	@Override
	 public int onStartCommand(Intent intent, int flags, int startId) {
	   
		Bundle bundle = intent.getExtras();
		SD_Key = bundle.getString("SD_Key");
		SD_Url  = bundle.getString("SD_url");
		startDaemon = bundle.getBoolean("startDN");

		if(startDaemon)	initDaemon(SD_Key, SD_Url);
		else stopSelf();
				 
		 return START_NOT_STICKY;
	 }
	 
	 	 
	 public void onDestroy() {
		 super.onDestroy();
	}
	 
	 
	@SuppressLint("NewApi")
	private void initDaemon(String SD_Key, String SD_Url){
	
		this.SD_Key = SD_Key;
		this.SD_Url = SD_Url;               
       
    	ns = new NotificationsManager(this);
 		
		NotificationTask = new updateNotificationAsyncTask();
		
		publicationsSmiled = ((DiscoverHoodApp)this.getApplicationContext()).getPublicationsSmiled();
				
		if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
             NotificationTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            NotificationTask.execute();
        }
	}
	 
	 
	class updateNotificationAsyncTask extends AsyncTask <Void, Void, Void> {
		 
	        @Override
	        protected Void doInBackground(Void... params) {
      	
	        	doPush();

	        	return null;
	        }

	        protected void onPostExecute(Boolean result){
	        	stopSelf();
	        }       
	    }
	 
	 
	 private void doPull(){

		 new SDPullService(SD_Url, SD_Key, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
				SDPullResponse pullResponse = (SDPullResponse) resultElement;
				
				if(pullResponse.getPublicationsNotifications().size()>0){
		        	ns.manageActionNotifications(pullResponse.getPublicationsNotifications());
				}
				else {
				   	((DiscoverHoodApp) getApplicationContext()).setCantpublicationNotifications(0);

				}
				if(pullResponse.getFriendshipsNotifications().size()>0){
					ns.manageFriendNotifications(pullResponse.getFriendshipsNotifications());
				}
				else {
				   	((DiscoverHoodApp) getApplicationContext()).setCantfriendRequestNotifications(0);
				}
				
				((DiscoverHoodApp) getApplicationContext()).setFriendRequests(pullResponse.getFrienshipsRequests());
				if(pullResponse.getFrienshipsRequests().size()>0){
					ns.manageFriendRequestNotifications(pullResponse.getFrienshipsRequests());
				}
				
				((DiscoverHoodApp) getApplicationContext()).setCantNotify();
				
			}

			@Override
			public void onIssues(String issues) {

			}

			@Override
			public void onError(SDError error) {

			}	 	 
		 });  
	}
	 
		 
	 
	 private void doPush(){
     	publicationsSmiled = ((DiscoverHoodApp)getApplicationContext()).getPublicationsSmiled();

     	notificationsPublicationRead = ((DiscoverHoodApp)getApplicationContext()).getNotificationsPublicationRead();
 		notificationsFriendRead  = ((DiscoverHoodApp)getApplicationContext()).getNotificationsFriendRead();
 		
		 new SDPushService(SD_Url, SD_Key, publicationsSmiled, notificationsPublicationRead, notificationsFriendRead,null, new SDConnectionResponseListener(){
			@Override
			public void onSuccess(String response, Object resultElement) {	
				publicationsSmiled.clear();
				notificationsPublicationRead.clear();
				notificationsFriendRead.clear();
	        	doPull();

			}

			@Override
			public void onIssues(String issues) {
	        	doPull();

			}

			@Override
			public void onError(SDError error) {
	        	doPull();
    		}	 	 
		 });  
	}
	 	 
	 
	 

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}
 		 
}
