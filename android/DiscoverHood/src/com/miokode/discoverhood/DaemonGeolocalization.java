package com.miokode.discoverhood;

import com.miokode.discoverhood.restful.SDAccountPOSTReportStatus;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDPullResponse;
import com.miokode.discoverhood.restful.SDPullService;
import com.miokode.discoverhood.restful.SDPushService;

import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
 
public class DaemonGeolocalization extends Service implements LocationListener {
 
    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;
    private boolean canGetLocation = false;
    private Location location; 
    private static final long MIN_DISTANCE = 200;
    private static final long MIN_TIME = 1000*60; 
   // private static final long MIN_DISTANCE = 0;
  //  private static final long MIN_TIME = 0; 
    protected LocationManager locationManager;
	private String SD_Key;
	private String SD_Url;
	private double latitude;
	private double longitude;
    
    public int onStartCommand(Intent intent, int flags, int startId) {
        //locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    	//getLocation();
    	Bundle bundle = intent.getExtras();
	
    	SD_Key = bundle.getString("SD_Key");
		SD_Url  = bundle.getString("SD_url");
		
		initDaemon(SD_Key, SD_Url);
    	
		return Service.START_NOT_STICKY;
    }
        
	public void onStart(Intent intent, int startId) {
    	//locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);    
    	//getLocation();
		Bundle bundle = intent.getExtras();
		SD_Key = bundle.getString("SD_Key");
		SD_Url  = bundle.getString("SD_url");
		
		initDaemon(SD_Key, SD_Url);
    }
    
	
	private void initDaemon(String sD_Key2, String sD_Url2) {
		
		this.SD_Key = SD_Key;
		this.SD_Url = SD_Url;
		
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		//getLocation();
        
    	doPush();

	}

    
    @Override
    public void onDestroy() {
    	locationManager.removeUpdates(this);
    }
        
    
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    
    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        if(location!=null){
    		((DiscoverHoodApp) this.getApplication()).setDistance(location.getLatitude(), location.getLongitude());
    		latitude = location.getLatitude();
    		longitude = location.getLongitude();
            //((DiscoverHoodApp) this.getApplication()).setLatitude(location.getLatitude());
        	//((DiscoverHoodApp) this.getApplication()).setLongitude(location.getLongitude());
        	canGetLocation = true;
			((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
        	
        }
        else {
        	((DiscoverHoodApp) this.getApplication()).setLatitude(0);
            ((DiscoverHoodApp) this.getApplication()).setLongitude(0);
            canGetLocation = false;
            ((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
            ((DiscoverHoodApp) this.getApplication()).showSearchingLocation();
        }
        
    }

    @Override
    public void onProviderDisabled(String provider) {
    		 ActivateGPS();
             ((DiscoverHoodApp) this.getApplication()).showSearchingLocation();
    }
            
    @Override
    public void onProviderEnabled(String provider) {
    	isGPSEnabled = true;
    	canGetLocation = true;
		((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
    	this.getLocation();
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    	 switch(status) {
         case LocationProvider.OUT_OF_SERVICE:{
        	 canGetLocation = false;
             ((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
         }
         break;
         case LocationProvider.TEMPORARILY_UNAVAILABLE: {
        	 
           	 canGetLocation = false;
             ((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
         }
              break;
         case LocationProvider.AVAILABLE:{
        	 canGetLocation = true;
     		((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
        	 getLocation();
        	 
         }
         break;
    	 }
    }
 
    
    public Location getLocation() {
        try {	
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!isGPSEnabled && !isNetworkEnabled) {       	
            	ActivateGPS();	
            } else {  
            	this.canGetLocation = true;
            }
            if(canGetLocation){
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                    if (locationManager != null) {
                    	location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);                    
                    	if (location != null) { 
                    		((DiscoverHoodApp) this.getApplication()).setDistance(location.getLatitude(), location.getLongitude());
                        	((DiscoverHoodApp) this.getApplication()).setLatitude(location.getLatitude());             
                            ((DiscoverHoodApp) this.getApplication()).setLongitude(location.getLongitude());
                            latitude = location.getLatitude();
                        	longitude = location.getLongitude();
                        }
                    }
                }
                else {   if (isGPSEnabled) {
                			if (location == null) {                				
                				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);    
                				if (locationManager != null) {
                					location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                					if (location != null) {              
                                		((DiscoverHoodApp) this.getApplication()).setDistance(location.getLatitude(), location.getLongitude());
                						((DiscoverHoodApp) this.getApplication()).setLatitude(location.getLatitude());
                						((DiscoverHoodApp) this.getApplication()).setLongitude(location.getLongitude());
                						latitude = location.getLatitude();
                                    	longitude = location.getLongitude();
                					}
                				}
                			}
                		}
                }
                if(location.getLatitude()!=0 && location.getLongitude()!=0) {
                	canGetLocation = true;
					((DiscoverHoodApp) getApplication()).setCanGetLocation(canGetLocation);
                }
                else{
                	canGetLocation = false;
					((DiscoverHoodApp) getApplication()).setCanGetLocation(canGetLocation);
                }
             }
            else {
            	((DiscoverHoodApp) this.getApplication()).setLatitude(0);
				((DiscoverHoodApp) this.getApplication()).setLongitude(0);
				canGetLocation = false;
				((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
            	     	
            }
 
        } catch (Exception e) {
            ((DiscoverHoodApp) this.getApplication()).showSearchingLocation();
        	// BORRAR ESTO ANTES DE ENTREGAR !!!!!!!!!!!!!!!!!!! 
        	//((DiscoverHoodApp) getApplication()).setLatitude(0);
			//((DiscoverHoodApp) getApplication()).setLongitude(0);
			canGetLocation = false;
			((DiscoverHoodApp) getApplication()).setCanGetLocation(canGetLocation);
        }
		((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
        return location;
    }
    
    
    
    /**
     * Muestra un cartel avisando que no esta activado el GPS y si desea habilitarlo
     */
      
    public void ActivateGPS(){
      
    	AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
           	   
        alertDialog.setTitle(getString(R.string.gps_no_enabled));

        alertDialog.setMessage(getString(R.string.enable_gps));
          
        alertDialog.setPositiveButton(getString(R.string.accept_gps), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }
        });
  
        alertDialog.setNegativeButton(getString(R.string.no_accept_gps), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });
  
        AlertDialog alert = alertDialog.create();
        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alert.show();

    	canGetLocation = false;
        
    	if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && location.getLatitude()!=0 && location.getLongitude()!=0)
    		canGetLocation = true;
    	
    	((DiscoverHoodApp) this.getApplication()).setCanGetLocation(canGetLocation);
 	}
           
    
    private void doPush(){
     	
		getLocation();

		 new SDAccountPOSTReportStatus(SD_Url, SD_Key,latitude, longitude, new SDConnectionResponseListener(){
			@Override
			public void onSuccess(String response, Object resultElement) {	
				
			}

			@Override
			public void onIssues(String issues) {

			}

			@Override
			public void onError(SDError error) {
    		}	 	 
		 });  
	}
	 	 
	 
}
