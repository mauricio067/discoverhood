package com.miokode.discoverhood;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;

/**
 * Clase con metodos auxiliares para manejar fechas.
 *
 */
public class DateTools {
	
	/**
	 * Devuelve el tiempo transcurrido desde una fecha dada al momento de ejecutar el metodo
	 * @param context
	 * @param fecha
	 * @return
	 */
	public static String getTimeAgo(Context context, Date fecha){
		/*Obtengo el tiempo transcurrido en milisegundos*/

		@SuppressWarnings("deprecation")
		Long tiempoTotal = fecha.getTime()+fecha.getTimezoneOffset()*60*1000;
		
		Calendar ci = Calendar.getInstance();//TimeZone.getTimeZone("GMT")
		tiempoTotal = ci.getTimeInMillis()- 
				tiempoTotal;

		/*Comparacion con segundos*/
		tiempoTotal= tiempoTotal/1000;
		if (tiempoTotal < 60) {
			if (tiempoTotal <=1) {
				tiempoTotal = (long) 2;
			}

			return (tiempoTotal +" "+ context.getString(R.string.segsago));
		}
		/*Paso a minutos*/
		tiempoTotal = tiempoTotal/60;

		if (tiempoTotal < 60) {
			if (tiempoTotal == 1 ) {
				return (1 +" "+ context.getString(R.string.minago));				
			}
			else {				
				return (tiempoTotal +" "+ context.getString(R.string.minsago));
			}
		}
		/*Paso a horas*/
		tiempoTotal = tiempoTotal/60;

		if (tiempoTotal < 24){
			if (tiempoTotal == 1 ) {
				return (1 +" "+ context.getString(R.string.hourago));				
			}
			else {				
				return (tiempoTotal +" "+ context.getString(R.string.hoursago));
			}
		}
		/*Paso a dias*/
		tiempoTotal = tiempoTotal/24;

		if (tiempoTotal < 30){
			if (tiempoTotal == 1 ) {
				return (1 +" "+ context.getString(R.string.dayhago));				
			}
			else {				
				return (tiempoTotal +" "+ context.getString(R.string.daysago));
			}
		}
		/*Paso a meses*/
		tiempoTotal = tiempoTotal/30;

		if (tiempoTotal < 12) {
			if (tiempoTotal == 1 ) {
				return (1 +" "+ context.getString(R.string.monthago));				
			}
			else {				
				return (tiempoTotal +" "+ context.getString(R.string.monthsago));
			}			
		}
		/*Paso a años */
		tiempoTotal = tiempoTotal/12;
		
		if (tiempoTotal != 0){
			if (tiempoTotal<= 1 ) {
				return (1 +" "+ context.getString(R.string.yearago));				
			}
			else {				
				return (tiempoTotal +" "+ context.getString(R.string.yearsago) );
			}		
		}
		
		return "";
	}
	
	/**
	 * Devuelve la edad indicada por la fecha.
	 * @param fecha
	 * @return
	 * 		Edad de la fecha
	 */
    public static String getAge(Date fecha){
		long ageInMillis = new Date().getTime() - fecha.getTime();
		Calendar age = Calendar.getInstance();
		age.setTimeInMillis(ageInMillis);
		if ((age.get(Calendar.YEAR)-1970)== 0){
			return "0";
		}else{
			return String.valueOf(age.get(Calendar.YEAR)-1970);
		}
    }
    
    /**
     * 
     * @param fecha
     * @return
     */
    @SuppressWarnings("deprecation")
	public static String getformatdate(Date fecha) {
    	//fecha.setTime(fecha.getTime() );
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd  H:m"); //TODO setear el locale del equipo
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		return (sdf.format( fecha ));
    }
}
