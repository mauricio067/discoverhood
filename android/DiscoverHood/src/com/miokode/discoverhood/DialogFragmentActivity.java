package com.miokode.discoverhood;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class DialogFragmentActivity extends FragmentActivity{
	protected ConnectionDialogs dialogs;
	protected String SD_Url;
	protected String SD_Key;	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.dialogs = new ConnectionDialogs(this);
		SD_Url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
	}
}
