package com.miokode.discoverhood;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.miokode.discoverhood.restful.SDFriendNotification;
import com.miokode.discoverhood.restful.SDFriendRequester;

public class DialogFriendRequest extends SherlockFragment {
    private FragmentActivity fa;

	private ListView listView, listViewFriendRequest; 
	private  ArrayList<SDFriendNotification> listNotify;
	private ArrayList<SDFriendRequester> listFriendRequest;
    public DialogFriendRequest() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_friend_request, container, false);
        listNotify =  (ArrayList<SDFriendNotification>)((DiscoverHoodApp) this.getActivity().getApplication()).getFriendRequestNotifications();
        listFriendRequest =  (ArrayList<SDFriendRequester>)((DiscoverHoodApp) this.getActivity().getApplication()).getFriendRequests();
	    fa = super.getActivity();
	    //solicitudes aceptadas AdapterListDialogFriendRequest
	    listView = (ListView) view.findViewById(android.R.id.list);
	    //solicitudes pedidas (AdapterListItemFriendRequest )
	    listViewFriendRequest = (ListView) view.findViewById(R.id.listfriendrequest);
	    
	    AdapterListDialogFriendRequest adapt = new AdapterListDialogFriendRequest(fa, listNotify);
        this.listView.setAdapter(adapt);
        AdapterListItemFriendRequest adapterfr = new AdapterListItemFriendRequest(fa,listFriendRequest);
        TextView nopending = (TextView)view.findViewById(R.id.nopendingrequests);
        adapterfr.setnoPendingRequest(nopending);
        this.listViewFriendRequest.setAdapter(adapterfr);
        ((DiscoverHoodApp) this.getActivity().getApplication()).setListDialogFriendRequest(adapt);
        ((DiscoverHoodApp) this.getActivity().getApplication()).setListItemFriendRequest(adapterfr);
        return view;
    }
    
	
}
