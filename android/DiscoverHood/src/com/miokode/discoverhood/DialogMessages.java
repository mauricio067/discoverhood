package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDConversationNotification;

public class DialogMessages extends Fragment{
    private FragmentActivity fa;
    private TextView nopending;
	private ListView listView;
	private  List<SDConversationNotification> messages;
    public DialogMessages() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialog_mesages, container, false);
	    fa = super.getActivity();
	    
	    nopending = (TextView) view.findViewById(R.id.nopendingmessages);
	    listView = (ListView) view.findViewById(android.R.id.list);
        messages =  (ArrayList<SDConversationNotification>)((DiscoverHoodApp) this.getActivity().getApplication()).getConversationNotifications();
        AdapterListDialogMessages Adapter =new AdapterListDialogMessages(fa, messages);
        Adapter.setnopending(this.nopending);
        Adapter.setFragment(super.getFragmentManager());
        ((DiscoverHoodApp) this.getActivity().getApplication()).setListDialogMessages(Adapter);

        this.listView.setAdapter(Adapter);
        
        TextView writeAMessage = (TextView) view.findViewById(R.id.write_mesage);
        
        writeAMessage.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
            	FragmentCreateNewConversation mContent = new FragmentCreateNewConversation();
          	   ((ActivityMenu)fa).switchContent(mContent);

			}
        	
        });

        return view;
    }
    
    
}
