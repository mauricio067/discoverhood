package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDPublicationNotification;

public class DialogRecentActivity extends Fragment {

	private ListView listView;
	private  List<SDPublicationNotification> notify;
    private FragmentActivity fa;

    public DialogRecentActivity() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_recent_activity, container,false);
	    fa = super.getActivity();
	    view.setOnFocusChangeListener(new OnFocusChangeListener() {          
    	    public void onFocusChange(View v, boolean hasFocus) {

    	        if(!hasFocus) {

    	        }
    	    }
    	});    	

	    listView = (ListView) view.findViewById(android.R.id.list);
	    notify =  (ArrayList<SDPublicationNotification>)((DiscoverHoodApp) this.getActivity().getApplication()).getPublicationNotifications();
        TextView nopending = (TextView)view.findViewById(R.id.noninformation);
        AdapterListDialogRecentActivity adapter = new AdapterListDialogRecentActivity(fa, notify);
        adapter.setnoPendingRequest(nopending);
        this.listView.setAdapter(adapter);
        ((DiscoverHoodApp) getActivity().getApplicationContext()).setAdapterRecentActivity(adapter);
        
        return view;
    }
    
    
}
