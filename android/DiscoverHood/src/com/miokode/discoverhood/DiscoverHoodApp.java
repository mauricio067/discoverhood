package com.miokode.discoverhood;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDConnection;
import com.miokode.discoverhood.restful.SDConversationNotification;
import com.miokode.discoverhood.restful.SDFriend;
import com.miokode.discoverhood.restful.SDFriendNotification;
import com.miokode.discoverhood.restful.SDFriendRequester;
import com.miokode.discoverhood.restful.SDPublication;
import com.miokode.discoverhood.restful.SDPublicationImage;
import com.miokode.discoverhood.restful.SDPublicationNotification;
import com.miokode.discoverhood.restful.SDPublicationNotification.Action;


public class DiscoverHoodApp extends Application {
	private SDConnection ServerConnection;
	private SDAccount userDataAccount;

	private static final String SD_host = "devmg.com";
	
	// DESARROLLO
	//private static final String SD_port = "5006";
	
	private static final String SD_port = "5005";
	
	//private static final String SD_host = "gragode.com.ar" ;

	//PRODUCCION
	//private static final String SD_port = "5001" ;

	private final String SD_url = "http://" + SD_host + ":" + SD_port; //+ "/v1";
	private String SD_key = null;
	private String SD_username;
	private DialogSherlockListFragment actionbarActive;
	private TextView iv = null;
	private TextView commentC = null;
	private boolean posted = false;
	private int cantfriendrequest = 0;		
	private DaemonForTasks syncService;
	private ActivityMain mainActivity;
	private GregorianCalendar born = null;
	private SplashScreen splashScreen = null;
	private boolean esN = false;

	//////  Atributos para las Notificaciones /////////////////////////////////////////////////////////////////////
	private ArrayList<Integer> NAids = new ArrayList<Integer>();
	private ArrayList<Integer> NMids = new ArrayList<Integer>();
	private ArrayList<Integer> NFids = new ArrayList<Integer>();
	private Map<String, ArrayList<Action>> ActionsNotifications = new LinkedHashMap<String, ArrayList<Action>>();
	private ArrayList<String> messagesNid = new ArrayList<String>();
	private ArrayList<String> friendRNid = new ArrayList<String>();
	private ArrayList<String> actionsRNid = new ArrayList<String>();
	private ArrayList<SDConversationNotification> conversationNotifications = new ArrayList<SDConversationNotification>();
	private ArrayList<SDPublicationNotification> publicationNotifications = new ArrayList<SDPublicationNotification>();
	private ArrayList<SDFriendNotification> friendRequestNotifications = new ArrayList<SDFriendNotification>();
	private ArrayList<SDFriendRequester> friendRequests = new ArrayList<SDFriendRequester>();

	private SDPublicationNotification publicationNotification = null;
	private int MESSAGE = 2;
	private int ACTION_SCS = 1;
	private int FRIENDREQUEST = 3;
	private int notificationType = 0;

	//////  Caches //////////////////////////////////////////////////////////////////////////////////////////////// 
	private MemoryCache memoryCache = new MemoryCache();
	private MemoryCache avatarCache = new MemoryCache();
	private MemoryCache imageCache = new MemoryCache();
	private Map<String, SDFriend> friendCache = Collections.synchronizedMap(new LinkedHashMap<String, SDFriend>(10,1.5f,true));
	
	private Bitmap PicTaken = null;
	private ArrayList<String> imagesURLS;
	private ArrayList<String> notificationsPublicationRead = new ArrayList<String>();
	private ArrayList<String> notificationsConversationRead  = new ArrayList<String>();
	private ArrayList<String> notificationsFriendRead  = new ArrayList<String>();
	public void setNotificationsFriendRead(ArrayList<String> notificationsFriendRead) {
		this.notificationsFriendRead = notificationsFriendRead;
	}
	private ArrayList<SDPublication> UserPublications = new ArrayList<SDPublication>();
	private ArrayList<SDPublication> GeneralPublications = new ArrayList<SDPublication>();
	private ArrayList<SDPublication> OtherUserPublications = new ArrayList<SDPublication>();
	private ArrayList<SDPublicationImage> ImagePublications = new ArrayList<SDPublicationImage>();
	private ArrayList<String> publicationsSmiled = new ArrayList<String>();
	private ArrayList<SDPublicationImage> imagesSwipe = new ArrayList<SDPublicationImage>();
	private Bitmap image;

	private SDPublication currentPublication;
	private SDAccount OtherAccount;
	
	////////////  LOCALIZACION /////////////////
	
	private double latitude = 0;
	private double longitude = 0;
	private boolean canGetLocation = false;
	private double distance = 0;
	
	/////////////////////////////////////////////////////////
	
	private Bitmap instanceBitmap = null;
	private ArrayList<String> userOldestPublications = new ArrayList<String>();
	private ArrayList<String> OtheruserOldestPublications = new ArrayList<String>();
	private ArrayList<String> generalOldestPublications = new ArrayList<String>();
	private int listPosition = 0;
	
	private int CantfriendRequestNotifications = 0;
	private int CantpublicationNotifications = 0;
	private int CantconversationNotifications = 0;
	
	    
    
	public void setIV(TextView iv){
		this.iv = iv;
	}
	public TextView getIV(){
		return iv;
	}

	public void setesN(boolean esN){
		this.esN = esN;
	}
	
	public boolean getesN(){
		return this.esN;
	}


	//////  SETTERS AND GETTERS ///////////////////////////////////////////////////////////////////////
	
	public void setMainActivity(ActivityMain activity){
		mainActivity = activity;
	}
	
	public ActivityMain getActivityMain(){
		return mainActivity;
	}
	
	public void setInstanceBitmap(Bitmap instanceBitmap){
		this.instanceBitmap = instanceBitmap;
	}
	
	public Bitmap getInstanceBitmap(){
		return this.instanceBitmap;
	}
	
	public void setServerConnectionData(String SD_username, String SD_password) {
		this.SD_username = SD_username;
		this.SD_key = "Basic " + new String(Base64.encode(( SD_username + ":" + SD_password).getBytes(),Base64.URL_SAFE|Base64.NO_WRAP));
	}
	
	public String getServerUrl(){
		return SD_url;
	}
	
	public String getServerKey(){
		return SD_key;
	}
	
	public void setServerKey(String SD_Key) {
		this.SD_key = SD_Key;
	}
		
	public SDConnection getServerConnection() {
		return ServerConnection;
	}

	public void setServerConnection(SDConnection serverConnection) {
		ServerConnection = serverConnection;
	}
	
	public SDAccount getUserDataAccount() {
		//Log.d("[GET USER DATA]", "obtuvo account");
		return userDataAccount;
	}

	public void setUserDataAccount(SDAccount userData) {
		//Log.d("[SET USER DATA]", "obtuvo account");
		userDataAccount = userData;
		SessionManagement generalSession = new SessionManagement(getApplicationContext());
		generalSession.createLoginSession(SD_key, userDataAccount.getFacebook_login() );
	}
	
	public void setGalleryMemoryCache(MemoryCache memoryCache){
		this.memoryCache=memoryCache;
	}
	
	public MemoryCache getGalleryMemoryCache(){
		return memoryCache;
	}
			
	public void setAvatarMemoryCache(MemoryCache avatarCache){
		this.avatarCache=avatarCache;
	}
	
	public MemoryCache getAvatarMemoryCache(){
		return avatarCache;
	}
	
	public Map<String, SDFriend> getFriendsCache(){
		return this.friendCache;
	}
	
	public void clearFriendsCache(){
		friendCache.clear();
	}
				
	public ArrayList<SDFriendRequester> getFriendRequests(){

		return friendRequests;
	}
	
	public void setFriendRequests(ArrayList<SDFriendRequester> friendRequests){
		this.friendRequests.clear();
		this.friendRequests.addAll(friendRequests);
		this.cantfriendrequest = this.friendRequests.size();
		if (adapterFriendRequest != null && this.adapterlistitemfriendrequest != null){
			this.adapterFriendRequest.notifyDataSetChanged();
			this.adapterlistitemfriendrequest.notifyDataSetChanged();
		}
	}
	
	//////////// Getters and setters for pushing actions /////////////////////////////
	
	public ArrayList<String> getPublicationsSmiled(){
		return publicationsSmiled;
	}
	public void togglePublicationsSmiled(String nid) {
		if (publicationsSmiled.contains(nid)) publicationsSmiled.remove(nid);
    	else publicationsSmiled.add(nid);
	
	}
	public boolean isTogglePublicationsSmiled(String nid){
		return(publicationsSmiled.contains(nid));
	}
	
	public ArrayList<String> getNotificationsPublicationRead() {
		return notificationsPublicationRead;
	}
	public void addNotificationsPublicationRead(String nid) {
		notificationsPublicationRead.add(nid);
	}
	
	public ArrayList<String> getNotificationsConversationRead() {
		return notificationsConversationRead;
	}
	
	public void addNotificationsConversationRead(String nid) {
		 notificationsConversationRead.add(nid);
	}
	
	public ArrayList<String> getNotificationsFriendRead() {
		return notificationsFriendRead;
	}
	
	public void addNotificationsFriendRead(String nid) {
		notificationsFriendRead.add(nid);
	}
	
	
	//////////// Getters y Setters para las Notificaciones /////////////////////////////
	
	public ArrayList<Integer> getNAids(){
		return this.NAids;
	}
	
	public ArrayList<Integer> getNMids(){
		return this.NMids;
	}
	
	public ArrayList<Integer> getNFids(){
		return this.NFids;
	}
	
	public void cleanNXids(ArrayList<Integer> NXids){
		NXids.clear();
	}
	
	public SDPublicationNotification getPublicationNotification(){
		return publicationNotification;
	}
	
	public void setPublicationNotification (SDPublicationNotification publicationNotification){
		this.publicationNotification = publicationNotification;
	}
	
	public Map<String, ArrayList<Action>> getActionsNotifications(){
		return ActionsNotifications;
	}
	
	public ArrayList<String> getMessagesNid(){
		return messagesNid;
	}
	
	public ArrayList<String> getFriendRequestsNid(){
		return friendRNid;
	}
	
	public ArrayList<String> getActionsNid(){
		return actionsRNid;
	}
	
	public void setConversationNotifications(ArrayList<SDConversationNotification> conversationNotification){
		this.conversationNotifications.clear();
		this.conversationNotifications.addAll(conversationNotification);
		Log.d("hola",String.valueOf(this.conversationNotifications.size()));
		if (adaptermesages != null ){
		    this.adaptermesages.notifyDataSetChanged();
		}
		
	}
	
	public ArrayList<SDConversationNotification> getConversationNotifications(){
		return conversationNotifications;
	}
	
	public void setPublicationNotifications (ArrayList<SDPublicationNotification> publicationNotifications){
		this.publicationNotifications.clear();
		this.publicationNotifications.addAll(publicationNotifications);
		if (adapterDialogRecentActivity!= null){
	    	this.adapterDialogRecentActivity.notifyDataSetChanged();
		}
	}
	
	public ArrayList<SDPublicationNotification> getPublicationNotifications(){
		return publicationNotifications;
	}
	
	public void setFriendRequestNotifications(ArrayList<SDFriendNotification> friendRequestNotifications){
		this.friendRequestNotifications.clear();
		this.friendRequestNotifications.addAll(friendRequestNotifications);
		if (adapterFriendRequest != null && this.adapterlistitemfriendrequest != null){
			this.adapterFriendRequest.notifyDataSetChanged();
			this.adapterlistitemfriendrequest.notifyDataSetChanged();
		}
	}
	
	public ArrayList<SDFriendNotification> getFriendRequestNotifications(){
		return friendRequestNotifications;
	}
	
	public boolean deleteFriendRequest(String id){
		for (int i = 0; i < this.friendRequests.size(); i++ ){
			if (this.friendRequests.get(i).getId() == id) {
				this.friendRequests.remove(i);
				return true;
			}
		}
		return false;
		
	}
	
	public void updatecantFriendRequest(){
		this.cantfriendrequest = this.friendRequests.size();
		this.setCantNotify();
	}
	
	public int getCantfriendRequestNotifications() {
		return this.CantfriendRequestNotifications;
	}

	public void setCantfriendRequestNotifications(int cantfriendRequestNotifications) {
		this.CantfriendRequestNotifications = cantfriendRequestNotifications;
		
	}

	public int getCantpublicationNotifications() {
		return this.CantpublicationNotifications;
	}

	public void setCantpublicationNotifications(int cantpublicationNotifications) {
		this.CantpublicationNotifications = cantpublicationNotifications;
		
	}

	public int getCantconversationNotifications() {
		return this.CantconversationNotifications;
	}

	public void setCantconversationNotifications(int cantconversationNotifications) {
		this.CantconversationNotifications = cantconversationNotifications;
		setCantNotify();
	}
		
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/////////////// PHOTO GALLERY//////////////////////////
	
	public Bitmap getPictureTaken(){
		return PicTaken;
	}
	
	public void setPictureTaken(Bitmap PicTaken){
		this.PicTaken = PicTaken;
	}
	
	public void setImagesURL(ArrayList<String> urls){
		imagesURLS=urls;
	}
	
	public ArrayList<String> getImagesURL(){
		return imagesURLS;
	}
	
	public void setMapSnapshot(Bitmap image){
		this.image=image;
	}
	
	public Bitmap getMapSnapshot(){
		return image;
	}
	
	public void setImagesSwipe(ArrayList<SDPublicationImage> imagesSwipe){
		this.imagesSwipe = imagesSwipe;
	}
	
	public ArrayList<SDPublicationImage> getImagesSwipe(){
		return imagesSwipe;
	}
	
	public void clearImagesSwipe(){
		imagesSwipe.clear();
	}

	public void setCurrentPublication(SDPublication currentPublication){
		this.currentPublication = currentPublication;
	}
	
	public SDPublication getCurrentPublication(){
		return currentPublication;
	}
	
	public SDAccount getOtherAccount(){
		return OtherAccount;
	}
	
	public void setOtherAccount(SDAccount OtherAccount){
		this.OtherAccount = OtherAccount;
	}
	
	
	
	
	//////////// METODOS GEOLOCALIZACION /////////////////////////
	
	
	public void setDistance(double lat, double lon){
		
		Location locationA = new Location("OldLocation");

    	locationA.setLatitude(this.latitude);
    	
    	locationA.setLongitude(this.longitude);

    	Location locationB = new Location("NewLocation");

    	locationB.setLatitude(lat);
    	locationB.setLongitude(lon);

    	double D = locationA.distanceTo(locationB);
    	
    	BigDecimal bd = new BigDecimal(D/1000);
        bd = bd.setScale(1, 6);
                
        distance += bd.doubleValue();
    	//Log.d("[DISTANCIA APP]", "Distancia: "+distance);
    	
    	latitude = lat;
    	longitude = lon;	
        
	}
	
	public void restoreDistance(){
		distance = 0;
	}
	
	public double getDistance(){
		return distance;
	}
	
	public void setLatitude(double l){
		latitude = l;
	}
	
	public void setLongitude(double l){
		longitude = l;
	}
	
	public double getLatitude(){
		return latitude;
	}
	
	public double getLongitude(){
		return longitude;
	}
	

	public void setCanGetLocation(boolean canGetLocation) {
		this.canGetLocation = canGetLocation;
	}
	
	public boolean CanGetLocation() {
		return this.canGetLocation;
	}

	////////////////////////////////////////////////////////////////

	private ActivityMenu am;
	
	public void setActivitMenu(ActivityMenu am){
		this.am = am;
	}
	
	public Activity getActivityMenu(){
		return am;
	}
	
	public void showSearchingLocation(){
		//Log.d("[am]", "muestro mensaje d efalta de posicion");
		if (am!=null){
			//Log.d("[am]", "existe el objeto");
			am.startSearchingLocation();
		}	
	}
	
	//////////////////////////////////////////////////////////////////////
	
	public ArrayList<String> getUserOldestPublications(){
		return userOldestPublications;
	}
	
	public ArrayList<String> getOtherUserOldestPublications(){
		return OtheruserOldestPublications;
	}
	
	public ArrayList<String> getGeneralOldestPublications(){
		return generalOldestPublications;
	}
	
	public  ArrayList<SDPublication> getUserPublications(){
			return UserPublications;
	}
	
	public  ArrayList<SDPublication> getGeneralPublications(){
		return GeneralPublications;
	}
	
	public void increaseCommentPublication(String id){
		//increaseComment(id, UserPublications);
		increaseCommentsAsyncTask ic = new increaseCommentsAsyncTask();
		Void params = null;
		ic.execute(params);
		
	}
	
	class increaseCommentsAsyncTask extends AsyncTask <Void, Void, Void> {
		 
       protected Void onPreExecute(Void p){
   		increaseComment(currentPublication.getId(), UserPublications);
    	   return p;
       }
		
		@Override
        protected Void doInBackground(Void... params) {
 	
	   		increaseComment(currentPublication.getId(), GeneralPublications);
        	
        	return null;
        }

        protected void onPostExecute(Boolean result){
       		increaseComment(currentPublication.getId(), OtherUserPublications);
        }       
 }
	
	private void increaseComment(String id, ArrayList<SDPublication> publi){
		boolean esta = false;
		Iterator<SDPublication> it = publi.iterator();
		while(it.hasNext()&& !esta){
			SDPublication p = (SDPublication)it.next();
			if(p.getId().compareTo(id)==0){
				p.increaseComments();
				p.setIComment(true);
				esta = true;
			}
		}
		//increaseComment(id, GeneralPublications);
		//increaseComment(id, OtherUserPublications);
	}
	
	
	public  ArrayList<SDPublication> getOtherUserPublications(){
		return OtherUserPublications;
	}
	
	public void ClearOtherUserPublications(){
		OtherUserPublications.clear();
		OtheruserOldestPublications.clear();
	}
	
	public void ClearUserPublications(){
		UserPublications.clear();
		userOldestPublications.clear();
	}
	
	public void ClearGeneralPublications(){
		GeneralPublications.clear();
		generalOldestPublications.clear();
	}
	
	public void ClearImagePublications(){
		ImagePublications.clear();
	}
	
	public void postSended(boolean posted){
		this.posted = posted;
	}
	
	public boolean isPostSended(){
		return posted;
	}

	/**********************************************************
	 * Notificaciones
	 **********************************************************/
	public void startNotificationThread(){
		new Thread(new Runnable() {
		    public void run() {
		        while(true)
		        {
	
		        }
		    }
		}).start();
	}
	
	public DialogSherlockListFragment getActionbarActive() {
		return actionbarActive;
	}
	DialogFragment actionbarfa = null;
	public void setActionbarActive(DialogFragment actionbara){
		this.actionbarfa = actionbara;
		this.actionbarActive = null;
	}
	
	public void setActionbarActive(DialogSherlockListFragment actionbarActive) {
		this.actionbarActive = actionbarActive;
		this.actionbarfa = null;
		setCantNotify();
	}
	
	public void setCantNotify(){
		if (this.adapterDialogRecentActivity != null){
			this.adapterDialogRecentActivity.notifyDataSetChanged();
		}
		if (this.actionbarfa != null) {
			actionbarfa.setCantconversationNotifications(CantconversationNotifications);
			actionbarfa.setCantfriendRequestNotifications(CantfriendRequestNotifications+this.friendRequests.size());
			actionbarfa.setCantpublicationNotifications(CantpublicationNotifications);
			this.actionbarfa.setactionbar();
		}
		if (this.actionbarActive != null){
			actionbarActive.setCantconversationNotifications(CantconversationNotifications);
			actionbarActive.setCantfriendRequestNotifications(CantfriendRequestNotifications+this.friendRequests.size());
			actionbarActive.setCantpublicationNotifications(CantpublicationNotifications);
			this.actionbarActive.setactionbar();
		}
	}
	
	public DaemonForTasks getSyncService() {
		return syncService;
	}
	
	public void setSyncService(DaemonForTasks syncService) {
		this.syncService = syncService;
	}
	
	/*aca guardo los datos del left menu para poder hacer update cada vez que se actualizan los datos.*/
	private FragmentLeftMenu fragmentlf;
	public void setFragmentLeftMenu(FragmentLeftMenu FLM){
		fragmentlf = FLM;
	}
	
	public void updateFragmentLeftMenu(SDAccount account){
		fragmentlf.updateUserInformation(account);
	}

	public void setListPosition(int listPosition) {
		this.listPosition = listPosition;
	}
	
	public int getListPosition(){
		return listPosition;
	}
	
	public void setPublicationNotificationreaded(){
		this.CantpublicationNotifications=0;
		int i;
		for (i=0 ; i <this.publicationNotifications.size() ; i++){
			 this.addNotificationsPublicationRead(this.publicationNotifications.get(i).getId());
		}
	}	
	
	public void setMessageNotificationreaded(){
		this.CantconversationNotifications=0;
		int i;
		for (i=0 ; i <this.conversationNotifications.size() ; i++){
			 this.addNotificationsConversationRead(this.conversationNotifications.get(i).getId());
	/*		 
			 if (this.conversationActual != null && this.mensajesActual != null){
				 if (this.conversationNotifications.get(i).getId() == this.conversationActual.getId()){
					 if (this.conversationNotifications.get(i).getMessage().getId() !=this.mensajesActual.get(this.mensajesActual.size()-1).getId()){
						 this.mensajesActual.add(this.mensajesActual.size()-1,this.conversationNotifications.get(i).getMessage() );
					 }
					 
				 }
			 }
			 
			 */
		}
	}
	
	/*Para poder actualizar el listado de notificaciones.*/
	AdapterListDialogRecentActivity adapterDialogRecentActivity;
	
	public void setAdapterRecentActivity(AdapterListDialogRecentActivity adapter){
		this.adapterDialogRecentActivity = adapter;
	}

	
	
	public void setBirthday(GregorianCalendar born) {
		this.born = born;
	}
	
	public GregorianCalendar getBirthday(){
		return born;
	}


	public void setTextViewColorC(TextView CTV) {
		this.commentC = CTV;
	}

	public TextView getTextViewColorC(){
		return this.commentC; 
	}

	public void setSplashScreen(SplashScreen splashScreen) {
		this.splashScreen  = splashScreen;
	}
	
	public SplashScreen getSplashScreen(){
		return this.splashScreen;
	}

	
	private String fn = null;

	
	public String getFragmentNotification() {
		return fn;
	}
	
	public void setFragmentNotification(String s){
		fn = s;
	}

	public void updateDistanceLeftMenu(String distancia) {
		fragmentlf.updateDistance(distancia);
		
	}
	
	
	
	/*Estas son funciones para poder obtener y setear el adaptador de las notificaciones
	 * asi cada vez que se agrega un elemento nuevo, se actualiza el listado. 
	 * La actualizacion del listado se hace al momento de setear el listado
	 * de elementos para que sea mas eficiente.*/

	private AdapterListDialogMessages adaptermesages;
	private AdapterListDialogFriendRequest adapterFriendRequest;
	private AdapterListItemFriendRequest adapterlistitemfriendrequest;
/*	AdapterListDialogRecentActivity adapterDialogRecentActivity;*/

	public void setListDialogMessages(AdapterListDialogMessages adap){
		this.adaptermesages = adap;
	}

	public void setListDialogFriendRequest(AdapterListDialogFriendRequest adap){
		this.adapterFriendRequest = adap;
	}	
	public void setListItemFriendRequest(AdapterListItemFriendRequest adap){
		this.adapterlistitemfriendrequest = adap;
	}
	
	
	
	public AdapterListDialogMessages getListDialogMessages(){
		return this.adaptermesages;
	}
	public AdapterListDialogFriendRequest getListDialogFriendRequest(){
		return this.adapterFriendRequest ;
	}	
	public AdapterListItemFriendRequest getListItemFriendRequest(){
		return this.adapterlistitemfriendrequest;
	}
	public AdapterListDialogRecentActivity getListDialogRecentActivity(){
		return this.adapterDialogRecentActivity;
	}
}

