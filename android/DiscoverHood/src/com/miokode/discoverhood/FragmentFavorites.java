package com.miokode.discoverhood;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDFavoritesGETList;
import com.miokode.discoverhood.restful.SDFavoritesGETListDates;
import com.miokode.discoverhood.restful.SDFavoritesPOSTRemove;
import com.miokode.discoverhood.restful.SDPublication;

public class FragmentFavorites extends DialogFragment {
	private ProgressDialog proD = null;
	private String SD_Url;
	private String SD_Key;
	private RelativeLayout ll;
	private FragmentActivity fa;
	private AdapterExpandableListFavorites listAdapter;
	private ExpandableListView expListView;
	private List<Calendar> listDataHeader;
	private HashMap<Calendar, List<SDPublication>> listDataChild = new HashMap<Calendar, List<SDPublication>>();
	private ArrayList<SDPublication> Publicaciones = new ArrayList<SDPublication>();
	private int previousGroup = -1;
	private int actualposition = -1;
	private TextView nonpubli;
	private View view;
		   
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true);
		SD_Url = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerUrl();
		SD_Key = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerKey();

		fa = super.getActivity();
		ll = (RelativeLayout) inflater.inflate(R.layout.screen_favorites,	container, false);
		view = (View)ll;
		// get the listview
		
		expListView = (ExpandableListView) ll.findViewById(R.id.favoritoslistview);
		nonpubli = (TextView) ll.findViewById(R.id.noninformation);

		/* Obtengo los meses del año que tienen favoritos */
		
		if (listDataHeader != null){
			listAdapter = new AdapterExpandableListFavorites(fa, listDataHeader,listDataChild);
			expListView.setAdapter(listAdapter);
		    previousGroup = -1;
			listAdapter.onGroupExpanded(actualposition);
			listAdapter.notifyDataSetChanged();
		}
		else {
			getMyMonthsWithFavorites();
			listDataHeader = new ArrayList<Calendar>();
		}

		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
			@Override
			public void onGroupExpand(int groupPosition) {
				// setProgressBarIndeterminateVisibility(true);

				actualposition =  groupPosition;
				if (listAdapter.isEmptyGroup(groupPosition)) {
					getFavoritesPublicacions((Calendar) listAdapter.getposgroup(groupPosition));
				}				
			}
		});
		
		Typeface face = Typeface.createFromAsset(fa.getAssets(),
				"fonts/arial.ttf");
		TextView pouallr = (TextView) fa.findViewById(R.id.pou_all_rigths);
		pouallr.setTypeface(face);
		nonpubli.setTypeface(face);

		return ll;
	}

	private void addPublications() {
		List<SDPublication> ListaPublicaciones = new ArrayList<SDPublication>();
		ListIterator<SDPublication> listItr = Publicaciones.listIterator();
		DateFormat format = DateFormat.getDateInstance(1);
		format.setTimeZone(TimeZone.getDefault()); // Seteo al imezone del
													// usuario
		Bitmap imagenmostrar = BitmapFactory
				.decodeFile("@drawable/discoverhood_logo");
		while (listItr.hasNext()) {
			SDPublication fav = listItr.next();
			ListaPublicaciones.add(fav);

		}
		// TODO agregar
	}

	/* preparación de los meses */
	private void preparemonths() {

	}

	/*
	 * Preparing the list data
	 */
	private void prepareListData() {
		/*
		 * Armo un HashMap<Calendar, List<SDFavorite>> vacio para cada fecha,
		 * luego por cada clic a fecha debe cargar las publicaciones
		 */
		List<SDPublication> pubvacia;

		//listDataHeader.size();
		int pos;
		for (pos = 0; pos < listDataHeader.size(); pos++) {
			pubvacia = new ArrayList<SDPublication>();
			System.out.println(listDataHeader);
			//Log.d("[elheader]", listDataHeader.get(pos).toString());
			listDataChild.put(listDataHeader.get(pos), pubvacia); // Header, Child}
		}
	}

	private String obtenerFecha(Integer mes, Integer anio) {
		// TODO Auto-generated method stub
		return "elmes";
	}

	/*
	 * Muestra un mensaje de error
	 */
	public void ShowErrorDialog(String msg) {
		final AlertDialog alertDialog = new AlertDialog.Builder(
				this.getActivity()).create();
		alertDialog.setTitle("ERROR");
		alertDialog.setMessage(msg);
		alertDialog.show();
	}

	public void showLoadingProgressDialog() {
		this.proD = ProgressDialog.show(this.getActivity(), "",
				getString(R.string.retrieving_publications), true, false);
	}

	public void dismissProgressDialog() {
		if (this.proD != null) {
			this.proD.dismiss();
		}
	}

	private void getFavoritesPublicacions(Calendar fecha) {
		showLoadingProgressDialog();
		new SDFavoritesGETList(SD_Url, SD_Key, fecha.get(Calendar.YEAR), fecha.get(Calendar.MONTH)+1,
				new SDConnectionResponseListener() {

					@Override
					public void onSuccess(String response, Object resultElement) {
    

						Publicaciones = (ArrayList<SDPublication>) resultElement;
						listAdapter.replaceitems(actualposition, Publicaciones);
						listAdapter.notifyDataSetChanged();
						if (listAdapter.getChildrenCount(actualposition) == 0) {
						}
						if (actualposition != previousGroup)
							expListView.collapseGroup(previousGroup);

						previousGroup = actualposition;
						addPublications();
						listAdapter.notifyDataSetChanged();
						expListView.expandGroup(actualposition);
						dismissProgressDialog();
  				        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
					}

					@Override
					public void onIssues(String issues) {
						dismissProgressDialog();
						ShowErrorDialog(getString(R.string.server_connection_error) + issues);
  				        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  

					}

					@Override
					public void onError(SDError error) {
						dismissProgressDialog();
  				        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  

						if (error.getReason() != null) {
							if (error.getDetails() == null) {
								ShowErrorDialog(error.getReason());
							} else {
								Enumeration<String> element = error
										.getDetails().elements();
								while (element.hasMoreElements()) {
									ShowErrorDialog(element.nextElement());
								}
							}
						} else {
							ShowErrorDialog(getString(R.string.server_connection_error));
							
						}
					}
				});

	}

	/*
	 * aca obtener los meses del año en los que hay publicaciones como
	 * favoritos.
	 */
	@SuppressLint("NewApi")
	private void getMyMonthsWithFavorites() {
		Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		//Log.d("orientation", String.valueOf(display.getRotation()));
		//Log.d("orientation", String.valueOf(display.getRotation()));
		if (display.getRotation()!=2) {
		if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
		      getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
		}
		else {		     
			getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
			
		}			
		}
		

		showLoadingProgressDialog();
		new SDFavoritesGETListDates(SD_Url, SD_Key,
				new SDConnectionResponseListener() {

					@Override
					public void onSuccess(String response, Object resultElement) {
						
						listDataHeader = (List<Calendar>) resultElement;

						prepareListData();

						/* CAMBIAR POR */
						listAdapter = new AdapterExpandableListFavorites(fa, listDataHeader,listDataChild);
						expListView.setAdapter(listAdapter);
						listAdapter.notifyDataSetChanged();
						
						dismissProgressDialog();
						if (!listDataHeader.isEmpty()){
							actualposition = listDataHeader.size()-1;
						getFavoritesPublicacions((Calendar) listDataHeader.get(actualposition));
						nonpubli.setVisibility(View.GONE);
						}
						else {
							nonpubli.setVisibility(View.VISIBLE);
						}
					}
					@Override
					public void onIssues(String issues) {
						dismissProgressDialog();
						ShowErrorDialog(getString(R.string.server_connection_error) + issues);
					}

					@Override
					public void onError(SDError error) {
						dismissProgressDialog();
						if (error.getReason() != null) {
							if (error.getDetails() == null) {
								ShowErrorDialog(error.getReason());
							} else {
								Enumeration<String> element = error
										.getDetails().elements();
								while (element.hasMoreElements()) {
									ShowErrorDialog(element.nextElement());
								}
							}
						} else {
							ShowErrorDialog(getString(R.string.server_connection_error));
						}
					}
				});

	}

	/* Funcion para quitar de favoritos a una publicacion */
	private void removePublicationToFavorites(SDPublication pub) {
		showLoadingProgressDialog();
		/*
		 * public SDFavoritesPOSTRemove(String server, String key, String
		 * publicationID, SDConnectionResponseListener respuestaListener) {
		 */
		new SDFavoritesPOSTRemove(SD_Url, SD_Key, pub.getId(),
				new SDConnectionResponseListener() {

					@Override
					public void onSuccess(String response, Object resultElement) {
						/*
						 * ACA DEBERÍAMOS AGREGAR UN CARTEL DE EXITO.
						 */

						// Publicaciones = (ArrayList<SDPublication>)
						// resultElement;
						// addPublications(Publicaciones);
						dismissProgressDialog();
					}

					@Override
					public void onIssues(String issues) {
						dismissProgressDialog();
						ShowErrorDialog(getString(R.string.server_connection_error) + issues);
					}

					@Override
					public void onError(SDError error) {
						dismissProgressDialog();
						if (error.getReason() != null) {
							if (error.getDetails() == null) {
								ShowErrorDialog(getString(R.string.isnotfavorite));
							} else {
								Enumeration<String> element = error
										.getDetails().elements();
								while (element.hasMoreElements()) {
									ShowErrorDialog(element.nextElement());
								}
							}
						} else {
							ShowErrorDialog(getString(R.string.server_connection_error));
						}
					}
				});

	}

	public void addtofavorites(View v) {
		int position = expListView.getPositionForView(v);
		SDPublication pub = (SDPublication) listAdapter.getChild(previousGroup,	position);
		removePublicationToFavorites(pub);
		listAdapter.removeitem(previousGroup, position);
		listAdapter.notifyDataSetChanged();
	}

}
