package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import java.util.Map;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDFriend;
import com.miokode.discoverhood.restful.SDFriendGETAll;
import com.miokode.discoverhood.restful.SDFriendRequester;

public class FragmentFriend extends DialogFragment {
    private ListView listView, listView_request;
    private ArrayList<SDFriendRequester> requestersList;
    private ArrayList<SDFriend> friendList;
    private RelativeLayout ll;
    private FragmentActivity fa;
    private AdapterListItemFriendNearest adapter; 
	private TextView nonpubli;
	private AdapterListItemFriendRequest adaptfr;
	private Map<String, SDFriend> friendCache;
	EditText search ;
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {
    	super.onCreateView(inflater, container, savedInstanceState);
	    //super.onCreate(savedInstanceState);
	    setRetainInstance(true);

	    fa = super.getActivity();
	    friendCache = ((DiscoverHoodApp)fa.getApplicationContext()).getFriendsCache();
	    
	    ll = (RelativeLayout) inflater.inflate(R.layout.screen_friend_list, container, false);
        search = (EditText) ll.findViewById(R.id.search_friend);
	    this.listView = (ListView) ll.findViewById(R.id.listFriend);
	    this.listView_request = (ListView) ll.findViewById(R.id.list_friend_request);
		nonpubli = (TextView) ll.findViewById(R.id.noninformation);

	
        Bitmap imagenmostrar;
        imagenmostrar = BitmapFactory.decodeFile("@drawable/photo_icon");
        
        setListViewHeightBasedOnChildren(listView);
        setListViewHeightBasedOnChildren(listView_request);

	    TextView pou = (TextView)ll.findViewById(R.id.pou_all_rigths);

        Typeface face = Typeface.createFromAsset(fa.getAssets(),"fonts/arial.ttf");
       // search.setTypeface(face);
        pou.setTypeface(face);


        if (requestersList == null){
        	requestersList = ((DiscoverHoodApp)getActivity().getApplicationContext()).getFriendRequests();
        }
        else {
			addRequests(requestersList);
        }
        if (friendList == null){
			friendList = new ArrayList<SDFriend>();
			if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
			    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
			}
			else {		     
			    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
			}	
			getFriends();
		}
        
        adaptfr = new AdapterListItemFriendRequest(fa,requestersList);
        adaptfr.setnoPendingRequest(nonpubli);
        this.listView_request.setAdapter(adaptfr);
        
       	adapter = new AdapterListItemFriendNearest(fa,this.friendList);
       	adapter.setnoPendingRequest(nonpubli);
    	this.listView.setAdapter(adapter);
    	adaptfr.setlistfriendlist(adapter);
        adaptfr.setnoPendingRequest(nonpubli);

        //adaptfr.setnonpubli(nonpubli);
    	
		if (adapter.isEmpty() ) {
			nonpubli.setVisibility(View.VISIBLE);	
		}
		else {
			nonpubli.setVisibility(View.GONE);
		}	
		
    	
    	search.addTextChangedListener(new TextWatcher() {

    	    @Override
    	    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
    	        // When user changed the Text
    	    }

    	    @Override
    	    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

    	    @Override
    	    public void afterTextChanged(Editable arg0) {
    	        adapter.filter(search.getText());

    	    }
    	});
        return ll;
	}
	
	/*
	 * Contamos la cantidad de filas que tendra la vista de amigos para luego poder mostrarlas en su totalidad, sin scroll.
	 * Esto se hace así por un bug. (no se puede anidar un listView en un scrollview.
	 */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
 
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
 
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    
	public LayoutInflater getSystemService(String layoutInflaterService) {
		// TODO Auto-generated method stub
		return null;
	}
    
    private void addRequests(ArrayList<SDFriendRequester> list){
		this.listView_request.setAdapter(new AdapterListItemFriendRequest(fa,(ArrayList<SDFriendRequester>) list));
    }
    
    private void addFriends(ArrayList<SDFriend>  list){
		ListIterator<SDFriend> listItr = list.listIterator();
		while(listItr.hasNext()){
			SDFriend conv = listItr.next();
			adapter.addfriend(conv);
		}
		adapter.filter("");
    }
	 
	 private void getFriends(){
		 dialogs.showLoadingProgressDialog(getString(R.string.retrieving_publications));
		 new SDFriendGETAll(SD_Url, SD_Key, new SDConnectionResponseListener(){ 
			@Override
			public void onSuccess(String response, Object resultElement) {
				addFriends((ArrayList<SDFriend> ) resultElement);

				dialogs.dismissProgressDialog();
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}

			@Override
			public void onIssues(String issues) {
				 dialogs.dismissProgressDialog();
				 dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
				 getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				 getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}	 	 
		 });  
		 
	 }
	 
}
