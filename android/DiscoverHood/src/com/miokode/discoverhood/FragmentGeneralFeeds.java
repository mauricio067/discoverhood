package com.miokode.discoverhood;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;
import java.util.TimeZone;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDFavoritesPOSTAdd;
import com.miokode.discoverhood.restful.SDFavoritesPOSTRemove;
import com.miokode.discoverhood.restful.SDPublication;
import com.miokode.discoverhood.restful.SDPublicationsGETAll;
import com.miokode.discoverhood.restful.SDPublicationsGETAllResponse;

import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

public class FragmentGeneralFeeds extends DialogSherlockListFragment{
	private ProgressDialog proD = null;
	private AdapterLisItemFeed adapter;
	private String SD_Url;
	private String SD_Key;	
	private ArrayList<SDPublication> Publicaciones;
	private ArrayList<SDPublication> AllPublicaciones;
	private TextView AddToFavorites;
    private View fragmentView;
	private View footerView;  
	private String oldestPublicationID = null;
    private ArrayList<String> generaloldestPublications;
    private PullToRefreshListView list;
    private TextView nonpubli;
	private boolean loading = false;
	@SuppressWarnings("unused")
	private String newestPublicationID;
	private int index;
	private Context activity;
	private String distancia = "0";
    private boolean posted = true; 
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		this.setRetainInstance(true);
		super.onCreateView(inflater, container, savedInstanceState);
	
		fragmentView = inflater.inflate(R.layout.screen_user_profile, container, false);
		
		activity =(ActivityMenu) this.getActivity();
		
		Log.e("fragment fragment general feed", String.valueOf(this.getId()));
					
		footerView = getLayoutInflater(savedInstanceState).inflate(R.layout.item_footer, null, false);
		
		this.nonpubli = (TextView) fragmentView.findViewById(R.id.noninformation);
		
		LinearLayout WritePost= (LinearLayout) fragmentView.findViewById(R.id.write_a_post);
        WritePost.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
        	     
            	startActivity(new Intent(getActivity(), ActivityWritePost.class));
        	    getActivity().overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );     
               	
            }
        });
		
		LinearLayout CheckIn= (LinearLayout) fragmentView.findViewById(R.id.check_in);
		CheckIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	   		 
        		startActivity(new Intent(getActivity(), ActivityCheckIn.class));
        		getActivity().overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );         
            	      
       		}
        });
		
		LinearLayout TakePicture= (LinearLayout) fragmentView.findViewById(R.id.write_post);
	    TakePicture.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            
            	startActivity(new Intent(getActivity(), ActivityImagePost.class));
            	getActivity().overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );       
            	
	        }
	    });
	    
	return fragmentView ;
	}
	
	@Override
	public void onSaveInstanceState(Bundle state) {
	      super.onSaveInstanceState(state);
	}
	
	public void onPause(){
		super.onPause();
		if(getListView()!=null){
			index = list.getFirstVisiblePosition();
			((DiscoverHoodApp) this.getActivity().getApplication()).setListPosition(index);
  		}
	}
	
	public void onResume(){
		super.onResume();
		adapter.notifyDataSetChanged();
		posted = ((DiscoverHoodApp) this.getActivity().getApplicationContext()).isPostSended();
        double distance = ((DiscoverHoodApp) this.getActivity().getApplication()).getDistance();

        if(posted){
        
        	((DiscoverHoodApp) this.getActivity().getApplication()).postSended(false);
        			
		        
	        	((DiscoverHoodApp) getActivity().getApplication()).ClearGeneralPublications();
				AllPublicaciones = ((DiscoverHoodApp) getActivity().getApplication()).getGeneralPublications();
				Publicaciones = new ArrayList<SDPublication>();
				adapter = new AdapterLisItemFeed(getActivity(),Publicaciones);
				list.setAdapter(adapter);
				if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
				    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
				}
				else {		     
				    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
				}	
				getMyPublications(null, true); 
		}
        
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);		
	
        SD_Url = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerKey();
    
        posted = ((DiscoverHoodApp) this.getActivity().getApplicationContext()).isPostSended();
             
        generaloldestPublications = ((DiscoverHoodApp) getActivity().getApplication()).getGeneralOldestPublications();

        double distance = ((DiscoverHoodApp) this.getActivity().getApplication()).getDistance();
                
       	DateFormat format = DateFormat.getDateInstance(1);
		format.setTimeZone(TimeZone.getDefault()); 
	
		list = (PullToRefreshListView) getListView();      
		    
		    ((PullToRefreshListView)list).setOnRefreshListener(new OnRefreshListener() {

	            @Override
	            public void onRefresh() {
	            	if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
	            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
	            	}
	            	else {		     
	            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
	            	}	
	            	
	               	getMyPublications(null, false);
	               	list.postDelayed(new Runnable() {
	               		@Override
	               		public void run() {
	               			list.onRefreshComplete();
	               		}
	                }, 2000);
	           }
	       });
		   
		   list.setOnScrollListener(new OnScrollListener() {           
				@Override
		    	public void onScrollStateChanged(AbsListView view, int scrollState) {
		        
		        	if (scrollState == SCROLL_STATE_IDLE) {
	    				
		    		        if (list.getLastVisiblePosition() >= list.getCount()-1) {
			        			if(!loading){
			   	                   list.addFooterView(footerView, null, false);
			   	                   loading=true;
			   	                   if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
			   	                       getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
			   	                   }
			   	                   else {		     
			   	                      getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
			   	                   }	
			   	                   getMyPublications(oldestPublicationID, false);
			        			}
		    			}
		    		}
		        }
		         
		        @Override
		        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount){
		        	
		        }
		    });
		 
		   if(savedInstanceState!=null && !posted){
		 		
			  //  ((ActivityMenuBase)getActivity()).refreshFragmentLeftMenu(distancia, null);
			  // ((DiscoverHoodApp) this.getActivity().getApplication()).updateDistanceLeftMenu(distancia);
		 		nonpubli.setVisibility(View.GONE);
		 		int listPosition = ((DiscoverHoodApp) getActivity().getApplication()).getListPosition();
		 		AllPublicaciones = ((DiscoverHoodApp) getActivity().getApplication()).getGeneralPublications();
		    	ArrayList<SDPublication> pub = new ArrayList<SDPublication>();
			    adapter = new AdapterLisItemFeed(getActivity(),pub);
			 	list.setAdapter(adapter);
			 	if(AllPublicaciones.size()>5 && AllPublicaciones.size()-1>listPosition){	 			
			 		AllPublicaciones.subList(listPosition+1, AllPublicaciones.size()).clear();
			 		generaloldestPublications.subList(listPosition+1, generaloldestPublications.size()).clear();
			 		oldestPublicationID = generaloldestPublications.get(generaloldestPublications.size()-1);
			 	}  
			 	
			 	addAllPublications(AllPublicaciones);
		
		    		 	
		   }else{
	 			((DiscoverHoodApp) this.getActivity().getApplication()).postSended(false);
	 			
	 			if (AllPublicaciones != null){
			 		nonpubli.setVisibility(View.GONE);
					ArrayList<SDPublication> pub = new ArrayList<SDPublication>();
					adapter = new AdapterLisItemFeed(getActivity(),pub);
					list.setAdapter(adapter);
					addAllPublications(AllPublicaciones);
				}
				else {

			        if (distance>=0.2){
			        	
			        	((DiscoverHoodApp) this.getActivity().getApplication()).restoreDistance();
			        	oldestPublicationID = null;
			        	newestPublicationID = null;
			        	index = 0;
			        }
			        	((DiscoverHoodApp) getActivity().getApplication()).ClearGeneralPublications();
						AllPublicaciones = ((DiscoverHoodApp) getActivity().getApplication()).getGeneralPublications();
						Publicaciones = new ArrayList<SDPublication>();
						adapter = new AdapterLisItemFeed(getActivity(),Publicaciones);
						list.setAdapter(adapter);
						Handler handler = new Handler(); 
					    handler.postDelayed(new Runnable() { 
					         public void run() { 
					        	 	adapter.getMyLocation();
					        	 	if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
					        	 	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
					        	 	}
					        	 	else {		     
					        	 	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
					        	 	}	
								    getMyPublications(null, true); 
					         } 
					    }, 2000); 

						
				}
		 	
		 	}
     }
	
	private void addPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		while(listItr.hasNext()){
			SDPublication publ = listItr.next();
			AllPublicaciones.add(publ);
			oldestPublicationID = publ.getId();
			generaloldestPublications.add(oldestPublicationID);
			adapter.addItem(publ);
		}
		adapter.notifyDataSetChanged();
     }


	private void addOldestPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		while(listItr.hasNext()){
			SDPublication publ = listItr.next();
			if (generaloldestPublications.isEmpty() || !generaloldestPublications.contains(publ.getId())){
				oldestPublicationID = publ.getId();
				generaloldestPublications.add(oldestPublicationID);
				AllPublicaciones.add(publ);
				adapter.addItem(publ);
			}
		}
		loading = false;
		adapter.notifyDataSetChanged();
     }
	 
	private void addAllPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		while(listItr.hasNext()){
			SDPublication publ = listItr.next();
			oldestPublicationID = publ.getId();
			generaloldestPublications.add(oldestPublicationID);
			adapter.addItem(publ);
		}	
		adapter.notifyDataSetChanged();
     }
	
	private void addNewestPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		int i = 0;
		while(listItr.hasNext()){
			SDPublication publ = (SDPublication)listItr.next();
			if (generaloldestPublications.isEmpty() || !generaloldestPublications.contains(publ.getId())){
				newestPublicationID = publ.getId();
				AllPublicaciones.add(publ);
				adapter.addFromTop(publ,i);
				i++;
			}
		}
		adapter.notifyDataSetChanged();
		list.onRefreshComplete();
	}
	
    /*
     * Muestra un mensaje de error
     */
    public void ShowErrorDialog( String msg ){   	
    		final AlertDialog alertDialog = new AlertDialog.Builder(this.getActivity()).create();
    		alertDialog.setTitle("ERROR");    	   		
    		alertDialog.setMessage(msg);
    		alertDialog.show();
    }
    
    public void showLoadingProgressDialog() {
		this.proD = ProgressDialog.show(this.getActivity(), "",  getString(R.string.retrieving_publications), true, false);
	}
	
	public void dismissProgressDialog(){
		if (this.proD != null ) {
			this.proD.dismiss();
		}
	}	
	
		
	private void getMyPublications(final String lastID, final boolean showDialog){
		 if(lastID==null && showDialog) dialogs.showLoadingProgressDialog(getString(R.string.retrieving_publications));
		 
		 new SDPublicationsGETAll(SD_Url,  SD_Key, lastID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
								
				SDPublicationsGETAllResponse resp = (SDPublicationsGETAllResponse) resultElement;
				
				distancia = Long.toString(resp.getDistance());
				
				//((ActivityMenuBase)getActivity()).refreshFragmentLeftMenu(Long.toString(resp.getDistance()), null);
				   ((DiscoverHoodApp) getActivity().getApplication()).updateDistanceLeftMenu(distancia);


				Publicaciones = resp.getPublications();
				if(lastID == null && showDialog && Publicaciones.size()>0){ 
    				nonpubli.setVisibility(View.GONE);
					addPublications(Publicaciones);
					dialogs.dismissProgressDialog();
				}
				
				else {
					  if(lastID == null && !showDialog && Publicaciones.size()>0){
					    addNewestPublications(Publicaciones);
			 		}else{	
						list.removeFooterView(footerView);
			 			addOldestPublications(Publicaciones);
			 		}
					  if (adapter.isEmpty()){
						  nonpubli.setVisibility(View.VISIBLE);
					  }
					  else {
						  nonpubli.setVisibility(View.GONE);
					  }
		 		}
				dialogs.dismissProgressDialog();
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}
				
			
			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();

				 dismissProgressDialog();
				 ShowErrorDialog(getString(R.string.server_connection_error)+ issues);
				 getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();

				 dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				 getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}	 	 
		 });  
		 
	 }
	 
	 
	 /*Funcion para agregar a favoritos una publicacion.*/
	 private void addPublicationToFavorites(SDPublication pub){
		 showLoadingProgressDialog();
		 new SDFavoritesPOSTAdd(SD_Url, SD_Key, pub.getId(), new SDConnectionResponseListener(){

				@Override
				public void onSuccess(String response, Object resultElement) {

					dismissProgressDialog();
					
					Toast toast = Toast.makeText(activity, activity.getResources().getString(R.string.publication_added_to_favorites), Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}

				@Override
				public void onIssues(String issues) {
					 dismissProgressDialog();
					 ShowErrorDialog(getString(R.string.server_connection_error) + issues);
				}

				@Override
				public void onError(SDError error) {
					 dismissProgressDialog();
					 if (error.getReason() != null ){
						 if (error.getDetails() == null){
							 ShowErrorDialog(error.getReason());
						 }else{
							 Enumeration<String> element = error.getDetails().elements();
							 while(element.hasMoreElements()){
								 ShowErrorDialog(element.nextElement());
							 }
						 }
					 }else{
						 ShowErrorDialog(getString(R.string.server_connection_error));
					 }
				}	 	 
			 });  
		 
	 }
	 
	 
	 
	 /*Funcion para quitar de favoritos a una publicacion*/
	private void removePublicationToFavorites(SDPublication pub){
		 showLoadingProgressDialog();

		 new SDFavoritesPOSTRemove(SD_Url, SD_Key, pub.getId(), new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {
			
				dismissProgressDialog();
				Toast toast = Toast.makeText(activity, activity.getResources().getString(R.string.publication_removed_from_favorites), Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}

			@Override
			public void onIssues(String issues) {
				 dismissProgressDialog();
				 ShowErrorDialog(getString(R.string.server_connection_error) + issues);
			}

			@Override
			public void onError(SDError error) {
				 dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 ShowErrorDialog(getString(R.string.server_connection_error));
				 }
			}	 	 
		 });  
		 
	 }	 
			

	public void addtofavorites(View v){
		 int position = getListView().getPositionForView(v);
		 SDPublication pub =  Publicaciones.get(position);
		 AddToFavorites = (TextView)v.findViewById(R.id.addTOfavorites);
		if (getString(R.string.Add_to_Favorites) == AddToFavorites.getText() ){
			 AddToFavorites.setText(R.string.Remove_Favorite);
			 addPublicationToFavorites(pub);
		 }
		else {
			 AddToFavorites.setText(R.string.Add_to_Favorites);
			 removePublicationToFavorites(pub);
		}
		 
	}
	
	

			
}



