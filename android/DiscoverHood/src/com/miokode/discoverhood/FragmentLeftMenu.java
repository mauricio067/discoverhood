package com.miokode.discoverhood;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.facebook.Session;
import com.miokode.discoverhood.restful.SDAccount;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class FragmentLeftMenu extends SherlockListFragment{
	SDAccount account;
	private ItemAdapter adapter;
    private MemoryCache imageCache;
//    private ImageLoader Iloader;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private TextView name;
	private ImageView avatar;
	private TextView born;
	private DateFormat format;
	private TextView age ;
	private String nickname = null;
	private String distance = " 0 ";
	private ItemMenu UserName;
	private Activity activity;

	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        
	return inflater.inflate(R.layout.screen_menu_item_list, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())      
        .build();
		imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
		imageLoader.init(config);
		if(savedInstanceState!=null && savedInstanceState.containsKey("distance")){
			distance = savedInstanceState.getString("distance");
		}else{
			distance = Double.toString(((DiscoverHoodApp) this.getActivity().getApplication()).getDistance());
		}
		//Log.d("[distance]", distance);
		activity = this.getActivity();
		account = ((DiscoverHoodApp) this.getActivity().getApplication()).getUserDataAccount();
		UserName = new ItemMenu(account.getName());
		if(nickname!=null && nickname.length()>0) UserName.setTitle(nickname);
		adapter=new ItemAdapter();
		addItems();
		final ListView list = getListView();
        LayoutInflater inflater = getLayoutInflater(savedInstanceState);
		format = DateFormat.getDateInstance(1);
		format.setTimeZone(TimeZone.getTimeZone("gmt"));
		imageCache = ((DiscoverHoodApp)getActivity().getApplication()).getAvatarMemoryCache();
//		Iloader = new ImageLoader(this.getActivity(), 180, imageCache, null);
		View mTop = inflater.inflate(R.layout.item_user_profile, null);
		name=(TextView)mTop.findViewById(R.id.profile_name);
	    if(nickname != null && nickname.length()>0) name.setText(nickname);
	    	else name.setText(account.getName());
		avatar = (ImageView)mTop.findViewById(R.id.post_user_image);
//		if(account.getAvatar()!=null) Iloader.DisplayImage(account.getAvatar().getPictureUrl().trim(), avatar);
		imageLoader.displayImage(account.getAvatar().getPictureUrl().trim(), avatar);
		born=(TextView)mTop.findViewById(R.id.profile_birthday);
		
	
	   born.setText(format.format(account.getBirthday()));
	
	   age =(TextView) mTop.findViewById(R.id.profile_age);
	   age.setText(getString(R.string.Age_edad) + " " +DateTools.getAge(account.getBirthday()));
	   TextView edit_profile = (TextView) mTop.findViewById(R.id.edit);
	   edit_profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	((ActivityMenu)getActivity()).switchContent(new FragmentEditUserProfile());
            }
        });
	    	    
	    list.addHeaderView(mTop);
		setListAdapter(adapter);
	}
	

	private void addItems(){
	
		adapter.addItem(UserName);
	    adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.General_Feed)+": "+distance+" "+getActivity().getResources().getString(R.string.km)));
		adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.Tools)));
		adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.My_Pictures)));
		adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.Message_Center)));
		adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.Favorite_Posts)));
		adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.Friend_List)));
		adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.More)));
		adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.Sign_Out)));
		adapter.addItem(new ItemMenu(getActivity().getResources().getString(R.string.Terms_and_Conditions_Privacy_Policy)));
	}
	
		
	
	private class ItemAdapter extends BaseAdapter {
		
 
        private ArrayList<ItemMenu> mData = new ArrayList<ItemMenu>();
        private LayoutInflater mInflater;
 
        public ItemAdapter() {
            mInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
 
        public void addItem(ItemMenu item) {
            mData.add(item);
            notifyDataSetChanged();
        }
 
               
        @Override
        public int getCount() {
            return mData.size();
        }
 
        @Override
        public ItemMenu getItem(int position) {
            return mData.get(position);
        }
 
        @Override
        public long getItemId(int position) {
            return position;
        }
 
        public void setItem(int position, ItemMenu item){
        	mData.set(position, item);
        	notifyDataSetChanged();
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();               
                
                if(position!=2 && position!=7){
                	convertView = mInflater.inflate(R.layout.bar_menu_item_clickable, null);
                    holder.textView = (TextView)convertView.findViewById(R.id.item_name);
                }
                else{
                	convertView = mInflater.inflate(R.layout.bar_menu_item_no_clickable, null);
                    holder.textView = (TextView)convertView.findViewById(R.id.item_name);
                }
                
                convertView.setTag(holder);
                
            } else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.textView.setText(mData.get(position).getTitle());
            return convertView;
        }
    }
	
	public static class ViewHolder {
        public TextView textView;
    }
    
    @Override
	public void onListItemClick(ListView listView, View view, int position, long id) {
    	super.onListItemClick(listView, view, position, id);
    	
    	switch(position){
    	
    	case 1: ((ActivityMenu) getActivity()).switchContent(new FragmentUserProfile());
  		
    	break;
    		
    	case 2: ((ActivityMenu) getActivity()).switchContent(new FragmentGeneralFeeds());
			 
        break;
    			
    	case 4: ((ActivityMenu) getActivity()).switchContent(new FragmentPhotoGallery());
				
    	break;
    		
    	case 5: ((ActivityMenu) getActivity()).switchContent(new FragmentListConversation());
				
    	break;   
			
    	case 6: ((ActivityMenu) getActivity()).switchContent(new FragmentFavorites());
     			
		break;
	    	          
    	case 7: ((ActivityMenu) getActivity()).switchContent(new FragmentFriend());
    			
	    break;
	 	          
    	case 9:     
    			closeUserProfile();
	    break;
	          
    	case 10: ((ActivityMenu) getActivity()).switchContent(new FragmentPrivacyTerms());
	    break;
	          
	    default:break;
	         
		}
   }
    
    private void  closeUserProfile(){
		//Log.d("facet ","va,ps a ver si hay session facebook");

    	AlertDialog dialog = new AlertDialog.Builder(getActivity())
        .setTitle("Sign Out")
        .setMessage("You want to Sign Out ?")
        .setPositiveButton("Sign Out", new AlertDialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				
				ProgressDialog.show(getActivity(), "", "Closing...", true, false);
				//TODO close fbaccount
				//Log.d("facet ","va,ps a ver si hay session facebook");

				if (account.getFacebook_login()){
					//Log.d("facet ","va,ps a ver si hay sessasdf asdf on facebook");
					callFacebookLogout(getActivity().getApplicationContext());

				}
				
				SessionManagement generalSession = new SessionManagement(((ActivityMenu) getActivity()).getApplicationContext()); 
				generalSession.logoutUser();
				getActivity().stopService(new Intent(getActivity(), DaemonForTasks.class));
				getActivity().stopService(new Intent(getActivity(), DaemonGeolocalization.class));
				
				new CodeVerification().execute();
			}
			
        })
        .setNegativeButton("Cancel", new AlertDialog.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	dialog.dismiss();
	        }
        })
        
       .create();
        dialog.show();
                       
    }
 
    
    /**
     * Logout From Facebook 
     */
    public static void callFacebookLogout(Context context) {
        Session session = Session.getActiveSession();
        if (session != null) {

            if (!session.isClosed()) {
                session.closeAndClearTokenInformation();
                //clear your preferences if saved
            }
        } else {

            session = new Session(context);
            Session.setActiveSession(session);

            session.closeAndClearTokenInformation();
                //clear your preferences if saved

        }

    }
    
    
    
 private class CodeVerification extends AsyncTask<String, Void, Object> {
	
  	protected String doInBackground(String... args) {
          for (int i=1;i<1400000;i++){  }
         return "Datos ya procesados (dataOK)";
     }

     protected void onPostExecute(Object result) {
    	 getActivity().finish();
    	 startActivity(new Intent(getActivity(), ActivityMain.class));
        
    }    
 }
	
 	
 	public void updateUserInformation(SDAccount account){				
 		imageCache = ((DiscoverHoodApp)activity.getApplication()).getAvatarMemoryCache();
 		
 		String nick = account.getNickname();
 		if(nick!=null && nick.length()>0) name.setText(nick);
 		else name.setText(account.getName());
//		Iloader = new ImageLoader(activity, 180, imageCache, null);
//		if(account.getAvatar()!=null) Iloader.DisplayImage(account.getAvatar().getPictureUrl().trim(), avatar);
 		if(account.getAvatar()!=null) imageLoader.displayImage(account.getAvatar().getPictureUrl().trim(), avatar);
		//born.setText(format.format(account.getBirthday()));
		born.setText(android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss", account.getBirthday()));
		Log.d("[BIRTHDAY LEFT!!!!!!!!]",  account.getBirthday().toString());

		age.setText(activity.getApplicationContext().getString(R.string.Age_edad) + " " +DateTools.getAge(account.getBirthday()));
		adapter.setItem(0,new ItemMenu(name.getText().toString()));
 	}
 
	private void signInWithFacebook() {
		    Session face = Session.getActiveSession();
		    face.close();
	}

	public void updateDistance(String distancia) {
	    adapter.setItem(1,new ItemMenu(getActivity().getResources().getString(R.string.General_Feed)+": "+distancia+" "+getActivity().getResources().getString(R.string.km)));
	    distance = distancia;
	}
	
	@Override
	public void onSaveInstanceState(Bundle state) {
	      super.onSaveInstanceState(state);
	      state.putString("distance", distance);
	
	     // Log.d("[distanceguardo]", distance);
	}
	
}