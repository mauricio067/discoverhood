package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDConversation;
import com.miokode.discoverhood.restful.SDConversationsGETAll;
import com.miokode.discoverhood.restful.SDError;

public class FragmentListConversation extends DialogFragment {
    private ListView listView;
    private FragmentActivity fa;
    private EditText SearchBox;
    private RelativeLayout createmessage;
    private AdapterConversationList adapter;
    private FragmentConversation fc;
    private TextView emptyMsg;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {
	    setRetainInstance(true);
		super.onCreateView(inflater, container, savedInstanceState);
		final View fragmentView = (ViewGroup) inflater.inflate(R.layout.screen_list_conversation, container, false);
	    fa = super.getActivity();
	    listView = (ListView) fragmentView.findViewById(R.id.listMessages);
	    createmessage = (RelativeLayout) fragmentView.findViewById(R.id.enableButton);
	    SearchBox = (EditText) fragmentView.findViewById(R.id.SearchBox);
        emptyMsg = (TextView) fragmentView.findViewById(R.id.noninformation);

		if (adapter == null){    	
		   this.adapter =  new AdapterConversationList(fa, new ArrayList<SDConversation>());
		   if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
			    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
			}
			else {		     
			    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
			}		

	       getConversations(null); //TODO paginar
		}

        this.listView.setAdapter(adapter);
        listView.setTextFilterEnabled(true);
        SearchBox.setFocusableInTouchMode(true);
        SearchBox.addTextChangedListener(new TextWatcher(){
			@Override
			public void afterTextChanged(Editable s) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) { }
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//Log.d("ad", "asdfasdf");
				//adapter.getFilter().filter(s);
				adapter.setFilter(s);
			}	
        });    
        
        createmessage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	Fragment mContent = new FragmentCreateNewConversation();

         	   ((ActivityMenu)fa).switchContent(mContent);
		
    			//TODO setear animacion
            }
		});
                
        listView.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            	SDConversation conv = (SDConversation) listView.getItemAtPosition(arg2);
            	fc= new FragmentConversation();
            	fc.setConversationID(conv.getId());
            	fc.setConversation(conv);
		 	   		Fragment fragment = fc;
	            	   ((ActivityMenu)fa).switchContent(fragment);
            }
	    });

	    return fragmentView;
	}

	private void addConversations(ArrayList<SDConversation> conversations) {
		ListIterator<SDConversation> listItr = conversations.listIterator();
		while(listItr.hasNext()){
			SDConversation conv = listItr.next();
			adapter.addConversation(conv);
		}
     }

	 private void getConversations(String convID){
		 dialogs.showLoadingProgressDialog(getString(R.string.retrieving_conversation));
		 new SDConversationsGETAll(SD_Url, SD_Key, convID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {

				ArrayList<SDConversation> conversations = (ArrayList<SDConversation>) resultElement;
				addConversations(conversations);
		        emptyMsg.setVisibility( conversations.isEmpty()? View.VISIBLE: View.GONE );

				dialogs.dismissProgressDialog();
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}

			@Override
			public void onError(SDError error) {
				 dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				 getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}	 	 
		 });  
		 
	 }
	 
	 
	 private void RefreshConversations(String convID){
		 new SDConversationsGETAll(SD_Url, SD_Key, convID, new SDConnectionResponseListener(){

			@Override
			public void onSuccess(String response, Object resultElement) {

				ArrayList<SDConversation> conversations = (ArrayList<SDConversation>) resultElement;


				adapter.clearConversations();
				ListIterator<SDConversation> listItr = conversations.listIterator();
				while(listItr.hasNext()){
					SDConversation conv = listItr.next();
					adapter.addConversation(conv);
				}
				
				
				
				
				emptyMsg.setVisibility( conversations.isEmpty()? View.VISIBLE: View.GONE );

				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}

			@Override
			public void onIssues(String issues) {
				dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}

			@Override
			public void onError(SDError error) {
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				 getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  
			}	 	 
		 });  
		 
	 }	 
	 @Override
	 public void onResume(){
		super.onResume();
		if (!adapter.isEmpty()){
		   if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
		      getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
		   }
		   else {		     
		      getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
		   }		
		
		   RefreshConversations(null); //TODO paginar
		}
	 }
 	 
	 
}
