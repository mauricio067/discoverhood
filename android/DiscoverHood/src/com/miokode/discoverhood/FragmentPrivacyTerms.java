package com.miokode.discoverhood;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class FragmentPrivacyTerms extends DialogFragment {

	 private TextView termCond, policyUse;
	 private ViewSwitcher switcher;
	 private View termsview, privacyview;
 
	 public FragmentPrivacyTerms (){
		  Log.i("MYEXPENSETRACKER","TermsAndConditions ()");
		 }
		 @Override
		 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			super.onCreateView(inflater, container, savedInstanceState);
		    setRetainInstance(true);
			View fragmentView = inflater.inflate(R.layout.screen_privacy_terms, container, false);
			
	        termCond = (TextView)fragmentView.findViewById(R.id.user_genre);
	        policyUse = (TextView)fragmentView.findViewById(R.id.post_distance);
	        switcher = (ViewSwitcher)fragmentView.findViewById(R.id.viewSwitcher);
	        termsview=(View)fragmentView.findViewById(R.id.scrollView1);
	        privacyview=(View)fragmentView.findViewById(R.id.scrollView2);
			policyUse.setTextColor(Color.parseColor("#cccccc"));
	    		        
			/*aca definimos el tipo de fuente*/
	        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),"fonts/arial.ttf");
		    TextView text1 = (TextView)fragmentView.findViewById(R.id.text_scroll1);
		    TextView text2 = (TextView)fragmentView.findViewById(R.id.text_scroll2);
		    TextView pouallr = (TextView)fragmentView.findViewById(R.id.pou_all_rigths);
		    termCond.setTypeface(face);
		    policyUse.setTypeface(face);
		    text1.setTypeface(face);
		    text2.setTypeface(face);
		    pouallr.setTypeface(face);
		    
		    termCond.setOnClickListener(new OnClickListener(){
		    	    @Override
		    	    public void onClick(View v){
		    	    	if (switcher.getCurrentView() != termsview) {
				    		termCond.setTextColor(Color.parseColor("#FFFFFF"));
				    		policyUse.setTextColor(Color.parseColor("#cccccc"));
				       		policyUse.setTypeface(null, Typeface.NORMAL);
				       		termCond.setTypeface(null, Typeface.BOLD);
				    		switcher.showPrevious(); 
				        }	
		    	    }
		    	});
		       
		    policyUse.setOnClickListener(new OnClickListener(){
		    	public void onClick(View v){
		    		if (switcher.getCurrentView() != privacyview){  
				   		termCond.setTextColor(Color.parseColor("#cccccc"));
				   		policyUse.setTextColor(Color.parseColor("#FFFFFF"));    		
				   		policyUse.setTypeface(null, Typeface.BOLD);
				   		termCond.setTypeface(null, Typeface.NORMAL);
				    	switcher.showNext();

			    }
		    	}
		    });
		    
		 return fragmentView;
		 }
  
}			
			
			
			
			
					
		
