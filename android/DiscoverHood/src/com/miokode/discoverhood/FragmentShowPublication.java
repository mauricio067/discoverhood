package com.miokode.discoverhood;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.miokode.discoverhood.restful.SDPublication;
import com.miokode.discoverhood.restful.SDPublicationNotification;
import eu.erikw.PullToRefreshListView;

public class FragmentShowPublication extends DialogSherlockListFragment{

	private ViewGroup view;
    private PullToRefreshListView list;
	private AdapterLisItemFeed adapter;
	private ArrayList<SDPublication> JustOnePublication;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {
	    super.onCreateView(inflater, container, savedInstanceState);
	    view = (ViewGroup) inflater.inflate(R.layout.screen_publication, container, false);
	    setRetainInstance(true);
	    return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);		
		SDPublicationNotification publi = (((DiscoverHoodApp) this.getActivity().getApplication()).getPublicationNotification());
		SDPublication p = (publi.getPublication());
		JustOnePublication = new ArrayList<SDPublication>();
	    adapter = new AdapterLisItemFeed(getActivity(), JustOnePublication);
		list = (PullToRefreshListView) this.getListView();      
 	    list.setAdapter(adapter);
 	    adapter.addItem(p);
	}
}
