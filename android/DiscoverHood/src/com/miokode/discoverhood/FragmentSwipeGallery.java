package com.miokode.discoverhood;

import java.util.ArrayList;
import com.actionbarsherlock.app.SherlockFragment;
import com.miokode.discoverhood.restful.SDPublication;
import com.miokode.discoverhood.restful.SDPublicationImage;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;

public class FragmentSwipeGallery extends SherlockFragment{
 
	private ArrayList<SDPublicationImage> Iswipe;
  
	private ImageLoader IL;	
	private View view;	
	private MemoryCache m;
	private SDPublicationImage post;
	private SwipeImagePagerAdapter swipeImageAdapter;
    private ViewPager viewPager;
    private int index;
    
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
    	view = inflater.inflate(R.layout.item_swipe_photo, container, false);
		index = this.getArguments().getInt("Iselected");
        m = new MemoryCache();
	    Iswipe = ((DiscoverHoodApp)getActivity().getApplication()).getImagesSwipe();
	    swipeImageAdapter = new SwipeImagePagerAdapter();
        viewPager = (ViewPager)view.findViewById(R.id.swipe_pager);
        viewPager.setAdapter(swipeImageAdapter);
        viewPager.setCurrentItem(index); 
        viewPager.setOffscreenPageLimit(2); 
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
                FragmentPhotoGallery.mSelected = i;
            }
 
            @Override
            public void onPageSelected(int i) {
            }
 
            @Override
            public void onPageScrollStateChanged(int i) {
 
            }
        });
       return view;
   }
    
    private class SwipeImagePagerAdapter extends PagerAdapter {
    	           	      
    	ImageView imageV;
    	ProgressBar spinner;
    	
    	int pos = -1;

    	boolean commented = false;
    	    	
    	@Override
        public int getCount() {
    		return Iswipe.size();
    	       
        }
      
    	@Override
       	public int getItemPosition(Object object) {
       	    return POSITION_NONE;
       	}
           
             
        public boolean hasCommented(){
        	return commented;
        }
                
        
        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
         
        	
        	LayoutInflater inflater = (LayoutInflater) collection.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            
        	view = inflater.inflate(R.layout.screen_photo, null);
        	            
        	imageV = (ImageView) view.findViewById(R.id.photo);
            
        	post =(SDPublicationImage) Iswipe.get(position);
           	
        	((DiscoverHoodApp) getActivity().getApplicationContext()).setCurrentPublication(post);
            
        	 spinner = (ProgressBar)view.findViewById(R.id.spinner);
             
        	 IL=new ImageLoader(getActivity().getApplicationContext(), 960, m, spinner);
        	
        	if(m.cantBitmaps()>10) m.clear();
        	
        	IL.DisplayImage(post.getPicture().getPictureUrl(), imageV);
        	      
        	TextView text = (TextView)view.findViewById(R.id.photo_text);
            
        	if (post.getText()!=null) text.setText(post.getText());
            
        	final ImageView isha = (ImageView) view.findViewById(R.id.i_share);
            
        	isha.setImageDrawable(null);
            
        	TextView shap = (TextView) view.findViewById(R.id.share);
            
        	shap.setText(null);
            
        	final TextView cantSH = (TextView) view.findViewById(R.id.shares);
        	
        	cantSH.setText(post.getSharesCounter()+" "+getActivity().getResources().getString(R.string.Shares));
	    	
        	TextView cantC = (TextView) view.findViewById(R.id.comments);
            
        	cantC.setText(post.getCommentsCounter()+" "+getActivity().getResources().getString(R.string.Comments));
            
        	((DiscoverHoodApp)getActivity().getApplicationContext()).setIV(cantC);
	    	
        	TableRow comments = (TableRow)view.findViewById(R.id.tableRow_comment);
			
        	comments.setOnClickListener(new OnClickListener() {
				 @Override
	                public void onClick(View v) {
			        	
						 commented = true;
						 
				         pos = position;
											
				         SDPublication ipost = (SDPublicationImage) Iswipe.get(position);
				         
			        	((DiscoverHoodApp) getActivity().getApplicationContext()).setCurrentPublication(ipost);
				            
		                 Intent inte = new Intent(getActivity(), ActivityCommentPost.class);
		                
		                 inte.putExtra("publicationID", ipost.getId());
		                	
		                 getActivity().startActivity(inte);
	                    	
		                 (getActivity()).overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up ); 
                           		                		                   
	               }
	            });
	
        	TextView smiles = (TextView) view.findViewById(R.id.smiles);
        	
        	smiles.setText(post.getSmilesCounter()+" "+getActivity().getResources().getString(R.string.Smiles));
        	
        	TextView smile = (TextView) view.findViewById(R.id.smile);

        	if (post.getIsmile()) {
            	smile.setTextColor(Color.MAGENTA);
            }
			else {
            	smile.setTextColor(Color.WHITE);
			}
          
        	final TextView Ssmiles = (TextView) view.findViewById(R.id.smiles);
        	final TextView Ssmile = (TextView) view.findViewById(R.id.smile);
			TableRow smilesT = (TableRow)view.findViewById(R.id.tableRow_smile);
			smilesT.setOnClickListener(new OnClickListener() {
				@Override
			    public void onClick(View v) {
			    	
			    	SDPublicationImage posti = Iswipe.get(position);

			    	((DiscoverHoodApp) getActivity().getApplicationContext()).togglePublicationsSmiled(posti.getId());
                				    	
                	if(posti.getIsmile()){    
                		
                		posti.setIsmile(false);
                		posti.decreaseSmiles();
                		Ssmiles.setText(posti.getSmilesCounter()+" "+getActivity().getResources().getString(R.string.Smiles));
            			Ssmile.setTextColor(Color.WHITE);
            			
            		}else{

            			posti.increaseSmiles();
           			    Ssmiles.setText(posti.getSmilesCounter()+" "+getActivity().getResources().getString(R.string.Smiles));
            			Ssmile.setTextColor(Color.MAGENTA);
            			posti.setIsmile(true);	
            		}
			    }
	            });
			  
			TextView ClosePic = (TextView)view.findViewById(R.id.close);
				ClosePic.setOnClickListener(new OnClickListener() {
		            @Override
		            public void onClick(View v) {
		            	
		            	FragmentManager FM=getActivity().getSupportFragmentManager();
		            	FM.beginTransaction()
		    			.replace(R.id.content_frame, new FragmentPhotoGallery())
		    			.commit();
		            	
		            }
					});
	             
			if (post.iComment()){
	           TextView Scomments = (TextView) view.findViewById(R.id.comment);
	           Scomments.setTextColor(Color.MAGENTA);
			}
	              			
			collection.addView(view, 0);
			return view;
        }
         
        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }
                
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }
        
        @Override
        public void finishUpdate(ViewGroup arg0) {
        }
 
        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }
        @Override
        public Parcelable saveState() {
            return null;
        }
        @Override
        public void startUpdate(ViewGroup arg0) {
        	
                     	
        }
        
   }
    
   public void onResume(){
    	super.onResume();	
    	if(swipeImageAdapter.hasCommented()){ 		   	
    		swipeImageAdapter.notifyDataSetChanged();
        }
  	}
  
}
    
    
    
