package com.miokode.discoverhood;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.TimeZone;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;
import com.miokode.discoverhood.restful.SDPublication;
import com.miokode.discoverhood.restful.SDPublicationNotification;
import com.miokode.discoverhood.restful.SDPublicationsGETMy;
import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class FragmentUserProfile extends DialogSherlockListFragment{
	@SuppressWarnings("unused")
	private static final Parcelable Liststate = null;
	private AdapterLisItemFeed adapter;
	private ArrayList<SDPublication> Publicaciones;
	private ArrayList<SDPublication> UserPublications;
	private MemoryCache imageCache;
    private ImageLoader Iloader;
    private View mTop;
    private PullToRefreshListView list;
    private String oldestPublicationID;
    @SuppressWarnings("unused")
	private String newestPublicationID;
    private ArrayList<String> oldestPublications;
	private View footerView;
    private TextView nonpubli;
	private View view;
	private boolean loading = false;
	private int index = 0;
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
	    super.onCreateView(inflater, container, savedInstanceState);
	    view = inflater.inflate(R.layout.screen_user_profile, container, false);
		setRetainInstance(true);
		this.nonpubli = (TextView) view.findViewById(R.id.noninformation);
	 	this.nonpubli.setText(this.getResources().getString(R.string.sincontenidouserfeed));

		LinearLayout WritePost= (LinearLayout) view.findViewById(R.id.write_a_post);
        WritePost.setOnClickListener(new OnClickListener() {
        	@Override
            public void onClick(View v) {
              
        	   startActivity(new Intent(getActivity(), ActivityWritePost.class));
    		   getActivity().overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );     
            	
            }
        });
		
		LinearLayout CheckIn= (LinearLayout) view.findViewById(R.id.check_in);
		CheckIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            
    			startActivity(new Intent(getActivity(), ActivityCheckIn.class));
    			getActivity().overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );         
            	
       		}
        });
		
		 LinearLayout TakePicture= (LinearLayout) view.findViewById(R.id.write_post);
	     TakePicture.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	startActivity(new Intent(getActivity(), ActivityImagePost.class));
            	getActivity().overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );       
            	
	        }
	        });
	 
	     return view ;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);		
	
		SD_Url = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerUrl();
        SD_Key = ((DiscoverHoodApp) this.getActivity().getApplication()).getServerKey();
        final SDAccount account = ((DiscoverHoodApp) this.getActivity().getApplication()).getUserDataAccount();
       
        boolean posted = ((DiscoverHoodApp) this.getActivity().getApplication()).isPostSended();
       
        imageCache = ((DiscoverHoodApp)getActivity().getApplication()).getAvatarMemoryCache();
      
        oldestPublications = ((DiscoverHoodApp) getActivity().getApplication()).getUserOldestPublications();
		
        DateFormat format = DateFormat.getDateInstance(1);
		format.setTimeZone(TimeZone.getTimeZone("gmt")); 
		
		Iloader = new ImageLoader(this.getActivity(), 200, imageCache, null);
	   
		mTop = getLayoutInflater(savedInstanceState).inflate(R.layout.item_user_profile, null);
		
		footerView = getLayoutInflater(savedInstanceState).inflate(R.layout.item_footer, null, false);
		
		ImageView avatar = (ImageView)mTop.findViewById(R.id.post_user_image);
	
		if(account.getAvatar()!=null) {
			Iloader.DisplayImage(account.getAvatar().getPictureUrl().trim(), avatar);
			avatar.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					String url = account.getAvatar().getPictureUrl();	
					Intent i= new Intent(getActivity(), ActivityEnlargeAvatar.class);
					i.putExtra("url",url);
					getActivity().startActivity(i);
				}
			});
		}
		
	    TextView name=(TextView)mTop.findViewById(R.id.profile_name);
	    name.setText(account.getName());
	    
	    TextView born=(TextView)mTop.findViewById(R.id.profile_birthday);
	    born.setText(format.format(account.getBirthday()));
		
		TextView age =(TextView) mTop.findViewById(R.id.profile_age);
		age.setText(getString(R.string.Age_edad) + " " +DateTools.getAge(account.getBirthday()));
		
	    TextView edit_profile = (TextView) mTop.findViewById(R.id.edit);
	    edit_profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	((ActivityMenu)getActivity()).switchContent(new FragmentEditUserProfile());
            }
        });
	    
	    list = (PullToRefreshListView) getListView();      
	    ((PullToRefreshListView)list).setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
            	if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
            	}
            	else {		     
            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
            	}	
            	getMyPublications(null, false);
            	list.postDelayed(new Runnable() {
                  @Override
                   public void run() {
                       list.onRefreshComplete();
                   }
                }, 2000);
            }
        });

	    list.setOnScrollListener(new OnScrollListener() {           
		
			@Override
	    	public void onScrollStateChanged(AbsListView view, int scrollState) {
	        	if (scrollState == SCROLL_STATE_IDLE) {
	        		if (list.getLastVisiblePosition() >= list.getCount()-1) {
	        			if(!loading){
		        		   list.addFooterView(footerView);
		   	               loading = true;
		               	if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
		            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
		            	}
		            	else {		     
		            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
		            	}	
		   	               getMyPublications(oldestPublicationID, false);
	        			}
	        		}
	        	  }
	    		
	        }
	         
	        @Override
	        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount){
	        	
	    		}
	    });
	 
	    list.addHeaderView(mTop);
	 	if(savedInstanceState!=null && !posted){
      	   nonpubli.setVisibility(View.GONE);
	 		int listPosition = ((DiscoverHoodApp) getActivity().getApplication()).getListPosition();
 			UserPublications = ((DiscoverHoodApp) getActivity().getApplication()).getUserPublications();
	    	ArrayList<SDPublication> pub = new ArrayList<SDPublication>();
		    adapter = new AdapterLisItemFeed(getActivity(),pub);
		 	list.setAdapter(adapter);
		 	if(UserPublications.size()>5 && UserPublications.size()-1>listPosition){	 
		 		UserPublications.subList(listPosition+1, UserPublications.size()).clear();
		 		oldestPublications.subList(listPosition+1, oldestPublications.size()).clear();
		 		oldestPublicationID = oldestPublications.get(oldestPublications.size()-1);
		 	}
			loadAllPublications();
			
	    }
	   	else{
	  		
	  		((DiscoverHoodApp) this.getActivity().getApplication()).postSended(false);
	    	if (UserPublications != null){
    		  nonpubli.setVisibility(View.GONE);
    		  ArrayList<SDPublication> pub = new ArrayList<SDPublication>();
    		  adapter = new AdapterLisItemFeed(getActivity(),pub);
    		  list.setAdapter(adapter);
    		  loadAllPublications();
	 	    
	    	}
	    	else {
	        	((DiscoverHoodApp) getActivity().getApplication()).ClearUserPublications();
	    		UserPublications = ((DiscoverHoodApp) getActivity().getApplication()).getUserPublications();
	    		Publicaciones = new ArrayList<SDPublication>();
	    		adapter = new AdapterLisItemFeed(getActivity(), Publicaciones);
	    		list.setAdapter(adapter);
            	if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
            	}
            	else {		     
            	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
            	}	
	    		getMyPublications(null, true);
	    	}
	  	
	   	}
	}
  
	private void addPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		while(listItr.hasNext()){
			SDPublication publ = (SDPublication)listItr.next();
			oldestPublicationID = publ.getId();
			oldestPublications.add(oldestPublicationID);
			UserPublications.add(publ);
			adapter.addItem(publ);
			list.onRefreshComplete();
		}
		adapter.notifyDataSetChanged();

	}
	
	private void addOldestPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		while(listItr.hasNext()){
			SDPublication publ = (SDPublication)listItr.next();
			if (oldestPublications.isEmpty() || !oldestPublications.contains(publ.getId())){
				oldestPublicationID = publ.getId();
				oldestPublications.add(oldestPublicationID);
				UserPublications.add(publ);
				adapter.addItem(publ);
			}
		}
		loading = false;
		adapter.notifyDataSetChanged();
	 }
	
	private void addNewestPublications(ArrayList<SDPublication> publications) {
		ListIterator<SDPublication> listItr = publications.listIterator();
		int i = 0;
		while(listItr.hasNext()){
			SDPublication publ = (SDPublication)listItr.next();
			if (oldestPublications.isEmpty() || !oldestPublications.contains(publ.getId())){
				newestPublicationID = publ.getId();
				UserPublications.add(publ);
				adapter.addFromTop(publ,i);
				i++;
			}
		}
		adapter.notifyDataSetChanged();
		list.onRefreshComplete();
	}
	
		
	private void loadAllPublications(){
		Iterator<SDPublication> it = UserPublications.iterator();
		while(it.hasNext()){
			SDPublication publ = it.next();
			oldestPublicationID = publ.getId();
			adapter.addItem(publ);
		}
        adapter.notifyDataSetChanged();
	}
	
	
	/**
	 * Obtengo las publicaciones
	 * 
	 */
	 private void getMyPublications(final String lastID, final boolean showDialog){
		if(lastID==null && showDialog) dialogs.showLoadingProgressDialog(getString(R.string.retrieving_publications));
		
		 new SDPublicationsGETMy(SD_Url, SD_Key, lastID, new SDConnectionResponseListener(){

			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(String response, Object resultElement) {

				Publicaciones = (ArrayList<SDPublication>) resultElement;
				if(Publicaciones.size()>0){
				if(lastID == null && showDialog && Publicaciones.size()>0){ 
					addPublications(Publicaciones);
					dialogs.dismissProgressDialog();
				}
				else {
					  if(lastID == null && !showDialog && Publicaciones.size()>0){
					    addNewestPublications(Publicaciones);
					   }else{	
						   list.removeFooterView(footerView);
						   addOldestPublications(Publicaciones);
					   }
				}
			}else dialogs.dismissProgressDialog();
					  if (adapter.isEmpty()){
						  nonpubli.setVisibility(View.VISIBLE);
					  }
					  else {
						  nonpubli.setVisibility(View.GONE);
					  }
				      getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  

			}

			@Override
			public void onIssues(String issues) {
				dialogs.dismissProgressDialog();
				dialogs.ShowErrorDialog(getString(R.string.server_connection_error) + issues);
				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  

			}

			@Override
			public void onError(SDError error) {
				dialogs.dismissProgressDialog();
				 if (error.getReason() != null ){
					 if (error.getDetails() == null){
						 dialogs.ShowErrorDialog(error.getReason());
					 }else{
						 Enumeration<String> element = error.getDetails().elements();
						 while(element.hasMoreElements()){
							 dialogs.ShowErrorDialog(element.nextElement());
						 }
					 }
				 }else{
					 dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
				 }
				 getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);  

			}	 	 
		 });  
		 
	 }
	  	 
	@Override
	public void onSaveInstanceState(Bundle state) {
	      super.onSaveInstanceState(state);
	}	 
	
	public void onPause(){
		super.onPause();
		index = list.getFirstVisiblePosition();
  		((DiscoverHoodApp) this.getActivity().getApplication()).setListPosition(index);
	}

	public void onResume(){
		super.onResume();
      
		adapter.notifyDataSetChanged();
		
        boolean posted = ((DiscoverHoodApp) this.getActivity().getApplication()).isPostSended();

        if(posted){
           	((DiscoverHoodApp) this.getActivity().getApplication()).postSended(false);
          	((DiscoverHoodApp) getActivity().getApplication()).ClearUserPublications();
    		UserPublications = ((DiscoverHoodApp) getActivity().getApplication()).getUserPublications();
    		Publicaciones = new ArrayList<SDPublication>();
    		adapter = new AdapterLisItemFeed(getActivity(), Publicaciones);
    		list.setAdapter(adapter);
        	if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
        	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
        	}
        	else {		     
        	    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
        	}	
    		getMyPublications(null, true);
    	}
    }
	

	
}
	  
	  
	  
