package com.miokode.discoverhood;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.miokode.discoverhood.restful.SDSSLSocketFactory;
 
public class ImageLoader {
    private MemoryCache memoryCache; 
    private FileCache fileCache;
    private int imageSize;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    private ExecutorService executorService;
    private Handler handler = new Handler();
	private ProgressBar spinner;

    public ImageLoader(Context context, int imageSize, MemoryCache memoryCache, ProgressBar spinner){
    	this.memoryCache = memoryCache;
    	fileCache = new FileCache(context);
        this.imageSize = imageSize;
        executorService = Executors.newFixedThreadPool(5);
        this.spinner = spinner;
    }
    
    public ImageLoader(Context context, int imageSize, MemoryCache memoryCache){
    	this.memoryCache = memoryCache;
    	fileCache = new FileCache(context);
        this.imageSize = imageSize;
        executorService = Executors.newFixedThreadPool(5);
        this.spinner = null;
    }
     
    public void setSpinner(ProgressBar spinner){
    	this.spinner = spinner;
    }
    
    public void DisplayImage(String url, ImageView imageView){
    	imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);
        if(bitmap!=null){
        	if (spinner !=null)spinner.setVisibility(View.GONE);
        	imageView.setImageBitmap(bitmap);
        }
        else{
             queuePhoto(url, imageView);
             
        }
    }
           
    private void queuePhoto(String url, ImageView imageView){
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));

    }
     
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;
        public PhotoToLoad(String u, ImageView i){
            url=u;
            imageView=i;
        }
    }
     
    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;
        PhotosLoader(PhotoToLoad photoToLoad){
            this.photoToLoad=photoToLoad;
        }
         
        @Override
        public void run() {
            try{
                if(imageViewReused(photoToLoad))
                    return;
                Bitmap bmp = getBitmap(photoToLoad.url);
                memoryCache.put(photoToLoad.url, bmp);
                if(imageViewReused(photoToLoad))
                    return;
                BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
                handler.post(bd);
            	if (spinner !=null) spinner.setVisibility(View.INVISIBLE);

                 
            }catch(Throwable th){
                th.printStackTrace();
            }
        }
    }
    

    
    private Bitmap getBitmap(String url){
        File f=fileCache.getFile(url);
        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;
        
        try {
            Bitmap bitmap=null;
            URL imageUrl = new URL(url);
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            HttpsURLConnection conn = (HttpsURLConnection)imageUrl.openConnection();
            SDSSLSocketFactory sslFactory = null;
			try {
				sslFactory = new SDSSLSocketFactory(null);
			} catch (KeyManagementException e1) {
				//e1.printStackTrace();
			} catch (UnrecoverableKeyException e1) {
				//e1.printStackTrace();
			} catch (NoSuchAlgorithmException e1) {
				//e1.printStackTrace();
			} catch (KeyStoreException e1) {
				//e1.printStackTrace();
			}

            conn.setSSLSocketFactory(sslFactory.getSslFactory());
            

            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();
                      
            OutputStream os = new FileOutputStream(f);
                        
            Utils.CopyStream(is, os);
             
            os.close();
            conn.disconnect();
                         
            bitmap = decodeFile(f);
             
            return bitmap;
             
        } catch (Throwable ex){
           ex.printStackTrace();
           if(ex instanceof OutOfMemoryError)
               memoryCache.clear();
           return null;
        }
    }
 
    private Bitmap decodeFile(File f){
        try {
             
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o);
            stream1.close();
             
            final int REQUIRED_SIZE=imageSize;
             
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2 < REQUIRED_SIZE || height_tmp/2 < REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }
             
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
             
        } catch (FileNotFoundException e) {
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
     
    boolean imageViewReused(PhotoToLoad photoToLoad){
         
        String tag=imageViews.get(photoToLoad.imageView);
        if(tag==null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }
  
    
    
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        
        public BitmapDisplayer(Bitmap b, PhotoToLoad p){
        	bitmap=b;
        	photoToLoad=p;
        }
        public void run() {
            if(imageViewReused(photoToLoad))
                return;
             
            if(bitmap!=null)
                photoToLoad.imageView.setImageBitmap(bitmap);
        
        }
    }
 
    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }
 
    private static class Utils {
        public static void CopyStream(InputStream is, OutputStream os){
            final int buffer_size=1024;
            try {
                 
                byte[] bytes=new byte[buffer_size];
                for(;;){
                    
                  int count=is.read(bytes, 0, buffer_size);
                  if(count==-1)
                      break;
                   
                  os.write(bytes, 0, count);
                }
            }
            catch(Exception ex){}
        }
    }
    
    public class NullHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            //Log.i("RestUtilImpl", "Approving certificate for " + hostname);
            return true;
        }
    }
}