package com.miokode.discoverhood;

import java.util.Date;

import android.graphics.Bitmap;
  
  
public class ItemDialogNotify {
	private String Id, Nombre, Mensaje;
	private Bitmap Imagen;
	private Date Fecha;
	private int type;
	
	public ItemDialogNotify(String id, String nombre, String mensaje,
			Bitmap imagen, Date fecha) {
		super();
		Id = id;
		Nombre = nombre;
		Mensaje = mensaje;
		Imagen = imagen;
		Fecha = fecha;
		
	}
	public ItemDialogNotify(String id, String nombre, String mensaje, int typ) {
		super();
		Id = id;
		Nombre = nombre;
		Mensaje = mensaje;
		type = typ;
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getMensaje() {
		return Mensaje;
	}
	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}
	public Bitmap getImagen() {
		return Imagen;
	}
	public void setImagen(Bitmap imagen) {
		Imagen = imagen;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public int gettype() {
		return type;
	}
	public void settype(int typ) {
		type= typ;
	}
}
