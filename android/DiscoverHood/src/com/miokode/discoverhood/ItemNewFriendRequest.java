package com.miokode.discoverhood;

import android.graphics.Bitmap;

public class ItemNewFriendRequest {
	
	private Bitmap Image;
	private String name;
	private String avatarURL;
	
	public ItemNewFriendRequest(Bitmap image, String name) {
		Image = image;
		this.name = name;
	}
	
	public ItemNewFriendRequest(String name) {
		this.name = name;
	}

	public Bitmap getImage() {
		return Image;
	}

	public void setImage(Bitmap image) {
		Image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAvatarURL() {
		return avatarURL;
	}
}
