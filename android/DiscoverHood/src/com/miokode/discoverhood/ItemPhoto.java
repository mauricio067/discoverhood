package com.miokode.discoverhood;

public class ItemPhoto {
	
	private int smiles;
	private int shares;
	private int comments;
	private String text;
	private String url;
	
		
	public ItemPhoto(String url) {
		this.url = url;
		smiles=shares=comments=0;
		text="";
	}
	
	public ItemPhoto(String url, int smiles, int shares, int comments, String text) {
		super();
		this.url = url;
		this.smiles = smiles;
		this.shares = shares;
		this.comments = comments;
		this.text=text;
	}
	
	public ItemPhoto(){
		smiles=shares=comments=0;
		text="";
		url=" ";
	}
	
	
	public int getSmiles() {
		return smiles;
	}
	
	public void setSmiles(int smiles) {
		this.smiles = smiles;
	}
	
	public int getShares() {
		return shares;
	}
	
	public void setShares(int shares) {
		this.shares = shares;
	}
	
	public int getComments() {
		return comments;
	}
	
	public void setComments(int comments) {
		this.comments = comments;
	}
	
	public void setText(String text){
		this.text=text;
	}
	
	public String getText(){
		return text;
	}
	public String getURL(){
		return url;
	}
	public void setURL(String url){
		this.url=url;
	}
		
}
