package com.miokode.discoverhood;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.miokode.discoverhood.restful.SDAccount;

public class LoginTools implements LocationListener {
	 private SessionManagement generalSession;
	 private ConnectionDialogs dialogs;
	 private Activity context;
     private String SD_url = null;
	 private String SD_Key = null;
	 private SDAccount account;
	 private LocationManager lm;  
     private Location location = null;
     private boolean canGetLocation = false;
 	 private boolean isGPSEnabled = false; 
	 private boolean isNetworkEnabled = false;
	 private Builder alertDialog;

	 public LoginTools(Activity context){
		 this.context = context;
		 this.SD_Key = ((DiscoverHoodApp) context.getApplication()).getServerKey();
		 this.SD_url = ((DiscoverHoodApp) context.getApplication()).getServerUrl();	 
		 generalSession = new SessionManagement(context.getApplication()); 
		 dialogs = new ConnectionDialogs(context);
		 lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
			
		 context.stopService(new Intent(context.getApplication(),DaemonForTasks.class));
		 context.stopService(new Intent(context.getApplication(),DaemonMessages.class));
	 }

	 public void startNextActivity(SDAccount account){
		    this.account = account;
		   	if (account!=null){ 
		    	if((((DiscoverHoodApp) context.getApplication()).CanGetLocation()) == false){
		    		getLocation();
		    	}else{
		  			if(account.getActive()){
		  				startNotificationService();
		  				startMessagesService();
		  				Intent intent = new Intent(context, DaemonGeolocalization.class);
		  				context.startService(intent);
		  				generalSession.createLoginSession(((DiscoverHoodApp) context.getApplication()).getServerKey(), account.getFacebook_login() );
		  				Intent i =new Intent(context, ActivityMenu.class);
		  				context.startActivity(i);
		  			
		  			}else{
		  				context.startActivity(new Intent(context, ActivityValidateCode.class));
		  			}
		  			context.overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
		  			context.finish();
		    	}
			}else{
		    	dialogs.ShowErrorDialogIteractive(context.getResources().getString(R.string.account_no_active),  new DialogInterface.OnClickListener() {	
		 			public void onClick(DialogInterface dialog, int which) {
		 				dialog.dismiss();	  	
		 				context.finish();
		 		    }
		 	});
			}
	    }
	   
	public void setSDKey(String SD_Key){
		this.SD_Key = SD_Key;
	}
	 
	 
	/**
	 * Dispara el servicio de notifiaciones   
	 */
	public void startNotificationService(){
		context.stopService(new Intent(context.getApplication(),DaemonForTasks.class));
	 	final long REPEAT_TIME = 1000 * 35;
	    AlarmManager service = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);   
	    Intent i = new Intent(context, DaemonForTasks.class);	   
	    Bundle bundle = new Bundle();
	    bundle.putString("SD_Key", this.SD_Key);
	    bundle.putString("SD_url", this.SD_url);	    
	    bundle.putBoolean("startDN", true);

	    i.putExtras(bundle);	    		    
	    PendingIntent pending = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);	   
	    Calendar cal = Calendar.getInstance();	   		    
	    cal.add(Calendar.SECOND, 2);	  
	    
	    service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), REPEAT_TIME, pending);
	 }
	 
	/**
	 * Dispara el servicio de mensajeria instantanea
	 */
	public void startMessagesService(){
		context.stopService(new Intent(context.getApplication(),DaemonMessages.class));
	 	final long REPEAT_TIME = 1000 * 30;
	    AlarmManager service = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);   
	    Intent i = new Intent(context, DaemonMessages.class);	   
	    Bundle bundle = new Bundle();
	    bundle.putString("SD_Key", this.SD_Key);
	    bundle.putString("SD_url", this.SD_url);		    
	    bundle.putBoolean("startDN", true);

	    i.putExtras(bundle);	    		    
	    PendingIntent pending = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);	   
	    Calendar cal = Calendar.getInstance();	   		    
	    cal.add(Calendar.SECOND, 1);	  
	    
	    service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), REPEAT_TIME, pending);
     }
	
	public boolean isActiveGPS(){
		isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return (isGPSEnabled || isNetworkEnabled);       	
	}
	
    public void ActivateGPS(){
		 alertDialog = new AlertDialog.Builder(context);
		 alertDialog.setTitle(context.getString(R.string.gps_no_enabled));
		 alertDialog.setMessage(context.getString(R.string.enable_gps));     
		 alertDialog.setPositiveButton(context.getString(R.string.accept_gps), new DialogInterface.OnClickListener() {
	    	 public void onClick(DialogInterface dialog,int which) {
	             Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	             context.startActivity(intent);
	             dialog.cancel();
	             //getLocation();
	         }
	     });

	     alertDialog.setNegativeButton(context.getString(R.string.no_accept_gps), new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int which) {
	        	 dialog.cancel();
	        	 context.finish();
	         }
	     });
	     alertDialog.show();
	}
    
    
    public void ActivateGPS(DialogInterface.OnClickListener positive){
		 alertDialog = new AlertDialog.Builder(context);
		 alertDialog.setTitle(context.getString(R.string.gps_no_enabled));
		 alertDialog.setMessage(context.getString(R.string.enable_gps));     
		 alertDialog.setPositiveButton(context.getString(R.string.accept_gps), positive);

	     alertDialog.setNegativeButton(context.getString(R.string.no_accept_gps), new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int which) {
	        	 dialog.cancel();
	        	 context.finish();
	         }
	     });
	     alertDialog.show();
	}
    
	 
	 public void getLocation() {
		 try {	
	            isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
	            isNetworkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	            if (!isGPSEnabled && !isNetworkEnabled) {       	
	            	ActivateGPS();	
	            	alertDialog.show();
	            } else {  
	            	canGetLocation = true;
	            }
	           
	            if(canGetLocation){
	                if (isNetworkEnabled) {
	                    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	                    if (lm != null) {
	                    	location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);                    
	                    	if (location != null) {                               
	                        	((DiscoverHoodApp) context.getApplication()).setLatitude(location.getLatitude());             
	                            ((DiscoverHoodApp) context.getApplication()).setLongitude(location.getLongitude());
	                        }
	                    }
	                }
	                else {   if (isGPSEnabled) {
	                			if (location == null) {                				
	                				lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);    
	                				if (lm != null) {
	                					location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	                					if (location != null) {                   
	                						((DiscoverHoodApp) context.getApplication()).setLatitude(location.getLatitude());
	                						((DiscoverHoodApp) context.getApplication()).setLongitude(location.getLongitude());
	                					}
	                				}
	                			}
	                		}
	                }
	                
	                if(location.getLatitude()!=0 && location.getLongitude()!=0) {
	                	canGetLocation = true;
						((DiscoverHoodApp) context.getApplication()).setCanGetLocation(canGetLocation);
	                }
	                else{
	                	canGetLocation = false;
						((DiscoverHoodApp) context.getApplication()).setCanGetLocation(canGetLocation);
	                }
	             }
	            else {
	            	((DiscoverHoodApp) context.getApplication()).setLatitude(0);
					((DiscoverHoodApp) context.getApplication()).setLongitude(0);
					canGetLocation = false;
					((DiscoverHoodApp) context.getApplication()).setCanGetLocation(canGetLocation);
	            }
	 
	        } catch (Exception e) {
	        	dialogs.ShowErrorDialogIteractive(context.getResources().getString(R.string.cannot_get_location),  new DialogInterface.OnClickListener() {	
	     			public void onClick(DialogInterface dialog, int which) {
	     				dialog.dismiss();	  	
	     				context.finish();
	     		    }
	     		});

	        	//TODO BORRAR ESTO ANTES DE ENTREGAR !!!!!!!!!!!!!!!!!!! 
	        	((DiscoverHoodApp) context.getApplication()).setLatitude(0);
				((DiscoverHoodApp) context.getApplication()).setLongitude(0);
				canGetLocation = true;
				((DiscoverHoodApp) context.getApplication()).setCanGetLocation(canGetLocation);
				///////////////////////////////////////////////////////////////////////
	        }
	        
	    }


	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
}
