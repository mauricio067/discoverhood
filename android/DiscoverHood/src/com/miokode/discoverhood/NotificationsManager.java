package com.miokode.discoverhood;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.miokode.discoverhood.DaemonForTasks.updateNotificationAsyncTask;
import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDConversationNotification;
import com.miokode.discoverhood.restful.SDFriendNotification;
import com.miokode.discoverhood.restful.SDFriendRequester;
import com.miokode.discoverhood.restful.SDPublicationNotification;
import com.miokode.discoverhood.restful.SDPublicationNotification.Action;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;


public class NotificationsManager {

	private NotificationManager notificationManager;
	private Notification myNotification;
	private Context context;
    private final int ACTION_SCS = 1;
    private final int MESSAGE = 2;
    private final int FRIENDREQUEST = 3;
    private final int NEWFRIEND = 4;
    private ArrayList<SDConversationNotification> conversationNotifications;
	private ArrayList<SDPublicationNotification> publicationNotifications;
	private ArrayList<SDFriendNotification> friendRequestNotifications;
	private int cantNewMessages = 0;
	private int cantNewActions = 0;
	private int cantNewFriendsR = 0;
	@SuppressWarnings("unused")
	private int notID;
	private ArrayList<String> messagesNid;
	private ArrayList<String> friendRNid;
	private ArrayList<String> actionsNid;
	private SDAccount account;
    private ArrayList<Integer> NAids;
	private ArrayList<Integer> NMids;
	private ArrayList<Integer> NFids;
	
    public NotificationsManager(Context context){
		this.context = context;
		account = ((DiscoverHoodApp) context.getApplicationContext()).getUserDataAccount();
		messagesNid = ((DiscoverHoodApp) context.getApplicationContext()).getMessagesNid();
		friendRNid = ((DiscoverHoodApp) context.getApplicationContext()).getFriendRequestsNid();
		actionsNid = ((DiscoverHoodApp) context.getApplicationContext()).getActionsNid();
		conversationNotifications = ((DiscoverHoodApp) context.getApplicationContext()).getConversationNotifications();
		publicationNotifications = ((DiscoverHoodApp) context.getApplicationContext()).getPublicationNotifications();
		friendRequestNotifications = ((DiscoverHoodApp) context.getApplicationContext()).getFriendRequestNotifications();
		cantNewMessages = cantNewActions = cantNewFriendsR = 0;
		notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NAids = ((DiscoverHoodApp)context.getApplicationContext()).getNAids();
		NMids = ((DiscoverHoodApp)context.getApplicationContext()).getNMids();
		NFids = ((DiscoverHoodApp)context.getApplicationContext()).getNFids();
			
	}
	
	public void sendNotification(String title, String message, int type, int notID, SDPublicationNotification publiN){

		if (type == 2 || type == 4 || type == 3 || (publiN!=null && !publiN.isReaded())){
			
			this.notID = notID;
			
			Intent intent = null;
			switch(type){
			
			case MESSAGE: {

				((DiscoverHoodApp)context.getApplicationContext()).setFragmentNotification("M");
				intent = new Intent(context, ActivityNotificationBridge.class);
				intent.putExtra("M", MESSAGE);

	    		
			}break;
			
			case ACTION_SCS:{
				
				intent = new Intent(context, ActivityShowPublication.class); 
				intent.putExtra("ID", publiN.getId());
				
			}break;
			
			case FRIENDREQUEST: {
				intent = new Intent(context,  ActivityNotificationBridge.class); 
				intent.putExtra("FR", FRIENDREQUEST);
			
			}break;
			
			case NEWFRIEND: {
				intent = new Intent(context,  ActivityNotificationBridge.class); 
				intent.putExtra("FR", FRIENDREQUEST);
			
			}break;
			
			}
		    	    
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
	        stackBuilder.addParentStack(ActivityMain.class);
	        stackBuilder.addNextIntent(intent);
	        PendingIntent pIntent = PendingIntent.getActivity(context, notID, intent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);
	       
	        myNotification = new NotificationCompat.Builder(context)
	        .setContentTitle(title)
	        .setContentText(message)
	        .setWhen(System.currentTimeMillis())
	        .setContentIntent(pIntent)
	        .setDefaults(Notification.DEFAULT_SOUND)
	        .setAutoCancel(true)
	        .setSmallIcon(R.drawable.logo_white_)
	        .build();
					
			//notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.notify(notID, myNotification);
					
		}
	}
	
	
    public void manageMessageNotifications(ArrayList<SDConversationNotification> NM){ 
    	cantNewMessages = 0;
    	Iterator<SDConversationNotification> ite = NM.iterator();
		 while(ite.hasNext()){
			 SDConversationNotification nm = (SDConversationNotification) ite.next();
			 String key = nm.getId();
			 if (messagesNid.isEmpty() || !messagesNid.contains(key)){
					messagesNid.add(key);
					//conversationNotifications.add(nm);
					if (!nm.isReaded())
						cantNewMessages++;
			}
		 }
		 
		if(cantNewMessages>0) {
			NMids.add(NM.hashCode());
			sendNotification(context.getString(R.string.new_messages_conversations), cantNewMessages+" "+ context.getString(R.string.new_messages), MESSAGE, NM.hashCode(), null);
			((DiscoverHoodApp) context.getApplicationContext()).setCantconversationNotifications(cantNewMessages  + ((DiscoverHoodApp) context.getApplicationContext()).getCantconversationNotifications());
			cantNewMessages = 0;
			((DiscoverHoodApp)context.getApplicationContext()).setConversationNotifications(NM);
		}			 
    }
	
		 
    /*ADMINISTRADOR DE LAS NOTIFICACIONES*/
    public void manageActionNotifications(final ArrayList<SDPublicationNotification> APN){	
		Iterator<SDPublicationNotification> ite = APN.iterator();
		while(ite.hasNext() && account!=null){
			SDPublicationNotification apn = (SDPublicationNotification) ite.next(); 
			String key = apn.getId();
			
			if ((actionsNid.isEmpty() || !actionsNid.contains(key))){

				actionsNid.add(key);
				publicationNotifications.add(cantNewActions,apn);
				if (!apn.isReaded()){
					cantNewActions++;
			}
				
							
			Action action =  apn.getAction();
			switch(action){

				case SMILE: {
					NAids.add(apn.getId().hashCode());
					sendNotification(apn.getActioner().getName(), context.getString(R.string.new_smiles), ACTION_SCS, apn.getId().hashCode(), apn);
				}
				break;
			
				case COMMENT: {
					NAids.add(apn.getId().hashCode());
					sendNotification(apn.getActioner().getName(), context.getString(R.string.new_comments),ACTION_SCS, apn.getId().hashCode(), apn);
				}
			    break;
				
				case SHARED:{
					NAids.add(apn.getId().hashCode());
					sendNotification(apn.getActioner().getName(), context.getString(R.string.new_shares),ACTION_SCS, apn.getId().hashCode(), apn);
				}
				break;
			
				default:
				break;
				}
			}
		}
		   	((DiscoverHoodApp) context.getApplicationContext()).setCantpublicationNotifications(cantNewActions  + ((DiscoverHoodApp) context.getApplicationContext()).getCantpublicationNotifications());
			cantNewActions = 0;
	}
	    
    public void manageFriendNotifications(ArrayList<SDFriendNotification> FN){
		 Iterator<SDFriendNotification> ite = FN.iterator();
		 //Log.d("[NF]", "llega a manadar noti");
		 while(ite.hasNext()){
			SDFriendNotification friendR = (SDFriendNotification) ite.next();
			String key = friendR.getId();
			if (friendRNid.isEmpty() || !friendRNid.contains(key)){
				friendRNid.add(key);
				friendRequestNotifications.add(friendR);
				cantNewFriendsR++;
				NFids.add(friendR.getId().hashCode());
				sendNotification(friendR.getSender().getName(), context.getString(R.string.new_friend), NEWFRIEND, friendR.getId().hashCode(), null);
 
			}
		 }
		 
		 if(cantNewFriendsR>0) {
		   	((DiscoverHoodApp) context.getApplicationContext()).setCantfriendRequestNotifications(cantNewFriendsR);
		   	
		   	cantNewFriendsR = 0;
		 }
    }
    
	public void manageFriendRequestNotifications(ArrayList<SDFriendRequester> frienshipsRequests) {
		int notID = frienshipsRequests.get(0).getId().hashCode();
		NFids.add(notID);
		sendNotification(context.getString(R.string.new_friend_requests_title),(context.getString(R.string.new_friend_request))+" "+frienshipsRequests.size()+" "+ context.getString(R.string.friend_request), FRIENDREQUEST, notID, null);
		
	}
	
	
	/*type = 1 : SMILES, SHARES AND COMMENTS
	 * 
	 * type = 2 : MESSAGES
	 * 
	 * type = 3 : FRIENDs REQUEST
	 * 
	 * type = 0 : ALL
	 */
	public void removeNotifications(int type){
		
		removeNotificationActionTask NotificationR = new removeNotificationActionTask(type);
    	NotificationR.execute();
	}
	
	
	class removeNotificationActionTask extends AsyncTask <Void, Void, Void> {
		 
		private int NotificacionaRemover;
		
		public removeNotificationActionTask(int noti){
			NotificacionaRemover = noti;
		}
		
        @Override
        protected Void doInBackground(Void... params) {
  	
        	switch(NotificacionaRemover){
        		case 1: {
        				for(int id:NAids){
        					notificationManager.cancel(id);
        				}
        				NAids.clear();
        		}break;
        	
        		case 2: {
						for(int id:NMids){
							notificationManager.cancel(id);
						}
						NMids.clear();
        		}break;
	
        		case 3: {
        				for(int id:NFids){
        					notificationManager.cancel(id);
        				}
        				NFids.clear();
        		}break;
        		
        		case 0: {
    					for(int id:NFids){
    						notificationManager.cancelAll();
    					}
    					NFids.clear();
    					NAids.clear();
    					NFids.clear();
        		}break;
        	
        	}
        	
        	return null;
        }
        
    }
	
	
	//METODO QUE SE DEBE LLAMAR PARA REMOVER NOTIFICACIONES DE LA BARRA DE SISTEMA
	/*	private void clearNotificationsBar(Context context){
	NotificationsManager nm=new NotificationsManager(context);
	nm.removeNotifications(2);
    
}*/

}
