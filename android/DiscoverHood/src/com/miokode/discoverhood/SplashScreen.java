package com.miokode.discoverhood;

import java.util.HashMap;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.facebook.AppEventsLogger;
import com.miokode.discoverhood.restful.SDAccount;
import com.miokode.discoverhood.restful.SDAccountGETMy;
import com.miokode.discoverhood.restful.SDConnectionResponseListener;
import com.miokode.discoverhood.restful.SDError;

public class SplashScreen extends  FragmentActivity {

    private static int SPLASH_TIME_OUT = 1000;
    private SessionManagement generalSession;
    private ConnectionDialogs dialogs;
    private String SD_url = null;
	private SDAccount account = null;
	private LoginTools LoginHelper;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_splash);
       //((DiscoverHoodApp)getApplication()).setSplashScreen(this);
        generalSession = new SessionManagement(getApplicationContext()); 
        dialogs = new ConnectionDialogs(this);
        SD_url = ((DiscoverHoodApp) this.getApplication()).getServerUrl();
                
		stopService(new Intent(this.getApplicationContext(),DaemonForTasks.class));
		stopService(new Intent(this.getApplicationContext(),DaemonMessages.class));

    }
    
    @Override
    protected void onResume() {
        super.onResume();
        LoginHelper = new LoginTools(this);
        //start();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
        new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
	           	if (!LoginHelper.isActiveGPS()){
	           		LoginHelper.ActivateGPS();
	           	}else{
	           		start();
	           	}
           }
       }, SPLASH_TIME_OUT);
    }
    
    
    private void start(){
            if (generalSession.isLoggedIn() && generalSession.isFacebookLoggedIn()) {
            	HashMap<String, String> user = generalSession.getUserDetails();
                String SD_KEY = user.get(SessionManagement.SD_KEY);
                LoginHelper.setSDKey(SD_KEY);
                ((DiscoverHoodApp) SplashScreen.this.getApplication()).setServerKey(SD_KEY);
       		  	Login();
        	}else{
                Intent i = new Intent(SplashScreen.this, ActivityMain.class);
                startActivity(i);
                finish();
        	}
    }
    
    public void startNextActivity(){
	   	if (account!=null){ 
  			if(account.getActive()){
  				LoginHelper.startNotificationService();
  				LoginHelper.startMessagesService();

  				generalSession.createLoginSession(((DiscoverHoodApp) this.getApplication()).getServerKey(), account.getFacebook_login() );
  				Intent i =new Intent(this, ActivityMenu.class);
  				this.startActivity(i);
  			}else{
  				this.startActivity(new Intent(this, ActivityValidateCode.class));
  			}
  			this.overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
  			this.finish();
	    }
		
    }
    
	private void guardarUsuarioApp( SDAccount account){
		((DiscoverHoodApp) this.getApplication()).setUserDataAccount(account);
		this.account = account;
	}
	
    //***********************************************************
  	//******************** Server connection  *******************
  	//***********************************************************
  	private void Login(){
  			dialogs.showLoadingProgressDialog(getString(R.string.connecting));
  			 String SD_Key = ((DiscoverHoodApp) this.getApplication()).getServerKey();
  			 LoginHelper.setSDKey(SD_Key);
  			 new SDAccountGETMy(SD_url, SD_Key, new SDConnectionResponseListener(){

  				@Override
  				public void onSuccess(String response, Object resultElement) {
  					dialogs.dismissProgressDialog();
  					SDAccount account = (SDAccount) resultElement;
  					guardarUsuarioApp(account);
  					startNextActivity();
  				}

  				@Override
  				public void onIssues(String issues) {
  					dialogs.dismissProgressDialog();
  					dialogs.ShowErrorDialog(getString(R.string.server_connection_error));
  				}

  				@Override
  				public void onError(SDError error) {
  					dialogs.dismissProgressDialog();
  					 if ( error!=null&& error.getCode() == 401 ) {
  						 if (account!=null && account.getFacebook_login()){
  					        	dialogs.ShowErrorDialogIteractive(getString(R.string.server_connection_error),  new DialogInterface.OnClickListener() {	
  					     			public void onClick(DialogInterface dialog, int which) {
  					     				dialog.dismiss();	  	
  					     				SplashScreen.this.finish();
  					     		    }
  					     		});
  						 }else{
					        	dialogs.ShowErrorDialogIteractive(getString(R.string.invalid_user_pass),  new DialogInterface.OnClickListener() {	
					     			public void onClick(DialogInterface dialog, int which) {
					     				dialog.dismiss();	  	
					                    Intent i = new Intent(SplashScreen.this, ActivityMain.class);
					                    startActivity(i);
					                    finish();
					     				SplashScreen.this.finish();
					     		    }
					     		});
  						 }
  					 }else{
				         dialogs.ShowErrorDialogIteractive(getString(R.string.server_connection_error),  new DialogInterface.OnClickListener() {	
				     			public void onClick(DialogInterface dialog, int which) {
				     				dialog.dismiss();	 
				                    Intent i = new Intent(SplashScreen.this, ActivityMain.class);
				                    startActivity(i);
				                    finish();
				     				SplashScreen.this.finish();
				     		    }
				     	 });
  					 }
  				}	 	 
  			 }); 
  		 }
  	
}
