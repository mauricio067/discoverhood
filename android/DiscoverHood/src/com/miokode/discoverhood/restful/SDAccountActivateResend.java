package com.miokode.discoverhood.restful;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import android.util.Log;

public class SDAccountActivateResend extends SDConnection {
	
	public SDAccountActivateResend(String server, String key, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		execute();
	}

	@Override
	protected void execute() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();     	        
		this.rpc = "resend_validation_email";
		this.post("resend_validation_email", params, respuestaListener);
		
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

}
