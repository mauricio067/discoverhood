package com.miokode.discoverhood.restful;

import android.util.Log;


public class SDAccountGET extends SDConnection {
	private String userid;
	
	public SDAccountGET(String server, String key, String userid, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.userid = userid;
		execute();
	}

	@Override
	protected void execute() {
		this.get("user/"+this.userid, respuestaListener);
	}

	@Override
	protected void processResponse( String result ) {
		//Log.i("[debugrind]", result.toString());
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		SDAccount account = null;
		if (!serverResponse.getMultipleItems()) {
			//Log.i("[account info]", serverResponse.getItemResponse().toString() );
			account = new SDAccount(serverResponse.getItemResponse());
			respuestaListener.onSuccess(result, account);
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}

}
