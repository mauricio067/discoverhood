package com.miokode.discoverhood.restful;

import java.io.ByteArrayOutputStream;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import android.graphics.Bitmap;

public class SDAccountPOST extends SDConnection {
		private SDAccount account;
		
		public SDAccountPOST(String server, String key, SDAccount account, SDConnectionResponseListener respuestaListener) {
			super(server, key, respuestaListener);
			this.account = account;
			execute();
		}

		@Override
		protected void execute() {
	        MultipartEntityBuilder entity = MultipartEntityBuilder.create();
	        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE); 
	     	entity.addTextBody("firstname",account.getFirstname().trim());
	     	entity.addTextBody("lastname", account.getLastname().trim());
	        entity.addTextBody("password", account.getPassword().trim());
	        entity.addTextBody("email", account.getEmail().trim());
	        entity.addTextBody("gender", account.getGender());
	        entity.addTextBody("birthday", SDTools.convertDate2String( account.getBirthday()));
	        if (account.getBitMapAvatar() != null){ 
		        ByteArrayOutputStream bao = new ByteArrayOutputStream();
		        account.getBitMapAvatar().compress(Bitmap.CompressFormat.PNG, 90, bao);
		        byte[] data = bao.toByteArray();
		        entity.addBinaryBody("avatar", data, ContentType.DEFAULT_BINARY,"avatar_"+account.getFirstname()+"_"+account.getLastname()+".png");
	        }   
	        if (account.getFacebook_login()){
	        	entity.addTextBody("fb_code", account.getFacebookCode());
	        }
			this.rpc = "create_account";
			this.postMultipart("create_account", entity, respuestaListener);     
		}

		@Override
		protected void processResponse( String result ) {
			 respuestaListener.onSuccess(result, null);
		}


}