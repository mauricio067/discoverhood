package com.miokode.discoverhood.restful;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import android.graphics.Bitmap;
import android.util.Log;

public class SDAccountPOSTEdit extends SDConnection {
	private SDAccount edited;
	private SDAccount original;
	
	public SDAccountPOSTEdit(String server, String key, SDAccount original, SDAccount edited, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.original = original;
		this.edited = edited;
		execute();
	}

	@Override
	protected void execute() {
        MultipartEntityBuilder entity = MultipartEntityBuilder.create();
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE); 
        if (edited.getNickname()!=null && edited.getNickname().length()>0 ){
        	entity.addTextBody("nickname",edited.getNickname());
        }
        if (edited.getFirstname()!=null && edited.getFirstname().length()>0 && edited.getFirstname().compareTo(original.getFirstname()) != 0){
        	entity.addTextBody("firstname",edited.getFirstname());
        }
        if (edited.getLastname()!=null && edited.getLastname().length()>0 && edited.getLastname().compareTo(original.getLastname()) != 0){
        	entity.addTextBody("lastname",edited.getLastname());
        }

     	if (!edited.getFacebook_login()){
     		if (edited.getPassword()!=null && edited.getPassword().length()>0)
     			entity.addTextBody("password", edited.getPassword());
            if (edited.getEmail()!=null && edited.getEmail().length()>0 && edited.getEmail().compareTo(original.getEmail()) != 0){
            	entity.addTextBody("email",edited.getEmail());
            }
     	}
     	
     	if (edited.getGender().compareTo(original.getGender()) != 0)
     		entity.addTextBody("gender", edited.getGender());
        
        Date dateOriginal = original.getBirthday();
        Date dateEdited = edited.getBirthday();
        if (dateOriginal.compareTo(dateEdited) != 0){
    		entity.addTextBody("birthday", SDTools.convertDate2String( edited.getBirthday()));
        }

        if (edited.getBitMapAvatar() != null){ 
	        ByteArrayOutputStream bao = new ByteArrayOutputStream();
	        edited.getBitMapAvatar().compress(Bitmap.CompressFormat.PNG, 90, bao);
	        byte[] data = bao.toByteArray();
	        entity.addBinaryBody("avatar", data, ContentType.DEFAULT_BINARY,"avatar_"+edited.getFirstname()+"_"+edited.getLastname()+".png");
        }
        
		this.rpc = "account/edit";
		this.postMultipart("account/edit", entity, respuestaListener);
        
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		SDAccount account = null;
		if (!serverResponse.getMultipleItems()) {
			account = new SDAccount(serverResponse.getItemResponse());
			respuestaListener.onSuccess(result, account);
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}