package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

public class SDAccountPOSTReportStatus extends SDConnection {
	private double latitude;
	private double longitude;
	
	public SDAccountPOSTReportStatus(String server, String key, double latitude, double longitude, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.latitude = latitude;
		this.longitude = longitude;
		execute();
	}

	@Override
	protected void execute() {   
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("latitude", this.latitude );	  
	        obj.put("longitude", this.longitude );	 
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "report_status";
		this.postJSON("report_status", obj, respuestaListener); 
	}

	@Override
	protected void processResponse( String result ) {
		 respuestaListener.onSuccess(result, null);
	}


}