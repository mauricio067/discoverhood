package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;
/*
 * Clase utilizada para mantener informacion acerca del autor de publicaciones, mensajes, comentarios, etc
 * 
 */
public class SDAuthor  {
	private String id;
	private String name;
	private SDImage avatar;
	
	
	/**
	 * Constructor utilizado para crear elementos en el sever
	 * 
	 * @param ID
	 * 		ID del author
	 */
	public SDAuthor(String ID){
		this.id = ID;
	}
	
	public SDAuthor(JSONObject json){
		this.id = getValueFromJSONString("_id", "", json);
		this.name = getValueFromJSONString("name", "", json);
	    try {
			this.avatar = new SDImage(json.getJSONObject("avatar"));
		} catch (JSONException e) {
			this.avatar = null;
			e.printStackTrace();
		} 
	}

	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto.
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 * 
	 */
	private String getValueFromJSONString( String JsonTag, String defaultValue, JSONObject json ){
		try{ 
			return json.getString(JsonTag); 
		} catch (JSONException e) {
			e.printStackTrace();
			return defaultValue; 
		}
	}
	
	public String getId(){
		return this.id;
	}
	
	public String getName() {
		return name;
	}

	public SDImage getAvatar() {
		return avatar;
	}
	public void setAvatar(SDImage avat){
		avatar = avat;
	}
	
}
