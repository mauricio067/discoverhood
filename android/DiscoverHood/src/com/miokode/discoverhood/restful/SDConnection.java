package com.miokode.discoverhood.restful;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;


public abstract class SDConnection {
	public static final int GET = 0;
	public static final int POST = 1;
	protected ArrayList <NameValuePair> headers;
	protected String server;
	protected String key;
	protected String rpc;
	protected SDConnectionResponseListener respuestaListener;
	
	/**
	 * Construye el objeto con los valores minimos para estableces la conexion con el server
	 * 
	 * @param server Url de conexion con el server
	 * @param key Auth code para la autenticacion, si es necesaria
	 * @param respuestaListener
	 */
    public SDConnection(String server, String key, SDConnectionResponseListener respuestaListener){
    	this.server = server;
    	this.key = key;
    	this.respuestaListener = respuestaListener;
        headers = new ArrayList<NameValuePair>();
        //execute(); //NOTE en los constructores este debe ir dentro del constructor :P
    }

    public void addHeader(String name, String value){
    	headers.add(new BasicNameValuePair(name, value));
    }
    
    /**
     * Contiene el tipo de conexion que realizara. 
     * Por ejemplo para un GET deberia incluir:
     * 
     * this.get( "user", respuestaListener);
     */
    protected abstract void execute();
    
    /**
     * Este metodo es llamado cuando la informacion es correctamente recibida por el server.
     * 
     * @param result
     */
    protected abstract void processResponse( String result );
    
    /**
     * Este metodo es ejecutado solo si existio un error en la conexion con el server, asi como errores
     * con codigo como 404.
     * 
     * @param errorCode
     * @param errorMessage
     * @param String result Este contiene informacion extra enviada desde el servidor para explicitar mas el error
     */
    protected void processError( Integer errorCode, String errorMessage, String result ){
		//Log.d("[ONERROR]", errorCode.toString() + errorMessage.toString());
		if (result != null) {
			SDError error = new SDError(result);
			if ( error!=null && error.getReason() != null ){
				respuestaListener.onError(error);
			}else{
				respuestaListener.onError(new SDError(errorCode, "Data retrieve error" ));
			}
		}else{
			respuestaListener.onError(new SDError(errorCode, errorMessage));
		}
    }
    	
    
    /////////////////////////////////////////////////////////////////////////
    // Getters and setters
    /////////////////////////////////////////////////////////////////////////
    public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getRpc() {
		return rpc != null ? rpc : "rpc";
	}
	public void setRpc(String rpc) {
		this.rpc = rpc;
	}

	
    /////////////////////////////////////////////////////////////////////////
    // Connection methods
    /////////////////////////////////////////////////////////////////////////
	protected void call(String url, List<NameValuePair> parameters, SDConnectionResponseListener onReponse) {		
		HttpPost http = new HttpPost(getServer() + "/"+ getRpc() +"/" + url);
		http.setHeader("Authorization" , getKey());
    	for(NameValuePair h : headers) {
    		http.addHeader(h.getName(), h.getValue());
        }
    	try {
    		http.setEntity(new UrlEncodedFormEntity(parameters));
    		
    	} catch (UnsupportedEncodingException e) {
			onReponse.onError( new SDError(501, e.getLocalizedMessage()));
		} finally {
		
			RestRequest restRequest = new RestRequest();
	    	restRequest.setOnReponse(onReponse);
	    	restRequest.setRequest(http);
	    	restRequest.execute();
		}
	    
	}

	protected void get(String url, SDConnectionResponseListener onReponse) {
		HttpGet request = new HttpGet(getServer() + "/" + url);
    	buildAndSend(request, onReponse);    	
    }
	
	protected void post(String url, List<NameValuePair> parameters, SDConnectionResponseListener onReponse) {
		HttpPost request = new HttpPost(getServer() + "/" + url);
    	try {
    		request.setEntity(new UrlEncodedFormEntity(parameters, "utf-8"));
    		
    	} catch (UnsupportedEncodingException e) {
			onReponse.onError( new SDError(501, e.getLocalizedMessage()));
		}
    	buildAndSend(request, onReponse);    	
    }
	
	protected void postJSON(String url, JSONObject json, SDConnectionResponseListener onReponse) {
		HttpPost request = new HttpPost(getServer() + "/" + url);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");
    	try {
    		request.setEntity(new StringEntity(json.toString()));
    		
    	} catch (UnsupportedEncodingException e) {
			onReponse.onError( new SDError(501, e.getLocalizedMessage()));
		}
    	buildAndSend(request, onReponse);    	
    }

	protected void postMultipart(String url, MultipartEntityBuilder entity, SDConnectionResponseListener onReponse) {
		HttpPost request = new HttpPost(getServer() + "/" + url);
    	
    	request.setEntity(entity.build());
    		

    	buildAndSend(request, onReponse);    	
    }
	
	protected void put(String url, List<NameValuePair> parameters, SDConnectionResponseListener onReponse) {
		HttpPut request = new HttpPut(getServer() + "/" + url);
    	try {
    		request.setEntity(new UrlEncodedFormEntity(parameters));
    		
    	} catch (UnsupportedEncodingException e) {
			onReponse.onError( new SDError(501, e.getLocalizedMessage()));
		}
    	buildAndSend(request, onReponse);    	
    }
	
    /////////////////////////////////////////////////////////////////////////
    // Build http methods
    /////////////////////////////////////////////////////////////////////////
	private void buildAndSend(HttpUriRequest request, SDConnectionResponseListener onReponse) {
		request.setHeader("Authorization" , getKey());
    	for(NameValuePair h : headers) {
            request.addHeader(h.getName(), h.getValue());
        }
		RestRequest restRequest = new RestRequest();
    	restRequest.setOnReponse(onReponse);
    	restRequest.setRequest(request);
    	restRequest.execute();
		
	}
	/**
	 * Subclase para realizar la conexion en segundo plano
	 *
	 */
	private class RestRequest extends AsyncTask<String, Integer, String> {
		private HttpUriRequest request;
		private SDConnectionResponseListener onReponse;
		private Integer statusCode;
		private String statusMessage;
	    ///////////////////////////
	    // Getters and setters
	    ///////////////////////////
		public HttpUriRequest getRequest() {
			return request;
		}
		public void setRequest(HttpUriRequest request) {
			this.request = request;
		}
		public SDConnectionResponseListener getOnReponse() {
			return onReponse;
		}
		public void setOnReponse(SDConnectionResponseListener onReponse) {
			this.onReponse = onReponse;
		}
		public Integer getStatusCode() {
			return statusCode;
		}
		public void setStatusCode(Integer errorCode) {
			this.statusCode = errorCode;
		}
		public String getStatusMessage() {
			return statusMessage;
		}
		public void setStatusMessage(String errorMessage) {
			this.statusMessage = errorMessage;
		}
		
		@Override
		protected String doInBackground(String... params) {
		    SDSSLSocketFactory sslFactory = null;
			try {
				sslFactory = new SDSSLSocketFactory(null);
			} catch (KeyManagementException e1) {
				//e1.printStackTrace();
			} catch (UnrecoverableKeyException e1) {
				//e1.printStackTrace();
			} catch (NoSuchAlgorithmException e1) {
				//e1.printStackTrace();
			} catch (KeyStoreException e1) {
				//e1.printStackTrace();
			}

		    HttpParams paramsSecure = new BasicHttpParams();
		    HttpProtocolParams.setVersion(paramsSecure, HttpVersion.HTTP_1_1);
		    HttpProtocolParams.setContentCharset(paramsSecure, HTTP.UTF_8);
		    
		    SchemeRegistry registry = new SchemeRegistry();
		    registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		    registry.register(new Scheme("https", sslFactory, 443));

		    ClientConnectionManager ccm = new ThreadSafeClientConnManager(paramsSecure, registry);
		    HttpClient client = new DefaultHttpClient(ccm, paramsSecure);
			
			//
			//HttpClient client = new DefaultHttpClient();
	        HttpResponse httpResponse;
	        String response = null;
	        try {
	        	httpResponse = client.execute(getRequest());
	        	setStatusCode(httpResponse.getStatusLine().getStatusCode());
	        	//Log.i("[statusCode]", Integer.toString(httpResponse.getStatusLine().getStatusCode()));
	        	setStatusMessage(httpResponse.getStatusLine().getReasonPhrase());
	            //Log.i("[ErrorMessage]", httpResponse.getStatusLine().getReasonPhrase());
	            HttpEntity entity = httpResponse.getEntity();
	            if (entity != null) {
	            	response = EntityUtils.toString(entity);
	            }	 
	            
	        } catch (IOException e) {
	        	setStatusCode(0);
	        	setStatusMessage(e.getMessage());
	        } catch (Exception e)  {
	        	setStatusCode(e.hashCode());
	        	setStatusMessage(e.getLocalizedMessage());
	        }
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			////Log.d("[error result]", result);
			////Log.d("[error code]", Integer.toString(statusCode) );
			////Log.d("[error messate]", getStatusMessage());
			if(getOnReponse() != null){
				Integer statusCode = getStatusCode();
				if ( statusCode == 200 || statusCode == 201 ){
					processResponse(result);
				} else if(statusCode == 0){
					processError(getStatusCode(), "Server not responding. Please try later.", result);
				}else{
					processError(getStatusCode(), getStatusMessage(), result);
				}
			}
		}
		
	}
    
}

