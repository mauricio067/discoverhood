package com.miokode.discoverhood.restful;

public interface SDConnectionResponseListener {
	public abstract void onSuccess(String response, Object resultElement );
	public abstract void onIssues(String issues);//Caoas que es mejor un arraylist
	public abstract void onError(SDError error);
}
