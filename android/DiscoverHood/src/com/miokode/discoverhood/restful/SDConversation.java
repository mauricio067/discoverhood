package com.miokode.discoverhood.restful;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDConversation extends SDObject {
	private HashMap<String, SDAuthor> people ;
	private SDMessage lastMsg;

	
	/**
	 * Constructor solo utilizado para preparar el objeto antes de enviarlo al server
	 * 
	 * @param publication
	 */
	public SDConversation(String userID ) {
		super();
		//this.text = texto;
	}
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDConversation(JSONObject json){
		super(json);
		JSONArray objArray;
		try {
			objArray = json.getJSONArray("people");
			people = new HashMap<String, SDAuthor>();
			for(int n = 0; n < objArray.length(); n++) {
				SDAuthor author = new SDAuthor(objArray.getJSONObject(n));
				people.put( author.getId(), author);
			}
		} catch (JSONException e) {
			people = null;
			//Log.d("[jsonRetrieveError]", "people not found");
		}
		try {
			lastMsg = new SDMessage(json.getJSONObject("msg"));
		} catch (JSONException e) {
			lastMsg = null;
			//Log.d("[jsonRetrieveError]", "msg not found");
		}
	}

	public String toString(){
		return "id: " + this.id ;
	}
	
	
	/////////////////////////////
	// Getters and setters
	/////////////////////////////
	public HashMap<String, SDAuthor> getPeople() {
		return people;
	}
	public void setPeople(HashMap<String, SDAuthor> peop){
		people = peop;
	}

	public SDMessage getLastMsg() {
		return lastMsg;
	}
	
	public void setLastMsg(SDMessage ms){
		lastMsg = ms;
	}
	
	
	
}
