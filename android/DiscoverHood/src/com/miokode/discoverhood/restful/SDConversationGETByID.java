package com.miokode.discoverhood.restful;

import android.util.Log;

public class SDConversationGETByID extends SDConnection{
	private String conversationID ;
	
	public SDConversationGETByID(String server, String key, String conversationID, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.conversationID = conversationID;
		execute();
	}

	@Override
	protected void execute() {
		this.get("conversation/" + this.conversationID , respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (!serverResponse.getMultipleItems()) {
			respuestaListener.onSuccess( result, new SDConversation(serverResponse.getItemResponse()) );
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}
