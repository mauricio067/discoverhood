package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDConversationPOSTAdd extends SDConnection {
	private String toUserID;
	private String text;
	
	public SDConversationPOSTAdd(String server, String key, String toUserID, String text, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.toUserID = toUserID;
		this.text = text;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("to", toUserID );	
	        obj.put("msg", text );
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "startNewConversation";
		this.postJSON("startNewConversation", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (!serverResponse.getMultipleItems()) {
			respuestaListener.onSuccess( result, new SDConversation(serverResponse.getItemResponse()) );
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}

}