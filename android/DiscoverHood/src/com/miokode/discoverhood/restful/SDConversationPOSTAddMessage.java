package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDConversationPOSTAddMessage extends SDConnection {
	private String text;
	private String conversationID;
	
	public SDConversationPOSTAddMessage(String server, String key, String text, String conversationID, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.text = text;
		this.conversationID = conversationID;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("msg", text );
	        obj.put("conversation", conversationID );
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "conversation/newMessage";
		this.postJSON("conversation/newMessage", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (!serverResponse.getMultipleItems()) {
			respuestaListener.onSuccess( result, new SDMessage(serverResponse.getItemResponse()) );
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}

}