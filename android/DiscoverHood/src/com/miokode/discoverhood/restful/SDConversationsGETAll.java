package com.miokode.discoverhood.restful;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class SDConversationsGETAll extends SDConnection{
	private ArrayList<SDConversation> conversations ;
	private String lastConversationID;
	
	public SDConversationsGETAll(String server, String key, String lastConversationID, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.conversations = new ArrayList<SDConversation>();
		this.lastConversationID = lastConversationID;
		execute();
	}

	@Override
	protected void execute() {
		if (lastConversationID == null){
			this.get("conversations", respuestaListener);
		}else{
			this.get("conversations/"+lastConversationID, respuestaListener);
		}
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					conversations.add( new SDConversation(items.getJSONObject(n)) );
				}
				respuestaListener.onSuccess(result, conversations);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
			 
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}
