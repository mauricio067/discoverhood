package com.miokode.discoverhood.restful;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class SDConversationsNotificationsGETAll extends SDConnection{
	private ArrayList<SDConversationNotification> notifications ;
	
	public SDConversationsNotificationsGETAll(String server, String key, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.notifications = new ArrayList<SDConversationNotification>();
		execute();
	}


	@Override
	protected void execute() {
		this.get("conversation/notifications", respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					notifications.add( new SDConversationNotification(items.getJSONObject(n)) );
				}
				respuestaListener.onSuccess(result, notifications);
			 } catch (JSONException e) {
					//e.printStackTrace();
				    //Log.d("[jsonerror]", "error al procesar la obtencion de todas las notificaiones de conversaciones");
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}
