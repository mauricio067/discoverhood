package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDFavoritesPOSTAdd extends SDConnection {
	private String publicationID;
	
	public SDFavoritesPOSTAdd(String server, String key, String publicationID, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publicationID = publicationID;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("publication", publicationID );	        
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "addFavorite";
		this.postJSON("addFavorite", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}


}