package com.miokode.discoverhood.restful;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class SDFriendGETRequestsReceived extends SDConnection{
	private ArrayList<SDFriendRequester> requesters ;

	
	public SDFriendGETRequestsReceived(String server, String key, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.requesters = new ArrayList<SDFriendRequester>();
		execute();
	}


	@Override
	protected void execute() {
		this.get("friendshipsrequestsreceived", respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					requesters.add( new SDFriendRequester(items.getJSONObject(n)) );
				}
				respuestaListener.onSuccess(result, requesters);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}