package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

public class SDFriendNotification extends SDNotification {
	private SDAuthor sender;
	public enum Result { ACCEPTED, REJECTED };
	private Result result;

	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDFriendNotification(JSONObject json){
		super(json);
		
	    String straction = getValueFromJSONString("result", "", json);
		if (straction.compareTo("ACCEPTED") == 0){
			this.result = Result.ACCEPTED;
		}else if(straction.compareTo("REJECTED") == 0){
			this.result = Result.REJECTED;
		}
		try {
			JSONObject objSender = json.getJSONObject("sender");
			this.sender = new SDAuthor(objSender);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	
	public String toString(){
		return "id: " + this.id ;
	}

	/////////////////////////////
	// Getters and setters
	/////////////////////////////
	public SDAuthor getSender() {
		return sender;
	}

	public Result getResult() {
		return result;
	}
	
	
}
