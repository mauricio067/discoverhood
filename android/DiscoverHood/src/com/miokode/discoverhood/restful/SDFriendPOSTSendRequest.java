package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDFriendPOSTSendRequest extends SDConnection {
	private String friendID;
	
	public SDFriendPOSTSendRequest(String server, String key, String friendID, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.friendID = friendID;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("friend", friendID );	        
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "sendfriendshiprequest";
		this.postJSON("sendfriendshiprequest", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}


}