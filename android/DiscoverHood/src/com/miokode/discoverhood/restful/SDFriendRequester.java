package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

public class SDFriendRequester extends SDObject {
	private SDAuthor requester;
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDFriendRequester(JSONObject json){
		super(json);
		try {
			this.requester = new SDAuthor(json.getJSONObject("requester"));
		} catch (JSONException e) {
			e.printStackTrace();
		} 
	}

	public SDAuthor getRequester(){
		return requester;
	}
	
	
}


