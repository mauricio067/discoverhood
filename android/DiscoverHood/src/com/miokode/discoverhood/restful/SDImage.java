package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

public class SDImage {
    private String picture_id;
    private String pictureUrl;
    private String pictureMD5;
    private String pictureThumbnailUrl;
	private String pictureThumbnailMD5;
	

	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDImage(JSONObject json){
		this.picture_id = getValueFromJSONString("id", "", json);
	    this.pictureUrl = getValueFromJSONString("url", "", json);
	    this.pictureMD5 = getValueFromJSONString("md5", "", json);
	    this.pictureThumbnailUrl = getValueFromJSONString("url_thumbnail", "", json); 
	    this.pictureThumbnailMD5 = getValueFromJSONString("thumbnail_md5", "", json);
	}
	
	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto.
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 * 
	 */
	private String getValueFromJSONString( String JsonTag, String defaultValue, JSONObject json ){
		try{ 
			return json.getString(JsonTag); 
		} catch (JSONException e) {
			//e.printStackTrace();
			return defaultValue; 
		}
	}
	
	public String getPicture_id() {
		return picture_id;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public String getPictureMD5() {
		return pictureMD5;
	}
	public String getPictureThumbnailUrl() {
		return pictureThumbnailUrl;
	}
	public String getPictureThumbnailMD5() {
		return pictureThumbnailMD5;
	}
	
	
}
