package com.miokode.discoverhood.restful;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public abstract class SDObject {
	protected String id;
	protected Date created;
	protected Date updated;
	protected String etag;
	
	
	protected SDObject(){}
	
	/**
	 * Constructor que busca y asigna los valores minimos dado un objecto JSON
	 * @param json
	 */
	protected SDObject(JSONObject json){
		this.id = getValueFromJSONString( "_id", "", json);
		//this.etag= getValueFromJSONString( "Etag", "", json);
		this.created = getValueFromJSONDate("created", new Date(), json);
		//this.updated = getValueFromJSONDate("updated", new Date(), json);
		
	}
	
	
	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto.
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 * 
	 */
	protected String getValueFromJSONString( String JsonTag, String defaultValue, JSONObject json ){
		try{ 
			return json.getString(JsonTag); 
		} catch (JSONException e) {
			//e.printStackTrace();
			//Log.d("[jsonRetrieveError]", JsonTag + " not found");
			return defaultValue; 
		}
	}
	
	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 */
	protected Date getValueFromJSONDate( String JsonTag, Date defaultValue, JSONObject json ){
		try{ 
			return SDTools.processDateString(json.getString(JsonTag));
		} catch (JSONException e) {
			//e.printStackTrace();
			//Log.d("[jsonRetrieveError]", JsonTag + " not found");
			return defaultValue;
		}
	}
	
	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 */
	protected Integer getValueFromJSONInteger( String JsonTag,  Integer defaultValue,  JSONObject json ){
		try{ 
			return json.getInt(JsonTag);
		} catch (JSONException e) {
			//e.printStackTrace();
			//Log.d("[jsonRetrieveError]", JsonTag + " not found");
			return defaultValue; 
		}
	}
	
	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 */
	protected Long getValueFromJSONLong( String JsonTag, Long defaultValue, JSONObject json ){
		try{ 
			return json.getLong(JsonTag);
		} catch (JSONException e) {
			//e.printStackTrace();
			//Log.d("[jsonRetrieveError]", JsonTag + " not found");
			return defaultValue; 
		}
	}
	
	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto
	 * 
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 */
	protected double getValueFromJSONDouble( String JsonTag, double defaultValue, JSONObject json ){
		try{ 
			//return json.getLong(JsonTag);
			return Double.parseDouble(json.getString(JsonTag));
		} catch (JSONException e) {
			//e.printStackTrace();
			//Log.d("[jsonRetrieveError]", JsonTag + " not found");
			return defaultValue; 
		}
	}
	
	/**
	 * Devuelve el valor leido del json al objeto dado, y en caso de no existir el tag json, le asigna un valor por defecto
	 *
	 * @param JsonTag Tag que se buscara dentro del objeto Json
	 * @param defaultValue Valor por defecto
	 * @param json Objeto Json que contiene la informacion a extraer
	 */
	protected Boolean getValueFromJSONBoolean( String JsonTag, Boolean defaultValue, JSONObject json ){
		try{ 
			return json.getBoolean(JsonTag);
		} catch (JSONException e) {
			//e.printStackTrace();
			//Log.d("[jsonRetrieveError]", JsonTag + " not found");
			return defaultValue; 
		}
	}
	
	///////////////////////////
	//// Getters and setters
	///////////////////////////
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public String getEtag() {
		return etag;
	}
	public void setEtag(String etag) {
		this.etag = etag;
	}
	
}
