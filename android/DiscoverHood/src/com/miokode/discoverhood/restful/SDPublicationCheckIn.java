package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;

public class SDPublicationCheckIn extends SDPublication{
	private Bitmap mapPicture;
	private SDImage mapImage;
	
	public SDPublicationCheckIn(String texto, double latitud, double longitud, Bitmap mapPicture) {
		super(texto, latitud, longitud);
		this.mapPicture=mapPicture;
	}
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDPublicationCheckIn(JSONObject json){
		super(json);
		try {
			this.mapImage = new SDImage(json.getJSONObject("mapImage"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public Bitmap getMapPicure(){
		return mapPicture;
	}
	
	public SDImage getMapImage(){
		return mapImage;
	}
}
