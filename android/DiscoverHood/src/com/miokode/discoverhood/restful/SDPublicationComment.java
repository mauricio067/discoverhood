package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

public class SDPublicationComment extends SDObject {
	private String content;
	private SDAuthor author;


	/**
	 * Constructor solo utilizado para preparar el objeto antes de enviarlo al server
	 * 
	 * @param publication
	 */
	/*public SDPublicationComment(String texto, double latitude, double longitude) {
		super();
		this.text = texto;
		this.latitude = latitude;
		this.longitude = longitude;
		this.smilesCounter=this.sharesCounter=this.commentsCounter=0;
	}*/
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDPublicationComment(JSONObject json){
		super(json);
		this.content = getValueFromJSONString("content", "", json);
		try {
			JSONObject objAvatar = json.getJSONObject("author");
			this.author = new SDAuthor(objAvatar);
		} catch (JSONException e) {
			this.author = null;
			e.printStackTrace();
		}
	}
	
	public String toString(){
		return "id: " + this.id + " texto " +this.content;
	}
	
	/////////////////////////////
	// Getters and setters
	/////////////////////////////
	public String getContent() {
		return this.content;
	}
	public SDAuthor getAuthor() {
		return this.author;
	}
	
}
