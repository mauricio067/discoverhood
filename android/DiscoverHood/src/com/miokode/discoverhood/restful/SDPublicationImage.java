package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;

public class SDPublicationImage extends SDPublication {
	private Bitmap mapPicture;
	private SDImage picture;


	public SDPublicationImage(String text, double latitude, double longitude, Bitmap mapPicture) {
		super(text, latitude, longitude);
		this.mapPicture=mapPicture;
	}
	
	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDPublicationImage(JSONObject json){
		super(json);
		try {
			this.picture = new SDImage(json.getJSONObject("picture"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public Bitmap getMapPicture(){
		return mapPicture;
	}
	
	public SDImage getPicture(){
		return picture;
	}
	
		

}
