package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDPublicationNotification extends SDNotification {
	private SDPublication publication;
	private SDAuthor actioner;
	public enum Action { SMILE, UNSMILE, SHARED, COMMENT };
	private Action action;

	/**
	 * Constructor que genera el objecto a partir de un objeto json dado
	 * 
	 * @param json
	 */
	public SDPublicationNotification(JSONObject json){
		super(json);
		String straction = getValueFromJSONString("action", "", json);
		if (straction.compareTo("SMILE") == 0){
			this.action = Action.SMILE;
		}else if(straction.compareTo("UNSMILE") == 0){
			this.action = Action.UNSMILE;
		}else if(straction.compareTo("SHARED") == 0){
			this.action = Action.SHARED;
		}else if(straction.compareTo("COMMENT") == 0){
			this.action = Action.COMMENT;
		}
	
		try {
			JSONObject objPublication = json.getJSONObject("publication");
			String tipo = objPublication.getString("type");
			if (tipo.compareTo("image") == 0) {
				this.publication = new SDPublicationImage(objPublication);
			}else if (tipo.compareTo("checkin") == 0) {
				this.publication = new SDPublicationCheckIn(objPublication);
			}else if (tipo.compareTo("shared") == 0) {
				this.publication = new SDPublicationShared(objPublication);
			}else{
				this.publication = new SDPublication(objPublication);
			}

		} catch (JSONException e) {
			//e.printStackTrace();
			//Log.d("[jsonRetrieveError]", "publication not found");
		}
		try {
			JSONObject objAuthor = json.getJSONObject("actioner");
			this.actioner = new SDAuthor(objAuthor);
		} catch (JSONException e) {
			//e.printStackTrace();
			//Log.d("[jsonRetrieveError]", "actioner not found");
		}
	}
	
	public String toString(){
		return "id: " + this.id ;
	}
	
	/////////////////////////////
	// Getters and setters
	/////////////////////////////
	public SDPublication getPublication() {
		return publication;
	}

	public SDAuthor getActioner() {
		return actioner;
	}

	public Action getAction() {
		return action;
	}
	
}
