package com.miokode.discoverhood.restful;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class SDPublicationNotificationGETAll  extends SDConnection{
	private ArrayList<SDPublicationNotification> notifications ;
	
	public SDPublicationNotificationGETAll(String server, String key, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.notifications = new ArrayList<SDPublicationNotification>();
		execute();
	}


	@Override
	protected void execute() {
		this.get("publication/notifications", respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					notifications.add( new SDPublicationNotification(items.getJSONObject(n)) );
				}
				respuestaListener.onSuccess(result, notifications);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}

}

