package com.miokode.discoverhood.restful;

import java.io.ByteArrayOutputStream;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import android.graphics.Bitmap;
import android.util.Log;

public class SDPublicationPOST extends SDConnection {
	private SDPublication publication;
	
	public SDPublicationPOST(String server, String key, SDPublication publication, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publication = publication;
		execute();
	}

	@Override
	protected void execute() {
        
        MultipartEntityBuilder entity = MultipartEntityBuilder.create();
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE); 

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        if (publication instanceof SDPublicationImage){
        	entity.addTextBody("type", "image");
        	((SDPublicationImage) publication).getMapPicture().compress(Bitmap.CompressFormat.PNG, 90, bao);
	        byte[] data = bao.toByteArray();
            entity.addBinaryBody("image", data, ContentType.DEFAULT_BINARY,"picture_.png");
        }else if (publication instanceof SDPublicationCheckIn ){
        	entity.addTextBody("type", "checkin");
          	((SDPublicationCheckIn) publication).getMapPicure().compress(Bitmap.CompressFormat.PNG, 90, bao);
	        byte[] data = bao.toByteArray();
            entity.addBinaryBody("image", data, ContentType.DEFAULT_BINARY,"picture_.png");
        	
        }else{
        	entity.addTextBody("type", "text");
        }
     	entity.addTextBody("text", publication.getText());
     	entity.addTextBody("latitude", String.valueOf(publication.getLatitud()));
        entity.addTextBody("longitude", String.valueOf(publication.getLongitud()));

        
        if (publication.isCrossPublication()) {
        	entity.addTextBody("to", String.valueOf(publication.getTo().getId()));
        }
		this.rpc = "create_publication";
		this.postMultipart("create_publication", entity, respuestaListener);
		
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
		/*SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					JSONObject objItem = items.getJSONObject(n);
					String tipo = objItem.getString("type");
					if (tipo.compareTo("image") == 0) {
						publication = new SDPublicationImage(items.getJSONObject(n)) ;
					}else if (tipo.compareTo("checkin") == 0) {
						publication = new SDPublicationCheckIn(items.getJSONObject(n)) ;
					}else{
						publication = new SDPublication(items.getJSONObject(n)) ;
					}
				}
				respuestaListener.onSuccess(result, publication);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}*/
	}

}
