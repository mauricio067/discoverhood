package com.miokode.discoverhood.restful;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDPublicationPOSTComment extends SDConnection {
	private String publicationID;
	private String text;
	
	public SDPublicationPOSTComment(String server, String key, String publicationID, String text, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publicationID = publicationID;
		this.text = text;
		execute();
	}

	@Override
	protected void execute() {
	    JSONObject obj = new JSONObject();
    	try {
	        obj.put("publication", publicationID );	  
	        obj.put("text", text );	 
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		this.rpc = "publication/addComment";
		this.postJSON("publication/addComment", obj, respuestaListener);       
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 	
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (!serverResponse.getMultipleItems()) {
			JSONObject objItem = serverResponse.getItemResponse();
			respuestaListener.onSuccess(result, new SDPublicationComment(objItem));
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}