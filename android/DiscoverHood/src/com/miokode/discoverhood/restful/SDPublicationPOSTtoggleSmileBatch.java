package com.miokode.discoverhood.restful;

import java.util.ArrayList;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


public class SDPublicationPOSTtoggleSmileBatch extends SDConnection {
	private ArrayList<String> publications;
	
	public SDPublicationPOSTtoggleSmileBatch(String server, String key, ArrayList<String> publications, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publications = publications;
		execute();
	}

	@Override
	protected void execute() {
		JSONObject objup = new JSONObject();
		try {
			JSONArray objarr = new JSONArray() ;
			ListIterator<String> listItr = publications.listIterator();
			int count = 0;
			while(listItr.hasNext()){
				String publID = listItr.next();
				JSONObject obj = new JSONObject();
		        obj.put("item"+count, publID );
		        objarr.put(obj);
				count++;
			}
			objup.put("publications", objarr);
    	} catch (JSONException e) {
    		e.printStackTrace();
    	}
		
		this.rpc = "toggleSmiles";
		this.postJSON("toggleSmiles", objup, respuestaListener); 
		
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}


}