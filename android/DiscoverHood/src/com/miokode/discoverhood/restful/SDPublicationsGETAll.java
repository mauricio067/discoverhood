package com.miokode.discoverhood.restful;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDPublicationsGETAll extends SDConnection{
	private ArrayList<SDPublication> publications ;
	private String lastPublicationID;
	
	public SDPublicationsGETAll(String server, String key, String lastPublicationID, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.publications = new ArrayList<SDPublication>();
		this.lastPublicationID = lastPublicationID;
		execute();
	}


	@Override
	protected void execute() {		
		if (lastPublicationID == null){
			this.get("publications", respuestaListener);
		}else{
			this.get("publications/"+lastPublicationID, respuestaListener);
		}
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		if (publications == null) publications =  new ArrayList<SDPublication>();
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		
		if (!serverResponse.getMultipleItems()) {
			SDPublicationsGETAllResponse reponse = new SDPublicationsGETAllResponse();
			JSONObject resp = serverResponse.getItemResponse();
			
			
			try {	
				reponse.setDistance(resp.getLong("distance"));
			} catch (JSONException e) {
				//Log.d("[jsonRetrieveError]", "Distance not found");
				respuestaListener.onError(new SDError(0,"Data retrieve error"));
			}
			try {
				JSONArray items = resp.getJSONArray("publications");
				for(int n = 0; n < items.length(); n++) {
					JSONObject objItem = items.getJSONObject(n);
					String tipo = objItem.getString("type");
					if (tipo.compareTo("image") == 0) {
						publications.add( new SDPublicationImage(items.getJSONObject(n)) );
					}else if (tipo.compareTo("checkin") == 0) {
						publications.add( new SDPublicationCheckIn(items.getJSONObject(n)) );
					}else if (tipo.compareTo("shared") == 0) {
						publications.add( new SDPublicationShared(items.getJSONObject(n)) );
					}else{
						publications.add( new SDPublication(items.getJSONObject(n)) );
					}
				}
				reponse.setPublications(publications);
				respuestaListener.onSuccess(result, reponse);
			} catch (JSONException e) {
				//Log.d("[jsonRetrieveError]", "publications not found");
				respuestaListener.onError(new SDError(0,"Data retrieve error"));
			}
			try {
				JSONObject obj = resp.getJSONObject("position reference");
				reponse.setLatitude(obj.getDouble("latitude"));
				reponse.setLongitude(obj.getDouble("longitude"));

			} catch (JSONException e) {
				//Log.d("[jsonRetrieveError]", "Position reference not found");
				respuestaListener.onError(new SDError(0,"Data retrieve error"));
			}
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
		
		/*if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					JSONObject objItem = items.getJSONObject(n);
					String tipo = objItem.getString("type");
					if (tipo.compareTo("image") == 0) {
						publications.add( new SDPublicationImage(items.getJSONObject(n)) );
					}else if (tipo.compareTo("checkin") == 0) {
						publications.add( new SDPublicationCheckIn(items.getJSONObject(n)) );
					}else if (tipo.compareTo("shared") == 0) {
						publications.add( new SDPublicationShared(items.getJSONObject(n)) );
					}else{
						publications.add( new SDPublication(items.getJSONObject(n)) );
					}
				}
				respuestaListener.onSuccess(result, publications);
			 } catch (JSONException e) {
					//e.printStackTrace();
					//Log.d("[jsonRetrieveError]",  "type not found");
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}*/
	}

}
