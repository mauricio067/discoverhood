package com.miokode.discoverhood.restful;

import java.util.ArrayList;

public class SDPublicationsGETAllResponse {
	private ArrayList<SDPublication> publications ;
	private long distance ;
	private double latitude;
	private double longitude;

	public SDPublicationsGETAllResponse(){
		this.publications = new ArrayList<SDPublication>();
		this.distance = 0;
		this.latitude = 0;
		this.longitude = 0;
	}
	
	public ArrayList<SDPublication> getPublications() {
		return publications;
	}

	public void setPublications(ArrayList<SDPublication> publications) {
		this.publications = publications;
	}

	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
