package com.miokode.discoverhood.restful;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class SDPublicationsGETComments extends SDConnection{
	private ArrayList<SDPublicationComment> comments ;
	private String publcationID;
	
	public SDPublicationsGETComments(String server, String key, String publcationID, SDConnectionResponseListener respuestaListener ) {
		super(server, key, respuestaListener);
		this.comments = new ArrayList<SDPublicationComment>();
		this.publcationID = publcationID;
		execute();
	}

	@Override
	protected void execute() {
		this.get("publication/" + publcationID +"/comments", respuestaListener);		
	}

	@Override
	protected void processResponse(String result) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		SDServerResponse serverResponse = new SDServerResponse(result.toString());
		if (serverResponse.getMultipleItems()) {
			JSONArray items = serverResponse.getItemsResponse();
			 try {
				for(int n = 0; n < items.length(); n++) {
					comments.add(new SDPublicationComment(items.getJSONObject(n)));
				}
				respuestaListener.onSuccess(result, comments);
			 } catch (JSONException e) {
					e.printStackTrace();
					respuestaListener.onError(new SDError(0,"Data retrieve error"));
			 }
		}else{
			respuestaListener.onError(new SDError(0,"Data retrieve error"));
		}
	}


}
