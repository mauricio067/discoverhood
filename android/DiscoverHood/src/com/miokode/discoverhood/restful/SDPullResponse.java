package com.miokode.discoverhood.restful;

import java.util.ArrayList;

public class SDPullResponse {
	private ArrayList<SDPublicationNotification> publNotif ;
	private ArrayList<SDFriendNotification> friendNotif ;
	private ArrayList<SDConversationNotification> convNotif ;
	private ArrayList<SDFriendRequester> frienshipsRequest ;
	
	
	public SDPullResponse(){
		this.publNotif = new ArrayList<SDPublicationNotification>();
		this.friendNotif = new ArrayList<SDFriendNotification>();
		this.convNotif = new ArrayList<SDConversationNotification>();
		this.frienshipsRequest = new ArrayList<SDFriendRequester>();
	}
	
	
	public ArrayList<SDPublicationNotification> getPublicationsNotifications() {
		return publNotif;
	}
	public void setPublicationsNotifications(ArrayList<SDPublicationNotification> publNotif) {
		this.publNotif = publNotif;
	}
	public ArrayList<SDFriendNotification> getFriendshipsNotifications() {
		return friendNotif;
	}
	public void setFriendshipsNotifications(ArrayList<SDFriendNotification> friendNotif) {
		this.friendNotif = friendNotif;
	}
	public ArrayList<SDConversationNotification> getConversationsNotifications() {
		return convNotif;
	}
	public void setConversationsNotifications(ArrayList<SDConversationNotification> convNotif) {
		this.convNotif = convNotif;
	}
	public ArrayList<SDFriendRequester> getFrienshipsRequests() {
		return frienshipsRequest;
	}
	public void setfrienshipsRequest(ArrayList<SDFriendRequester> requesters) {
		this.frienshipsRequest = requesters;
	}
	
	
	
}
