package com.miokode.discoverhood.restful;

import java.util.ArrayList;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SDPushService extends SDConnection {
	private ArrayList<String> publications;
	private ArrayList<String> publNotif ;
	private ArrayList<String> friendNotif ;
	private ArrayList<String> convNotif ;
	
	public SDPushService(String server, String key, ArrayList<String> publications, ArrayList<String> publNotif, ArrayList<String> friendNotif, ArrayList<String> convNotif, SDConnectionResponseListener respuestaListener) {
		super(server, key, respuestaListener);
		this.publications = publications;
		this.publNotif = publNotif;
		this.friendNotif = friendNotif;
		this.convNotif = convNotif;
		execute();
	}

	@Override
	protected void execute() {   
	    JSONObject obj = new JSONObject();
    	try {
    		if (publications!= null && publications.size()>0)
				obj.put("toggleSmiles", processArrays(publications) );	 
    		if (publNotif!= null && publNotif.size()>0)
    			obj.put("notifpublications", processArrays(publNotif) );
    		if (convNotif!= null && convNotif.size()>0)
    			obj.put("notifmessages", processArrays(convNotif) );
    		if (friendNotif!= null && friendNotif.size()>0)
    			obj.put("notiffriendships", processArrays(friendNotif) );
    		
    	} catch (JSONException e) {
    		//e.printStackTrace();
    	}
    	////Log.d("[pushsalida]", obj.toString());
		this.rpc = "pushservice";
		this.postJSON("pushservice", obj, respuestaListener); 
	}

	@Override
	protected void processResponse( String result ) {
		//Log.d("[ONRESPONSE]",  result.toString()); 
		respuestaListener.onSuccess(result, null);
	}

	private JSONArray processArrays(ArrayList<String> array){
		JSONArray objarr = null;
		try {
			objarr = new JSONArray() ;
			ListIterator<String> listItr = array.listIterator();
			int count = 0;
			while(listItr.hasNext()){
				String publID = listItr.next();
				JSONObject obj = new JSONObject();
		        obj.put("item"+count, publID );
		        objarr.put(obj);
				count++;
			}
    	} catch (JSONException e) {
    		//e.printStackTrace();
    	}
		return objarr;
	}

}
