import json
import os
#import datetime
from datetime import datetime
from urlparse import urlparse
from bson import ObjectId
from bottle import request, response, get, post, auth_basic, abort
from models import Accounts
#from PIL import Image
from mongoengine import ValidationError, DoesNotExist, NotUniqueError
from Models.Accounts import check_auth, change_password, change_email, set_new_email, send_recuperation_message, validate_POST_form, resend_validation_code, validate_POST_edit_form
import ErrorsParsers
from image import crate_image

@post('/create_account')
def account_create():
    ''' Create new account. '''
    #data = request.body.readline()
    #if not data:
        #abort(400, 'No data received' )
    #entity = json.loads(data)
    #errors = validate_POST(entity)
    #if errors:
        #abort(403, {'reason': 'invalid posted data', 'details':errors } )
    ##from_json
    #user = Accounts()
    #user.firstname = entity['firstname']
    #user.lastname = entity['lastname']
    #user.password = entity['password']
    #user.email = entity['email']
    #user.gender = entity['gender']
    #print entity['avatar']
    #print request.files.get("avatar")
    #upload = request.files.get("avatar")
    #print upload
    #user.avatar = upload
    #try :
        #user.save()
    #except ValidationError as msg:
        #abort(403, {'reason': 'Invalid posted data', 'details':msg.to_dict() } )
    #except NotUniqueError:
        #abort(403, 'Email already registered' )
    #print request.forms
    data = request.body.readline()

    if not data:
        abort(400, 'No data received' )
    errors = validate_POST_form(request.forms)
    if errors:
        abort(403, {'reason': 'invalid posted data', 'details':errors } )
    user = Accounts()
    user.firstname = request.forms.firstname
    user.lastname = request.forms.lastname
    if request.forms.get('fb_code') is None:
        user.fb = False
        user.password = request.forms.password
    else:
        user.fb_code = request.forms.fb_code
        user.fb = True
    user.email = request.forms.email
    user.gender = request.forms.gender
    #user.birthday = datetime.strptime(request.forms.birthday, '%a, %d %b %Y %H:%M:%S GMT')


    upload = request.files.avatar
    if (not upload is None):
        try:
            file_path=crate_image(upload)
            user.avatar.put(file_path)
            os.remove(file_path)
        except AttributeError:
            print 'avatar error'
    try :
        user.save()
    except ValidationError as msg:
        abort(403, {'reason': 'Invalid posted data', 'details':msg.to_dict() } )
    except NotUniqueError:
        abort(403, 'Email already registered' )


@post('/account/edit')
@auth_basic(check_auth)
def account_edit():
    ''' Edit fields account. '''
    username, password = request.auth or (None, None)
    data = request.body.readline()
    if not data:
        abort(400, 'No data received' )

    errors = validate_POST_edit_form(request.forms)
    if errors:
        abort(403, {'reason': 'invalid posted data', 'details':errors } )

    user = Accounts.objects.get(email=username)

    if (not request.forms.get('nickname') is None) and ( not request.forms.nickname is user.nickname):
        Accounts.objects(email=username).update_one(set__nickname=request.forms.nickname)

    if (request.forms.get('nickname') is None):
        del user.nickname
        user.save()

    if (not request.forms.get('firstname') is None) and ( not request.forms.firstname is user.firstname):
        Accounts.objects(email=username).update_one(set__firstname=request.forms.firstname)

    if (not request.forms.get('lastname') is None) and ( not request.forms.lastname is user.lastname):
        Accounts.objects(email=username).update_one(set__lastname=request.forms.lastname)

    if (not request.forms.get('gender') is None) and ( not request.forms.gender is user.gender):
        Accounts.objects(email=username).update_one(set__gender=request.forms.gender)

    if not request.forms.get('birthday') is None:
        dateelement = datetime.strptime(request.forms.birthday, '%a, %d %b %Y %H:%M:%S GMT')
        if (not dateelement is user.birthday ):
            Accounts.objects(email=username).update_one(set__birthday=dateelement)

    if not request.forms.get('password') is None:
        change_password(user,request.forms.password)

    if (not request.forms.get('email') is None) and ( not request.forms.email is user.email):
        set_new_email(user,request.forms.email)

    upload = request.files.avatar
    #upload = request.get('avatar')
    if (not upload is None):
        try:
           file_path=crate_image(upload)
           user.avatar.replace(file_path)
           user.save()
            #elimino el archivo temporal
           os.remove(file_path)

        except AttributeError:
            print 'empty avatar'

    try:
        ourl = urlparse(request.url)
        user = Accounts.get_own_account(username, ourl.scheme + "://" + ourl.netloc)
        return user
    except DoesNotExist:
        error = 'User data not found'
        abort(404, error)


@get('/user/:id')
@auth_basic(check_auth)
def account_get_user_info(id):
    """
        Devuelve la informacion de un usuario determinado
        @param id ID del usuario
    """
    try :
        username, password = request.auth or (None, None)
        myuser = Accounts.objects.get(email=username)
        ourl = urlparse(request.url)
        user = Accounts.get_other_account(id, myuser, ourl.scheme + "://" + ourl.netloc)
    except DoesNotExist:
        #abort(404, 'No account with id %s' % id)
        abort(404, "Use doesn't exists.")
    return user

@get('/account')
@auth_basic(check_auth)
def account_get_my_account():
    """
        Devuelve la informacion del propio usuario
    """
    username, password = request.auth or (None, None)
    try:
        ourl = urlparse(request.url)
        user = Accounts.get_own_account(username, ourl.scheme + "://" + ourl.netloc)
    except DoesNotExist:
        error = 'User data not found'
        abort(404, error)
    return user

@post('/activate_account')
@auth_basic(check_auth)
def account_activate():
    """
        Activa una cuenta
    """
    username, password = request.auth or (None, None)
    data = request.body.readline()
    #print(data)
    error = None
    if not data:
        abort(400, 'No data received' )

    entity = json.loads(data)
    if not entity.has_key('validation_code'):
        error = 'Validation code is requiered'
    elif not (entity['validation_code'] >= 100000 and entity['validation_code'] <= 999999) :
        error = 'Validation code must be an integer with exactly 6 digits'
    user = Accounts.objects.get(email=username)
    if not (user.validation_code == entity['validation_code']):
        error= 'Incorrect validation code.'
    #print errors
    if error :
        abort(403, error )
    user.active = True
    try :
        user.save()
    except ValidationError as msg:
        abort(403, "Server error, can't activate account")


@post('/resend_validation_email')
@auth_basic(check_auth)
def account_resend_validation_email():
    ''' Recrea el validation code y envia un correo nuevamente '''
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    if user.active :
         abort(403, 'Account already activated.')
    resend_validation_code(user)



@post('/forgot_pass')
def account_forgot_pass():
    ''' Envia un correo de recuperacion a la cuenta '''
    data = request.body.readline()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('email'):
        abort(403, 'email is requiered')
    try:
        user = Accounts.objects.get(email=entity['email'])
    except DoesNotExist:
        abort(403, 'Email not registered')
    send_recuperation_message(user)



@post('/restore_pass')
def account_restore_pass():
    ''' verifica el code y cambia el pass a la cuenta '''
    data = request.body.readline()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    errors = dict()
    if not entity.has_key('email'):
        errors['email'] = 'email is requiered'
    if not entity.has_key('password'):
        errors['password'] = 'password is requiered'
    if not entity.has_key('validation_code'):
        errors['validation_code'] = 'validation_code is requiered'
    if errors:
        abort(403,{'reason': 'Invalid posted data', 'details':errors })
    try:
        user = Accounts.objects.get(email=entity['email'])
        if not (entity['validation_code'] == user.validation_code):
            errors['validation_code'] = 'Invalid code'
        else:
            change_password(user,entity['password'])
    except DoesNotExist:
        errors['email'] = 'Incorrect email %s' % entity['email']
    if errors:
        abort(403,{'reason': 'Invalid posted data', 'details':errors })



@get('/avatar/:id')
#@auth_basic(check_auth)
def account_get_avatar(id):
    """
        Devuelve SOLO la imagen del avatar dado
    """
    try:
        queryset = Accounts.objects(avatar=ObjectId(id))
        user = queryset.next()
    except StopIteration:
        error = 'Image not found'
        abort(404, error)
    response.content_type = 'image/png'
    return user.avatar.read()


@get('/avatar_thumbnail/:id')
#@auth_basic(check_auth)
def account_get_avatar(id):
    """
        Devuelve SOLO el thimbnal de la imagen del avatar dado
    """
    try:
        queryset = Accounts.objects(avatar=ObjectId(id))
        user = queryset.next()
    except StopIteration:
        error = 'Image not found'
        abort(404, error)
    response.content_type = 'image/png'
    return user.avatar.thumbnail.read() #TODO capturar error si no existe thumbnail




@post('/report_status')
@auth_basic(check_auth)
def account_report_status():
    """
        Reporta la actividad y estado del usuario
    """
    username, password = request.auth or (None, None)
    data = request.body.readline()
    errors = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('latitude'):
        errors['latitude'] = 'Latitude is needed'
    if not entity.has_key('longitude'):
        errors['longitude'] = 'Longitude is needed'

    if errors :
        abort(403,{'reason': 'Invalid posted data', 'details':errors })

    user = Accounts.objects.get(email=username)
    if not ((float(entity['latitude']) is 0.0) and (float(entity['longitude']) is 0.0)):
        user.last_position = [float(entity['latitude']), float(entity['longitude'])]
    user.last_connection = datetime.now
    try :
        user.save()
    except ValidationError as msg:
        abort(403, "Server error, can't update account status")
