import json
from bottle import response



def abort(code=500, data_error='Unknown Error: Application stopped.'):
    """ Aborts execution and causes a HTTP error. """
    
    
    raise HTTPError(code, data_error)



def parse_errors(code, response) :
    response.status = code
    errorsParse = dict()
    errorsParse['error'] = code
    if code == 400:
        errorsParse['reason'] = 'No enougth data received'
    elif code == 403:
        errorsParse['reason'] = 'Invalid posted data'
    elif code == 404:
        errorsParse['reason'] = 'Not found'
    elif code == 500:
        errorsParse['reason'] = 'Server error'
    return errorsParse

def parse_errors_details(code, response, errors_detail) :
    errorsParse = parse_errors(code, response)
    errorsParse['details'] = errors_detail
    return errorsParse

def parse_400( reason, details ) :
    errorsParse = dict()
    errorsParse['error'] = 400
    errorsParse['reason'] = reason or 'No enougth data received'
    if not (details == None):
        errorsParse['details'] = details
    return errorsParse

def parse_403( reason, details) :
    errorsParse = dict()
    errorsParse['error'] = 403
    errorsParse['reason'] = reason or 'Invalid posted data'
    if not (details == None):
        errorsParse['details'] = details
    return errorsParse

def parse_500( reason, details) :
    errorsParse = dict()
    errorsParse['error'] = 500
    errorsParse['reason'] = reason or 'Internal error'
    if not (details == None):
        errorsParse['details'] = details
    return errorsParse

def parse_404(errors) :
    errorsParse = dict()
    errorsParse['error'] = 404
    errorsParse['reason'] = 'Not found'
    errorsParse['details'] = errors
    return errorsParse
