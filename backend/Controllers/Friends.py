import json
from urlparse import urlparse
from bottle import request, response, get, post, abort, auth_basic
from mongoengine import ValidationError, DoesNotExist, Q
from models import Accounts, Friendships, FriendshipsRequests, NotificationFriendshipsProcess
from Models.Accounts import check_auth
from Models.Friends import friends_get_all, friends_get_all_friendhips_received, friends_get_all_friendhips_sended, friends_parse_friend, friends_parse_all_notification, friends_notifications_set_readed



@post('/sendfriendshiprequest')
@auth_basic(check_auth)
def friends_send_request():
    """
        Envia una peticion de amistad a un usuario dado
    """
    username, password = request.auth or (None, None)
    data = request.body.readline()
    error = None
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('friend'):
        abort(403, 'Friend id is requiered')
    try:
        friend = Accounts.objects().with_id(entity['friend'])
    except ValidationError:
        abort(404, "User doesn't exists.")
    if friend is None:
        abort(404, "User doesn't exists.")
 
    user = Accounts.objects.get(email=username)
    

    friendship = Friendships.objects( (Q(one=user.id) & Q(other=friend.id) )| (Q(other=user.id) & Q(one=friend.id)) )
    if len(friendship) is not 0 :
        abort(404, "Already is a friend.")
    else:
        try:
            friendshiprequest = FriendshipsRequests.objects.get( user=user.id, requested=friend.id )
            abort(404, "Already you made a request to this user.")
        except DoesNotExist:
            try:
                friendshiprequest = FriendshipsRequests.objects.get( user=friend.id, requested=user.id )
                abort(404, "The user made a request to you before.")
            except DoesNotExist:
                friendreq = FriendshipsRequests()
                friendreq.user = user
                friendreq.requested = friend
                friendreq.save()
    


@get('/friends')
@auth_basic(check_auth)
def friends_get_all_friends():
    """
        Devuelve la lista de amigos de un usuario
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return friends_get_all(user, ourl.scheme + "://" + ourl.netloc)


@get('/friendshipsrequestsreceived')
@auth_basic(check_auth)
def friends_get_all_friendshiprequestsreceived():
    """
        Devuelve la lista de peticion pendientes de amigos recibidas
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return friends_get_all_friendhips_received(user, ourl.scheme + "://" + ourl.netloc)


@get('/friendshipsrequestssended')
@auth_basic(check_auth)
def friends_get_all_friendshiprequestssended():
    """
        Devuelve la lista de peticion pendientes de amigos enviadas
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return friends_get_all_friendhips_sended(user, ourl.scheme + "://" + ourl.netloc)


@post('/processfriendshiprequest')
@auth_basic(check_auth)
def friends_process_friendship_request():
    """
        Procesa una peticion de amistad recivida
    """
    username, password = request.auth or (None, None)
    data = request.body.readline()
    error = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('action'):
        error['action'] = 'Action is requiered'
    elif not entity['action'] in ['accept', 'reject']:
        error['action'] = 'Action option must be accept or reject'
    if not entity.has_key('request'):
        error['request'] = 'Request id is requiered'
    if error:
        abort(403, error)
        
    try:
        requ = FriendshipsRequests.objects().with_id(entity['request'])
    except ValidationError:
        abort(404, "Friendship request doesn't exists.")
    if requ is None:
        abort(404, "Friendship request doesn't exists.")

    user = Accounts.objects.get(email=username)

    if not (requ.requested.id == user.id):
        abort(403, 'Request is not for you')
    else:
        userSender = Accounts.objects.get(id=requ.user.id)
        notif = NotificationFriendshipsProcess()
        notif.user = userSender
        notif.sender = user
        if entity['action'] in ['accept']:
            friend = Friendships()
            friend.one = requ.user
            friend.other = user
            friend.save()
            requ.delete()
            notif.result = 'ACCEPTED'
            notif.save()
            ourl = urlparse(request.url)
            return friends_parse_friend(requ.user, ourl.scheme + "://" + ourl.netloc)
        else:
            requ.delete()
            #notif.result = 'REJECTED'
            #notif.save()



@post('/deletefriendship')
@auth_basic(check_auth)
def friends_delete_friendship():
    """
       Elimina una amistad
    """
    username, password = request.auth or (None, None)
    data = request.body.readline()
    error = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('friend'):
        error['friend'] = 'Friend is requiered'
    if error:
        abort(403, error)
        
    user = Accounts.objects.get(email=username)
    
    friendship = Friendships.objects( (Q(one=user.id) & Q(other=entity['friend']) )| (Q(other=user.id) & Q(one=entity['friend'])) )
    if len(friendship) is not 0 :
        friendship.delete()
    else:
        abort(403, "Is not your friend yeat.")

    
    
@get('/friendships/notification')
@auth_basic(check_auth)
def friends_get_all_notification():
    """
        Devuelve la lista de notificaciones de las peticiones aceptadas o rechazadas que hizo
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return friends_parse_all_notification(user, ourl.scheme + "://" + ourl.netloc)

@post('/friendships/notifications/setreaded')
@auth_basic(check_auth)
def notifications_set_readed():
    """
        Obtiene un arreglo de notificaciones y las setea como leidas
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
       
    data = request.body.readline()
    error = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)

    #TODO Check que sea un arra
    if not entity.has_key('notifications'):
        error['notifications'] = 'Notifications array is required'
        
    if error :
        abort(403, error)
        
    errors = friends_notifications_set_readed(entity['notifications'])
    if errors :
        abort(403, {'reason': "Error on set readed notifications", 'details':errors } )