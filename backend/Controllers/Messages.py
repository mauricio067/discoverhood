import json
import os
from urlparse import urlparse
from bottle import request, response, get, post, abort, auth_basic
from mongoengine import ValidationError, DoesNotExist
from models import Accounts, Message, Conversation, NotificationMessage
from Models.Accounts import check_auth
from Models.Messages import message_get_msgs_conversations, message_get_all_conversations, parse_conversation, message_notify_new_message, parse_message, message_notifications_get_all_unreaded, conversation_notifications_set_readed
from Models.Messages import message_get_next_conversations, message_get_first_conversations, message_get_first_messages, message_get_next_messages, message_get_new_messages, message_notifications_get_all

@post('/startNewConversation')
@auth_basic(check_auth)
def messages_start_new_conversation():
    """
        Crea una nueva conversation
    """

    data = request.body.readline()
    error = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data,'ISO-8859-1')
    if not entity.has_key('to'):
        error['to'] = 'User id is required'
    if not entity.has_key('msg'):
        error['msg'] = 'Message is required'

    if error :
        abort(403, error)

    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    try:
        userto = Accounts.objects.get(id=entity['to'])
    except DoesNotExist:
        abort(403, "Destination user doesn't exists.")

    conver = Conversation()
    conver.people = [user,userto]
    conver.save()

    msg = Message()
    msg.author = user
    msg.readedby = [user]
    msg.text = entity['msg']
    msg.conversation = conver

    msg.save()
    #TODO si hay una conversation con esta persona directmente agrega un nuevo mensaje a esa conversation




    #notificacion de nuevo mensaje
    message_notify_new_message(user, userto, conver, msg)

    ourl = urlparse(request.url)
    return parse_conversation(conver, user, ourl.scheme + "://" + ourl.netloc)



@get('/conversations')
@auth_basic(check_auth)
def messages_get_all_conversations():
    """
        Devuelve las primeras conversaciones de un usuario
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return message_get_first_conversations(user, ourl.scheme + "://" + ourl.netloc)
    #return message_get_all_conversations(user, ourl.scheme + "://" + ourl.netloc)

@get('/conversations/<publID:re:[a-z,0-9]+>')
@auth_basic(check_auth)
def messages_get_all_conversations(publID):
    """
        Devuelve el listado de conversacion, comenzando por la pasada por parametro
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return message_get_next_conversations(user, conv, ourl.scheme + "://" + ourl.netloc)
    #return message_get_all_conversations(user, ourl.scheme + "://" + ourl.netloc)


@get('/conversation/:id')
@auth_basic(check_auth)
def messages_get_conversation(id):
    """
        Devuelve una conversacion con los ultimos mensajes
    """
    try :
        conv = Conversation.objects().with_id(id)
    except ValidationError:
        abort(404, "Conversation doesn't exists.")
    if conv is None:
        abort(404, "Conversation doesn't exists.")

    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return parse_conversation(conv, user, ourl.scheme + "://" + ourl.netloc)



@get('/conversation/:id/messages')
@auth_basic(check_auth)
def messages_get_first_messages_crtl(id):
    """
        Devuelve los mensajes mas nuevos de una conversacion
    """
    try :
        conv = Conversation.objects().with_id(id)
    except ValidationError:
        abort(404, "Conversation doesn't exists.")
    if conv is None:
        abort(404, "Conversation doesn't exists.")
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return message_get_first_messages(user, conv, ourl.scheme + "://" + ourl.netloc)


@get('/conversation/:id/messages/<msgID:re:[a-z,0-9]+>')
@auth_basic(check_auth)
def messages_get_next_messages_crtl(id, msgID):
    """
        Devuelve los mensajes a partir de un mensaje dado de una conversacion
    """
    try :
        conv = Conversation.objects().with_id(id)
    except ValidationError:
        abort(404, "Conversation doesn't exists.")
    if conv is None:
        abort(404, "Conversation doesn't exists.")

    try :
        msg = Message.objects().with_id(msgID)
    except ValidationError:
        abort(404, "Message doesn't exists.")
    if msg is None:
        abort(404, "Message doesn't exists.")

    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return message_get_next_messages(user, conv, msg, ourl.scheme + "://" + ourl.netloc)


@get('/conversation/:id/newmessages/<msgID:re:[a-z,0-9]+>')
@auth_basic(check_auth)
def messages_get_new_messages_crtl(id, msgID):
        """
             Devuelve los mensajes mas nuevos a partir
        """
        try :
            conv = Conversation.objects().with_id(id)
        except ValidationError:
            abort(404, "Conversation doesn't exists.")
        if conv is None:
            abort(404, "Conversation doesn't exists.")

        try :
            msg = Message.objects().with_id(msgID)
        except ValidationError:
            abort(404, "Message doesn't exists.")
        if msg is None:
            abort(404, "Message doesn't exists.")

        username, password = request.auth or (None, None)
        user = Accounts.objects.get(email=username)
        ourl = urlparse(request.url)
        return message_get_new_messages(user, conv, msg, ourl.scheme + "://" + ourl.netloc)

@post('/conversation/newMessage')
@auth_basic(check_auth)
def messages_create_new_message():
    """
        Crea un nuevo mensaje de una conversation
    """
    data = request.body.readline()
    error = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data, 'ISO-8859-1')

    if not entity.has_key('msg'):
        error['msg'] = 'Message is required'
    if not entity.has_key('conversation'):
        error['msg'] = 'Conversation is required'

    if error :
        abort(403, error)

    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)


    try:
        conver = Conversation.objects.get(id=entity['conversation'])
    except DoesNotExist:
        abort(403, "Converation doesn't exists.")


    msg = Message()
    msg.author = user
    msg.readedby = [user]
    msg.text = entity['msg']
    msg.conversation = conver
    msg.save()

    #TODO enviar notificacion a los usuarios de la conversation. Obviamente quitandonos a nosotros
    #message_notify_new_message(user, userto, conver, conver.msgs[0]._id)

    for userto in conver.people:
        if not userto.id == user.id:
            message_notify_new_message(user, userto, conver, msg)

    ourl = urlparse(request.url)
    return parse_message(msg, user, ourl.scheme + "://" + ourl.netloc)


#************************************************
# Notificaions
#***********************************************


@get('/conversation/notifications')
@auth_basic(check_auth)
def messages_get_all_unreaded_notifications():
    """
        Devuelve las notificaciones no leidas por el usuario
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)

    return message_notifications_get_all(user, ourl.scheme + "://" + ourl.netloc)



@post('/conversation/notifications/setreaded')
@auth_basic(check_auth)
def notifications_set_readed():
    """
        Obtiene un arreglo de notificaciones y las setea como leidas
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)

    data = request.body.readline()
    error = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)

    #TODO Check que sea un arra
    if not entity.has_key('notifications'):
        error['notifications'] = 'Notifications array is required'

    if error :
        abort(403, error)

    errors = conversation_notifications_set_readed(entity['notifications'])
    if errors :
        abort(403, {'reason': "Error on set readed notifications", 'details':errors } )
