import json
import os
from random import randrange
from urlparse import urlparse
from bson import ObjectId
from bottle import request, response, get, post, abort, auth_basic
from mongoengine import ValidationError, DoesNotExist, Q
from models import Accounts, Favorite, Publication, ImagePublication, CheckInPublication, Comment, SharedPublication
from Models.Accounts import check_auth
from Models.Publications import validate_POST_form, get_all_publications, get_all_publications_from_user, get_all_user_picture_publications
from Models.Publications import get_first_news_publications_from_user, get_all_publication_notifications_from_user
from Models.Publications import favorites_get_date_groups, favorites_get_all_from_dategroup, notication_create, get_all_comments, parse_comment, get_simple_clear
from Models.Publications import get_next_news_publications_from_user, get_all_new_publications, get_next_publications, publication_notifications_set_readed, publication_batch_toggle_smiles
from image import crate_image

@post('/create_publication')
@auth_basic(check_auth)
def publication_create():
    """
        Crea una nueva publicacion
    """
    username, password = request.auth or (None, None)
    data = request.body.readline()
    if not data:
        abort(400, 'No data received' )
    errors = validate_POST_form(request.forms)
    if errors:
        abort(403, {'reason': 'invalid posted data', 'details':errors } )

    if not (request.forms.get('to') is None):
        try:
            userTo = Accounts.objects.get( id=request.forms.to )
        except DoesNotExist:
            errors['to'] = "User doesn't exist"
            abort(403, {'reason': 'invalid posted data', 'details':errors } )

    if request.forms.type in ('image', 'checkin') :
        upload = request.files.image
        if upload is not None:
            file_path=crate_image(upload)
        else:
            abort(403, 'Images is needed')

    if request.forms.type in ('text'):
        publ = Publication()
    elif request.forms.type in ('image'):
        publ = ImagePublication()
        publ.Image.put(file_path)
    elif request.forms.type in ('checkin'):
        publ = CheckInPublication()
        publ.MapImage.put(file_path)


    user = Accounts.objects.get(email=username)
    publ.author = user
    publ.text = request.forms.text
    publ.created_position = [float(request.forms.latitude), float(request.forms.longitude)]

    if not (request.forms.get('to') is None):
        publ.to = userTo
    try:
        publ.save()
        #ourl = urlparse(request.url)
        #return get_simple_clear(user, publ, ourl.scheme + "://" + ourl.netloc )
    except ValidationError as msg:
        abort(403, {'reason': "Can't create publication", 'details':msg.to_dict() } )



@post('/publication/share')
@auth_basic(check_auth)
def publication_share():
    """
        Hace un share de una publicacion dada
    """
    username, password = request.auth or (None, None)

    data = request.body.readline()
    errors = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('publication'):
        errors['Publication'] = 'Publication id is requiered'
    if not entity.has_key('latitude'):
        errors['latitude'] = 'latitude is requiered'
    if not entity.has_key('longitude'):
        errors['longitude'] = 'longitude is requiered'

    if errors:
        abort(403, {'reason': 'invalid posted data', 'details':errors } )


    try:
        publ = Publication.objects().with_id(entity['publication'])
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if publ is None:
        abort(404, "Publication doesn't exists.")

    user = Accounts.objects.get(email=username)

    if user.id is publ.author.id:
        abort(403, "You can't share your own publication")

    #TODO chequear que ya no hayas shareado

    sha = SharedPublication.objects( Q(shared_publication=entity['publication']) & Q(author=user.id) )
    if len(sha) :
        abort(403, "You can't share this publication again.")

    sha = SharedPublication()
    sha.author = user
    sha.created_position = [float(entity['latitude']), float(entity['longitude'])]
    sha.shared_publication = publ

    try:
        sha.save()
        ourl = urlparse(request.url)
        if not publ.author.id is user.id:
            notication_create( publ.author, user, 'SHARED', sha)
        return get_simple_clear(user, publ, ourl.scheme + "://" + ourl.netloc )
    except ValidationError as msg:
        abort(403, {'reason': "Can't share publication", 'details':msg.to_dict() } )



@post('/toggleSmile')
@auth_basic(check_auth)
def publication_toggle_smile():
    """
        Agrega o quita un smile a una publicacion
    """
    username, password = request.auth or (None, None)

    data = request.body.readline()
    error = None
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('publication'):
        abort(403, 'Publication id is requiered')

    try:
        publ = Publication.objects().with_id(entity['publication'])
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if publ is None:
        abort(404, "Publication doesn't exists.")

    user = Accounts.objects.get(email=username)
    #if publ.author is user :
        #abort(403, "You can't smile you publication." )
    try:
        publ = Publication.objects().get(id=entity['publication'], smiles=user.id)
        Publication.objects(id=entity['publication']).update_one(pull__smiles=user.id)
        if not publ.author.id is user.id:
            notication_create( publ.author, user, 'UNSMILE', publ)
    except DoesNotExist:
        Publication.objects(id=entity['publication']).update_one(push__smiles=user.id)
        if not publ.author.id is user.id:
            notication_create( publ.author, user, 'SMILE', publ)



@post('/toggleSmiles')
@auth_basic(check_auth)
def publication_toggle_smiles():
    """
        Agrega o quita un smile a una lista de publicaciones
    """
    username, password = request.auth or (None, None)

    data = request.body.readline()
    error = None
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('publications'):
        abort(403, 'Publications array is requiered')

    user = Accounts.objects.get(email=username)

    errors = publication_batch_toggle_smiles(user, entity['publications'])
    if errors :
        abort(403, {'reason': "Error on set readed notifications", 'details':errors } )


@get('/publications')
@auth_basic(check_auth)
def publications_get_all():
    """
        Devuelve el listado de publicaciones globales
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    #return get_all_publications(user, ourl.scheme + "://" + ourl.netloc)
    return get_all_new_publications(user, ourl.scheme + "://" + ourl.netloc)


@get('/publications/<publID:re:[a-z,0-9]+>')
@auth_basic(check_auth)
def publications_get_all(publID):
    """
        Devuelve el listado de publicaciones globales, comenzando por la pasada por parametro
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    try :
        publ = Publication.objects().with_id(publID)
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if publ is None:
        abort(404, "Publication doesn't exists.")

    ourl = urlparse(request.url)
    return get_next_publications(user, publ, ourl.scheme + "://" + ourl.netloc)


@get('/publications/user/:id')
@auth_basic(check_auth)
def publications_get_all(id):
    """
        Devuelve el listado de publicaciones realizadas por un usuario dado
    """
    try :
        ourl = urlparse(request.url)
        user = Accounts.objects().with_id(id)
    except ValidationError:
        abort(404, "User doesn't exists.")
    if user is None:
        abort(404, "User doesn't exists.")
    ourl = urlparse(request.url)
    return get_first_news_publications_from_user(user,ourl.scheme + "://" + ourl.netloc)
    #return get_all_publications_from_user(user,ourl.scheme + "://" + ourl.netloc)


@get('/publications/user/:id/<publID:re:[a-z,0-9]+>')
@auth_basic(check_auth)
def publications_get_all(id, publID):
    """
        Devuelve el listado de publicaciones mas antiguas de un usuario dado, comenzando por la pasada por parametro
    """
    try :
        ourl = urlparse(request.url)
        user = Accounts.objects().with_id(id)
    except ValidationError:
        abort(404, "User doesn't exists.")
    if user is None:
        abort(404, "User doesn't exists.")

    try :
        publ = Publication.objects().with_id(publID)
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if publ is None:
        abort(404, "Publication doesn't exists.")

    ourl = urlparse(request.url)
    return get_next_news_publications_from_user(user, publ, ourl.scheme + "://" + ourl.netloc)


@get('/mypublications')
@auth_basic(check_auth)
def publications_get_all():
    """
        Devuelve el listado de publicaciones de tu usuario con solo las ultima publicaciones
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return get_first_news_publications_from_user(user,ourl.scheme + "://" + ourl.netloc)
    #return get_all_publications_from_user(user,ourl.scheme + "://" + ourl.netloc)


@get('/mypublications/:id')
@auth_basic(check_auth)
def publications_get_all(id):
    """
        Devuelve el listado de publicaciones mas antiguas de tu usuario, comenzando por la pasada por parametro
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)

    try :
        publ = Publication.objects().with_id(id)
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if publ is None:
        abort(404, "Publication doesn't exists.")

    return get_next_news_publications_from_user(user, publ, ourl.scheme + "://" + ourl.netloc)



@get('/mypublications/pictures')
@auth_basic(check_auth)
def publications_get_all():
    """
        Devuelve el listado de publicaciones de imagenes de tu usuario
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return get_all_user_picture_publications(user,ourl.scheme + "://" + ourl.netloc)



@get('/picture/:id')
#@auth_basic(check_auth)
def publications_get_picture(id):
    """
        Devuelve SOLO la imagen dada
    """
    try:
        queryset = ImagePublication.objects(Image=ObjectId(id))
        publ = queryset.next()
    except StopIteration:
        abort(404, 'Picture not found')
    response.content_type = 'image/png'
    return publ.Image.read()


@get('/picture_thumbnail/:id')
#@auth_basic(check_auth)
def publications_get_picture_thumbnail(id):
    """
        Devuelve SOLO el thumbnal de la imagen dada
    """
    try:
        queryset = ImagePublication.objects(Image=ObjectId(id))
        publ = queryset.next()
    except StopIteration:
        abort(404, 'Picture not found')
    response.content_type = 'image/png'
    return  publ.Image.thumbnail.read() #TODO capturar error si no existe thumbnail


@get('/mapPreview/:id')
#@auth_basic(check_auth)
def publications_get_mapPreview(id):
    """
        Devuelve SOLO la imagen dada
    """
    try:
        queryset = CheckInPublication.objects(MapImage=ObjectId(id))
        publ = queryset.next()
    except StopIteration:
        abort(404, 'Picture not found')
    response.content_type = 'image/png'
    return publ.MapImage.read()


@get('/mapPreview_thumbnail/:id')
#@auth_basic(check_auth)
def publications_get_mapPreview_thumbnail(id):
    """
        Devuelve SOLO el thumbnal de la imagen dada
    """
    try:
        queryset = CheckInPublication.objects(MapImage=ObjectId(id))
        publ = queryset.next()
    except StopIteration:
        abort(404, 'Picture not found')
    response.content_type = 'image/png'
    return  publ.MapImage.thumbnail.read() #TODO capturar error si no existe thumbnail



@get('/publication/:id/comments')
@auth_basic(check_auth)
def publications_get_comments(id):
    """
        Devuelve todos los comentarios de una publicacion
    """
    data = request.body.readline()

    try:
        publ = Publication.objects().with_id(id)
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if publ is None:
        abort(404, "Publication doesn't exists.")

    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return get_all_comments(publ, ourl.scheme + "://" + ourl.netloc)

@post('/publication/addComment')
@auth_basic(check_auth)
def publications_add_comment():
    """
        Crea un nuevo comentario
    """
    data = request.body.readline()
    error = None
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data, 'ISO-8859-1')
    if not entity.has_key('publication'):
        abort(403, 'Publication id is requiered')
    if not entity.has_key('text'):
        abort(403, 'Comment text is requiered')


    try:
        publ = Publication.objects().with_id(entity['publication'])
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if publ is None:
        abort(404, "Publication doesn't exists.")

    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)

    com = Comment()
    com.content = entity['text']
    com.author = user

    Publication.objects(id=publ.id).update_one(push__comments=com)
    ourl = urlparse(request.url)
    if not (publ.author.id == user.id):
        notication_create( publ.author, user, 'COMMENT', publ)

    for coment in publ.comments:
        if (not (coment.author.id == user.id)) and (not (coment.author.id == publ.author.id)) :
            notication_create( coment.author, user, 'COMMENT', publ)

    return parse_comment(com, ourl.scheme + "://" + ourl.netloc)


#############################################
### Favorites
#############################################

@post('/addFavorite')
@auth_basic(check_auth)
def favorites_add():
    """
        Marca una publicacion como favorita
    """
    username, password = request.auth or (None, None)

    data = request.body.readline()
    error = None
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('publication'):
        abort(403, 'Publication id is requiered')

    user = Accounts.objects.get(email=username)
    try:
        publ = Publication.objects().with_id(entity['publication'])
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if user is None:
        abort(404, "Publication doesn't exists.")

    try:
        fav = Favorite.objects.get(owner=user.id,publication=publ.id)
        abort(404, "Publication already is a favorite.")
    except DoesNotExist:
        fav = Favorite()
        fav.owner = user
        fav.publication = publ
        fav.save()

@post('/removeFavorite')
@auth_basic(check_auth)
def favorites_add():
    """
        Elimina una publicacion como favorita
    """
    username, password = request.auth or (None, None)

    data = request.body.readline()
    error = None
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)
    if not entity.has_key('publication'):
        abort(403, 'Publication id is requiered')

    user = Accounts.objects.get(email=username)
    try:
        publ = Publication.objects().with_id(entity['publication'])
    except ValidationError:
        abort(404, "Publication doesn't exists.")
    if user is None:
        abort(404, "Publication doesn't exists.")

    try:
        fav = Favorite.objects.get(owner=user.id,publication=publ.id)
        fav.delete()
    except DoesNotExist:
        abort(404, "Publication isn't a favorite.")



@get('/getFavorites/<year:int>/<month:int>')
@auth_basic(check_auth)
def favorites_get_all(year, month):
    """
        Devuelve el listado de favoritos
    """
    error = dict()
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    if not (year >= 1900 and year <= 2050):
       error['year'] = 'year must be real'
    if not (month >= 1 and month <= 12):
       error['month'] = 'month must be an integer betwenn 1 and 12'
    if error :
        abort(403, error )
    ourl = urlparse(request.url)
    return favorites_get_all_from_dategroup(user, year, month, ourl.scheme + "://" + ourl.netloc)


@get('/getListofDates')
@auth_basic(check_auth)
def favorites_get_list_of_dates():
    """
        Devuelve un diccionario con los grupos de fechas de las publicaciones agregadas a favorites
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    return favorites_get_date_groups(user)



#*********************************************
# Notification
#*********************************************

@get('/publication/notifications')
@auth_basic(check_auth)
def publications_get_all():
    """
        Devuelve las notificaciones sobre las publicaciones de los usuarios
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)
    ourl = urlparse(request.url)
    return get_all_publication_notifications_from_user(user,ourl.scheme + "://" + ourl.netloc)


@post('/publication/notifications/setreaded')
@auth_basic(check_auth)
def notifications_set_readed():
    """
        Obtiene un arreglo de notificaciones y las setea como leidas
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)

    data = request.body.readline()
    error = dict()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)

    #TODO Check que sea un arra
    if not entity.has_key('notifications'):
        error['notifications'] = 'Notifications array is required'

    if error :
        abort(403, error)

    errors = publication_notifications_set_readed(entity['notifications'])
    if errors :
        abort(403, {'reason': "Error on set readed notifications", 'details':errors } )
