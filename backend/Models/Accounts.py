import datetime
import bcrypt
from random import randrange
from mongoengine import Document, DoesNotExist, signals, ValidationError
from mongoengine import StringField, DateTimeField, EmailField, ImageField, IntField, BooleanField, ReferenceField, ListField, GeoPointField
from BaseDocument import BaseDocument
from validate_email import validate_email
from send_email import send_validation_code, send_recuperation_email
from math import radians, sqrt, sin, cos, atan2

def geocalc(lat1, lon1, lat2, lon2):
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)
    dlon = lon1 - lon2
    EARTH_R = 6372.8
    y = sqrt(
        (cos(lat2) * sin(dlon)) ** 2
        + (cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dlon)) ** 2
        )
    x = sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(dlon)
    c = atan2(y, x)
    return EARTH_R * c


#*********************************
# Funcion para la authentication
#********************************
def check_auth(username, password) :
    try:
        user = Accounts.objects.get(email=username)
        #print username
        #print password
        #print user.fb
        #print user.fb_code
        #print user.fb_code_salt
        #print bcrypt.hashpw(password, user.fb_code_salt)
        if user.fb :
            if bcrypt.hashpw(password, user.fb_code_salt) == user.fb_code :
                return True
        else:
            if bcrypt.hashpw(password.encode('utf-8'), user.password.encode('utf-8')) == user.password :
                return True
    except DoesNotExist:
        return False
    return False


#*********************************
# Funcion varias
#********************************
def validate_POST(data) :
    errors = dict()
    if not data.has_key('email'):
        errors['email'] = 'email is requiered'
    elif not validate_email(data['email'], verify=False) :
        errors['email'] = "The email '%s' must be real" %data['email']
        #TODO check unique
    if not data.has_key('password'):
        errors['password'] = 'Password is requiered.'
    elif len(data['password']) < 6 or len(data['password']) > 20 :
        errors['password'] = 'The password might have more than 6 characters and less than 20.'
    return errors
    #TODO check genfer M o F

def validate_POST_form(data) :
    errors = dict()
    if data.get('email') is None:
        errors['email'] = 'email is requiered'
    elif not validate_email(data.email, verify=False) :
        errors['email'] = "The email '%s' must be real" %data.email
        #TODO check unique
    if data.get('fb_code') is None:
        if data.password is None:
            errors['password'] = 'Password is requiered.'
        elif len(data.password) < 6 or len(data.password) > 20 :
            errors['password'] = 'The password might have more than 6 characters and less than 20.'
    return errors

def validate_POST_edit_form(data) :
    errors = dict()

    if (not data.get('nickname') is None) and (len(data.nickname) < 2 or len(data.nickname) > 20) :
        errors['nickname'] = 'The nickname might have more than 6 characters and less than 20.'
    if (not data.get('firstname') is None) and (len(data.firstname) < 2 or len(data.firstname) > 50) :
        errors['firstname'] = 'The firstname might have more than 6 characters and less than 50.'
    if (not data.get('lastname') is None) and (len(data.lastname) < 2 or len(data.lastname) > 50) :
        errors['lastname'] = 'The lastname might have more than 6 characters and less than 50.'

    if (not data.get('email') is None) and (not validate_email(data.email, verify=False)) :
        errors['email'] = "The email '%s' must be real" %data.email
    if not data.get('password') is None:
        if len(data.password) < 6 or len(data.password) > 20 :
            errors['password'] = 'The password might have more than 6 characters and less than 20.'
    return errors

def resend_validation_code( user ):
    user.validation_code = randrange(100000,999999)
    user.save()
    send_validation_code( user.firstname + " " + user.lastname, user.email, user.validation_code )

def change_password( user, password) :
    user.password_salt = bcrypt.gensalt()
    user.password = bcrypt.hashpw(password.encode('utf-8'), user.password_salt)
    user.active = True
    user.save()

def send_recuperation_message(user):
    user.validation_code = randrange(100000,999999)
    user.save()
    send_recuperation_email(user.firstname + " " + user.lastname, user.email, user.validation_code)

def set_new_email( user, email ):
    """
        Setea el nuevo email y el nuevo estado de aceptacion de nueva cuenta.
        Si exite ya ese email o new_email, devuelve un error
    """
    #if no existe email ni new_email

    user.active = False
    user.save()

def change_email( user, email ):
    user.email = email
    del user.new_email
    user.active = True
    user.save()



#*********************************
# Defino el modelo
#********************************
GENDERS = ('M', 'F')

class Accounts(BaseDocument):
    #meta = {'collection': 'users'}

    nickname = StringField(min_length=2, max_length=20)
    email = EmailField(required=True, unique=True)
    new_email = EmailField()
    password = StringField( min_length=6)
    password_salt = StringField()
    avatar = ImageField( size=(400, 400, True), thumbnail_size=(180, 180, True), collection_name='avatars')
    firstname = StringField(required=True, min_length=2,max_length=50)
    lastname = StringField(required=True, min_length=2, max_length=50)
    gender = StringField(required=True, max_length=1, choices=GENDERS)
    role = StringField(required=True, default="user")
    birthday = DateTimeField(required=True, default=datetime.datetime.now)
    last_position = GeoPointField(required=False, default=[0,0])
    last_connection = DateTimeField()
    fb = BooleanField(default=False)
    fb_code = StringField()
    fb_code_salt = StringField()
    validation_code = IntField(min_value=100000, max_value=999999)
    active = BooleanField(default=False)

    #def clean(self):


    def get_name_to_show(self):
        """
            Devuelte un human readable account name
        """
        if self.nickname is not None :
            return self.nickname
        else:
            return self.firstname + " " + self.lastname

    def parse_avatar_data(self, url):
        """
            Devuelte un diccionario con la informacion parseada del avatar
        """
        try:
            return { '_id': self.avatar._id, 'md5': self.avatar.md5, 'thumbnail_md5': self.avatar.thumbnail.md5, 'url': url + "/avatar/" + str(self.avatar._id), 'url_thumbnail': url + "/avatar_thumbnail/" + str(self.avatar._id) }
        except AttributeError:
            return None
        return None

    @classmethod
    def get_own_account(self, username, url ):
        queryset = Accounts.objects(email=username).exclude( "password", "password_salt", "fb_code", "fb_code_salt", "role", "validation_code")
        try :
            user = queryset.next()
            salida = user.to_mongo()
            if salida['birthday'].year < 1900 :
                del salida['birthday']
            #TODO chequear correctamente avatar vacio
            try:
                if user.avatar is not None:
                    salida['avatar'] = user.parse_avatar_data(url)
            except AttributeError:
                del user.avatar

            return salida
        except StopIteration:
            raise queryset._document.DoesNotExist("User doesn't exist'.")


    @classmethod
    def get_other_account(self, id, owndata, url ):
        try:
            user = Accounts.objects().exclude( "password", "password_salt", "role", "validation_code", "active" ).with_id(id)
        except ValidationError:
            raise DoesNotExist("User doesn't exist.")
        if user is None:
            raise DoesNotExist("User doesn't exist.")
        salida = user.to_mongo()
        if salida['birthday'].year < 1900 :
            del salida['birthday']
        try:
            salida['avatar'] = user.parse_avatar_data(url)
        except AttributeError:
            del user.avatar

        if user.last_connection is None:
            salida['online'] = False;
        else :
            if salida.has_key('last_connection'):
                del salida['last_connection']
                if (datetime.datetime.now() - user.last_connection).seconds < 300 :
                    salida['online'] = True;
                else:
                    salida['online'] = False;

        if salida.has_key('last_position') and not owndata.last_position is None :
            salida['distance'] = geocalc(salida['last_position'][0], salida['last_position'][1], owndata.last_position[0], owndata.last_position[0] );
            del salida['last_position']

        return salida


    #@queryset_manager
    #def own_account(doc_cls, queryset):
        ##return queryset.filter(published=True)
        #return queryset.exclude( "password", "password_salt", "role", "validation_code", "fb")

    @classmethod
    def post_save(cls, sender, document, **kwargs):
        """
            Enviara un correo luego de crear una nueva cuenta
        """
        if 'created' in kwargs:
            if kwargs['created']:
                if not document.fb:
                    document.password_salt = bcrypt.gensalt()
                    #Para las password se utilizan una codificacion de utf-8
                    document.password = bcrypt.hashpw(document.password.encode('utf-8'), document.password_salt)
                    document.validation_code = randrange(100000,999999)
                    document.save()
                    print "correo enviado a :" + document.email
                    send_validation_code( document.firstname + " " + document.lastname, document.email, document.validation_code )
                    document.fb = False
                else:
                    document.fb_code_salt = bcrypt.gensalt()
                    document.fb_code = bcrypt.hashpw(document.fb_code, document.fb_code_salt)
                    document.fb = True
                    document.active = True
                    document.save()
            #else:
                #logging.debug("Updated")

    def prepare_json_send_owner(self):
        """
            Devuelve una estructura json con los elementos del objeto para ser
            utilizado en el caso en que se necesite devolver cuando el usuario necesite sus datos
        """
        data = dict()

        data['nickname'] = self.nickname
        data['email'] = self.email
        data['firstname'] = self.firstname
        data['lastname'] = self.lastname
        data['gender'] = self.gender
        data['birthday'] = self.birthday
        data['active'] = self.active
        return data

signals.post_save.connect(Accounts.post_save, sender=Accounts)
