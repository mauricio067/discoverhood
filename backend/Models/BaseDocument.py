import datetime
from bson.objectid import ObjectId
from mongoengine import Document, EmbeddedDocument
from mongoengine import StringField, DateTimeField, ObjectIdField


class BaseDocument(Document):
    #def check_permissions(self):
    created = DateTimeField(default=datetime.datetime.now)
    updated = DateTimeField(default=datetime.datetime.now)
    Etag = StringField() #TODO autogenerate
    meta = { 'abstract': True }
    
    
class BaseEmbeddedDocument(EmbeddedDocument):
    _id = ObjectIdField()
    created = DateTimeField(default=datetime.datetime.now)
    updated = DateTimeField(default=datetime.datetime.now)
    Etag = StringField() #TODO autogenerate
    meta = { 'abstract': True }
    
    def clean(self):
            self._id = ObjectId()
