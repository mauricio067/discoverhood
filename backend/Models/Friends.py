from datetime import datetime
from mongoengine import  Q, ReferenceField, StringField
from BaseDocument import BaseDocument
from Notifications import Notification

from Accounts import Accounts

#*********************************
# Funcion varias
#********************************
def friends_parse_friend(friend, url):
    return { '_id': friend.id, 'name': friend.get_name_to_show(), 'avatar': friend.parse_avatar_data(url) }



def friends_get_all(user, url):
    """
        Devuelve la lista de amigos de un usuario
    """
    items = []
    for fri in Friendships.objects(Q(one=user.id) | Q(other=user.id) ):
        if fri.one.id == user.id:
            friend = fri.other
        else:
            friend = fri.one
        items.append( friends_parse_friend(friend, url) )
    return {'items':items}


def friends_parse_request(friend, user, url, requester):
    """
        Parsea los datos para devolverlos. requester es booleano
    """
    request = friend.to_mongo()
    del request['user']
    del request['requested']
    requester = dict()
    requester['_id'] = friend.user.id
    requester['name'] = friend.user.get_name_to_show()
    requester['avatar'] = friend.user.parse_avatar_data(url)
    if requester['avatar'] is None:
        del requester['avatar']
    if requester:
        request['requester'] = requester
    else :
        request['requested'] = requester
    return request


def friends_get_all_friendhips_received(user, url):
    """
        Devuelve la lista de peticion pendientes de amigos recibidas
    """
    items = []
    for fri in FriendshipsRequests.objects(requested=user.id ):
        items.append(friends_parse_request(fri, user, url, True))
    return {'items':items}


def friends_get_all_friendhips_sended(user, url):
    """
        Devuelve la lista de peticion pendientes de amigos enviadas
    """
    items = []
    for fri in FriendshipsRequests.objects(user=user.id ):
        items.append(friends_parse_request(fri, user, url, False))
    return {'items':items}
    
#*********************************
# Defino modelos
#*********************************
class Friendships(BaseDocument):
    one = ReferenceField(Accounts) 
    other = ReferenceField(Accounts) 

class FriendshipsRequests(BaseDocument):
    user = ReferenceField(Accounts) 
    requested = ReferenceField(Accounts) 
    
    
    
#*********************************
# Notificaciones
#*********************************
def friends_notifications_set_readed(notifications):
    """
        Setea como leidas todas las notificaciones dentro del arreglo dado
    """
    #TODO check si la notificacion es propia del usuario
    errors = dict()
    for notifi in notifications:
        for key, value in notifi.iteritems() :
            noti = None
            try :
                noti = NotificationFriendshipsProcess.objects().with_id(value)
            except ValidationError:
                errors[key] = "Notification "+value+" doesn't exists."
            if noti:
                noti.readed = True
                noti.save()
            else:
                errors[key] = "Notification "+value+" doesn't exists."
    return errors

def friends_parse_all_notification(user, url):
    items = []
    for notif in NotificationFriendshipsProcess.objects(user=user.id, readed=False).order_by('-created'):
        noti = notif.to_mongo()
        sender = dict()
        sender['_id'] = noti['sender']
        sender['name'] = notif.sender.get_name_to_show()
        sender['avatar'] = notif.sender.parse_avatar_data(url)
        if sender['avatar'] is None:
            del sender['avatar']
        noti['sender'] = sender
        items.append(noti)
    return {'items':items}
    
    
PROCESSACTIONRESULT = ('ACCEPTED', 'REJECTED')

class NotificationFriendshipsProcess(Notification):
    sender = ReferenceField(Accounts)
    result = StringField(required=True, max_length=10, choices=PROCESSACTIONRESULT)