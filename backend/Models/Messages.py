from datetime import datetime
from mongoengine import  Q, EmbeddedDocument, ValidationError
from mongoengine import StringField, ObjectIdField, ReferenceField, EmbeddedDocumentField, ListField
from BaseDocument import BaseDocument, BaseEmbeddedDocument
from Notifications import Notification
from Accounts import Accounts


#*********************************
# Funcion varias
#********************************

def parse_message(msg, user, url):
    """
        Devuelve la informacion de un mensaje para mostrarlo
    """
    #ms = msg.to_mongo()
    author = dict()
    #author['_id'] = msg.author.id
    #author['name'] = msg.author.get_name_to_show()
    #author['avatar'] = msg.author.parse_avatar_data(url)
    #if author['avatar'] is None:
        #del author['avatar']

    ms = msg.to_mongo()

    #ms['author'] = author

    if (user.id in msg.readedby):
        ms['readed'] = True
    else:
        ms['readed'] = False
    del ms['readedby']
    #ms['conversation'] = msg.conversation.id
    return ms


def parse_conversation(conv, user, url):
    """
        Devuelve toda la informacion de una conversation con el ultimo mensaje
    """
    peoples = []
    for people in conv.people :
        peo = dict()
        peo['_id'] = people.id
        peo['name'] = people.get_name_to_show()
        peo['avatar'] = people.parse_avatar_data(url)
        if peo['avatar'] is None:
            del peo['avatar']
        peoples.append(peo)

    con = conv.to_mongo()
    con['people'] = peoples
    msg = Message.objects(conversation=conv.id).order_by('-created').first()
    con['msg'] = parse_message(msg, user, url)
    return con


def message_get_all_conversations(user, url):
    """
        Devuelve todas las conversaciones con el ultimo mensaje para un usuario
    """
    items = []
    for conv in Conversation.objects(people=user).order_by('-created'):
        items.append(parse_conversation(conv, user, url))
    return {'items':items }


def message_get_first_conversations(user, url):
    """
        Devuelve las primeras conversaciones con el ultimo mensaje para un usuario
    """
    items = []
    for conv in Conversation.objects(people=user).order_by('-created')[:10]:
        items.append(parse_conversation(conv, user, url))
    return {'items':items }


def message_get_next_conversations(user, convBegin, url):
    """
        Devuelve las conversaciones mas nuevas a partir de una conversacion dada
    """
    items = []
    for conv in Conversation.objects( Q(people=user) & Q(created__lte=convBegin.created) ).order_by('-created')[:5]:
        items.append(parse_conversation(conv, user, url))
    return {'items':items }


def message_get_first_messages(user, conversation, url):
    """
        Devuelve los mensajes mas nuevos de una conversacion
    """
    items = []
    for msg in Message.objects(conversation=conversation).order_by('created')[:15]:
        items.append(parse_message(msg, user, url))
    return {'items':items }


def message_get_next_messages(user, conversation, mgsBegin, url):
    """
        Devuelve los mensajes de una conversacion a partir de un mensaje dado
    """
    items = []
    for msg in Message.objects( Q(conversation=conversation) & Q(created__lte=mgsBegin.created) ).order_by('created')[:15]:
        items.append(parse_message(msg, user, url))
    return {'items':items }

def message_get_new_messages(user, conversation, lastMsg, url):
    """
       Devuelve los mensajes mas nuevos de una conversacion a partir de un mensaje dado
    """
    items = []
    for msg in Message.objects( Q(conversation=conversation) & Q(author__ne=user) & Q(created__gt=lastMsg.created) ).order_by('created'):
        items.append(parse_message(msg, user, url))
    return {'items':items }


def message_get_msgs_conversations(user, conversation):
    """
        Devuelve todos los mensajes para una conversacion
    """
    items = []
    for msg in Conversation.msgs:
        msg.to_mongo()

        # msg['last_msg_text'] = conv.msgs.first().text
        #TODO agregar si lo leyo o no

        items.append(msg)
    return {'items':items }

#*********************************
# Notificaciones
#********************************
def conversation_notifications_set_readed(notifications):
    """
        Setea como leidas todas las notificaciones dentro del arreglo dado
    """
    #TODO check si la notificacion es propia del usuario
    errors = dict()
    for notifi in notifications:
        for key, value in notifi.iteritems() :
            noti = None
            try :
                noti = NotificationMessage.objects().with_id(value)
            except ValidationError:
                errors[key] = "Notification "+value+" doesn't exists."
            if noti:
                noti.readed = True
                noti.save()
            else:
                errors[key] = "Notification "+value+" doesn't exists."
    return errors

def message_notify_set_conversation_readed(user, conversation):
        """
            Setea todos los mensajes de una conversacion como leida
        """
        for notif in NotificationMessage.objects(user=user, conversation=conversation, readed=False).order_by('-created'):
            notif.readed = True
            notif.save()

def message_notify_new_message(user, userDest, conversation, message):
    message_notify_set_conversation_readed(userDest, conversation)
    notif = NotificationMessage()
    notif.user = userDest
    notif.sendedby = user
    notif.conversation = conversation
    notif.message = message
    notif.save()


def message_parse_notification(notif, user, url):
    """
        Devuelve la informacion de una notificacion para ser mostrada
    """
    ns = notif.to_mongo()
    sendedby = dict()
    sendedby['_id'] = notif.sendedby.id
    sendedby['name'] = notif.sendedby.get_name_to_show()
    sendedby['avatar'] = notif.sendedby.parse_avatar_data(url)
    if sendedby['avatar'] is None:
        del sendedby['avatar']
    ns['sendedby'] = sendedby
    try :
        msg = notif.message
        ns['message'] = parse_message(msg, user, url)
    except ValidationError:
        ns['message'] = ""

    return ns



def message_notifications_get_all_unreaded(user, url):
    """
        Devuelve todas las notificaciones de mensajes sin leer
    """
    items = []
    for notif in NotificationMessage.objects(user=user, readed=False).order_by('-created'):
        items.append(message_parse_notification(notif, user, url))
    return {'items':items }

def message_notifications_get_all(user, url):
        """
            Devuelve todas las notificaciones de un usuario dado.
            Si las notificaciones sin leer no alcanzan a las 10 como minimo, se completa con notificaciones leidas
        """
        items = []
        conver = []
        for noti in NotificationMessage.objects(user=user, readed=False).order_by('-created'):
            items.append(message_parse_notification(noti, user, url) )
            conver.append(noti.conversation.id)

        length = len(items)
        if length < 10 :
            for noti in NotificationMessage.objects(user=user, readed=True).order_by('-created')[:10-length]:
                if not (noti.conversation.id in conver):
                    items.append(message_parse_notification(noti, user, url) )
                    conver.append(noti.conversation.id)

        return {'items':items }


#*********************************
# Defino modelos
#********************************
class Conversation(BaseDocument):
    people = ListField(ReferenceField(Accounts))
    #msgs = ListField(ReferenceField(Message))

class Message(BaseDocument):
    author = ReferenceField(Accounts)
    readedby = ListField(ReferenceField(Accounts))
    text = StringField(required=True, max_length=140) #TODO determinar cual es la mayor longitud deseada)
    conversation = ReferenceField(Conversation)

#*********************************
# Notificaciones
#********************************

class NotificationMessage(Notification):
    sendedby = ReferenceField(Accounts)
    conversation = ReferenceField(Conversation)
    message = ReferenceField(Message)



