from datetime import datetime
from mongoengine import StringField, DateTimeField, ReferenceField, BooleanField
from BaseDocument import BaseDocument
from Accounts import Accounts



class Notification(BaseDocument):
    user = ReferenceField(Accounts)
    readed = BooleanField(default=False)
    readedTime = DateTimeField(default=datetime.now)
    meta = { 'abstract': True }