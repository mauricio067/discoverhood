import datetime
from dateutil.relativedelta import relativedelta
from mongoengine import  EmbeddedDocument, DoesNotExist, Q, ValidationError
from mongoengine import StringField, ReferenceField, ImageField, ListField, GeoPointField, EmbeddedDocumentField, IntField
from BaseDocument import BaseDocument, BaseEmbeddedDocument
from Notifications import Notification
from Accounts import Accounts
from bson import ObjectId

#*********************************
# Funcion varias
#********************************
def validate_POST(data) :
    errors = dict()
    if not data.has_key('email'):
        errors['email'] = 'email is requiered'
    elif not validate_email(data['email'], verify=False) :
        errors['email'] = "The email '%s' must be real" %data['email']
        #TODO check unique
    if not data.has_key('password'):
        errors['password'] = 'Password is requiered.'
    elif len(data['password']) < 6 or len(data['password']) > 20 :
        errors['password'] = 'The password might have more than 6 characters and less than 20.'
    return errors

def validate_POST_form(data) :
    """
        Verifica los campos de ingreso previa a la careacion de una publicacion
    """
    errors = dict()
    if data.type is None:
        errors['type'] = 'Type of publication is needed'
    elif data.type not in ('text', 'image', 'checkin'):
        errors['type'] = 'Incorrect type of publication'
    if data.latitude is None:
        errors['latitude'] = 'Latitude of publication is needed'
    #elif type(data.latitude.is_numeric()) is not float:
        #errors['latitude'] = 'Latitude must be a float value'
    if data.longitude is None:
        errors['longitude'] = 'Longitude of publication is needed'
    #elif type(data.longitude.is_numeric()) is not float:
        #errors['longitude'] = 'Longitude must be a float value'
    return errors

def parse_image_data( image, resource, url ):
    img = dict()
    img['url'] = url + "/" + resource + "/" + str(image._id)
    img['md5'] = image.md5
    img['url_thumbnail'] =  url + "/" + resource + "_thumbnail/" + str(image._id)
    img['thumbnail_md5'] = image.thumbnail.md5
    return img

def get_simple_clear( user, publication, url):
    """
        Devuelve un diccionario con el objeto, filtrando ciertos datos.
        En lugar de la lista de smiles, comentarios y shareds, muestra un conteo de estos.
    """
    dic = publication.to_mongo()
    if isinstance(publication, ImagePublication):
        dic['type'] = 'image'
        dic['picture'] = parse_image_data( publication.Image, "picture", url)
        del dic['Image']
    elif isinstance(publication, CheckInPublication):
        dic['type'] = 'checkin'
        dic['mapImage'] = parse_image_data( publication.MapImage, "mapPreview",url)
        del dic['MapImage']
    elif isinstance(publication, SharedPublication):
        dic['type'] = 'shared'
        dic['shared_publication'] = get_simple_clear(user, publication.shared_publication, url)
    else:
        dic['type'] = 'text'
    del dic['_cls']

    if user.id in dic['smiles']:
        dic['ismile'] = True
    else:
        dic['ismile'] = False

    del dic['smiles']
    dic['smilesCounter'] = len(publication.smiles)
    del dic['comments']
    dic['commentsCounter'] = len(publication.comments)
    dic['sharesCounter'] = len(SharedPublication.objects(shared_publication = publication ))
    dic['ishared'] = len(SharedPublication.objects( Q(shared_publication=publication) & Q(author=user) )) > 0

    if len(Publication.objects( Q(comments__author=user) & Q(id=publication.id) )) :
        dic['iComment'] = True
    else :
        dic['iComment'] = False

    author = dict()
    author['_id'] = dic['author']
    author['name'] = publication.author.get_name_to_show()
    author['avatar'] = publication.author.parse_avatar_data(url)
    if author['avatar'] is None:
        del author['avatar']
    dic['author'] = author

    dic['latitude'] = dic['created_position'][0]
    dic['longitude'] = dic['created_position'][1]
    del dic['created_position']

    if not publication.to is None:
        dic['to'] = { '_id': dic['to'], 'name': publication.to.get_name_to_show()}

    try:
        fav = Favorite.objects.get(owner=user,publication=publication )
        dic['favorite'] = True
    except DoesNotExist:
        dic['favorite'] = False

    return dic


def get_all_publications_from_user(user,  url):
    """
        Devuelve todas las publicaciones de un usuario dado
    """
    items = []
    for publ in Publication.objects( Q(author=user) | Q(to=user) ).order_by('-created'):
        items.append(get_simple_clear(user, publ, url))
    return {'items':items }


def get_first_news_publications_from_user(user,  url):
    """
        Devuelve las publicaciones mas nuevas de un usuario dado
    """
    items = []
    for publ in Publication.objects( Q(author=user) | Q(to=user) ).order_by('-created')[:5]:
        items.append(get_simple_clear(user, publ, url))
    return {'items':items }

def get_next_news_publications_from_user(user, publBegin, url):
    """
        Devuelve las publicaciones mas nuevas a partir de una publicacion dada de un usuario dado
    """
    items = []
    for publ in Publication.objects( (Q(author=user) | Q(to=user)) & Q(created__lte=publBegin.created)).order_by('-created')[:5]:
        items.append(get_simple_clear(user, publ, url))
    return {'items':items }


def calculate_general_publication_months_window(user):
    """
        Calcula que rango de meses para las publicaciones generales
    """
    distance = 3
    topDate = datetime.datetime.today() + relativedelta(months=12)
    volumenPubl = 0

    maxDate = datetime.datetime.today() + relativedelta(months=1)
    while volumenPubl < 35 and maxDate < topDate:
        maxDate = maxDate + relativedelta(months=1)
        while volumenPubl < 35 and distance > 0:
            volumenPubl = len( Publication.objects(created_position__within_distance=[(user.last_position[0], user.last_position[1]), distance], to=None, created__lte=maxDate).order_by('-created')[:35])
            if volumenPubl < 35:
                if distance is 3:
                    distance = 10
                elif distance is 10:
                    distance = 100
                elif distance is 100:
                    distance = 0

    if (volumenPubl < 35):
        distance = 100
    return (maxDate, distance)


def get_all_publications(user, url):
    """
        Devuelve todas las publicaciones
    """
    items = []
    for publ in Publication.objects().order_by('-created'):
        items.append(get_simple_clear(user, publ, url))
    return {'items':items }


def get_all_new_publications(user, url):
    """
       Devuelve las publicaciones mas nueva dentro de un radio de distancia especifica
       Primero 3km, si no existen prublicaciones se siguen con 10km, 100km y sino se devuelve vacio
    """
    items = []
    (maxDate, distance) = calculate_general_publication_months_window(user)

    for publ in Publication.objects(created_position__within_distance=[(user.last_position[0], user.last_position[1]), distance], to=None, created__lte=maxDate).order_by('-created')[:5]:
        items.append(get_simple_clear(user, publ, url))
    return {'distance':distance,  'publications':items, 'position reference':{'latitude':user.last_position[0] , 'longitude': user.last_position[1]} }



def get_next_publications(user, publBegin, url):
    """
        Devuelve las publicaciones mas nuevas a partir de una publicacion dada de un usuario dado
    """
    items = []
    (maxDate, distance) = calculate_general_publication_months_window(user)

    for publ in Publication.objects( Q(created__lte=publBegin.created), created_position__within_distance=[(user.last_position[0], user.last_position[1]), distance], to=None).order_by('-created')[:5]:
        items.append(get_simple_clear(user, publ, url))
    return {'distance':distance,  'publications':items, 'position reference':{'latitude':user.last_position[0] , 'longitude': user.last_position[1]} }


def get_all_user_picture_publications( user, url):
    """
        Devuelve todas las publicaciones de imagenes de un usuario dado
    """
    items = []
    for publ in ImagePublication.objects(author=user).order_by('-created'):
        items.append(get_simple_clear(user, publ, url))
    return {'items':items }




#def parse_shared_publication(user, shapubl, url):
    #"""
        #Devuelve la publicacion compartida
    #"""
    #dic = get_simple_clear(user, shapubl.publication, url)
    #dic['type'] = 'shared'
    #if (shapubl.author.id == user.id) :
        #dic['ishared'] = True
    #else
        #dic['ishared'] = False

    #dic['latitude'] = shapubl['created_position'][0]
    #dic['longitude'] = shapubl['created_position'][1]

    #sharedby = dict()
    #sharedby['_id'] = dic['author']
    #sharedby['name'] = shapubl.author.get_name_to_show()
    #sharedby['avatar'] = shapubl.author.parse_avatar_data(url)
    #if sharedby['avatar'] is None:
        #del sharedby['avatar']
    #dic['sharedby'] = sharedby

    #return dic

def parse_comment(comment, url):
    """
        devuelve un diccionario con la informacion de un comentario
    """
    com = comment.to_mongo()
    author = dict()
    author['_id'] = com['author']
    author['name'] = comment.author.get_name_to_show()
    author['avatar'] = comment.author.parse_avatar_data(url)
    if author['avatar'] is None:
        del author['avatar']
    com['author'] = author
    return com

def get_all_comments(publication, url):
    """
        Devuelve todos los comentarios
    """
    #publ = publication.to_mongo()
    items = []
    #TODO order by created
    for comm in publication.comments:
        items.append(parse_comment(comm, url))
    #for comm in Publication.objects(id=self.id).order_by('-created'):
        #items.append(get_simple_clear(user, publ, url))
    return {'items':items }


def publication_batch_toggle_smiles(user, publications):
    """
        Realiza un toggle smiles a las publicaciones dadas
    """
    errors = dict()
    for publi in publications:
        for key, value in publi.iteritems() :
            publ = None
            try:
                publ = Publication.objects().with_id(value)
            except ValidationError:
                errors[key] = "Publication "+value+" doesn't exists."

            if publ is None:
                errors[key] = "Publication "+value+" doesn't exists."
            else:
                try:
                    publ = Publication.objects().get(id=value, smiles=user.id)
                    Publication.objects(id=value).update_one(pull__smiles=user.id)
                    if not publ.author.id is user.id:
                        notication_create( publ.author, user, 'UNSMILE', publ)
                except DoesNotExist:
                    Publication.objects(id=value).update_one(push__smiles=user.id)
                    if not publ.author.id is user.id:
                        notication_create( publ.author, user, 'SMILE', publ)
                except ValidationError:
                    errors[key] = "Publication "+value+" doesn't exists."
    return errors

#*********************************
# Defino modelos
#********************************
class Comment(BaseEmbeddedDocument):
    content = StringField(max_length=140) #TODO determinar cual es la mayor longitud deseada
    author = ReferenceField(Accounts)


class Publication(BaseDocument):
    text = StringField( max_length=140) #TODO determinar cual es la mayor longitud deseada
    created_position = GeoPointField()
    comments = ListField(EmbeddedDocumentField(Comment))
    smiles = ListField(ReferenceField(Accounts))
    author = ReferenceField(Accounts) #mirar reverse_delete_rule=CASCADE
    to = ReferenceField(Accounts) #Solo se usa cuando se postea en el feedwall de otro usuario
    meta = {'allow_inheritance': True}

    @classmethod
    def get_after_create(self):
        publ = self.to_mongo()


class ImagePublication(Publication):
    Image = ImageField( size=(2048, 2048, True), thumbnail_size=(180, 180, True))

class CheckInPublication(Publication):
    MapImage = ImageField( size=(800, 800, True), thumbnail_size=(180, 180, True))

#class SharedPublication(BaseDocument):
    #publication = ReferenceField(Publication)
    #created_position = GeoPointField(required=True)
    #author = ReferenceField(Accounts) #mirar reverse_delete_rule=CASCADE
class SharedPublication(Publication): #el resto de los campos permaneceran vacios
    shared_publication = ReferenceField(Publication)


#*********************************
# Favorites
#********************************

#*********************************
# Funcion varias
#********************************

def favorites_get_date_groups(user):
    """
        Devuelve un diccionarion de anios con las lista de meses de todas los favoritos del usuario
    """
    items = dict()
    for fav in Favorite.objects(owner=user):
        if not items.has_key(fav.created.year):
            items[fav.created.year] = [fav.created.month]
        elif not fav.created.month in items[fav.created.year]:
            items[fav.created.year].append(fav.created.month)
    return items

def favorites_get_all_from_dategroup(user, y, m, url):
    """
        Devuelve la lista completa de favoritos para un mes y anio especifico
    """
    items = []
    for fav in Favorite.objects(owner=user, year=y, month=m):
        publ = Publication.objects().with_id(fav.publication.id)
        items.append(get_simple_clear(user, publ, url))
    return {'items':items }

def favorites_get_first_page_from_dategroup(user, y, m, url):
    """
        Devuelve una lista limitada de los primeros favoritos para un mes y anio especifico
    """
    items = []
    for fav in Favorite.objects(owner=user, year=y, month=m)[:5]:
        publ = Publication.objects().with_id(fav.publication.id)
        items.append(get_simple_clear(user, publ, url))
    return {'items':items }

def favorites_get_paged_from_dategroup(user, y, m, lastID, url):
    """
        Devuelve una lista limitada de favoritos para un mes y anio especifico, a partir del lastID dado
    """
    items = []
    for fav in Favorite.objects(owner=user, year=y, month=m):
        publ = Publication.objects().with_id(fav.publication.id)
        items.append(get_simple_clear(user, publ, url))
    return {'items':items }


#*********************************
# Defino modelos
#********************************
class Favorite(BaseDocument):
    owner = ReferenceField(Accounts)
    publication = ReferenceField(Publication)
    #campos solo utilez para mejorar la obtencion de la informacion
    year = IntField(min_value=1950, max_value=2050)
    month = IntField(min_value=1, max_value=12)

    def clean(self):
        self.year = self.created.year
        self.month = self.created.month




#***********************************
# Notifications
#***********************************
def publication_notifications_set_readed(notifications):
    """
        Setea como leidas todas las notificaciones dentro del arreglo dado
    """
    #TODO check si la notificacion es propia del usuario
    errors = dict()
    for notifi in notifications:
        for key, value in notifi.iteritems() :
            noti = None
            try :
                noti = NotificationActivityPublication.objects().with_id(value)
            except ValidationError:
                errors[key] = "Notification "+value+" doesn't exists."
            if noti:
                noti.readed = True
                noti.save()
            else:
                errors[key] = "Notification "+value+" doesn't exists."
    return errors


def publication_parse_notification(notif, user, url):
    """
        Devuelve la informacion de una notificacion para ser mostrada
    """
    ns = notif.to_mongo()
    actioner = dict()
    actioner['_id'] = notif.actioner.id
    actioner['name'] = notif.actioner.get_name_to_show()
    actioner['avatar'] = notif.actioner.parse_avatar_data(url)
    if actioner['avatar'] is None:
        del actioner['avatar']
    ns['actioner'] = actioner
    ns['publication'] = get_simple_clear(user, notif.publication, url)
    return ns

def get_all_publication_notifications_from_user(user, url):
    """
        Devuelve todas las notificaciones de un usuario dado
    """
    items = []
    for noti in NotificationActivityPublication.objects(user=user, readed=False).order_by('-created'):
        items.append( publication_parse_notification(noti, user, url) )

    length = len(items)
    if length < 10 :
        for noti in NotificationActivityPublication.objects(user=user, readed=True).order_by('-created')[:10-length]:
            items.append( publication_parse_notification(noti, user, url) )
    return {'items':items }

def notication_create(user, actioner, action, publication):
    """
        Crea una nueva notification de publicacion
    """
    notif = NotificationActivityPublication()
    notif.user = user #Para quien es la notificacion
    notif.actioner = actioner
    notif.publication = publication
    notif.action = action
    notif.save()

ACTIVITYPUBLICATION = ('SMILE', 'UNSMILE', 'SHARED', 'COMMENT')
class NotificationActivityPublication(Notification):
    actioner = ReferenceField(Accounts) #quien realizo el smile, shared u otra accion
    action = StringField(required=True, max_length=10, choices=ACTIVITYPUBLICATION)
    publication = ReferenceField(Publication) #sobre que publicacion se realizo
