from validate_email import validate_email

#accounts_schema = {
    #'firstname': {
        #'type': 'string',
        #'minlength': 1,
        #'maxlength': 20,
    #},
    #'lastname': {
        #'type': 'string',
        #'minlength': 1,
        #'maxlength': 20,
    #},
    #'gender_male': { #True means male 
        #'type': 'boolean',
        #'nullable': True,
    #},
    #'email': {
        #'type': 'string',
        #'required': True,
        #'unique': True,
        #'validemailaddress': True,
    #},
    #'born': {
        #'type': 'datetime',
    #},
    #'password': {
        #'type': 'string',
        #'minlength': 6,
        #'maxlength': 20,
        #'required': True,
    #},
    #'roles': {
        #'type': 'list',
        #'allowed': ['user', 'superuser', 'admin'],
        ##'required': True,
    #},
    #'facebook_login': {
        #'type': 'boolean',
    #},
    #'active': {
        #'type': 'boolean',
    #},
#}

""" player class takes dictionary items and turns them into class objects """
class Account:
    def __init__(self, **entries):
        self.__dict__.update(entries)





#**************************
# Evalua y retorna issues
#**************************
def validate_POST(data) :
    errors = dict()
    if not data.has_key('email'):
        errors['email'] = 'email is requiered'
    elif not validate_email(data['email'], verify=False) :
        errors['email'] = "The email '%s' must be real" %data['email']
        #TODO check unique
    if not data.has_key('password'):
        errors['password'] = 'Password is requiered.'
    elif len(data['password']) < 6 or len(data['password']) > 20 :
        errors['password'] = 'The password might have more than 6 characters and less than 20.'
    return errors
        
        

#**************************
# Crea una nueva cuenta
#**************************
def createAccount(db, data) :
    #db['created']
    #db['updated']
    #db['etag']
    data['active'] = False
    account = Account(data)
    try:
        db['accounts'].save(data)
    except ValidationError as ve:
        abort(400, str(ve))