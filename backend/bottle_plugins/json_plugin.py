from inspect import getargspec
from json import dumps, load
from bottle import request, response
from bson.objectid import ObjectId

from json import JSONEncoder, dumps as jsonify
from datetime import datetime


class DHJsonEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return str(obj.strftime("%a, %d %b %Y %H:%M:%S GMT"))
        if isinstance(obj, ObjectId):
            return str(obj)
        return JSONEncoder.default(self, obj)



class JsonPlugin(object):
    def __init__(self, keyword):
        self.keyword = keyword

    def apply(self, callback, context):
        def wrapper(*args, **kwargs):
            _args = getargspec(callback)[0]
            if self.keyword in _args:
                try:
                    kwargs[self.keyword] = load(request.body)
                except:
                    kwargs[self.keyword] = None
            body, status = callback(*args, **kwargs)
            response.content_type = 'application/json'
            response.status = status
            if body is not None:
                return dumps(body, separators=(',', ':'), cls=DHJsonEncoder)
            return ''
        return wrapper