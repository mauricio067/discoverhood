import logging

MongoDB = {
    'ALIAS': 'DiscoverHood',
    'HOST': '127.0.0.1',
    'PORT': 27017,
    'USER': '',
    'PASS': '',
    'DBNAME': 'api'
}

LOGGIN = {
    'NAME': 'DHSD',
    'LEVEL': logging.DEBUG,
    'SERVER' : {
        'FORMAT': '[%(asctime)s] [%(levelname)s] - %(message)s',
        'FILE': './server.log'
    },
    'CONNECTIONS' : {
        'FORMAT': '%(asctime)-15s %(clientip)s %(user)-8s %(message)s',
        'FILE': './connections.log'
    }
}
