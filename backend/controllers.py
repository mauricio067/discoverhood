# -*- coding: utf-8 -*-
import json
from datetime import datetime
from urlparse import urlparse
from bottle import request, response, get, post, abort, auth_basic
from Controllers import Accounts, Publications, Friends, Messages, Notifications
from models import Accounts
from Models.Accounts import check_auth
from Models.Publications import get_all_publication_notifications_from_user, publication_batch_toggle_smiles, publication_notifications_set_readed
from Models.Messages import message_notifications_get_all_unreaded, conversation_notifications_set_readed
from Models.Friends import friends_parse_all_notification, friends_get_all_friendhips_received, friends_notifications_set_readed

PAGE_SIZE = 5

#@get(['/', '/list', '/list/:page#\d+#'])
#@view('list.mako')
#def list(page=0):
    #''' List messages. '''
    #page = int(page)
    #prev_page = None
    #next_page = None
    #if page > 0:
        #prev_page = page - 1
    #if Publication.objects.count() > (page + 1) * PAGE_SIZE:
        #next_page = page + 1
    #msgs = (Publication.objects
            #.order_by('-date')
            #.skip(page * PAGE_SIZE)
            #.limit(PAGE_SIZE))
    #return {'messages': msgs,
            #'prev_page': prev_page,
            #'next_page': next_page,
           #}

#@post('/create')
#def create():
    #''' Save new message. '''
    #if not (request.POST.get('nickname') and request.POST.get('text')):
        #redirect('/')
    #msg = Publication()
    #msg.nickname = request.POST['nickname']
    #msg.text = request.POST['text']
    #if 'image' in request.files:
        #upload = request.files['image']
        #if not upload.filename.lower().endswith(
                #('.jpg', '.jpeg', '.png', '.bmp', '.gif')):
            #redirect('/')
        #mime = mimetypes.guess_type(upload.filename)[0]
        #msg.image_filename = upload.filename
        ## Save fullsize image
        #msg.image.put(upload.file, content_type=mime)
        ## Create and save thumbnail
        #image = Image.open(msg.image)
        #image.thumbnail((80, 60), Image.ANTIALIAS)
        #data = StringIO.StringIO()
        #image.save(data, image.format)
        #data.seek(0)
        #msg.thumb.put(data, content_type=mime)
    #msg.save()
    #redirect('/')

#@get('/:image_type#(image|thumb)#/:docid')
#def get_image(image_type, docid):
    #''' Send image or thumbnail from file stored in the database. '''
    #f = Publication.objects.with_id(ObjectId(docid))[image_type]
    #response.content_type = f.content_type
    #return HTTPResponse(f)

#@get('/static/:filename#.+#')
#def get_static_file(filename):
    #''' Send static files from ./static folder. '''
    #return static_file(filename, root='./static')




@get('/pullservice')
@auth_basic(check_auth)
def pullservice():
    """
        Indica el estado del usuario, lo actualiza y devuelve todas las notificaciones pendientes de lectura
    """
    username, password = request.auth or (None, None)
    user = Accounts.objects.get(email=username)

    ourl = urlparse(request.url)
    salida = dict()
    salida['notifications'] = dict()
    salida['notifications']['publications'] = get_all_publication_notifications_from_user(user,ourl.scheme + "://" + ourl.netloc)
    #salida['notifications']['messages'] = message_notifications_get_all_unreaded(user,ourl.scheme + "://" + ourl.netloc)
    salida['notifications']['friends'] = friends_parse_all_notification(user,ourl.scheme + "://" + ourl.netloc)
    salida['friendships_requests'] = friends_get_all_friendhips_received(user,ourl.scheme + "://" + ourl.netloc)

    user.last_connection = datetime.now
    try :
        user.save()
    except ValidationError as msg:
        abort(403, "Server error, can't update account status")

    return salida



@post('/pushservice')
@auth_basic(check_auth)
def pushservice():
    """
        Aplica cambios enviados desde el app.
        Toggle smiles de una lista de publicaciones
        Setea notificaciones como leidas

    """
    username, password = request.auth or (None, None)

    data = request.body.readline()
    if not data:
        abort(400, 'No data received' )
    entity = json.loads(data)

    user = Accounts.objects.get(email=username)
    errors = dict()

    if entity.has_key('toggleSmiles'):
        error = publication_batch_toggle_smiles(user, entity['toggleSmiles'])
        if error:
            errors['toggleSmiles'] = error

    if entity.has_key('notifpublications'):
        error = publication_notifications_set_readed(entity['notifpublications'])
        if error:
            errors['notifpublications'] = error

    if entity.has_key('notifmessages'):
        error = conversation_notifications_set_readed(entity['notifmessages'])
        if error:
            errors['notifmessages'] = error

    if entity.has_key('notiffriendships'):
        error = friends_notifications_set_readed(entity['notiffriendships'])
        if error:
            errors['notiffriendships'] = error


    if errors :
        print errors
        abort(403, {'reason': "Error on push process", 'details':errors } )

