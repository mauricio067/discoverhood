import json
from bottle import response, error

#TODO ver error 405
@error(400)
def custom401(error):
    response.content_type = 'application/json'
    errorParser = dict()
    errorParser['error'] = 400
    errorParser['reason'] = error.body or 'No enought data'
    return json.dumps(errorParser)

@error(401)
def custom401(error):
    response.content_type = 'application/json'
    errorParser = dict()
    errorParser['error'] = 401
    errorParser['reason'] = 'You need to be authenticated access this resource'
    return json.dumps(errorParser)

@error(403)
def custom403(error):
    response.content_type = 'application/json'
    errorParser = dict()
    if (type(error.body) == dict):
        errorParser = error.body or 'You need to be authenticated access this resource'
    else:
        errorParser['reason'] = error.body or 'You need to be authenticated access this resource'
    errorParser['error'] = 403
    return json.dumps(errorParser)

@error(404)
def custom404(error):
    response.content_type = 'application/json'
    errorParser = dict()
    errorParser['error'] = 404
    errorParser['reason'] = error.body or 'Not found'
    return json.dumps(errorParser)

@error(500)
def custom500(error):
    response.content_type = "application/json"
    errorParser = dict()
    errorParser['error'] = 500
    errorParser['reason'] = 'Server internal error'
    return json.dumps(errorParser)


handler = {
    401: custom401,
    404: custom404,
    403: custom403,
    500: custom500, #TODO revisar que no funciona bien
}
