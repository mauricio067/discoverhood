import os
from random import randrange
from bottle import abort

def crate_image(path_image):
	name, ext= os.path.splitext(path_image.filename)
	if ext not in('.png','.jpg','jpeg'):
		abort(403,"File extension not allow")
	save_path="tmp/DH"
	if not os.path.exists(save_path):
		os.mkdirs(save_path)
	file_path = "{path}/{file}".format(path=save_path, file=path_image.filename)
	while os.path.exists(file_path):
		file_path = "{path}/{file}".format(path=save_path, file=name+str(randrange(1,9999))+ext)
	with open(file_path, 'w+b') as open_file:
	    open_file.write(path_image.file.read())

	return file_path

	