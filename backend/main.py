import json
import bottle
import logging
from datetime import datetime
from bson.objectid import ObjectId
from json import JSONEncoder, dumps as jsonify
from config import MongoDB, LOGGIN
from bottle import route, run, request, abort, error, install, server_names, ServerAdapter

import cherrypy

import pymongo
import accounts
import error_handler

import controllers
#**********************
# LOGGIN CONFIGURATION
#**********************
#logging.basicConfig(level=LOGGIN['LEVEL'])

#LOG_server = logging.getLogger(LOGGIN['NAME'])
#handler = logging.FileHandler(LOGGIN['SERVER']['FILE'])
#handler.setLevel(logging.DEBUG)
#handler.setFormatter( logging.Formatter(LOGGIN['SERVER']['FORMAT']) )
#LOG_server.addHandler(handler)


application = bottle.default_app()

#app.install(JsonPlugin('data'))


class DHJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            try:
                return str(obj.strftime("%a, %d %b %Y %H:%M:%S GMT"))
            except ValueError:
                return "";
        if isinstance(obj, ObjectId):
            return str(obj)
        return JSONEncoder.default(self, obj)

class SSLCherryPyServer(ServerAdapter):
    def run(self, handler):
        from cherrypy import wsgiserver
        import ssl
        server = wsgiserver.CherryPyWSGIServer((self.host, self.port), handler)
        server.ssl_certificate = "cacert.pem"
        server.ssl_private_key = "privkey.pem"
        server.ssl_version = ssl.PROTOCOL_SSLv23
        server.thread_pool = 3
        server.statistics = False
        server.socket_timeout = 30
        server.socket_queue_size = 20
        wsgiserver.CherryPyWSGIServer.environment ='production'
        wsgiserver.CherryPyWSGIServer.autoreload_on = True
        wsgiserver.CherryPyWSGIServer.logscreen = False
        try:
            server.start()
        finally:
            server.stop()

class StripPathMiddleware(object):
        '''
        Get that slash out of the request
        '''
        def __init__(self, a):
            self.a = a
        def __call__(self, e, h):
            e['PATH_INFO'] = e['PATH_INFO'].rstrip('/')
            return self.a(e, h)

application.config['autojson'] = True
application.install(bottle.JSONPlugin(json_dumps=lambda s: json.dumps(s, cls=DHJsonEncoder)))
application.debuf = False
#bottle.run(application, host='127.0.0.1', port=5004, server='cherrypy' )

if __name__ == "__main__":
    app = bottle.default_app()
    application.config['autojson'] = True
    app.error_handler = error_handler.handler
    #app.install(JsonPlugin('data'))
    app.install(bottle.JSONPlugin(json_dumps=lambda s: json.dumps(s, cls=DHJsonEncoder)))
    run( app, host='0.0.0.0', port=5005)
    run(app=StripPathMiddleware(app), host='0.0.0.0', port=5005, server=SSLCherryPyServer )

