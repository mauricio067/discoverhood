import datetime
from config import MongoDB
from mongoengine import Document, register_connection
from mongoengine import DateTimeField, ReferenceField, BooleanField

from Models.Accounts import Accounts
from Models.Publications import Publication, ImagePublication, CheckInPublication, SharedPublication, Comment,  Favorite, NotificationActivityPublication
#from Models.Favorites import Favorite
from Models.Friends import Friendships, FriendshipsRequests, NotificationFriendshipsProcess
from Models.Messages import Message, Conversation, NotificationMessage
from Models.Notifications import Notification


try:
    register_connection(alias='default', name=MongoDB['DBNAME'], host=MongoDB['HOST'], port=MongoDB['PORT'], is_slave=False, read_preference=False, slaves=None, username=MongoDB['USER'], password=MongoDB['PASS'])
except ConnectionError as msg:
    print msg