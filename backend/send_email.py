import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText



def send_validation_code(username, email, code):
    _from = "dev@discoverHood.com"
    msg = MIMEMultipart('alternative')
    msg['Subject'] = 'Codigo activacion'
    msg['From'] = _from
    msg['To'] = email

    text = "Hi " + username + "!\nYour validation code is: " + `code`
    html = """\
    <html>
    <head></head>
    <body>
        <p>Hi  """ + username + """!<br>
        Your validation code is: <strong>""" + `code` + """</strong><br>
        </p>
    </body>
    </html>
    """

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via local SMTP server.
    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    # s.sendmail(me, you, msg.as_string())

    # Send the message via our own SMTP server, but don't include the
    # envelope header.

    #s = smtplib.SMTP('smtp.gmail.com')
    #s.sendmail(_from, email, msg.as_string())
    #s.quit()

    ##Modificaciones para Servidor SMTP de google
    send_smtp_email(_from, email, msg.as_string())
    
    
    
    
def send_recuperation_email(username, email, code):
    _from = "dev@discoverHood.com"
    msg = MIMEMultipart('alternative')
    msg['Subject'] = 'Codigo activacion'
    msg['From'] = _from
    msg['To'] = email

    text = "Hi !\nYour validation code is: " + `code`
    html = """\
    <html>
    <head></head>
    <body>
        <p>Hi  !<br>
        Your validation code is: <strong>""" + `code` + """</strong><br>
        </p>
    </body>
    </html>
    """

    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')
    msg.attach(part1)
    msg.attach(part2)
    send_smtp_email(_from, email, msg.as_string())


def send_smtp_email(_from, email, msg):
    s = smtplib.SMTP("smtp.gmail.com", 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('mauricio067@gmail.com', 'sikadtea3105')
    s.sendmail(_from, email, msg)
    s.quit()
